package it.polimi.ingsw.lm_belloni_cof.test.controller;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.PlayerData;

public class PlayerDataTest {
	private PlayerData playerData;
	@Before
	public void initialize(){
		this.playerData = new PlayerData("test","test");
	}
	@Test
	public void testUsernamePassword() {
		assertEquals(this.playerData.getUsername(),"test");
		assertEquals(this.playerData.getPassword(),"test");
	}
	@Test
	public void testGameWon(){
		assertEquals(this.playerData.getGamesWon(),0);
		this.playerData.addAWonGame();
		assertEquals(this.playerData.getGamesWon(),1);
	}
	@Test
	public void testTotalGameTime(){
		assertEquals(this.playerData.getTotalGameTime(),0);
		this.playerData.addGameTime(10*60*1000); // ten minutes game
		assertEquals(this.playerData.getTotalGameTime(),10*60*1000);
	}
}
