package it.polimi.ingsw.lm_belloni_cof.test.controller.actions;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.ElectACouncillorAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.MainAction;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidActionException;
import it.polimi.ingsw.lm_belloni_cof.model.Councillor;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMap;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Region;

public class ElectACouncillorActionTest {
	private Game game;
	private GameMap gameMap;

	@Before
	public void initialize() {
		List<String> usernames = Arrays.asList("P1", "P2", "P3");
		this.gameMap = new GameMapFactory().getGameMap("default");
		this.game = new Game(gameMap, usernames);
	}

	@Test
	public void test() {
		Player p1 = this.game.getPlayerByUsername("P1");
		int beforeCoins = p1.getCoins();
		Region region = Region.COAST;
		Councillor c = null;
		try {
			c = this.game.getACouncillorByPoliticColor(this.game.getAvailableCouncillorColors().get(0));
		} catch (InvalidActionException e) {
			fail();
		}
		MainAction elect = new ElectACouncillorAction(this.game, p1, region, c);
		try {
			elect.execute();
		} catch (ActionExecutionException e) {
			fail();
		}
		assertTrue(elect.hasBeenExecuted());
		assertEquals(beforeCoins + 4, p1.getCoins());
		assertTrue(this.game.getCouncilByRegion(region).contains(c.getCouncillorColor()));

	}

}
