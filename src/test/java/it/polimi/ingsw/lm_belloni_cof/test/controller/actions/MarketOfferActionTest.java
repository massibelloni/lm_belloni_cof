package it.polimi.ingsw.lm_belloni_cof.test.controller.actions;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.MarketAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.MarketOfferAction;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidActionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughAssistantsException;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMap;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.MarketOffer;
import it.polimi.ingsw.lm_belloni_cof.model.Ownable;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Region;

public class MarketOfferActionTest {

	@Test
	public void test() {
		List<String> usernames = Arrays.asList("P1", "P2", "P3");
		GameMap gameMap = new GameMapFactory().getGameMap("default");
		Game game = new Game(gameMap, usernames);
		Player p = game.getPlayerByUsername("P1");
		try {
			game.initializeMarketSession();
			assertTrue(game.getOffersByPlayer(p).isEmpty());
			int assistants = p.getNumberOfAssistants();
			MarketOffer o1 = new MarketOffer(p.getAnAssistant(), 3);
			assertEquals(assistants - 1, p.getNumberOfAssistants());
			MarketOffer o2 = new MarketOffer(p.getPoliticsHand().remove(0), 10);
			MarketAction offer1 = new MarketOfferAction(game, p, o1);
			offer1.execute();
			assertTrue(offer1.hasBeenExecuted());
			MarketAction offer2 = new MarketOfferAction(game, p, o2);
			offer2.execute();
			assertTrue(offer2.hasBeenExecuted());
			// checking the model
			assertFalse(game.getOffersByPlayer(p).isEmpty());
			List<MarketOffer> offeredByPlayer = game.getOffersByPlayer(p);
			for (MarketOffer playerOffer : offeredByPlayer) {
				assertNull(playerOffer.getObjectForSale().getOwner());
				Ownable o = playerOffer.getObjectForSale();
				assertTrue(o == o1.getObjectForSale() || o == o2.getObjectForSale());
				assertTrue(playerOffer.getPrice() == (o == o1.getObjectForSale() ? 3 : 10));
			}
			Player p2 = game.getPlayerByUsername("P2");
			game.acquireAPermitTile(p2, Region.COAST, 1);
			MarketOffer o3 = new MarketOffer(p2.getPermitsAcquired().remove(0), 50);
			offer1 = new MarketOfferAction(game, p2, o3);
			offer1.execute();
			assertFalse(game.getOffersByPlayer(p2).isEmpty());
			offeredByPlayer = game.getOffersByPlayer(p2);
			for (MarketOffer playerOffer : offeredByPlayer) {
				assertNull(playerOffer.getObjectForSale().getOwner());
				Ownable o = playerOffer.getObjectForSale();
				assertTrue(o == o3.getObjectForSale());
				assertEquals(playerOffer.getPrice(), 50);
			}
		} catch (ActionExecutionException | NotEnoughAssistantsException | InvalidActionException e) {
			fail();
		}
	}
}
