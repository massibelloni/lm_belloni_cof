package it.polimi.ingsw.lm_belloni_cof.test.controller.actions;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.EmptyQuickAction;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.Player;

public class EmptyQuickActionTest {
	private Game game;

	@Before
	public void initialize() {
		List<String> usernames = Arrays.asList("P1", "P2", "P3");
		this.game = new Game(new GameMapFactory().getGameMap("default"), usernames);

	}

	@Test
	public void test() {
		Player p1 = this.game.getPlayerByUsername("P1");
		EmptyQuickAction empty = new EmptyQuickAction(this.game, p1);
		empty.execute();
		assertTrue(empty.hasBeenExecuted());
	}

}
