package it.polimi.ingsw.lm_belloni_cof.test.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.exceptions.EmptyDeckException;
import it.polimi.ingsw.lm_belloni_cof.model.Politic;
import it.polimi.ingsw.lm_belloni_cof.model.PoliticColor;
import it.polimi.ingsw.lm_belloni_cof.model.PoliticDeck;

public class PoliticDeckTest {

	@Test
	public void test() {
		PoliticDeck deck1 = PoliticDeck.getPoliticDeckInstance(4);
		int supposedTotal = (PoliticDeck.FOUR_PLAYERS_SINGLE_COLORED * (PoliticColor.values().length - 1))
				+ PoliticDeck.FOUR_PLAYERS_JOKERS;
		List<Politic> deckCopy = new ArrayList<Politic>();
		try{
			for(int i=0;i<supposedTotal;i++)
				deckCopy.add(deck1.draw());
			int multicolored = 0;
			for(Politic politic:deckCopy){
				if(politic.getPoliticColor() == PoliticColor.MULTI_COLORED)
					multicolored++;
			}
			assertEquals(multicolored,PoliticDeck.FOUR_PLAYERS_JOKERS);
		} catch(EmptyDeckException e){
			fail();
		}
		try{
			Politic anotherOne = deck1.draw();
			fail();
			anotherOne.hasAnOwner();
		} catch(EmptyDeckException e){}
		
	}
	
	@Test 
	public void testMoreThan4(){
		PoliticDeck deck1 = PoliticDeck.getPoliticDeckInstance(8);
		assertNotNull(deck1);
		List<Politic> deckCopy = new ArrayList<Politic>();
		try{
			for(int i=0;i<180;i++)
				deckCopy.add(deck1.draw());
			int multicolored = 0;
			for(Politic politic:deckCopy){
				if(politic.getPoliticColor() == PoliticColor.MULTI_COLORED)
					multicolored++;
			}
			assertEquals(multicolored,PoliticDeck.JOKERS_PER_PLAYER*8);
		} catch(EmptyDeckException e){
			fail();
		}
		try{
			Politic anotherOne = deck1.draw();
			fail();
			anotherOne.hasAnOwner();
		} catch(EmptyDeckException e){}
	}

}
