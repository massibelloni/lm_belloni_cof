package it.polimi.ingsw.lm_belloni_cof.test.model;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.model.Bonus;
import it.polimi.ingsw.lm_belloni_cof.model.BonusType;
import it.polimi.ingsw.lm_belloni_cof.model.RewardToken;

public class RewardTokenTest {

	@Test
	public void test() {
		RewardToken rewardToken = new RewardToken();
		Bonus b1 = new Bonus(BonusType.ASSISTANT,2);
		Bonus b2 = new Bonus(BonusType.COIN,3);
		rewardToken.addBonusToTileContent(b1);
		rewardToken.addBonusToTileContent(b2);
		assertNotNull(rewardToken.getBonuses());
		assertEquals(rewardToken.getBonuses().size(),2);
		assertTrue(rewardToken.getBonuses().contains(b1) && rewardToken.getBonuses().contains(b2));
	}

}
