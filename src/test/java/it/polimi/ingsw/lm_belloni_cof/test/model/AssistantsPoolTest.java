package it.polimi.ingsw.lm_belloni_cof.test.model;

import static org.junit.Assert.*;

import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;

import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughAssistantsException;
import it.polimi.ingsw.lm_belloni_cof.model.Assistant;
import it.polimi.ingsw.lm_belloni_cof.model.AssistantsPool;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Politic;

public class AssistantsPoolTest {

	@Test
	public void testAssistantFromThePool() {
		AssistantsPool assistantsPool = new AssistantsPool();
		Assistant assistant = null;
		assistant = assistantsPool.getAnAssistant();
		if (assistant == null)
			fail();
		else
			assert (assistant instanceof Assistant);
	}

	@Test
	public void testAssistantOwner() throws NotEnoughAssistantsException {
		ArrayList<Politic> politicsHand = new ArrayList<Politic>();
		LinkedList<Assistant> assistantsOwned = new LinkedList<Assistant>();
		Player player = new Player("", 0, 0, politicsHand, assistantsOwned);
		AssistantsPool assistantsPool = new AssistantsPool();
		player.giveAnAssistant(assistantsPool.getAnAssistant());
		assertEquals(player.getAnAssistant().getOwner(), player);
	}

	@Test
	public void testAssistantResetOwner() throws NotEnoughAssistantsException {
		ArrayList<Politic> politicsHand = new ArrayList<Politic>();
		LinkedList<Assistant> assistantsOwned = new LinkedList<Assistant>();
		Player player = new Player("", 0, 0, politicsHand, assistantsOwned);
		AssistantsPool assistantsPool = new AssistantsPool();
		player.giveAnAssistant(assistantsPool.getAnAssistant());
		assistantsPool.returnAnAssistant(player.getAnAssistant());
		assertEquals(assistantsPool.getAnAssistant().getOwner(), null);
	}

	@Test
	public void testGetNAssistants() {
		AssistantsPool pool = new AssistantsPool();
		List<Assistant> assistants = pool.getNAssistants(5);
		for (int i = 0; i < 5; i++)
			assertTrue(assistants.get(i) instanceof Assistant);
	}
}