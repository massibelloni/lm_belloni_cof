package it.polimi.ingsw.lm_belloni_cof.test.controller.actions;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.BuildAnEmporiumWithTheHelpOfTheKingAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.MainAction;
import it.polimi.ingsw.lm_belloni_cof.controller.states.TurnMachine;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.CitiesNotConnectedException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughCoinsException;
import it.polimi.ingsw.lm_belloni_cof.model.Bonus;
import it.polimi.ingsw.lm_belloni_cof.model.BonusType;
import it.polimi.ingsw.lm_belloni_cof.model.City;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMap;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Politic;
import it.polimi.ingsw.lm_belloni_cof.model.PoliticColor;
import it.polimi.ingsw.lm_belloni_cof.model.RewardToken;

public class BuildAnEmporiumWithTheHelpOfTheKingActionTest {
	private Game game;
	private GameMap gameMap;
	private TurnMachine turnMachine;

	@Before
	public void initialize() {
		List<String> usernames = Arrays.asList("P1", "P2", "P3");
		this.gameMap = new GameMapFactory().getGameMap("default");
		this.game = new Game(gameMap, usernames);
		this.turnMachine = new TurnMachine(this.game);
	}

	@Test
	public void testOK() {
		Player p1 = this.game.getPlayerByUsername("P1");
		int beforeCoins = p1.getCoins();
		// looking for a city 2 steps close to king's location
		List<City> cityList = this.game.getCityList();
		City cityToBuildOn = null;
		for (City city : cityList)
			try {
				if (this.gameMap.getRoadsBetweenCities(city, this.game.getKingsLocation()) == 2) {
					cityToBuildOn = city;
				}
			} catch (CitiesNotConnectedException e) {
				continue;
			}
		assertNotNull(cityToBuildOn);
		assertFalse(this.game.getPlayersWithAnEmporiumOn(cityToBuildOn).contains(p1));
		List<Politic> multiColoredToUse = new ArrayList<>();
		for (int i = 0; i < 4; i++) {
			Politic cheat = new Politic(PoliticColor.MULTI_COLORED);
			p1.giveAPolitic(cheat);
			multiColoredToUse.add(cheat);
		}
		MainAction buildAnEmpKing = new BuildAnEmporiumWithTheHelpOfTheKingAction(game, p1, cityToBuildOn,
				multiColoredToUse, turnMachine);
		try {
			buildAnEmpKing.execute();
		} catch (ActionExecutionException e) {
			fail();
		}
		assertTrue(buildAnEmpKing.hasBeenExecuted());
		int coinsFromBonuses = 0;
		for (RewardToken token : this.game.getRewardTokensOn(cityToBuildOn))
			for (Bonus bonus : token.getBonuses())
				if (bonus.getBonusType() == BonusType.COIN)
					coinsFromBonuses += bonus.getNumberOfTimes();
		assertEquals(beforeCoins - 4 * 1 - 2 * 2 + coinsFromBonuses, p1.getCoins());
		assertTrue(this.game.getPlayersWithAnEmporiumOn(cityToBuildOn).contains(p1));
		assertEquals(this.game.getKingsLocation(), cityToBuildOn);

	}

	@Test
	public void testKOCoins() {
		Player p1 = this.game.getPlayerByUsername("P1");
		// looking for a city 3 steps close to king's location
		List<City> cityList = this.game.getCityList();
		City cityToBuildOn = null;
		for (City city : cityList)
			try {
				if (this.gameMap.getRoadsBetweenCities(city, this.game.getKingsLocation()) == 3) {
					cityToBuildOn = city;
				}
			} catch (CitiesNotConnectedException e) {
				continue;
			}
		assertNotNull(cityToBuildOn);
		assertFalse(this.game.getPlayersWithAnEmporiumOn(cityToBuildOn).contains(p1));
		// using only one politic will make the cost of the operation grow
		List<Politic> multiColoredToUse = new ArrayList<>();
		Politic cheat = new Politic(PoliticColor.MULTI_COLORED);
		p1.giveAPolitic(cheat);
		multiColoredToUse.add(cheat);
		MainAction buildAnEmpKing = new BuildAnEmporiumWithTheHelpOfTheKingAction(game, p1, cityToBuildOn,
				multiColoredToUse, turnMachine);
		try {
			buildAnEmpKing.execute();
			fail();
		} catch (ActionExecutionException e) {
			assertEquals(e.getMessage(),"Not enough coins to complete this action.");
			assertTrue(e.getCause() instanceof NotEnoughCoinsException);
		}
	}

}
