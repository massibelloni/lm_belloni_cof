package it.polimi.ingsw.lm_belloni_cof.test.model;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.model.Bonus;
import it.polimi.ingsw.lm_belloni_cof.model.BonusType;
import it.polimi.ingsw.lm_belloni_cof.model.Region;
import it.polimi.ingsw.lm_belloni_cof.model.RegionRewardTile;
import it.polimi.ingsw.lm_belloni_cof.model.RegionRewardTileContent;

public class RegionRewardTileTest {

	@Test
	public void test() {
		Region region = Region.MOUNTAINS;
		Bonus b1 = new Bonus(BonusType.MAIN_ACTION, 2);
		Bonus b2 = new Bonus(BonusType.NOBILITY, 1);
		RegionRewardTileContent content = new RegionRewardTileContent(region);
		content.addBonusToTileContent(b1);
		content.addBonusToTileContent(b2);
		RegionRewardTile regionRewardTile = new RegionRewardTile(content);
		assertEquals(regionRewardTile.getRegion(), region);
		List<Bonus> bonuses = regionRewardTile.getBonuses();
		assertTrue(bonuses.contains(b1) && bonuses.contains(b2));
		assertEquals(bonuses.size(), 2);
	}

}
