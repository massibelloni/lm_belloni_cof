package it.polimi.ingsw.lm_belloni_cof.test.controller;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.util.Arrays;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.Broadcaster;
import it.polimi.ingsw.lm_belloni_cof.controller.GameController;
import it.polimi.ingsw.lm_belloni_cof.controller.PlayerData;
import it.polimi.ingsw.lm_belloni_cof.messages.ActionNotExecutedMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.BroadcastMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ChatRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ChatResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.DisconnectedPlayerBroadcastMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.GameFinishedMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.GameUpdateMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
import it.polimi.ingsw.lm_belloni_cof.messages.PoliticsRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.PoliticsResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ReconnectedPlayerBroadcastMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.DrawAPoliticActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.ElectACouncillorActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.EmptyQuickActionActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.EndMarketBuyActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.EndMarketOfferActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.view.shared.SubscriberInterface;

public class GameControllerTest {
	private Broadcaster broadcaster;
	private GameController gameController;

	@Before
	public void initialize() {
		this.broadcaster = new Broadcaster();
	}

	@Test
	public void testIsFinished() {
		this.gameController = new GameController("GAME0", new GameMapFactory(), this.broadcaster);
		assertFalse(gameController.isGameFinished());
	}

	@Test
	public void testGetGameId() {
		this.gameController = new GameController("GAME0", new GameMapFactory(), this.broadcaster);
		assertEquals(gameController.getGameId(), "GAME0");
	}

	@Test
	public void testGameNotStarted() {
		this.gameController = new GameController("GAME0", new GameMapFactory(), this.broadcaster);
		assertTrue(this.gameController
				.handleRequest(new ChatRequestMessage(null, null)) instanceof ActionNotExecutedMessage);
	}

	@Test
	public void testAddPlayer() {
		this.gameController = new GameController("GAME0", new GameMapFactory(), this.broadcaster);
		assertEquals(gameController.getNumberOfPlayers(), 0);
		gameController.addPlayer(new PlayerToken("test"), new PlayerData("test", "test"));
		assertEquals(gameController.getNumberOfPlayers(), 1);

	}

	@Test
	public void testStartGame() {
		this.gameController = new GameController("GAME0", new GameMapFactory(), this.broadcaster);
		PlayerToken token1 = new PlayerToken("test1");
		PlayerToken token2 = new PlayerToken("test2");
		try {
			gameController.addPlayer(token1, new PlayerData("test1", "test"));
			this.broadcaster.subscribe(new TestSubscriber(), token1.getUsername());
			gameController.addPlayer(token2, new PlayerData("test2", "test"));
			this.broadcaster.subscribe(new TestSubscriber(), token2.getUsername());
		} catch (RemoteException e) {
			fail();
		}
		gameController.startGame();
		/* the message is now correctly handled */
		assertFalse(this.gameController
				.handleRequest(new ChatRequestMessage(token1, "test")) instanceof ActionNotExecutedMessage);
		assertTrue(this.gameController
				.handleRequest(new ChatRequestMessage(token1, "test")) instanceof ChatResponseMessage);
	}

	@Test
	public void testDisconnection() {
		PlayerData data1 = new PlayerData("test1", "test1");
		PlayerData data2 = new PlayerData("test2", "test2");
		this.gameController = new GameController("GAME0", new GameMapFactory(), this.broadcaster);
		PlayerToken token1 = new PlayerToken("test1");
		TestDisconnectionSubscriber subscriber1 = new TestDisconnectionSubscriber();
		PlayerToken token2 = new PlayerToken("test2");
		TestDisconnectionSubscriber subscriber2 = new TestDisconnectionSubscriber();
		try {
			this.broadcaster.subscribe(subscriber1, token1.getUsername());
			this.broadcaster.subscribe(subscriber2, token2.getUsername());
		} catch (RemoteException e) {
			fail();
		}
		/* adding players to game */
		this.gameController.addPlayer(token1, data1);
		this.gameController.addPlayer(token2, data2);
		/* starting game */
		this.gameController.startGame();
		PlayerToken playing = (this.gameController.getCurrentPlayer().equals("test1")) ? token1 : token2;
		PlayerToken waiting = (playing.equals(token1)) ? token2 : token1;
		assertFalse(this.gameController
				.handleRequest(new DrawAPoliticActRqstMsg(playing)) instanceof ActionNotExecutedMessage);
		/* disconnecting playing player */
		this.gameController.disconnectPlayer(playing.getUsername());
		/* waiting player now plays */
		assertEquals(this.gameController.getCurrentPlayer(), waiting.getUsername());
		assertFalse(this.gameController
				.handleRequest(new DrawAPoliticActRqstMsg(waiting)) instanceof ActionNotExecutedMessage);
		/* the disconnected one can't do anything */
		assertTrue(this.gameController
				.handleRequest(new EmptyQuickActionActRqstMsg(playing)) instanceof ActionNotExecutedMessage);
	}

	@Test
	public void testReconnection() {
		/* to test this i've first to simulate a disconnection */
		PlayerData data1 = new PlayerData("test1", "test1");
		PlayerData data2 = new PlayerData("test2", "test2");
		this.gameController = new GameController("GAME0", new GameMapFactory(), this.broadcaster);
		PlayerToken token1 = new PlayerToken("test1");
		TestReconnectionSubscriber subscriber1 = new TestReconnectionSubscriber();
		PlayerToken token2 = new PlayerToken("test2");
		TestReconnectionSubscriber subscriber2 = new TestReconnectionSubscriber();
		try {
			this.broadcaster.subscribe(subscriber1, token1.getUsername());
			this.broadcaster.subscribe(subscriber2, token2.getUsername());
		} catch (RemoteException e) {
			fail();
		}
		/* adding players to game */
		this.gameController.addPlayer(token1, data1);
		this.gameController.addPlayer(token2, data2);
		/* starting game */
		this.gameController.startGame();
		PlayerToken playing = (this.gameController.getCurrentPlayer().equals("test1")) ? token1 : token2;
		PlayerToken waiting = (playing.equals(token1)) ? token2 : token1;
		assertFalse(this.gameController
				.handleRequest(new DrawAPoliticActRqstMsg(playing)) instanceof ActionNotExecutedMessage);
		/* disconnecting playing player */
		this.gameController.disconnectPlayer(playing.getUsername());
		/* waiting player now plays */
		assertEquals(this.gameController.getCurrentPlayer(), waiting.getUsername());
		assertFalse(this.gameController
				.handleRequest(new DrawAPoliticActRqstMsg(waiting)) instanceof ActionNotExecutedMessage);
		/* the disconnected one can't do anything */
		assertTrue(this.gameController
				.handleRequest(new EmptyQuickActionActRqstMsg(playing)) instanceof ActionNotExecutedMessage);
		/* completing the ex-waiting player turn */
		assertFalse(this.gameController
				.handleRequest(new EmptyQuickActionActRqstMsg(waiting)) instanceof ActionNotExecutedMessage);
		/*
		 * before the ex-waiting main action the disconnected player reconnect
		 */
		this.gameController.reconnectPlayer(playing.getUsername());
		TestReconnectionSubscriber waitingSubscriber  = (waiting.equals(token1)) ? subscriber1 : subscriber2;
		assertEquals(waitingSubscriber.getReconnected(),playing.getUsername());
		/* but waiting has to play */
		this.gameController.handleRequest(
				new ElectACouncillorActRqstMsg(waiting, "coast", waitingSubscriber.getAvailableCouncillor()));
		/* not it's time to the reconnected one to play */
		assertEquals(this.gameController.getCurrentPlayer(),playing.getUsername());
	}

	@Test
	public void testOnGameFinished() {
		PlayerData data1 = new PlayerData("test1", "test1");
		PlayerData data2 = new PlayerData("test2", "test2");
		this.gameController = new GameController("GAME0", "maps/special.xml", new GameMapFactory(), this.broadcaster);
		PlayerToken token1 = new PlayerToken("test1");
		TestFinishedSubscriber subscriber1 = new TestFinishedSubscriber();
		PlayerToken token2 = new PlayerToken("test2");
		TestFinishedSubscriber subscriber2 = new TestFinishedSubscriber();
		try {
			this.broadcaster.subscribe(subscriber1, token1.getUsername());
			this.broadcaster.subscribe(subscriber2, token2.getUsername());
		} catch (RemoteException e) {
			fail();
		}
		assertFalse(this.gameController.isGameFinished());
		assertFalse(subscriber1.isGameFinished());
		assertFalse(subscriber2.isGameFinished());
		/* adding players to game */
		this.gameController.addPlayer(token1, data1);
		this.gameController.addPlayer(token2, data2);
		/* starting game */
		this.gameController.startGame();
		/*
		 * the winner will be the first to play that will build an emp in king
		 * location
		 */
		String winner = this.gameController.getCurrentPlayer();
		assertNotEquals(winner, "");
		PlayerToken winnerToken = null;
		if (winner.equals("test1"))
			winnerToken = token1;
		else
			winnerToken = token2;
		PlayerToken loserToken = (winnerToken == token1) ? token2 : token1;
		do {
			DrawAPoliticActRqstMsg draw = new DrawAPoliticActRqstMsg(winnerToken);
			this.gameController.handleRequest(draw);
			assertFalse(this.gameController.isGameFinished());
			assertFalse(subscriber1.isGameFinished());
			// i've to satisfy at least one councillor in the king region
			PoliticsRequestMessage politicsRequest = new PoliticsRequestMessage(winnerToken);
			PoliticsResponseMessage politicsResponse = (PoliticsResponseMessage) this.gameController
					.handleRequest(politicsRequest);
			if (politicsResponse.getPoliticColors().contains("multi_colored")) {
				BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg build = new BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg(
						winnerToken, "arkon", Arrays.asList("multi_colored"));
				this.gameController.handleRequest(build);
			} else {
				this.gameController.handleRequest(
						new ElectACouncillorActRqstMsg(winnerToken, "coast", subscriber1.getAvailableCouncillor()));
			}
			this.gameController.handleRequest(new EmptyQuickActionActRqstMsg(winnerToken));
			/* i've to finish the turn */
			/* loser token turn */
			DrawAPoliticActRqstMsg draw2 = new DrawAPoliticActRqstMsg(loserToken);
			this.gameController.handleRequest(draw2);
			this.gameController.handleRequest(new EmptyQuickActionActRqstMsg(loserToken));
			this.gameController.handleRequest(
					new ElectACouncillorActRqstMsg(loserToken, "coast", subscriber2.getAvailableCouncillor()));
			/* market offer */
			this.gameController.handleRequest(new EndMarketOfferActRqstMsg(winnerToken));
			this.gameController.handleRequest(new EndMarketOfferActRqstMsg(loserToken));
			/* market buy */
			if (this.gameController.getCurrentPlayer().equals(winnerToken.getUsername())) {
				this.gameController.handleRequest(new EndMarketBuyActRqstMsg(winnerToken));
				this.gameController.handleRequest(new EndMarketBuyActRqstMsg(loserToken));
			} else {
				this.gameController.handleRequest(new EndMarketBuyActRqstMsg(loserToken));
				this.gameController.handleRequest(new EndMarketBuyActRqstMsg(winnerToken));
			}
		} while (!this.gameController.isGameFinished());
		assertTrue(this.gameController.isGameFinished());
		assertTrue(subscriber1.isGameFinished());
		assertTrue(subscriber2.isGameFinished());
		assertEquals(subscriber1.getWinner(), winner);
		assertEquals(subscriber2.getWinner(), winner);
	}

}

class TestReconnectionSubscriber implements SubscriberInterface {
	private String reconnected;
	private String availableCouncillor;

	@Override
	public void update(BroadcastMessage msg) throws RemoteException {
		if (msg instanceof ReconnectedPlayerBroadcastMessage) {
			this.reconnected = ((ReconnectedPlayerBroadcastMessage) msg).getReconnected();
		} else if (msg instanceof GameUpdateMessage) {
			this.availableCouncillor = new JSONObject(((GameUpdateMessage) msg).getUpdate()).getJSONArray("councillors")
					.getString(0);
		}
	}

	public String getReconnected() {
		return this.reconnected;
	}

	public String getAvailableCouncillor() {
		return this.availableCouncillor;
	}

}

class TestDisconnectionSubscriber implements SubscriberInterface {
	private String disconnected;

	@Override
	public void update(BroadcastMessage msg) throws RemoteException {
		if (msg instanceof DisconnectedPlayerBroadcastMessage) {
			DisconnectedPlayerBroadcastMessage discMsg = (DisconnectedPlayerBroadcastMessage) msg;
			this.disconnected = discMsg.getDisconnected();
		}

	}

	public String getDisconnected() {
		return this.disconnected;
	}

}

class TestFinishedSubscriber implements SubscriberInterface {
	private boolean isGameFinished = false;
	private String availableCouncillor;
	private String winner;

	@Override
	public void update(BroadcastMessage msg) throws RemoteException {
		if (msg instanceof GameFinishedMessage) {
			this.isGameFinished = true;
			this.winner = ((GameFinishedMessage) msg).getWinner();
		} else if (msg instanceof GameUpdateMessage) {
			this.availableCouncillor = new JSONObject(((GameUpdateMessage) msg).getUpdate()).getJSONArray("councillors")
					.getString(0);
		}

	}

	public String getAvailableCouncillor() {
		return this.availableCouncillor;
	}

	public String getWinner() {
		return this.winner;
	}

	public boolean isGameFinished() {
		return isGameFinished;
	}

}
