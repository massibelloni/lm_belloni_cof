package it.polimi.ingsw.lm_belloni_cof.test.controller.actions;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.ChangeBusinessPermitTilesAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.QuickAction;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMap;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.Permit;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Region;

public class ChangeBusinessPermitTilesActionTest {
	private Game game;
	private GameMap gameMap;

	@Before
	public void initialize() {
		List<String> usernames = Arrays.asList("P1", "P2", "P3");
		this.gameMap = new GameMapFactory().getGameMap("default");
		this.game = new Game(gameMap, usernames);

	}

	@Test
	public void test() {
		Player p1 = game.getPlayerByUsername("P1");
		int oldAss = p1.getNumberOfAssistants();
		List<Permit> beforeActionPermits = this.game.showFaceUpPermitTiles(Region.COAST);
		QuickAction chngTilesAct = new ChangeBusinessPermitTilesAction(game, p1, Region.COAST);
		try {
			chngTilesAct.execute();
		} catch (ActionExecutionException e) {
			fail();
		}
		assertTrue(chngTilesAct.hasBeenExecuted());
		assertEquals(oldAss-1,p1.getNumberOfAssistants());
		List<Permit> afterActionPermits = this.game.showFaceUpPermitTiles(Region.COAST);
		for (Permit before : beforeActionPermits)
			assertFalse(afterActionPermits.contains(before));

	}

}
