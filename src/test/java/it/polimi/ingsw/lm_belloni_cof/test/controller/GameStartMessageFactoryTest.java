package it.polimi.ingsw.lm_belloni_cof.test.controller;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.GameStartMessageFactory;
import it.polimi.ingsw.lm_belloni_cof.controller.GameUpdateMessageFactory;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.EmptyQuickAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.QuickAction;
import it.polimi.ingsw.lm_belloni_cof.controller.states.StatesManager;
import it.polimi.ingsw.lm_belloni_cof.controller.states.TurnMachine;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.messages.GameStartMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.GameUpdateMessage;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMap;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;

public class GameStartMessageFactoryTest {

	@Test
	public void test() {
		List<String> usernames = Arrays.asList("P1","P2","P3");
		Collections.shuffle(usernames);
		GameMap gameMap = new GameMapFactory().getGameMap("default");
		Game game = new Game(gameMap,usernames);
		@SuppressWarnings("unused")
		StatesManager turnManager = new TurnMachine(game);
		GameStartMessage message = GameStartMessageFactory.getGameStartMessage(game, gameMap);
		assertNotNull(message);
		assertNotNull(message.getData());
		assertEquals(message.getLastActionDescription(),"The game has started.");
		assertTrue(message.getData().contains("P1"));
		assertTrue(message.getData().contains("P2"));
		QuickAction empty = new EmptyQuickAction(game,game.getPlayerByUsername("P1"));
		try {
			empty.execute();
		} catch (ActionExecutionException e) {
			fail();
		}
		GameUpdateMessage update = GameUpdateMessageFactory.getGameUpdateMessage(game,empty);
		assertEquals(update.getUpdate(),message.getUpdate());
		//System.out.println(message.getData());
		//System.out.println(message.getUpdate());
	}

}
