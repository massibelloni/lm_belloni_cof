package it.polimi.ingsw.lm_belloni_cof.test.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.model.Bonus;
import it.polimi.ingsw.lm_belloni_cof.model.BonusType;
import it.polimi.ingsw.lm_belloni_cof.model.City;
import it.polimi.ingsw.lm_belloni_cof.model.CityColor;
import it.polimi.ingsw.lm_belloni_cof.model.ColorRewardTileContent;
import it.polimi.ingsw.lm_belloni_cof.model.FinalScoreRules;
import it.polimi.ingsw.lm_belloni_cof.model.GameMap;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.KingRewardTileContent;
import it.polimi.ingsw.lm_belloni_cof.model.PermitContent;
import it.polimi.ingsw.lm_belloni_cof.model.Region;
import it.polimi.ingsw.lm_belloni_cof.model.RegionRewardTileContent;
import it.polimi.ingsw.lm_belloni_cof.model.RewardToken;

public class GameMapFactoryTest {

	@Test
	public void testCreation() {
		GameMapFactory factory = new GameMapFactory();
		GameMap g1 = factory.getGameMap("default");
		GameMap g2 = factory.getGameMap("default");
		assertEquals(g1, g2);
	}

	@Test
	public void testFromXML() {
		GameMapFactory factory = new GameMapFactory();
		GameMap g1 = factory.getGameMap("maps/test.xml");

		// INITIAL SETTINGS
		assertEquals(g1.getMaxVictoryPoints(), 50);
		assertEquals(g1.getStartingKingLocation().getName().toLowerCase(), "juvelar");
		GameMap def = factory.getGameMap("default");
		assertNotEquals(g1, def);
		assertEquals(g1, factory.getGameMap("maps/test.xml"));
		assertEquals(g1.getInitialPoliticsPerPlayer(), 6);
		assertEquals(g1.getInitialAssistantsNumber(), 1);
		assertEquals(g1.getInitialCoinsAmount(), 4);
		assertEquals(g1.getEmporiumsPerPlayer(), 10);
		assertEquals(g1.getMaxCoins(), 20);

		// FINAL SCORE SETTINGS
		FinalScoreRules finalScoreRules = g1.getFinalRules();
		assertEquals(finalScoreRules.getLastEmporiumPoints(), 3);
		assertEquals(finalScoreRules.getFirstNobilityTrackPoints(), 5);
		assertEquals(finalScoreRules.getSecondNobilityTrackPoints(), 2);
		assertEquals(finalScoreRules.getPermitPoints(), 3);

		// NOBILITY TRACK
		assertEquals(g1.getNobilityTrackSize(), 3);
		List<Bonus> nobilityTrackPos0 = g1.getBonusesForANobilityTrackPosition(0);
		assertEquals(nobilityTrackPos0.size(), 8);
		List<Bonus> nobilityTrackPos1 = g1.getBonusesForANobilityTrackPosition(1);
		Bonus ownedPermitBonus = nobilityTrackPos1.get(0);
		assertEquals(ownedPermitBonus.getBonusType(), BonusType.PERMIT_BONUS);
		assertEquals(ownedPermitBonus.getNumberOfTimes(), 1);
		Bonus victoryBonus = nobilityTrackPos1.get(1);
		assertEquals(victoryBonus.getBonusType(), BonusType.VICTORY_POINT);
		assertEquals(victoryBonus.getNumberOfTimes(), 7);
		Bonus drawPermitBonus = nobilityTrackPos1.get(2);
		assertEquals(drawPermitBonus.getBonusType(), BonusType.TAKE_PERMIT);
		assertEquals(drawPermitBonus.getNumberOfTimes(), 1);
		Bonus drawPoliticBonus = nobilityTrackPos1.get(3);
		assertEquals(drawPoliticBonus.getBonusType(), BonusType.POLITIC);
		assertEquals(drawPoliticBonus.getNumberOfTimes(), 2);
		// Now MAIN_ACTION Bonus should be skipped because its attribute
		// numberOfTimes is 0, so the next BonusType in the list is CITY_REWARD
		Bonus expectedTokenBonus = nobilityTrackPos1.get(4);
		assertEquals(expectedTokenBonus.getBonusType(), BonusType.CITY_REWARD);
		assertEquals(expectedTokenBonus.getNumberOfTimes(), 2);
		// Now look into the third position of the nobility track, i'm expecting
		// that it is empty because all bonuses has only null attributes
		List<Bonus> nobilityTrackPos2 = g1.getBonusesForANobilityTrackPosition(2);
		assertTrue(nobilityTrackPos2.isEmpty());

		// CITIES
		assertEquals(g1.getCityList().size(), 15);
		List<City> cityList = g1.getCityList();
		List<String> expectedCities = Arrays.asList("arkon", "burgen", "castrum", "dorful", "esti", "framek", "graden",
				"hellar", "indur", "juvelar", "kultos", "lyram", "merkatim", "naris", "osium");
		int numberOfCities = expectedCities.size();
		assertEquals(numberOfCities, 15);
		for (int i = 0; i < numberOfCities; i++) {
			if (cityList.get(i).getName().equals(expectedCities.get(i)))
				assertTrue(true);
			else
				fail();
		}
		for (int i = 0; i < 15; i++) {
			if (i < 5)
				assertEquals(cityList.get(i).getCityRegion(), Region.COAST);
			else if (i >= 5 && i < 10)
				assertEquals(cityList.get(i).getCityRegion(), Region.HILLS);
			else if (i >= 10 && i < 15)
				assertEquals(cityList.get(i).getCityRegion(), Region.MOUNTAINS);
		}
		List<CityColor> expectedColors = Arrays.asList(CityColor.CYAN, CityColor.YELLOW, CityColor.GRAY, CityColor.GRAY,
				CityColor.RED, CityColor.YELLOW, CityColor.GRAY, CityColor.RED, CityColor.RED, CityColor.PURPLE,
				CityColor.YELLOW, CityColor.GRAY, CityColor.CYAN, CityColor.RED, CityColor.YELLOW);
		for (int i = 0; i < numberOfCities; i++) {
			if (cityList.get(i).getCityColor() == expectedColors.get(i))
				assertTrue(true);
			else
				fail();
		}
		List<Integer> expectedBonusNumber = Arrays.asList(1, 2, 1, 1, 2, 1, 2, 1, 5, 0, 1, 1, 1, 1, 1);
		for (int i = 0; i < numberOfCities; i++) {
			if (cityList.get(i).getBonusNumber() == expectedBonusNumber.get(i))
				assertTrue(true);
			else
				fail();
		}
		City arkon = cityList.get(0);
		List<City> connectedToArkon = g1.getConnectedCities(arkon);
		assertEquals(connectedToArkon.size(), 2);
		assertTrue(connectedToArkon.get(0).getName().equals("burgen"));
		assertTrue(connectedToArkon.get(1).getName().equals("castrum"));
		// Also merkatim, that hasn't any tag <connection> should be connected
		// to some others cities
		// because the map in an undirected-graph
		for (int i = 0; i < numberOfCities; i++) {
			if (cityList.get(i).getName().equals("merkatim")) {
				City merkatim = cityList.get(i);
				List<City> connectedToMerkatim = g1.getConnectedCities(merkatim);
				assertEquals(connectedToMerkatim.size(), 2);
				assertTrue(connectedToMerkatim.get(0).getName().equals("hellar"));
				assertTrue(connectedToMerkatim.get(1).getName().equals("osium"));
			}
		}

		// PERMIT
		List<PermitContent> loadedContentsCoast = g1.getPermitContentsByRegion(Region.COAST);
		List<PermitContent> loadedContentsHills = g1.getPermitContentsByRegion(Region.HILLS);
		assertEquals(loadedContentsCoast.size(), 2);
		assertEquals(loadedContentsHills.size(), 1);
		PermitContent firstLoadedContentCoast = loadedContentsCoast.get(0);
		PermitContent secondLoadedContentCoast = loadedContentsCoast.get(1);
		assertEquals(firstLoadedContentCoast.getCities().size(), 1);
		assertTrue(firstLoadedContentCoast.getCities().get(0).getName().equals("dorful"));
		assertEquals(secondLoadedContentCoast.getCities().size(), 1);
		assertTrue(secondLoadedContentCoast.getCities().get(0).getName().equals("arkon"));
		// first permit
		List<Bonus> firstLoadedCoastBonuses = g1.getPermitContentsByRegion(Region.COAST).get(0).getBonuses();
		assertEquals(firstLoadedCoastBonuses.size(), 1);
		assertEquals(firstLoadedCoastBonuses.get(0).getBonusType(), BonusType.VICTORY_POINT);
		assertEquals(firstLoadedCoastBonuses.get(0).getNumberOfTimes(), 7);
		// second permit
		List<Bonus> secondLoadedCoastBonuses = g1.getPermitContentsByRegion(Region.COAST).get(1).getBonuses();
		assertEquals(secondLoadedCoastBonuses.size(), 3);
		assertEquals(secondLoadedCoastBonuses.get(0).getBonusType(), BonusType.COIN);
		assertEquals(secondLoadedCoastBonuses.get(0).getNumberOfTimes(), 3);
		assertEquals(secondLoadedCoastBonuses.get(1).getBonusType(), BonusType.VICTORY_POINT);
		assertEquals(secondLoadedCoastBonuses.get(1).getNumberOfTimes(), 7);
		assertEquals(secondLoadedCoastBonuses.get(2).getBonusType(), BonusType.NOBILITY);
		assertEquals(secondLoadedCoastBonuses.get(2).getNumberOfTimes(), 4);
		// third, and last, permit. The only one in HILLS region
		List<Bonus> firstLoadedHillsBonuses = g1.getPermitContentsByRegion(Region.HILLS).get(0).getBonuses();
		assertEquals(firstLoadedHillsBonuses.size(), 2);
		assertEquals(firstLoadedHillsBonuses.get(0).getBonusType(), BonusType.NOBILITY);
		assertEquals(firstLoadedHillsBonuses.get(0).getNumberOfTimes(), 1);
		assertEquals(firstLoadedHillsBonuses.get(1).getBonusType(), BonusType.POLITIC);
		assertEquals(firstLoadedHillsBonuses.get(1).getNumberOfTimes(), 1);

		// COLOR REWARD TILES
		List<ColorRewardTileContent> clrRwdContents = new ArrayList<>();
		for (CityColor color : CityColor.values()) {
			if (color == CityColor.PURPLE)
				assertNull(g1.getRewardTileContentByCityColor(color));
			else
				clrRwdContents.add(g1.getRewardTileContentByCityColor(color));
		}
		assertEquals(clrRwdContents.size(), 4);
		for (ColorRewardTileContent clrRwdContent : clrRwdContents) {
			assertEquals(1, clrRwdContent.getBonuses().size());
			assertEquals(BonusType.VICTORY_POINT, clrRwdContent.getBonuses().get(0).getBonusType());
			assertEquals(5, clrRwdContent.getBonuses().get(0).getNumberOfTimes());
		}

		// REGION REWARD TILES
		List<RegionRewardTileContent> rgnRwdContents = new ArrayList<>();
		for (Region region : Region.values()) {
			if (region == Region.KING)
				assertNull(g1.getRewardTileContentByRegion(region));
			else
				rgnRwdContents.add(g1.getRewardTileContentByRegion(region));
		}
		assertEquals(rgnRwdContents.size(), 3);
		for (RegionRewardTileContent rgnRwdContent : rgnRwdContents) {
			assertEquals(1, rgnRwdContent.getBonuses().size());
			assertEquals(BonusType.VICTORY_POINT, rgnRwdContent.getBonuses().get(0).getBonusType());
			assertEquals(5, rgnRwdContent.getBonuses().get(0).getNumberOfTimes());
		}

		// KING REWARD TILES
		List<KingRewardTileContent> kingRwdContents = g1.getKingRewardTileContents();
		assertEquals(5, kingRwdContents.size());
		for (int i = 0; i < 5; i++) {
			assertEquals(i + 1, kingRwdContents.get(i).getPickingOrder());
		}
		for (int i = 0; i < 5; i++) {
			KingRewardTileContent kingRwd = kingRwdContents.get(i);
			assertEquals(1, kingRwd.getBonuses().size());
			assertEquals(BonusType.VICTORY_POINT, kingRwd.getBonuses().get(0).getBonusType());
			if (kingRwd.getPickingOrder() == 1)
				assertEquals(25, kingRwd.getBonuses().get(0).getNumberOfTimes());
			else if (kingRwd.getPickingOrder() == 2)
				assertEquals(18, kingRwd.getBonuses().get(0).getNumberOfTimes());
			else if (kingRwd.getPickingOrder() == 3)
				assertEquals(12, kingRwd.getBonuses().get(0).getNumberOfTimes());
			else if (kingRwd.getPickingOrder() == 4)
				assertEquals(7, kingRwd.getBonuses().get(0).getNumberOfTimes());
			else
				assertEquals(3, kingRwd.getBonuses().get(0).getNumberOfTimes());

		}

		// REWARD TOKEN
		List<RewardToken> rwdTokens = g1.getRewardTokens();
		assertEquals(15, rwdTokens.size());
		// first reward token
		RewardToken firstRwdToken = rwdTokens.get(0);
		assertEquals(1, firstRwdToken.getBonuses().size());
		assertEquals(BonusType.NOBILITY, firstRwdToken.getBonuses().get(0).getBonusType());
		assertEquals(1, firstRwdToken.getBonuses().get(0).getNumberOfTimes());
		// second reward token
		RewardToken secondRwdToken = rwdTokens.get(1);
		assertEquals(1, secondRwdToken.getBonuses().size());
		assertEquals(BonusType.COIN, secondRwdToken.getBonuses().get(0).getBonusType());
		assertEquals(2, secondRwdToken.getBonuses().get(0).getNumberOfTimes());
		// third reward token
		RewardToken thirdRwdToken = rwdTokens.get(2);
		assertEquals(1, thirdRwdToken.getBonuses().size());
		assertEquals(BonusType.NOBILITY, thirdRwdToken.getBonuses().get(0).getBonusType());
		assertEquals(1, thirdRwdToken.getBonuses().get(0).getNumberOfTimes());
		// sixth reward token, it should be empty
		RewardToken sixthRwdToken = rwdTokens.get(5);
		assertEquals(0, sixthRwdToken.getBonuses().size());
		// seventh reward token
		RewardToken seventhRwdToken = rwdTokens.get(6);
		List<Bonus> seventhRwdBonuses = seventhRwdToken.getBonuses();
		assertEquals(2, seventhRwdBonuses.size());
		assertEquals(BonusType.COIN, seventhRwdBonuses.get(0).getBonusType());
		assertEquals(3, seventhRwdBonuses.get(0).getNumberOfTimes());
		assertEquals(BonusType.VICTORY_POINT, seventhRwdBonuses.get(1).getBonusType());
		assertEquals(1, seventhRwdBonuses.get(1).getNumberOfTimes());

	}
}
