package it.polimi.ingsw.lm_belloni_cof.test.controller.actions;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.BuildAnEmporiumWithTheHelpOfTheKingAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.DrawAPoliticAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.EmptyQuickAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.EndMarketBuyAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.EndMarketOfferAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.MainAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.MarketBuyAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.MarketOfferAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.QuickAction;
import it.polimi.ingsw.lm_belloni_cof.controller.states.StatesManager;
import it.polimi.ingsw.lm_belloni_cof.controller.states.TurnMachine;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.GameFinishedException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidActionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughAssistantsException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughCoinsException;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMap;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.MarketOffer;
import it.polimi.ingsw.lm_belloni_cof.model.Ownable;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Region;

public class MarketBuyActionTest {
	Game game;
	StatesManager states;
	Player p1;
	Player p2;

	@Before
	public void initialize() throws GameFinishedException, NotEnoughAssistantsException, InvalidActionException {
		GameMap gameMap = new GameMapFactory().getGameMap("default");
		gameMap.setMaxCoins(50); // cheat
		List<String> usernames = Arrays.asList("P1", "P2");
		this.game = new Game(gameMap, usernames);
		this.states = new TurnMachine(game);
		// some fake actions to correctly get to Market
		DrawAPoliticAction draw = new DrawAPoliticAction(null, null);
		MainAction main = new BuildAnEmporiumWithTheHelpOfTheKingAction(null, null, null, null, null);
		QuickAction quick = new EmptyQuickAction(null, null);
		states.changeState("P1", draw);
		states.changeState("P1", main);
		states.changeState("P1", quick);
		states.changeState("P2", draw);
		states.changeState("P2", main);
		states.changeState("P2", quick);
		// now both in market offer
		// P1 placing two offers
		p1 = game.getCurrentPlayer();
		MarketOffer p1o1 = new MarketOffer(p1.getAnAssistant(), 15);
		MarketOffer p1o2 = new MarketOffer(p1.getPoliticsHand().remove(0), 12);
		MarketOffer p1o3 = new MarketOffer(p1.getPoliticsHand().remove(0), 2);
		MarketOfferAction p1OfferAct = new MarketOfferAction(game, p1, p1o1);
		p1OfferAct.execute();
		states.changeState(p1.getUsername(), p1OfferAct);
		p1OfferAct = new MarketOfferAction(game, p1, p1o2);
		p1OfferAct.execute();
		states.changeState(p1.getUsername(), p1OfferAct);
		p1OfferAct = new MarketOfferAction(game, p1, p1o3);
		p1OfferAct.execute();
		states.changeState(p1.getUsername(), p1OfferAct);
		states.changeState(p1.getUsername(), new EndMarketOfferAction(game,p1));
		p2 = game.getCurrentPlayer();
		game.acquireAPermitTile(p2, Region.HILLS, 1); // CHEAT
		MarketOffer p2o1 = new MarketOffer(p2.getPermitsAcquired().remove(0), 10);
		MarketOfferAction p2OfferAct = new MarketOfferAction(game, p2, p2o1);
		p2OfferAct.execute();
		states.changeState(p2.getUsername(), p2OfferAct);
		states.changeState(p2.getUsername(), new EndMarketOfferAction(game,p2));
	}

	@Test
	public void test() throws NotEnoughCoinsException, GameFinishedException {
		Player current = game.getPlayerByUsername(states.getCurrentPlayer());
		Player other = (current == p1) ? (p2) : (p1);
		current.moveOnCoinsTrack(20, 50); // cheat
		other.moveOnCoinsTrack(20, 50); // cheat
		int beforeSaleBuyer = current.getCoins();
		int beforeSaleSeller = other.getCoins();
		List<MarketOffer> availables = this.game.getOffersByPlayer(other);
		MarketOffer used = availables.get(0);
		MarketBuyAction marketBuy1 = new MarketBuyAction(game, current, other, used.getObjectForSale());
		try {
			marketBuy1.execute();
			assertTrue(marketBuy1.hasBeenExecuted());
			assertEquals(used.getObjectForSale().getOwner(), current);
			assertEquals(beforeSaleBuyer - used.getPrice(), current.getCoins());
			assertEquals(beforeSaleSeller + used.getPrice(), other.getCoins());
		} catch (ActionExecutionException e) {
			fail();
		}
		MarketBuyAction endCurrent = new EndMarketBuyAction(this.game, current);
		try {
			endCurrent.execute();
		} catch (ActionExecutionException e) {
			fail();
		}
		this.states.changeState(current.getUsername(), endCurrent);
		// now it's time for other to buy something
		beforeSaleBuyer = other.getCoins();
		beforeSaleSeller = current.getCoins();
		assertEquals(game.getCurrentPlayer(), other);
		availables = this.game.getOffersByPlayer(current);
		used = availables.get(0);
		MarketBuyAction marketBuy2 = new MarketBuyAction(game, other, current, used.getObjectForSale());
		try {
			marketBuy2.execute();
		} catch (ActionExecutionException e) {
			fail();
		}
		assertTrue(marketBuy2.hasBeenExecuted());
		assertEquals(used.getObjectForSale().getOwner(), other);
		assertEquals(beforeSaleBuyer - used.getPrice(), other.getCoins());
		assertEquals(beforeSaleSeller + used.getPrice(), current.getCoins());
		// depending on this specific initialization there must be an unsold
		// item
		// i don't know which one of the couple, the buy order is random
		assertTrue(!this.game.getOffersByPlayer(current).isEmpty() || !this.game.getOffersByPlayer(other).isEmpty());
		// but one must be empty
		assertTrue(this.game.getOffersByPlayer(current).isEmpty() || this.game.getOffersByPlayer(other).isEmpty());
		MarketBuyAction endOther = new EndMarketBuyAction(this.game, other);
		try {
			endOther.execute();
		} catch (ActionExecutionException e) {
			fail();
		}
		states.changeState(other.getUsername(), endOther);
		// now market buy should be finished
		// unsold items to old owners
		assertFalse(this.game.inMarketBuy() || this.game.inMarketOffer());
		for (MarketOffer offer : game.getOffersByPlayer(current))
			assertEquals(offer.getObjectForSale().getOwner(), current);
		for (MarketOffer offer : game.getOffersByPlayer(other))
			assertEquals(offer.getObjectForSale().getOwner(), other);
	}
	
	@Test
	public void emptyBuyTest() throws GameFinishedException{
		Map<Ownable,Player> preEmptyBuy = new HashMap<>();
		for (MarketOffer offer : game.getOffersByPlayer(p1))
			preEmptyBuy.put(offer.getObjectForSale(), p1);
		for (MarketOffer offer : game.getOffersByPlayer(p2))
			preEmptyBuy.put(offer.getObjectForSale(), p2);
		for(Map.Entry<Ownable, Player> entry:preEmptyBuy.entrySet())
			assertEquals(entry.getKey().getOwner(),null);
		EndMarketBuyAction empty1 = new EndMarketBuyAction(game,p1);
		states.changeState(p1.getUsername(),empty1);
		EndMarketBuyAction empty2 = new EndMarketBuyAction(game,p2);
		states.changeState(p2.getUsername(),empty2);
		for(Map.Entry<Ownable, Player> entry:preEmptyBuy.entrySet())
			assertEquals(entry.getKey().getOwner(),entry.getValue());
	}

}
