package it.polimi.ingsw.lm_belloni_cof.test.model;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.model.FakePlayer;

public class FakePlayerTest {

	@Test
	public void testCreation() {
		FakePlayer fakePlayer=new FakePlayer();
		assertEquals(fakePlayer.getUsername(), "Council of Four");
	}

}
