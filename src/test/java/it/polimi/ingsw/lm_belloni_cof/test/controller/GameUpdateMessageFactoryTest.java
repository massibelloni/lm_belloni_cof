package it.polimi.ingsw.lm_belloni_cof.test.controller;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.GameUpdateMessageFactory;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.EmptyQuickAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.QuickAction;
import it.polimi.ingsw.lm_belloni_cof.controller.states.StatesManager;
import it.polimi.ingsw.lm_belloni_cof.controller.states.TurnMachine;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.messages.GameUpdateMessage;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMap;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;

public class GameUpdateMessageFactoryTest {

	@Test
	public void test() {
		List<String> usernames = Arrays.asList("P1","P2","P3");
		Collections.shuffle(usernames);
		GameMap gameMap = new GameMapFactory().getGameMap("default");
		Game game = new Game(gameMap,usernames);
		@SuppressWarnings("unused")
		StatesManager turnManager = new TurnMachine(game);
		QuickAction empty = new EmptyQuickAction(game,game.getPlayerByUsername("P1"));
		try {
			empty.execute();
		} catch (ActionExecutionException e) {
			fail();
		}
		GameUpdateMessage message = GameUpdateMessageFactory.getGameUpdateMessage(game,empty);
		assertNotNull(message);
		assertNotNull(message.getLastActionDescription());
		assertEquals(message.getLastActionDescription(),"P1 hasn't performed any quick action.");
		System.out.println(message.getUpdate());
	}

}
