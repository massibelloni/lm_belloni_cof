package it.polimi.ingsw.lm_belloni_cof.test.controller.actions;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.EngageAnAssistantAction;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.Player;

public class EngageAnAssistantActionTest {
	private Game game;

	@Before
	public void initialize() {
		List<String> usernames = Arrays.asList("P1", "P2", "P3");
		this.game = new Game(new GameMapFactory().getGameMap("default"), usernames);
	}

	@Test
	public void test() {
		Player p1 = this.game.getPlayerByUsername("P1");
		int beforeCoins = p1.getCoins();
		int beforeAssistants = p1.getNumberOfAssistants();
		EngageAnAssistantAction engage = new EngageAnAssistantAction(game, p1);
		try {
			engage.execute();
		} catch (ActionExecutionException e) {
			fail();
		}
		assertTrue(engage.hasBeenExecuted());
		assertEquals(beforeCoins - 3, p1.getCoins());
		assertEquals(beforeAssistants + 1, p1.getNumberOfAssistants());
	}

}
