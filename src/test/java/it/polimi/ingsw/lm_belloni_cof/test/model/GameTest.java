package it.polimi.ingsw.lm_belloni_cof.test.model;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.exceptions.CitiesNotConnectedException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.CouncilUnsatisfiedException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.EmptyDeckException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidActionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughAssistantsException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughCoinsException;
import it.polimi.ingsw.lm_belloni_cof.model.Assistant;
import it.polimi.ingsw.lm_belloni_cof.model.Bonus;
import it.polimi.ingsw.lm_belloni_cof.model.City;
import it.polimi.ingsw.lm_belloni_cof.model.CityColor;
import it.polimi.ingsw.lm_belloni_cof.model.Councillor;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMap;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.MarketOffer;
import it.polimi.ingsw.lm_belloni_cof.model.Permit;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Politic;
import it.polimi.ingsw.lm_belloni_cof.model.PoliticColor;
import it.polimi.ingsw.lm_belloni_cof.model.Region;
import it.polimi.ingsw.lm_belloni_cof.model.RewardToken;

/**
 * 
 * Those test cases simulate a simple gameplays, in order to tests some game's
 * situations.
 * 
 */
public class GameTest {
	private GameMap gameMap;
	private List<String> usernames;
	private Game game;
	private Player p1;
	private Player p2;
	private Player p3;
	private Player p4;

	@Before
	public void initialization() {
		this.gameMap = new GameMapFactory().getGameMap("default");
		this.usernames = new ArrayList<>();
		for (int i = 1; i <= 4; i++)
			usernames.add(Integer.toString(i));
		this.game = new Game(gameMap, usernames);
		this.p1 = game.getPlayerByUsername("1");
		this.p2 = game.getPlayerByUsername("2");
		this.p3 = game.getPlayerByUsername("3");
		this.p4 = game.getPlayerByUsername("4");
	}

	@Test
	public void testInitialSetUp() {
		int position = 0;
		for (String username : game.getPlayerUsernames()) {
			Player player = game.getPlayerByUsername(username);
			assertEquals(player.getPoliticsHand().size(), gameMap.getInitialPoliticsPerPlayer());
			assertEquals(player.getNumberOfAssistants(), gameMap.getInitialAssistantsNumber() + position);
			assertEquals(player.getCoins(), gameMap.getInitialCoinsAmount() + position);
			position++;
		}
	}

	@Test
	public void testInitialSetUpWith2Players() {
		List<String> newUsernames = new ArrayList<>();
		for (int i = 1; i <= 2; i++) {
			newUsernames.add(Integer.toString(i));
		}
		Game newGame = new Game(gameMap, newUsernames);
		// there should be emporiums already built by a fake player
		for (City city : newGame.getCityList()) {
			List<Player> players = newGame.getPlayersWithAnEmporiumOn(city);
			if (!(players.isEmpty())) {
				assertEquals(players.size(), 1);
				assertEquals(players.get(0).getUsername(), "Council of Four");
			}
		}
	}

	@Test
	public void testElectACouncillor() throws InvalidActionException {
		List<PoliticColor> availableColors = new ArrayList<>(game.getAvailableCouncillorColors());
		Collections.shuffle(availableColors);
		// choose randomly between available colors
		PoliticColor chosenColor = availableColors.get(0);
		Councillor chosenCouncillor = game.getACouncillorByPoliticColor(chosenColor);
		List<PoliticColor> councilBeforeElection = game.getCouncilByRegion(Region.COAST);
		int coinsBeforeElection = p1.getCoins();
		int numberOfAvailableCouncillorsBeforeElection = game.numberOfAvailableCouncillors();
		game.electACouncillor(p1, Region.COAST, chosenCouncillor);
		List<PoliticColor> councilAfterElection = game.getCouncilByRegion(Region.COAST);
		assertEquals(councilBeforeElection.get(1), councilAfterElection.get(0));
		assertEquals(councilBeforeElection.get(2), councilAfterElection.get(1));
		assertEquals(councilBeforeElection.get(3), councilAfterElection.get(2));
		assertEquals(chosenColor, councilAfterElection.get(3));
		assertEquals(coinsBeforeElection + 4, p1.getCoins());
		assertEquals(numberOfAvailableCouncillorsBeforeElection, game.numberOfAvailableCouncillors());
	}

	@Test
	public void testAcquireABusinessPermitTile() {
		int politicsOwned = p2.getPoliticsHand().size();
		assertEquals(p2.getPermitsAcquired().size(), 0);
		for (int i = 0; i < 4; i++) {
			p2.getPoliticsHand().add(new Politic(PoliticColor.MULTI_COLORED));
		}
		List<Politic> politicsToUse = new ArrayList<>();
		for (int j = politicsOwned; j <= politicsOwned + 3; j++) {
			politicsToUse.add(p2.getPoliticsHand().get(j));
		}
		int coinsBeforeAction = p2.getCoins();
		Permit chosenPermit = game.showFaceUpPermitTiles(Region.HILLS).get(0);
		Permit otherPermit = game.showFaceUpPermitTiles(Region.HILLS).get(1);
		List<Bonus> bonuses = new ArrayList<>();

		try {
			bonuses = game.acquireAPermitTile(p2, Region.HILLS, 1, politicsToUse);
		} catch (InvalidActionException e) {
			fail();
		} catch (NotEnoughCoinsException|CouncilUnsatisfiedException e) {
			fail();
		}
		// Correclty calculated coins to pay
		assertEquals(coinsBeforeAction - 4, p2.getCoins());
		// Correctly assigned Permit tile
		assertEquals(p2.getPermitsAcquired().size(), 1);
		if (chosenPermit == p2.getPermitsAcquired().get(0)) {
			assertTrue(true);
		} else
			fail();
		// Correctly setted Owner attribute
		assertEquals(chosenPermit.getOwner(), p2);
		// All bonuses are catched
		if (bonuses.containsAll(chosenPermit.getBonuses())) {
			assertTrue(true);
		} else
			fail();
		// Chosen permit substituted
		assertEquals(game.showFaceUpPermitTiles(Region.HILLS).size(), 2);
		// Permit nr. 2 not touched
		if (otherPermit == game.showFaceUpPermitTiles(Region.HILLS).get(1))
			assertTrue(true);
		else
			fail();
	}

	@Test
	public void testBuildAnEmporiumUsingAPermitTile() {
		int politicsOwned = p2.getPoliticsHand().size();
		for (int i = 0; i < 4; i++) {
			p1.getPoliticsHand().add(new Politic(PoliticColor.MULTI_COLORED));
		}
		List<Politic> politicsToUse = new ArrayList<>();
		for (int j = politicsOwned; j <= politicsOwned + 3; j++) {
			politicsToUse.add(p1.getPoliticsHand().get(j));
		}
		try {// It shoud work because it is already tested
			game.acquireAPermitTile(p1, Region.COAST, 1, politicsToUse);
		} catch (CouncilUnsatisfiedException | InvalidActionException | NotEnoughCoinsException e) {
			fail();
		}
		// Now p1 has a permit tile in his hand
		for (int i = 0; i < 10; i++)
			;
		p1.giveAnAssistant(new Assistant());
		int assistantsBeforeAction = p1.getNumberOfAssistants();
		Permit permitToUse = p1.getPermitsAcquired().get(0);
		List<City> citiesOnPermit = new ArrayList<>(p1.getPermitsAcquired().get(0).getCities());
		// Randomly choose a city on which to buld the emporium
		Collections.shuffle(citiesOnPermit);
		City chosenCity = citiesOnPermit.get(0);
		List<Bonus> bonusesAcquired = new ArrayList<>();
		try {
			bonusesAcquired = game.buildAnEmporiumUsingAPermitTile(p1, permitToUse, chosenCity);
		} catch (InvalidActionException | NotEnoughAssistantsException e) {
			fail();
		}
		// There's the new emporium?
		List<Player> playersWithAnEmpHere = game.getPlayersWithAnEmporiumOn(chosenCity);
		assertEquals(playersWithAnEmpHere.size(), 1);
		// p1 is the first to have built an emporium
		assertEquals(playersWithAnEmpHere.get(0), p1);
		// No assistants to pay, he is the first, and only to have built an
		// emporium
		assertEquals(assistantsBeforeAction, p1.getNumberOfAssistants());
		// Correctly catched bonuses
		List<Bonus> expectedBonuses = new ArrayList<>();
		for (RewardToken cityTokens : game.getRewardTokensOn(chosenCity)) {
			for (Bonus bonus : cityTokens.getBonuses()) {
				expectedBonuses.add(bonus);
			}
		}
		assertTrue(bonusesAcquired.containsAll(expectedBonuses));
		// the list bonusesAcquired must contains only bonuses over city
		// choseCity, because
		// he has no others emporiums on the map
		assertEquals(bonusesAcquired.size(), expectedBonuses.size());
		// player has the same number of permit tile than before this action
		assertEquals(p1.getPermitsAcquired().size(), 1);
		// But this permit is used
		assertTrue(p1.getPermitsAcquired().get(0).hasBeenUsed());
	}

	@Test
	public void testBuildEmporiumWithTheHelpOfTheKing()
			throws NotEnoughCoinsException, NotEnoughAssistantsException, InvalidActionException, CitiesNotConnectedException, CouncilUnsatisfiedException {
		City kingLocation = game.getKingsLocation();
		p1.moveOnCoinsTrack(gameMap.getMaxCoins(), gameMap.getMaxCoins());
		p1.getPoliticsHand().clear();
		for (int i = 0; i < 12; i++)
			p1.getPoliticsHand().add(new Politic(PoliticColor.MULTI_COLORED));
		int coinsBeforeAction = p1.getCoins();
		List<Politic> politicsForFirstEmporium = new ArrayList<>();
		List<Politic> politicsForSecondEmporium = new ArrayList<>();
		List<Politic> politicsForThirdEmporium = new ArrayList<>();
		// Fill 3 list of politics to use
		for (int i = 0; i <= 11; i++) {
			if (i <= 3)
				politicsForFirstEmporium.add(p1.getPoliticsHand().get(i));
			else if (i > 3 && i <= 7)
				politicsForSecondEmporium.add(p1.getPoliticsHand().get(i));
			else if (i > 7 && i <= 11)
				politicsForThirdEmporium.add(p1.getPoliticsHand().get(i));
		}
		try {
			game.buildAnEmporiumWithTheHelpOfTheKing(p1, kingLocation, politicsForFirstEmporium);
		} catch (CouncilUnsatisfiedException|NotEnoughCoinsException | NotEnoughAssistantsException | InvalidActionException e) {
			fail();
		}
		// i've to pay only for using multi_colored politics card,
		// not also for king movements because i have built in kingLocation
		int coinsAfterFirstAction = p1.getCoins();
		assertEquals(coinsBeforeAction - 4, coinsAfterFirstAction);
		// checking if emporium has beeen built
		assertTrue(game.getPlayersWithAnEmporiumOn(kingLocation).contains(p1));
		// Now build an emporium in a ramdom city in order to check payments due
		// to king's movements
		List<City> randCityList = new ArrayList<>(game.getCityList());
		Collections.shuffle(randCityList);
		City chosenCity = null;
		for (int i = 0; i < gameMap.getCityList().size(); i++) {
			if (randCityList.get(i) != kingLocation) {
				chosenCity = randCityList.get(i);
				break;
			}
		}
		City newKingLocation = null;
		p1.moveOnCoinsTrack(gameMap.getMaxCoins(), gameMap.getMaxCoins());
		try {
			int kingDistance = gameMap.getRoadsBetweenCities(chosenCity, kingLocation);
			game.buildAnEmporiumWithTheHelpOfTheKing(p1, chosenCity, politicsForSecondEmporium);
			// checking coins payed
			int coinsAfterSecondAction = p1.getCoins();
			assertEquals(gameMap.getMaxCoins() - 2 * kingDistance - 4, coinsAfterSecondAction);
			newKingLocation = game.getKingsLocation();
			// king is in a new city now
			assertEquals(chosenCity, newKingLocation);
		} catch (CouncilUnsatisfiedException|CitiesNotConnectedException e) {
			/* move on */}
		// now i've to build near a city where i already have an emporium
		// in order to check if bonus are catched correctly
		City whereToBuild = null;
		for (City city : game.getCityList()) {
			if (gameMap.areCitiesDirectlyConnected(city, chosenCity) && city != kingLocation) {
				whereToBuild = city;
				break;
			}
		}
		List<Bonus> gainedBonuses = new ArrayList<>();
		// giving coins to p1 in order to avoid that he finish coins during the
		// action
		p1.moveOnCoinsTrack(gameMap.getMaxCoins(), gameMap.getMaxCoins());
		gainedBonuses = game.buildAnEmporiumWithTheHelpOfTheKing(p1, whereToBuild, politicsForThirdEmporium);
		// now p1 should have bonuses from city whereToBuild and city chosenCity
		// because they are neighboring
		game.setCurrentPlayer(p1);
		List<RewardToken> tokenFromWhereToBuild = game.getRewardTokensOn(whereToBuild);
		List<RewardToken> tokenFromChosenCity = game.getRewardTokensOn(chosenCity);
		List<Bonus> bonusFromWhereToBuild = new ArrayList<>();
		List<Bonus> bonusFromChosenCity = new ArrayList<>();
		for (RewardToken rewardToken : tokenFromWhereToBuild)
			bonusFromWhereToBuild.addAll(rewardToken.getBonuses());
		for (RewardToken rewardToken : tokenFromChosenCity)
			bonusFromChosenCity.addAll(rewardToken.getBonuses());
		assertTrue(gainedBonuses.containsAll(bonusFromWhereToBuild));
		assertTrue(gainedBonuses.containsAll(bonusFromChosenCity));
	}
	
	@Test
	public void testEngageAnAssistant(){
		int coinsBeforeAction=p3.getCoins();
		int numberOfAssistantsBeforeAction=p3.getNumberOfAssistants();
		try {
			game.engageAnAssistant(p3);
		} catch (NotEnoughCoinsException e) {
			fail();
		}
		assertEquals(p3.getCoins(),coinsBeforeAction-3);
		assertEquals(p3.getNumberOfAssistants(),numberOfAssistantsBeforeAction+1);
	}
	
	@Test
	public void testChangeBusinessPermitTiles(){
		Permit permitBeforeAction1=game.showFaceUpPermitTiles(Region.MOUNTAINS).get(0);
		Permit permitBeforeAction2=game.showFaceUpPermitTiles(Region.MOUNTAINS).get(1);
		int numberOfAssistantsBeforeAction=p3.getNumberOfAssistants();
		try {
			game.changeBusinessPermitTiles(p3, Region.MOUNTAINS);
		} catch (NotEnoughAssistantsException e) {
			fail();
		}
		Permit permitAfterAction1=game.showFaceUpPermitTiles(Region.MOUNTAINS).get(0);
		Permit permitAfterAction2=game.showFaceUpPermitTiles(Region.MOUNTAINS).get(1);
		int numberOfAssistantsAfterAction=p3.getNumberOfAssistants();
		assertEquals(game.showFaceUpPermitTiles(Region.MOUNTAINS).size(),2);
		assertNotEquals(permitAfterAction1,permitBeforeAction1);
		assertNotEquals(permitAfterAction2,permitBeforeAction2);
		assertEquals(numberOfAssistantsAfterAction,numberOfAssistantsBeforeAction-1);
	}
	

	@Test
	public void testSendAnAssistantToElectACouncillor() {
		// i've to check that the size of the available councillors doesn't
		// change
		// when a player sends an assistant to elect a councillor
		int dimBeforeAction = game.numberOfAvailableCouncillors();
		try {
			PoliticColor councillorColorChosen = game.getAvailableCouncillorColors()
					.get((int) Math.random() * (dimBeforeAction - 1));
			Councillor toPutInCouncil = game.getACouncillorByPoliticColor(councillorColorChosen);
			game.sendAnAssistantToElectACouncillor(p1, Region.COAST, toPutInCouncil);
			assertTrue(game.getCouncilByRegion(Region.COAST).contains(councillorColorChosen));
			assertEquals(dimBeforeAction, game.numberOfAvailableCouncillors());
		} catch (Exception e) {
			fail();
		}
	}
		
		
	@Test
	public void testEmporiumsInAllCitiesOfAGivenRegion() throws CouncilUnsatisfiedException{
		List<City> whereToBuild=new ArrayList<>();
		for (City city:gameMap.getCityList()){
			if(city.getCityRegion()==Region.COAST){
				whereToBuild.add(city);
			}
		}
		int maxCoins=gameMap.getMaxCoins();
		int emporiumsToBuild=whereToBuild.size();
		//Give to the player the cards he needs to build the necessary number of emporiums
		List<Politic> politicsNeeded=new ArrayList<>();
		for(int i=0;i<emporiumsToBuild;i++)
			for(int j=0;j<4;j++)
				politicsNeeded.add(new Politic(PoliticColor.MULTI_COLORED));
		
		p4.getPoliticsHand().clear();
		assertTrue(p4.getPoliticsHand().isEmpty());
		p4.getPoliticsHand().addAll(politicsNeeded);
		//Now player has only MULTI_COLORED politics card, in order to build an emporium wherever it is necessary
		assertEquals(emporiumsToBuild*4,p4.getPoliticsHand().size());
		List<Bonus> lastEmporiumBonuses=new ArrayList<>();
		while(!whereToBuild.isEmpty()){
			City currentCity=whereToBuild.get(0);
			List<Politic>politicsToUse=new ArrayList<>();
			for(int i=0;i<4;i++){
				politicsToUse.add(p4.getPoliticsHand().get(i));
			}
			try {
				p4.moveOnCoinsTrack(maxCoins, maxCoins);
			} catch (NotEnoughCoinsException e1) {
				fail();				
			}
			try {
				if(whereToBuild.size()>1){
					game.buildAnEmporiumWithTheHelpOfTheKing(p4, currentCity, politicsToUse);
					assertFalse(game.hasRegionRewardTileAlreadyBeenUsed(Region.COAST));
					//Checking that the player doesn't pick up a king Reward Tile 
					//without having built in all cities of a region
					try {
						assertEquals(1,game.peekKingRewardTopTile().getPickingOrder());
					} catch (EmptyDeckException e) {
						fail();
					}
				}
				else{
					lastEmporiumBonuses.addAll(game.buildAnEmporiumWithTheHelpOfTheKing(p4, currentCity, politicsToUse));
					assertTrue(game.hasRegionRewardTileAlreadyBeenUsed(Region.COAST));
					//Checking also the correct pickingOrder of the king Reward Tile picked from the tile when
					//a player build an emporium in all cities of a given region
					try {
						assertEquals(2,game.peekKingRewardTopTile().getPickingOrder());
					} catch (EmptyDeckException e) {
						fail();
					}
				}
			} catch (NotEnoughCoinsException e) {
				fail();
			} catch (NotEnoughAssistantsException e) {
				fail();
			} catch (CitiesNotConnectedException e) {
				fail();
			} catch (InvalidActionException e) {
				fail();	
			}
			whereToBuild.remove(currentCity);
		}
		List<Bonus> coastExpectedBonuses=gameMap.getRewardTileContentByRegion(Region.COAST).getBonuses();
		List<Bonus> kingTopTileExpectedBonuses=gameMap.getKingRewardTileContents().get(0).getBonuses();
		assertTrue(lastEmporiumBonuses.containsAll(coastExpectedBonuses));
		assertTrue(lastEmporiumBonuses.containsAll(kingTopTileExpectedBonuses));
		
	}
	
	@Test
	public void testEmporiumsInAllCitiesOfAGivenColor() throws CouncilUnsatisfiedException{
		List<City> whereToBuild=new ArrayList<>();
		for(City city:gameMap.getCityList())
			if(city.getCityColor()==CityColor.RED)
				whereToBuild.add(city);
		
		int maxCoins=gameMap.getMaxCoins();
		int emporiumsToBuild=whereToBuild.size();
		List<Politic> politicsNeeded=new ArrayList<>();
		for(int i=0;i<emporiumsToBuild;i++)
			for(int j=0;j<4;j++)
				politicsNeeded.add(new Politic(PoliticColor.MULTI_COLORED));
		
		p4.getPoliticsHand().clear();
		assertTrue(p4.getPoliticsHand().isEmpty());
		p4.getPoliticsHand().addAll(politicsNeeded);
		assertEquals(emporiumsToBuild*4,p4.getPoliticsHand().size());
		List<Bonus> lastEmporiumBonuses=new ArrayList<>();
		
		while(!whereToBuild.isEmpty()){
			City currentCity=whereToBuild.get(0);
			List<Politic>politicsToUse=new ArrayList<>();
			for(int i=0;i<4;i++){
				politicsToUse.add(p4.getPoliticsHand().get(i));
			}
			try {
				p4.moveOnCoinsTrack(maxCoins, maxCoins);
			} catch (NotEnoughCoinsException e1) {
				fail();				
			}
			try {
				if(whereToBuild.size()>1){
					game.buildAnEmporiumWithTheHelpOfTheKing(p4, currentCity, politicsToUse);
					assertFalse(game.hasColorRewardTileAlreadyBeenUsed(CityColor.RED));
					try {
						assertEquals(1,game.peekKingRewardTopTile().getPickingOrder());
					} catch (EmptyDeckException e) {
						fail();
					}
				}
				else{
					lastEmporiumBonuses.addAll(game.buildAnEmporiumWithTheHelpOfTheKing(p4, currentCity, politicsToUse));
					assertTrue(game.hasColorRewardTileAlreadyBeenUsed(CityColor.RED));
					try {
						assertEquals(2,game.peekKingRewardTopTile().getPickingOrder());
					} catch (EmptyDeckException e) {
						fail();
					}
				}
			} catch (NotEnoughCoinsException e) {
				fail();
			} catch (NotEnoughAssistantsException e) {
				fail();
			} catch (CitiesNotConnectedException e) {
				fail();
			} catch (InvalidActionException e) {
				fail();	
			}
			whereToBuild.remove(currentCity);
		}
		List<Bonus> redExpectedBonuses=gameMap.getRewardTileContentByCityColor(CityColor.RED).getBonuses();
		List<Bonus> kingTopTileExpectedBonuses=gameMap.getKingRewardTileContents().get(0).getBonuses();
		assertTrue(lastEmporiumBonuses.containsAll(redExpectedBonuses));	
		assertTrue(lastEmporiumBonuses.containsAll(kingTopTileExpectedBonuses));
		
	}
	
	@Test
	public void offerNotOwnedOwnableTest(){
		game.initializeMarketSession();
		MarketOffer offer=new MarketOffer(new Assistant(), 5);
		try {
			game.addPlayerOffer(p1, offer);
		} catch (InvalidActionException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void tryToBuyNonOfferedOwnableTest() {
		game.initializeMarketSession();
		int maxCoins=this.gameMap.getMaxCoins();
		try {
			p2.moveOnCoinsTrack(maxCoins, maxCoins);
		} catch (NotEnoughCoinsException e3) {
			fail();
		}
		try {
			game.drawAPolitic(p2);
		} catch (EmptyDeckException e1) {
			fail();
		}
		try {
			game.engageAnAssistant(p2);
		} catch (NotEnoughCoinsException e2) {
			fail();
		}
		MarketOffer offer=new MarketOffer(p2.getPoliticsHand().get(0),5);
		try {
			game.addPlayerOffer(p2, offer);
		} catch (InvalidActionException e1) {
			fail();
		}
		
		try {
			game.tryToBuy(p1, p2, p2.getAnAssistant());
		} catch (InvalidActionException e) {
			assertTrue(true);
		} catch (NotEnoughCoinsException e) {
			fail();
		} catch (NotEnoughAssistantsException e) {
			fail();
		}
	}
	
	@Test
	public void buildAnEmporiumUsingAPermitTileOnACityNotContainedInTheCurrentPermitTest(){
		int maxCoins=this.gameMap.getMaxCoins();
		try {
			p1.moveOnCoinsTrack(maxCoins, maxCoins);
		} catch (NotEnoughCoinsException e3) {
			fail();
		}
		List<Politic> politicsNeeded=new ArrayList<>();
		for(int j=0;j<4;j++)
			politicsNeeded.add(new Politic(PoliticColor.MULTI_COLORED));
		p1.getPoliticsHand().clear();
		assertTrue(p1.getPoliticsHand().isEmpty());
		p1.getPoliticsHand().addAll(politicsNeeded);
		try {
			game.acquireAPermitTile(p1, Region.COAST, 1);
		} catch (InvalidActionException e) {
			fail();
		}
		City chosenCity=gameMap.getCitiesByRegion(Region.HILLS).get(0);
		try {
			game.buildAnEmporiumUsingAPermitTile(p1, p1.getPermitsAcquired().get(0), chosenCity);
		} catch (InvalidActionException e) {
			assertTrue(true);
		} catch (NotEnoughAssistantsException e) {
			fail();
		}
	}
	
	@Test
	public void BuildAnEmporiumUsingAnAlreadyUsedPermitTest(){
		int maxCoins=this.gameMap.getMaxCoins();
		try {
			p1.moveOnCoinsTrack(maxCoins, maxCoins);
		} catch (NotEnoughCoinsException e3) {
			fail();
		}
		List<Politic> politicsNeeded=new ArrayList<>();
		for(int i=0;i<8;i++)
			politicsNeeded.add(new Politic(PoliticColor.MULTI_COLORED));
		p1.getPoliticsHand().clear();
		assertTrue(p1.getPoliticsHand().isEmpty());
		p1.getPoliticsHand().addAll(politicsNeeded);
		try {
			game.acquireAPermitTile(p1, Region.COAST, 1);
		} catch (InvalidActionException e) {
			fail();		}
		try {
			game.buildAnEmporiumUsingAPermitTile(p1, p1.getPermitsAcquired().get(0), p1.getPermitsAcquired().get(0).getCities().get(0));
		} catch (InvalidActionException e) {
			fail();
		} catch (NotEnoughAssistantsException e) {
			fail();
		}
		try {
			if(p1.getPermitsAcquired().get(0).getCities().size()>1)
				game.buildAnEmporiumUsingAPermitTile(p1, p1.getPermitsAcquired().get(0), p1.getPermitsAcquired().get(0).getCities().get(1));
			else
				game.buildAnEmporiumUsingAPermitTile(p1, p1.getPermitsAcquired().get(0), p1.getPermitsAcquired().get(0).getCities().get(0));
		} catch (InvalidActionException e) {
			assertTrue(true);
		} catch (NotEnoughAssistantsException e) {
			fail();
		}
		
	}
	
	@Test
	public void BuildAnEmporiumUsingANonOwnedPermitTest(){
		int maxCoins=this.gameMap.getMaxCoins();
		try {
			p1.moveOnCoinsTrack(maxCoins, maxCoins);
		} catch (NotEnoughCoinsException e3) {
			fail();
		}
		List<Politic> politicsNeeded=new ArrayList<>();
		for(int i=0;i<8;i++)
			politicsNeeded.add(new Politic(PoliticColor.MULTI_COLORED));
		p1.getPoliticsHand().clear();
		assertTrue(p1.getPoliticsHand().isEmpty());
		p1.getPoliticsHand().addAll(politicsNeeded);
		try {
			game.acquireAPermitTile(p1, Region.COAST, 1);
		} catch (InvalidActionException e) {
			fail();		
		}
		try {
			game.buildAnEmporiumUsingAPermitTile(p2, p1.getPermitsAcquired().get(0), p1.getPermitsAcquired().get(0).getCities().get(0));
		} catch (InvalidActionException e) {
			assertTrue(true);
		} catch (NotEnoughAssistantsException e) {
			fail();
		}
	}
	
	@Test
	public void BuildAnEmporiumWithTheHelpOfTheKingWithoutEnoughAssistantsTest(){
		int maxCoins=this.gameMap.getMaxCoins();
		try {
			p1.moveOnCoinsTrack(maxCoins, maxCoins);
		} catch (NotEnoughCoinsException e3) {
			fail();
		}
		try {
			p2.moveOnCoinsTrack(maxCoins, maxCoins);
		} catch (NotEnoughCoinsException e3) {
			fail();
		}
		List<Politic> politicsNeeded=new ArrayList<>();
		for(int i=0;i<4;i++)
			politicsNeeded.add(new Politic(PoliticColor.MULTI_COLORED));
		p1.getPoliticsHand().clear();
		p2.getPoliticsHand().clear();
		assertTrue(p1.getPoliticsHand().isEmpty());
		assertTrue(p2.getPoliticsHand().isEmpty());
		p1.getPoliticsHand().addAll(politicsNeeded);
		p2.getPoliticsHand().addAll(politicsNeeded);
		List<Politic> politicsToUse1=new ArrayList<>();
		politicsToUse1.addAll(p1.getPoliticsHand());
		List<Politic> politicsToUse2=new ArrayList<>();
		politicsToUse2.addAll(p2.getPoliticsHand());
		try {
			game.buildAnEmporiumWithTheHelpOfTheKing(p1, gameMap.getStartingKingLocation(), politicsToUse1);
		} catch (NotEnoughCoinsException e) {
			fail();
		} catch (NotEnoughAssistantsException e) {
			fail();
		} catch (CitiesNotConnectedException e) {
			fail();
		} catch (InvalidActionException e) {
			fail();
		} catch (CouncilUnsatisfiedException e) {
			fail();
		}
		try {
			game.returnNAssistantsToThePool(p2.getNAssistants(p2.getNumberOfAssistants()));
		} catch (NotEnoughAssistantsException e1) {
			fail();
		}
		try {
			game.buildAnEmporiumWithTheHelpOfTheKing(p2, gameMap.getStartingKingLocation(), politicsToUse2);
		} catch (NotEnoughCoinsException e) {
			fail();
		} catch (NotEnoughAssistantsException e) {
			assertTrue(true);
		} catch (CitiesNotConnectedException e) {
			fail();
		} catch (InvalidActionException e) {
			fail();
		} catch (CouncilUnsatisfiedException e) {
			fail();
		}
	}
	
	@Test
	public void AcquirePermitWithOutOfBoundIndexTest(){
		int maxCoins=this.gameMap.getMaxCoins();
		try {
			p1.moveOnCoinsTrack(maxCoins, maxCoins);
		} catch (NotEnoughCoinsException e3) {
			fail();
		}
		List<Politic> politicsNeeded=new ArrayList<>();
		for(int i=0;i<4;i++)
			politicsNeeded.add(new Politic(PoliticColor.MULTI_COLORED));
		p1.getPoliticsHand().clear();
		assertTrue(p1.getPoliticsHand().isEmpty());
		p1.getPoliticsHand().addAll(politicsNeeded);

		try {
			game.acquireAPermitTile(p1, Region.COAST, 0);
		} catch (InvalidActionException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void getCouncillorByPoliticColorFailureTest(){
		try {
			game.getACouncillorByPoliticColor(PoliticColor.MULTI_COLORED);
		} catch (InvalidActionException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void buildAnEmporiumOnACityWhereThePlayerHasAlreadyBuiltTest(){
		int maxCoins=this.gameMap.getMaxCoins();
		try {
			p1.moveOnCoinsTrack(maxCoins, maxCoins);
		} catch (NotEnoughCoinsException e3) {
			fail();
		}
		List<Politic> politicsNeeded=new ArrayList<>();
		for(int i=0;i<8;i++)
			politicsNeeded.add(new Politic(PoliticColor.MULTI_COLORED));
		p1.getPoliticsHand().clear();
		assertTrue(p1.getPoliticsHand().isEmpty());
		p1.getPoliticsHand().addAll(politicsNeeded);
		List<Politic> politicsToUse1=new ArrayList<>();
		politicsToUse1.addAll(p1.getPoliticsHand().subList(0, 4));
		List<Politic> politicsToUse2=new ArrayList<>();
		politicsToUse2.addAll(p1.getPoliticsHand().subList(4, 8));
		try {
			game.buildAnEmporiumWithTheHelpOfTheKing(p1, gameMap.getStartingKingLocation(), politicsToUse1);
		} catch (NotEnoughCoinsException e) {
			fail();
		} catch (NotEnoughAssistantsException e) {
			fail();
		} catch (CitiesNotConnectedException e) {
			fail();
		} catch (InvalidActionException e) {
			fail();
		} catch (CouncilUnsatisfiedException e) {
			fail();
		}
		try {
			p1.moveOnCoinsTrack(maxCoins, maxCoins);
		} catch (NotEnoughCoinsException e3) {
			fail();
		}
		try {
			game.buildAnEmporiumWithTheHelpOfTheKing(p1, gameMap.getStartingKingLocation(), politicsToUse2);
		} catch (NotEnoughCoinsException e) {
			fail();
		} catch (NotEnoughAssistantsException e) {
			fail();
		} catch (CitiesNotConnectedException e) {
			fail();
		} catch (InvalidActionException e) {
			assertTrue(true);
		} catch (CouncilUnsatisfiedException e) {
			fail();
		}
	}
	
	@Test
	public void buildAnEmporiumWithoutPolitics(){
		int maxCoins=this.gameMap.getMaxCoins();
		try {
			p1.moveOnCoinsTrack(maxCoins, maxCoins);
		} catch (NotEnoughCoinsException e3) {
			fail();
		}
		p1.getPoliticsHand().clear();
		assertTrue(p1.getPoliticsHand().isEmpty());
		List<Politic> politicsToUse=new ArrayList<>();
		politicsToUse.addAll(p1.getPoliticsHand());
		try {
			game.buildAnEmporiumWithTheHelpOfTheKing(p1, gameMap.getStartingKingLocation(), politicsToUse);
		} catch (NotEnoughCoinsException e) {
			fail();
		} catch (NotEnoughAssistantsException e) {
			fail();
		} catch (CitiesNotConnectedException e) {
			fail();
		} catch (InvalidActionException e) {
			assertTrue(true);
		} catch (CouncilUnsatisfiedException e) {
			fail();
		}
	}
	
	@Test
	public void councilUnsatisfiedTest(){
		while(game.getCouncilByRegion(Region.KING).contains(PoliticColor.BLACK)){
			PoliticColor color=null;
			while(color==null){
				for(PoliticColor politicColor:PoliticColor.values()){
					if(politicColor!=PoliticColor.BLACK)
						if(game.getAvailableCouncillorColors().contains(politicColor)){
							color=politicColor;
							break;
						}
				}
			}
			try {
				game.electACouncillor(p1, Region.KING, game.getACouncillorByPoliticColor(color));
			} catch (InvalidActionException e) {
				fail();			}
			color=null;
		}
		assertFalse(game.getCouncilByRegion(Region.KING).contains(PoliticColor.BLACK));
		//Here i haven't any black councillor on King balcony
		int maxCoins=this.gameMap.getMaxCoins();
		try {
			p1.moveOnCoinsTrack(maxCoins, maxCoins);
		} catch (NotEnoughCoinsException e3) {
			fail();
		}
		p1.getPoliticsHand().clear();
		p1.getPoliticsHand().add(new Politic(PoliticColor.BLACK));
		p1.getPoliticsHand().add(new Politic(PoliticColor.MULTI_COLORED));
		List<Politic> politicsToUse=new ArrayList<>();
		politicsToUse.addAll(p1.getPoliticsHand());
		assertEquals(2,politicsToUse.size());
		try {
			game.buildAnEmporiumWithTheHelpOfTheKing(p1, gameMap.getStartingKingLocation(), politicsToUse);
		} catch (NotEnoughCoinsException e) {
			fail();
		} catch (NotEnoughAssistantsException e) {
			fail();
		} catch (CitiesNotConnectedException e) {
			fail();
		} catch (InvalidActionException e) {
			fail();
		} catch (CouncilUnsatisfiedException e) {
			assertTrue(true);
		}
		
	}	
	
}
