package it.polimi.ingsw.lm_belloni_cof.test.controller.actions;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.BonusResponseAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.BuildAnEmporiumWithTheHelpOfTheKingAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.CityTokenResponseAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.DrawAPoliticAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.EmptyQuickAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.MainAction;
import it.polimi.ingsw.lm_belloni_cof.controller.states.TurnMachine;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.GameFinishedException;
import it.polimi.ingsw.lm_belloni_cof.model.Bonus;
import it.polimi.ingsw.lm_belloni_cof.model.BonusType;
import it.polimi.ingsw.lm_belloni_cof.model.City;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMap;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Politic;
import it.polimi.ingsw.lm_belloni_cof.model.PoliticColor;
import it.polimi.ingsw.lm_belloni_cof.model.RewardToken;

public class CityTokenResponseActionTest {
	private Game game;
	private GameMap gameMap;
	private Player p1;
	private TurnMachine turnMachine;
	private City chosenCity;

	@Before
	public void initialize() throws GameFinishedException {
		List<String> usernames = Arrays.asList("P1", "P2", "P3");
		this.gameMap = new GameMapFactory().getGameMap("default");
		this.game = new Game(this.gameMap, usernames);
		this.turnMachine = new TurnMachine(this.game);
		this.turnMachine.changeState("P1", new DrawAPoliticAction(null, null));
		// i've to build an emporium on a city
		this.p1 = this.game.getPlayerByUsername("P1");
		List<City> cityList = this.game.getCityList();
		City cityToBuildOn = null;
		for (City city : cityList)
			if (this.gameMap.areCitiesDirectlyConnected(city, this.game.getKingsLocation())) {
				List<RewardToken> tokens = this.game.getRewardTokensOn(city);
				boolean nobility = false;
				for(RewardToken token:tokens){
					for(Bonus bonus:token.getBonuses()){
						if(bonus.getBonusType() == BonusType.NOBILITY){
							nobility = true;
							break;
						}
					}
					if(nobility == true)
						break;
				}
				if(!nobility){
					cityToBuildOn = city;
					break;
				}
			}
		List<Politic> multiColoredToUse = new ArrayList<>();
		for (int i = 0; i < 4; i++) {
			Politic cheat = new Politic(PoliticColor.MULTI_COLORED);
			p1.giveAPolitic(cheat);
			multiColoredToUse.add(cheat);
		}
		MainAction buildAnEmpKing = new BuildAnEmporiumWithTheHelpOfTheKingAction(game, p1, cityToBuildOn,
				multiColoredToUse, turnMachine);
		try {
			buildAnEmpKing.execute();
		} catch (ActionExecutionException e) {
			fail();
		}
		this.chosenCity = cityToBuildOn;
		// now p1 has an emporium on chosen city
		// .. gameflow ...
		assertEquals(this.game.getCurrentPlayer().getUsername(), "P1");
		this.turnMachine.askForCityTokenResponseWaitingState(1);
	}

	@Test
	public void test() throws GameFinishedException {
		// player is in CityTokenResponseWaitingState
		assertFalse(this.turnMachine.getState("P1").isAllowedToExecute(new EmptyQuickAction(null, null)));
		BonusResponseAction cityTokenResponse = new CityTokenResponseAction(this.game, this.p1, this.chosenCity,
				this.turnMachine);
		try {
			cityTokenResponse.execute();
		} catch (ActionExecutionException e) {
			fail();
			e.printStackTrace();
		}
		this.turnMachine.changeState(p1.getUsername(), cityTokenResponse);
		assertTrue(this.turnMachine.getState("P1").isAllowedToExecute(new EmptyQuickAction(null, null)));
		assertEquals(cityTokenResponse.getActionExecutedMessage().getCityTokenResponsesAdded(),-1);

	}

}
