package it.polimi.ingsw.lm_belloni_cof.test.controller.actions;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.AcquireABusinessPermitTileAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.MainAction;
import it.polimi.ingsw.lm_belloni_cof.controller.states.TurnMachine;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.model.Assistant;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.Permit;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Politic;
import it.polimi.ingsw.lm_belloni_cof.model.PoliticColor;
import it.polimi.ingsw.lm_belloni_cof.model.Region;

public class AcquireABusinessPermitTileActionTest {
	private Game game;
	private TurnMachine turnMachine;

	@Before
	public void initialize() {
		List<String> usernames = Arrays.asList("P1", "P2");
		this.game = new Game(new GameMapFactory().getGameMap("default"), usernames);
		this.turnMachine = new TurnMachine(this.game);

	}

	@Test
	public void test() {
		Player p1 = this.game.getPlayerByUsername("P1");
		p1.giveAnAssistant(new Assistant());
		Permit toAcquire = this.game.showFaceUpPermitTiles(Region.COAST).get(0);
		assertFalse(toAcquire.hasAnOwner());
		List<Politic> multiColoredToUse = new ArrayList<>();
		for (int i = 0; i < 4; i++) {
			Politic cheat = new Politic(PoliticColor.MULTI_COLORED);
			p1.giveAPolitic(cheat);
			multiColoredToUse.add(cheat);
		}
		MainAction acquireATile = new AcquireABusinessPermitTileAction(this.game, p1, Region.COAST, 1,
				multiColoredToUse, this.turnMachine);
		try {
			acquireATile.execute();
		} catch (ActionExecutionException e) {
			fail();
		}
		assertTrue(acquireATile.hasBeenExecuted());
		assertNotEquals(acquireATile.getActionDescription(), "");
		for (int i = 0; i < 4; i++)
			assertNull(multiColoredToUse.get(i).getOwner());
		assertEquals(toAcquire.getOwner(), p1);
	}

}
