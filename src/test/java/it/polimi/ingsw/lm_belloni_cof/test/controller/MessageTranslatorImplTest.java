package it.polimi.ingsw.lm_belloni_cof.test.controller;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.MessageTranslator;
import it.polimi.ingsw.lm_belloni_cof.controller.MessageTranslatorImpl;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.DrawAPoliticAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.ElectACouncillorAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.MainAction;
import it.polimi.ingsw.lm_belloni_cof.controller.states.TurnMachine;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.CitiesNotConnectedException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.GameFinishedException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidMessageException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughAssistantsException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughCoinsException;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.*;
import it.polimi.ingsw.lm_belloni_cof.model.Assistant;
import it.polimi.ingsw.lm_belloni_cof.model.Bonus;
import it.polimi.ingsw.lm_belloni_cof.model.BonusType;
import it.polimi.ingsw.lm_belloni_cof.model.City;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMap;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.Permit;
import it.polimi.ingsw.lm_belloni_cof.model.PermitContent;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Politic;
import it.polimi.ingsw.lm_belloni_cof.model.PoliticColor;
import it.polimi.ingsw.lm_belloni_cof.model.Region;
import it.polimi.ingsw.lm_belloni_cof.model.RewardToken;

public class MessageTranslatorImplTest {
	private Game game;
	private GameMap gameMap;
	private TurnMachine turn;
	private MessageTranslator translator;
	private PlayerToken token;
	private Player p1;

	@Before
	public void initialize() throws GameFinishedException {
		List<String> usernames = Arrays.asList("P1", "P2", "P3");
		this.gameMap = new GameMapFactory().getGameMap("default");
		this.game = new Game(this.gameMap, usernames);
		this.turn = new TurnMachine(game);
		this.translator = new MessageTranslatorImpl(this.game, this.turn);
		this.token = new PlayerToken("P1");
		this.p1 = this.game.getPlayerByUsername("P1");
		// ..
		DrawAPoliticAction fakeDraw = new DrawAPoliticAction(null, null);
		this.turn.changeState("P1", fakeDraw);
	}
	
	@Test
	public void testIncorrectUsername(){
		AcquireABusinessPermitTileActRqstMsg msg = new AcquireABusinessPermitTileActRqstMsg(new PlayerToken("fake"),
				"COAST", 2, Arrays.asList("multi_colored"));
		try{
			msg.getAction(this.translator);
		} catch(InvalidMessageException e){
			assertNotNull(e);
			assertEquals(e.getMessage(),"fake is not a correct username.");
		}
	}
	
	@Test
	public void testIncorrectRegion(){
		AcquireABusinessPermitTileActRqstMsg msg = new AcquireABusinessPermitTileActRqstMsg(this.token,
				"fake", 2, Arrays.asList("multi_colored"));
		try{
			msg.getAction(this.translator);
		} catch(InvalidMessageException e){
			assertNotNull(e);
			assertEquals(e.getMessage(),"fake is not a known region.");
		}
	}
	
	@Test
	public void testIncorrectPolitics(){
		AcquireABusinessPermitTileActRqstMsg msg = new AcquireABusinessPermitTileActRqstMsg(this.token,
				"coast", 2, Arrays.asList());
		try{
			msg.getAction(this.translator);
			fail();
		} catch(InvalidMessageException e){
			assertNotNull(e);
			assertEquals(e.getMessage(),"You must specify from 1 to 4 politics.");
		}
	}

	@Test
	public void testAcquireABusinessPermitTileAction() {
		try {
			p1.moveOnCoinsTrack(5, this.gameMap.getMaxCoins()); // CHEAT
		} catch (NotEnoughCoinsException e1) {
			fail();
		}
		int beforeCoins = p1.getCoins();
		Politic multi = new Politic(PoliticColor.MULTI_COLORED); // CHEAT
		p1.getPoliticsHand().add(multi);
		Region region = Region.COAST;
		List<Permit> permits = this.game.showFaceUpPermitTiles(region);
		Permit notOwnedYet = permits.get(1);
		assertFalse(notOwnedYet.hasAnOwner());
		AcquireABusinessPermitTileActRqstMsg msg = new AcquireABusinessPermitTileActRqstMsg(this.token,
				region.toString(), 2, Arrays.asList("multi_colored"));
		Action action = null;
		try {
			action = msg.getAction(translator);
		} catch (InvalidMessageException e) {
			fail(e.getMessage());
		}
		try {
			action.execute();
		} catch (ActionExecutionException e) {
			fail(e.getMessage());
		}
		assertEquals(notOwnedYet.getOwner(), p1);
		int coinsBonus = 0;
		for (Bonus b : notOwnedYet.getBonuses())
			if (b.getBonusType() == BonusType.COIN)
				coinsBonus += b.getNumberOfTimes();
			else if (b.getBonusType() == BonusType.NOBILITY && b.getNumberOfTimes() == 2)
				coinsBonus += 1;
		assertEquals(p1.getCoins(), beforeCoins - 1 - 10 + coinsBonus);
	}

	@Test
	public void testWrongPermit(){
		BuildAnEmporiumUsingAPermitTileActRqstMsg msg = new BuildAnEmporiumUsingAPermitTileActRqstMsg(this.token,
				"arkon", "29");
		try{
			msg.getAction(this.translator);
			fail();
		} catch(InvalidMessageException e){
			assertNotNull(e);
			assertEquals(e.getMessage(),msg.getPermit()+" is not a correct permit.");
		}
	}
	
	@Test
	public void testBuildAnEmporiumUsingAPermitTileAction() {
		// OBTAINING A PERMIT TILE USING MESSAGE
		try {
			p1.moveOnCoinsTrack(5, this.gameMap.getMaxCoins()); // CHEAT
		} catch (NotEnoughCoinsException e1) {
			fail();
		}
		Politic multi = new Politic(PoliticColor.MULTI_COLORED); // CHEAT
		p1.getPoliticsHand().add(multi);
		Region region = Region.COAST;
		List<Permit> permits = this.game.showFaceUpPermitTiles(region);
		Permit notOwnedYet = permits.get(1);
		AcquireABusinessPermitTileActRqstMsg msg = new AcquireABusinessPermitTileActRqstMsg(this.token,
				region.toString(), 2, Arrays.asList("multi_colored"));
		Action action = null;
		try {
			action = msg.getAction(translator);
		} catch (InvalidMessageException e) {
			fail(e.getMessage());
		}
		try {
			action.execute();
		} catch (ActionExecutionException e) {
			fail(e.getMessage());
		}
		// USING IT TO BUILD
		Permit owned = notOwnedYet;
		City city = owned.getCities().get(0);
		assertFalse(this.game.getPlayersWithAnEmporiumOn(city).contains(p1));
		String cityName = city.getName().toLowerCase();
		BuildAnEmporiumUsingAPermitTileActRqstMsg msg2 = new BuildAnEmporiumUsingAPermitTileActRqstMsg(this.token,
				cityName, "1");
		Action action2 = null;
		try {
			action2 = msg2.getAction(translator);
		} catch (InvalidMessageException e) {
			fail(e.getMessage());
		}
		try {
			action2.execute();
		} catch (ActionExecutionException e) {
			fail(e.getMessage());
		}
		assertTrue(this.game.getPlayersWithAnEmporiumOn(city).contains(p1));

	}

	@Test
	public void testBuildAnEmporiumWithTheHelpOfTheKingAction() {
		int beforeCoins = p1.getCoins();
		// cheat -> p1 won't pay anything to satisfy the council
		List<PoliticColor> kingCouncil = game.getCouncilByRegion(Region.KING);
		for (PoliticColor color : kingCouncil)
			p1.giveAPolitic(new Politic(color));
		// looking for a city 3 steps far from king's location
		City chosenCity = null;
		for (City city : this.game.getCityList())
			try {
				if (this.gameMap.getRoadsBetweenCities(this.game.getKingsLocation(), city) == 3) {
					chosenCity = city;
					break;
				}
			} catch (CitiesNotConnectedException e) {
				continue;
			}
		assertFalse(game.getPlayersWithAnEmporiumOn(chosenCity).contains(p1));
		List<String> colorStrings = new ArrayList<>();
		for (PoliticColor color : kingCouncil)
			colorStrings.add(color.toString().toLowerCase());
		BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg msg = new BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg(
				this.token, chosenCity.getName().toLowerCase(), colorStrings);
		try {
			Action action = msg.getAction(translator);
			action.execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		int coinsBonus = 0;
		for (RewardToken r : game.getRewardTokensOn(chosenCity))
			for (Bonus b : r.getBonuses())
				if (b.getBonusType() == BonusType.COIN)
					coinsBonus += b.getNumberOfTimes();
				else if (b.getBonusType() == BonusType.NOBILITY && b.getNumberOfTimes() == 2)
					coinsBonus += 1;
		assertTrue(game.getPlayersWithAnEmporiumOn(chosenCity).contains(p1));
		assertEquals(p1.getCoins(), beforeCoins - 6 + coinsBonus);
	}

	@Test
	public void testChangeBusinessPermitTilesAction() {
		Region region = Region.MOUNTAINS;
		List<Permit> faceUps = game.showFaceUpPermitTiles(region);
		ChangeBusinessPermitTilesActRqstMsg msg = new ChangeBusinessPermitTilesActRqstMsg(token,
				region.toString().toLowerCase().toString());
		try {
			Action action = msg.getAction(translator);
			action.execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertFalse(faceUps.containsAll(game.showFaceUpPermitTiles(region)));
		for (Permit p : game.showFaceUpPermitTiles(region))
			assertFalse(faceUps.contains(p));
	}
	
	@Test
	public void testChangeBusinessPermitTilesKINGAction() {
		Region region = Region.KING;
		ChangeBusinessPermitTilesActRqstMsg msg = new ChangeBusinessPermitTilesActRqstMsg(token,
				region.toString().toLowerCase().toString());
		try {
			msg.getAction(translator);
			fail();
		} catch (Exception e) {
			assertNotNull(e);
			assertEquals(e.getMessage(),"King region has no permit tiles on it.");
		}
	}

	@Test
	public void testCityTokenResponseActionOk() {
		// i've to build an emporium on the city before collecting the bonuses
		// WITH THE HELP OF THE KING
		// cheat -> p1 won't pay anything to satisfy the council
		List<PoliticColor> kingCouncil = game.getCouncilByRegion(Region.KING);
		for (PoliticColor color : kingCouncil)
			p1.giveAPolitic(new Politic(color));
		// looking for a city connected to king's location and with no nobility
		// on it
		City chosenCity = null;
		for (City city : this.game.getCityList())
			try {
				if (this.gameMap.getRoadsBetweenCities(this.game.getKingsLocation(), city) >= 1) {
					boolean nobility = false;
					for (RewardToken r : game.getRewardTokensOn(city))
						for (Bonus b : r.getBonuses()) {
							if (b.getBonusType() == BonusType.NOBILITY)
								nobility = true;
						}
					if (!nobility) {
						chosenCity = city;
						break;
					}
				}
			} catch (CitiesNotConnectedException e) {
				continue;
			}

		List<String> colorStrings = new ArrayList<>();
		for (PoliticColor color : kingCouncil)
			colorStrings.add(color.toString().toLowerCase());
		BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg msg = new BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg(
				this.token, chosenCity.getName().toLowerCase(), colorStrings);
		try {
			Action action = msg.getAction(translator);
			action.execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		// p1 has a city on chosenCity
		int beforeCoins = p1.getCoins();
		int beforeAssistants = p1.getNumberOfAssistants();
		int beforePolitics = p1.getPoliticsHand().size();
		int beforeVictories = p1.getVictoryPoints();
		int beforeNobility = p1.getNobilityTrackPosition();
		CityTokenResponseActRqstMsg msg1 = new CityTokenResponseActRqstMsg(token, chosenCity.getName().toLowerCase());
		try {
			Action action1 = msg1.getAction(translator);
			action1.execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertTrue(p1.getCoins() != beforeCoins || p1.getNumberOfAssistants() != beforeAssistants
				|| p1.getPoliticsHand().size() != beforePolitics || p1.getVictoryPoints() != beforeVictories
				|| beforeNobility != p1.getNobilityTrackPosition());
	}

	@Test
	public void testCityTokenResponseActionNoEmporium() {
		City city = this.game.getCityList().get(0);
		CityTokenResponseActRqstMsg msg = new CityTokenResponseActRqstMsg(token, city.getName().toLowerCase());
		try {
			msg.getAction(translator);
			fail("It is supposed to throw an exception.");
		} catch (InvalidMessageException e) {
			assertEquals(e.getMessage(),
					"You must have an emporium on " + msg.getCity() + " in order to collect the bonuses on it.");
		}
	}

	@Test
	public void testDrawAPoliticAction() {
		List<Politic> before = new ArrayList<>(p1.getPoliticsHand());
		DrawAPoliticActRqstMsg msg = new DrawAPoliticActRqstMsg(this.token);
		try {
			Action action = msg.getAction(translator);
			action.execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertEquals(before.size() + 1, p1.getPoliticsHand().size());
		assertTrue(p1.getPoliticsHand().containsAll(before));
	}

	@Test
	public void testElectACouncillorAction() {
		Region region = Region.HILLS;
		PoliticColor councillorColor = game.getAvailableCouncillorColors().get(0);
		ElectACouncillorActRqstMsg msg = new ElectACouncillorActRqstMsg(token, region.toString().toLowerCase(),
				councillorColor.toString().toLowerCase());
		Action action = null;
		try {
			action = msg.getAction(translator);
			action.execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertTrue(action.hasBeenExecuted());
		assertTrue(game.getCouncilByRegion(region).contains(councillorColor));
	}
	
	@Test
	public void testElectACouncillorWrongColor() {
		Region region = Region.HILLS;
		ElectACouncillorActRqstMsg msg = new ElectACouncillorActRqstMsg(token, region.toString().toLowerCase(),
				"red");
		try {
			msg.getAction(translator);
			fail();
		} catch (Exception e) {
			assertNotNull(e);
			assertEquals(e.getMessage(),"red is not a known color.");
		}
	}

	@Test
	public void testEmptyQuickAction() {
		EmptyQuickActionActRqstMsg msg = new EmptyQuickActionActRqstMsg(token);
		try {
			Action a = msg.getAction(translator);
			a.execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void testEngageAnAssistantAction() {
		int beforeCoins = p1.getCoins();
		int beforeAssistants = p1.getNumberOfAssistants();
		EngageAnAssistantActRqstMsg msg = new EngageAnAssistantActRqstMsg(token);
		try {
			Action action = msg.getAction(translator);
			action.execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertEquals(p1.getCoins(), beforeCoins - 3);
		assertEquals(beforeAssistants, p1.getNumberOfAssistants() - 1);
	}

	@Test
	public void testFaceUpPermitResponseAction() {
		Region region = Region.HILLS;
		Permit notOwned = game.showFaceUpPermitTiles(region).get(0);
		assertFalse(notOwned.hasAnOwner());
		FaceUpPermitResponseActRqstMsg msg = new FaceUpPermitResponseActRqstMsg(token, region.toString().toLowerCase(),
				1);
		try {
			Action action = msg.getAction(translator);
			action.execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertTrue(notOwned.hasAnOwner());
		assertEquals(notOwned.getOwner(), p1);
	}
	
	@Test
	public void testFaceUpPermitWrongResponseAction() {
		Region region = Region.HILLS;
		FaceUpPermitResponseActRqstMsg msg = new FaceUpPermitResponseActRqstMsg(token, region.toString().toLowerCase(),
				3);
		try {
			msg.getAction(translator);
			fail();
		} catch (Exception e) {
			assertNotNull(e);
			assertEquals(e.getMessage(),"indexInRegion must be 1 or 2.");
		}
	}
	
	@Test
	public void testOwnedPermitWrongIndex() {
		OwnedPermitResponseActRqstMsg msg = new OwnedPermitResponseActRqstMsg(token, "a");
		try {
			msg.getAction(translator);
			fail();
		} catch (Exception e) {
			assertNotNull(e);
			assertEquals(e.getMessage(),msg.getPermit()+" is not a correct permit.");
		}

	}
	
	@Test
	public void testOwnedPermitNonOwned() {
		OwnedPermitResponseActRqstMsg msg = new OwnedPermitResponseActRqstMsg(token, "1");
		try {
			msg.getAction(translator);
			fail();
		} catch (Exception e) {
			assertNotNull(e);
			assertEquals(e.getMessage(),msg.getPermit()+" is not a correct permit.");
		}

	}

	@Test
	public void testOwnedPermitResponseAction() {
		// I'VE TO ACQUIRE A PERMIT
		Region region = Region.HILLS;
		FaceUpPermitResponseActRqstMsg msg = new FaceUpPermitResponseActRqstMsg(token, region.toString().toLowerCase(),
				1);
		try {
			Action action = msg.getAction(translator);
			action.execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		// Permit owned = notOwned;
		// NOW I CAN USE IT TO COLLECT ITS BONUSES AGAIN
		// p1 has a city on chosenCity
		int beforeCoins = p1.getCoins();
		int beforeAssistants = p1.getNumberOfAssistants();
		int beforePolitics = p1.getPoliticsHand().size();
		int beforeVictories = p1.getVictoryPoints();
		int beforeNobility = p1.getNobilityTrackPosition();
		OwnedPermitResponseActRqstMsg msg1 = new OwnedPermitResponseActRqstMsg(token, "1");
		try {
			Action action1 = msg1.getAction(translator);
			action1.execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertTrue(p1.getCoins() != beforeCoins || p1.getNumberOfAssistants() != beforeAssistants
				|| p1.getPoliticsHand().size() != beforePolitics || p1.getVictoryPoints() != beforeVictories
				|| beforeNobility != p1.getNobilityTrackPosition());

	}

	@Test
	public void testPerformAnAdditionalMainActionAction() throws GameFinishedException {
		/* cheat */
		this.p1.giveNAssistants(Arrays.asList(new Assistant(),new Assistant(),new Assistant()));
		// perform a fake main action
		MainAction main = new ElectACouncillorAction(null, null, null, null);
		assertTrue(turn.getState("P1").isAllowedToExecute(main));
		this.turn.changeState("P1", main);
		assertFalse(turn.getState("P1").isAllowedToExecute(main));
		PerformAnAdditionalMainActionActRqstMsg msg = new PerformAnAdditionalMainActionActRqstMsg(token);
		Action action = null;
		try {
			action = msg.getAction(translator);
			action.execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		this.turn.changeState("P1", action);
		assertTrue(turn.getState("P1").isAllowedToExecute(main));
	}

	@Test
	public void testSendAnAssistantToElectACouncillorAction() {
		int beforeAssistants = p1.getNumberOfAssistants();
		Region region = Region.HILLS;
		PoliticColor councillorColor = game.getAvailableCouncillorColors().get(0);
		SendAnAssistantToElectACouncillorActRqstMsg msg = new SendAnAssistantToElectACouncillorActRqstMsg(token,
				region.toString().toLowerCase(), councillorColor.toString().toLowerCase());
		try {
			Action action = msg.getAction(translator);
			action.execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertEquals(p1.getNumberOfAssistants() + 1, beforeAssistants);
		assertTrue(game.getCouncilByRegion(region).contains(councillorColor));
	}

	@Test
	public void testMarketOfferPoliticAction() {
		// cheat
		this.game.initializeMarketSession();
		assertTrue(this.game.getOffersByPlayer(p1).isEmpty());
		p1.giveAPolitic(new Politic(PoliticColor.BLACK));
		MarketOfferActRqstMsg msg = new MarketOfferActRqstMsg(token, "politic black", 5);
		try {
			msg.getAction(translator).execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertFalse(this.game.getOffersByPlayer(p1).isEmpty());
		assertTrue(((Politic) this.game.getOffersByPlayer(p1).get(0).getObjectForSale())
				.getPoliticColor() == PoliticColor.BLACK);
		assertTrue(this.game.getOffersByPlayer(p1).get(0).getPrice() == 5);
	}
	
	@Test
	public void testMarketOfferPoliticNonWellFormed() {
		// cheat
		this.game.initializeMarketSession();
		assertTrue(this.game.getOffersByPlayer(p1).isEmpty());
		p1.giveAPolitic(new Politic(PoliticColor.BLACK));
		MarketOfferActRqstMsg msg = new MarketOfferActRqstMsg(token, "politic", 5);
		try {
			msg.getAction(translator);
			fail();
		} catch (Exception e) {
			assertNotNull(e);
			assertEquals(e.getMessage(),"politic is not a well formed politic card.");
		}
	}
	
	@Test
	public void testMarketOfferPoliticWrongColor() {
		// cheat
		this.game.initializeMarketSession();
		assertTrue(this.game.getOffersByPlayer(p1).isEmpty());
		p1.giveAPolitic(new Politic(PoliticColor.BLACK));
		MarketOfferActRqstMsg msg = new MarketOfferActRqstMsg(token, "politic red", 5);
		try {
			msg.getAction(translator);
			fail();
		} catch (Exception e) {
			assertNotNull(e);
			assertEquals(e.getMessage(),"red is not a known color.");
		}
	}
	
	@Test
	public void testMarketOfferAssistantAction() {
		// cheat
		this.game.initializeMarketSession();
		assertTrue(this.game.getOffersByPlayer(p1).isEmpty());
		p1.giveAnAssistant(new Assistant());
		MarketOfferActRqstMsg msg = new MarketOfferActRqstMsg(token, "assistant", 5);
		try {
			msg.getAction(translator).execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertFalse(this.game.getOffersByPlayer(p1).isEmpty());
		assertTrue(this.game.getOffersByPlayer(p1).get(0).getObjectForSale() instanceof Assistant);
		assertTrue(this.game.getOffersByPlayer(p1).get(0).getPrice() == 5);
	}
	
	@Test
	public void testMarketOfferNotEnoughAssistant() {
		// cheat
		this.game.initializeMarketSession();
		assertTrue(this.game.getOffersByPlayer(p1).isEmpty());
		try {
			p1.getAnAssistant();
		} catch (NotEnoughAssistantsException e1) {
			fail("Surely has one");
		}
		MarketOfferActRqstMsg msg = new MarketOfferActRqstMsg(token, "assistant", 5);
		try {
			msg.getAction(translator);
			fail();
		} catch (Exception e) {
			assertNotNull(e);
			assertEquals(e.getMessage(),"You don't have enough assistants.");
		}
	}
	
	@Test
	public void testMarketOfferNegativePriceAction() {
		// cheat
		this.game.initializeMarketSession();
		assertTrue(this.game.getOffersByPlayer(p1).isEmpty());
		p1.giveAnAssistant(new Assistant());
		MarketOfferActRqstMsg msg = new MarketOfferActRqstMsg(token, "assistant", -5);
		try {
			msg.getAction(translator);
			fail();
		} catch (Exception e) {
			assertNotNull(e);
			assertEquals(e.getMessage(),"Price must be positive.");
		}
	}
	
	@Test
	public void testMarketOfferPermitAction() {
		// cheat
		this.game.initializeMarketSession();
		assertTrue(this.game.getOffersByPlayer(p1).isEmpty());
		/* creating a fake permit */
		PermitContent c = new PermitContent(Region.COAST);
		c.addCity(this.game.getCityList().get(0));
		Permit p = new Permit(c);
		p1.giveAPermit(p);
		MarketOfferActRqstMsg msg = new MarketOfferActRqstMsg(token, "permit 1", 5);
		try {
			msg.getAction(translator).execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertFalse(this.game.getOffersByPlayer(p1).isEmpty());
		assertTrue(this.game.getOffersByPlayer(p1).get(0).getObjectForSale() == p);
		assertTrue(this.game.getOffersByPlayer(p1).get(0).getPrice() == 5);
	}
	
	@Test
	public void testMarketOfferPermitNonWellFormed() {
		// cheat
		this.game.initializeMarketSession();
		MarketOfferActRqstMsg msg = new MarketOfferActRqstMsg(token, "permit a", 5);
		try {
			msg.getAction(translator);
			fail();
		} catch (Exception e) {
			assertNotNull(e);
			assertEquals(e.getMessage(),"permit a is not a well formed permit.");
		}
	}
	
	@Test
	public void testMarketOfferPermitNonOwned() {
		// cheat
		this.game.initializeMarketSession();
		MarketOfferActRqstMsg msg = new MarketOfferActRqstMsg(token, "permit 1", 5);
		try {
			msg.getAction(translator);
			fail();
		} catch (Exception e) {
			assertNotNull(e);
			assertEquals(e.getMessage(),"1 is not available in your hand.");
		}
	}
	
	@Test
	public void testMarketIncorrect() {
		// cheat
		this.game.initializeMarketSession();
		MarketOfferActRqstMsg msg = new MarketOfferActRqstMsg(token, "coins", 5);
		try {
			msg.getAction(translator);
			fail();
		} catch (Exception e) {
			assertNotNull(e);
			assertEquals(e.getMessage(),"coins is not a correct object.");
		}
	}
	
	@Test
	public void testMarketBuyAction() {
		// p1 put an object for sale
		// cheat
		this.game.initializeMarketSession();
		p1.giveAPolitic(new Politic(PoliticColor.BLACK));
		MarketOfferActRqstMsg msg = new MarketOfferActRqstMsg(token, "politic black", 5);
		try {
			msg.getAction(translator).execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		this.game.inMarketBuy();
		Player p2 = this.game.getPlayerByUsername("P2");
		PlayerToken token2 = new PlayerToken(p2.getUsername());
		int coinsBefore = p2.getCoins();
		int blackBefore = 0;
		for (Politic politic : p2.getPoliticsHand())
			if (politic.getPoliticColor() == PoliticColor.BLACK)
				blackBefore++;
		MarketBuyActRqstMsg msg1 = new MarketBuyActRqstMsg(token2, "P1", 1);
		try {
			msg1.getAction(translator).execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertEquals(p2.getCoins(), coinsBefore - 5);
		int blackAfter = 0;
		for (Politic politic : p2.getPoliticsHand())
			if (politic.getPoliticColor() == PoliticColor.BLACK)
				blackAfter++;
		assertEquals(blackAfter, blackBefore + 1);
	}

	@Test
	public void testMarketBuyActionNoSuchObj() {
		// p1 put an object for sale
		// cheat
		this.game.initializeMarketSession();
		p1.giveAPolitic(new Politic(PoliticColor.BLACK));
		MarketOfferActRqstMsg msg = new MarketOfferActRqstMsg(token, "politic black", 5);
		try {
			msg.getAction(translator).execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		this.game.inMarketBuy();
		MarketBuyActRqstMsg msg1 = new MarketBuyActRqstMsg(new PlayerToken("P2"), "P1", 3);
		try {
			msg1.getAction(translator).execute();
			fail("An exception must be thrown.");
		} catch (Exception e) {
			assertEquals(e.getMessage(),"P1 hasn't offered such object.");
		}
		MarketBuyActRqstMsg msg2 = new MarketBuyActRqstMsg(new PlayerToken("P1"), "P1", 3);
		try {
			msg2.getAction(translator).execute();
			fail("An exception must be thrown.");
		} catch (Exception e) {
			assertEquals(e.getMessage(),"You can't buy your own objects.");
		}
	}
	
	@Test
	public void testMarketBuyActionNoSeller() {
		// p1 put an object for sale
		// cheat
		this.game.initializeMarketSession();
		p1.giveAPolitic(new Politic(PoliticColor.BLACK));
		MarketOfferActRqstMsg msg = new MarketOfferActRqstMsg(token, "politic black", 5);
		try {
			msg.getAction(translator).execute();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		this.game.inMarketBuy();
		MarketBuyActRqstMsg msg1 = new MarketBuyActRqstMsg(new PlayerToken("P2"), "fake", 3);
		try {
			msg1.getAction(translator).execute();
			fail("An exception must be thrown.");
		} catch (Exception e) {
			assertNotNull(e);
			assertEquals(e.getMessage(),"fake is not a correct username.");
		}
	}
}
