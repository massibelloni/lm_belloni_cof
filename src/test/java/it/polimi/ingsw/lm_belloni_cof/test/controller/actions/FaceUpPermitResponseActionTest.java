package it.polimi.ingsw.lm_belloni_cof.test.controller.actions;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.DrawAPoliticAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.EmptyQuickAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.FaceUpPermitResponseAction;
import it.polimi.ingsw.lm_belloni_cof.controller.states.TurnMachine;

import it.polimi.ingsw.lm_belloni_cof.exceptions.GameFinishedException;

import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.Permit;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Region;

public class FaceUpPermitResponseActionTest {
	private Game game;
	private TurnMachine turnMachine;

	@Before
	public void initialize() throws GameFinishedException {
		List<String> usernames = Arrays.asList("P1", "P2", "P3");
		this.game = new Game(new GameMapFactory().getGameMap("default"), usernames);
		this.turnMachine = new TurnMachine(this.game);
		this.turnMachine.changeState("P1", new DrawAPoliticAction(null, null));
		// .. gameflow ...
		assertEquals(this.game.getCurrentPlayer().getUsername(), "P1");
		this.turnMachine.askForFaceUpPermitResponseWaitingState(1);
	}

	@Test
	public void test() throws GameFinishedException {
		assertFalse(this.turnMachine.getState("P1").isAllowedToExecute(new EmptyQuickAction(null,null)));
		Player p1 = this.game.getPlayerByUsername("P1");
		Region region = Region.HILLS;
		Permit permit = game.showFaceUpPermitTiles(region).get(0);
		assertFalse(permit.hasAnOwner());
		FaceUpPermitResponseAction response = new FaceUpPermitResponseAction(this.game, p1, region, 1, this.turnMachine);
		response.execute();
		assertTrue(response.hasBeenExecuted());
		assertEquals(permit.getOwner(),p1);
		this.turnMachine.changeState("P1", response);
		assertTrue(this.turnMachine.getState("P1").isAllowedToExecute(new EmptyQuickAction(null,null)));
	}

}
