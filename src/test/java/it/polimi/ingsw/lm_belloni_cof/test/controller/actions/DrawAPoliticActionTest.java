package it.polimi.ingsw.lm_belloni_cof.test.controller.actions;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.DrawAPoliticAction;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Politic;

public class DrawAPoliticActionTest {
	private Game game;

	@Before
	public void initialize() {
		List<String> usernames = Arrays.asList("P1", "P2", "P3");
		this.game = new Game(new GameMapFactory().getGameMap("default"), usernames);
	}

	@Test
	public void test() {
		Player p1 = this.game.getPlayerByUsername("P1");
		List<Politic> beforeDraw = new ArrayList<Politic>(p1.getPoliticsHand());
		DrawAPoliticAction draw = new DrawAPoliticAction(this.game, p1);
		try {
			draw.execute();
		} catch (ActionExecutionException e) {
			fail();
		}
		assertTrue(draw.hasBeenExecuted());
		assertTrue(p1.getPoliticsHand().containsAll(beforeDraw));
		assertEquals(p1.getPoliticsHand().size(), beforeDraw.size() + 1);

	}

}
