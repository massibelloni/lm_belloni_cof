package it.polimi.ingsw.lm_belloni_cof.test.model;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.exceptions.EmptyDeckException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughAssistantsException;
import it.polimi.ingsw.lm_belloni_cof.model.Assistant;
import it.polimi.ingsw.lm_belloni_cof.model.AssistantsPool;
import it.polimi.ingsw.lm_belloni_cof.model.Permit;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Politic;
import it.polimi.ingsw.lm_belloni_cof.model.PoliticColor;
import it.polimi.ingsw.lm_belloni_cof.model.PoliticDeck;

public class PlayerTest {

	@Test
	public void testPolitics() {
		PoliticDeck deck = PoliticDeck.getPoliticDeckInstance(4);
		Player p1 = new Player("prova", 2, 10, deck.getNPolitics(5), new ArrayList<Assistant>());
		try {
			Politic drawn = deck.draw();
			p1.getPoliticsHand().add(drawn);
			assertTrue(p1.getPoliticsHand().contains(drawn));
		} catch (EmptyDeckException e) {
			fail();
		}
		Politic politic = new Politic(PoliticColor.MULTI_COLORED);
		p1.getPoliticsHand().add(politic);
		boolean multicoloredfound = false;
		for (Politic p : p1.getPoliticsHand())
			if (p.getPoliticColor() == PoliticColor.MULTI_COLORED) {
				multicoloredfound = true;
				break;
			}
		assertTrue(multicoloredfound);

	}

	@Test
	public void testAssistants() {
		AssistantsPool pool = new AssistantsPool();
		Player p1 = new Player("prova", 2, 10, new ArrayList<Politic>(), pool.getNAssistants(5));
		try {
			for (int i = 0; i < 5; i++) {
				pool.returnAnAssistant(p1.getAnAssistant());
			}
		} catch (NotEnoughAssistantsException e) {
			fail();
		}
		try {
			p1.getAnAssistant();
			fail();
		} catch (NotEnoughAssistantsException e) {
			assertNotNull(e);
		}

	}

	@Test
	public void testPermit() {
		Player p1 = new Player("prova", 2, 10, new ArrayList<Politic>(), new ArrayList<Assistant>());
		assertEquals(p1.getPermitsAcquired().size(), 0);
		Permit emptyPermit = new Permit(null);
		p1.giveAPermit(emptyPermit);
		for (Permit p : p1.getPermitsAcquired())
			assertEquals(p.getOwner(), p1);
	}

}
