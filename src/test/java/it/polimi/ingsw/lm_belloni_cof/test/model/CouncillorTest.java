package it.polimi.ingsw.lm_belloni_cof.test.model;

import static org.junit.Assert.*;

import java.security.InvalidParameterException;

import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.model.Councillor;
import it.polimi.ingsw.lm_belloni_cof.model.PoliticColor;

public class CouncillorTest {

	@Test
	public void testColor() {
		PoliticColor color = PoliticColor.ORANGE;
		Councillor councillor = new Councillor(color);
		assertEquals("Checking if the color is stored properly",councillor.getCouncillorColor(),color);
	}
	@Test
	public void testMulti(){
		try{
			PoliticColor color = PoliticColor.MULTI_COLORED;
			new Councillor(color);
			fail();
		} catch(InvalidParameterException e){
			assertNotNull(e);
		}
	}

}
