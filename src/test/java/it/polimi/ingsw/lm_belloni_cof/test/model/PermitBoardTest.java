package it.polimi.ingsw.lm_belloni_cof.test.model;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.model.GameMap;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.Permit;
import it.polimi.ingsw.lm_belloni_cof.model.PermitBoard;
import it.polimi.ingsw.lm_belloni_cof.model.PermitDeck;
import it.polimi.ingsw.lm_belloni_cof.model.Region;

public class PermitBoardTest {

	@Test
	public void test() {
		GameMap defaultMap = new GameMapFactory().getGameMap("default");
		PermitBoard board = new PermitBoard(
				PermitDeck.getPermitDeckInstance(defaultMap.getPermitContentsByRegion(Region.HILLS),4));
		List<Permit> faceUp = board.showFaceUpPermitTiles();
		Permit p = board.pickUpAPermitTile(1);
		assertTrue(faceUp.contains(p));
		board.changeTiles();
		List<Permit> faceUpAfterChanging = board.showFaceUpPermitTiles();
		for(Permit f:faceUp)
			assertFalse(faceUpAfterChanging.contains(f));
	}

}
