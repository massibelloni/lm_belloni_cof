package it.polimi.ingsw.lm_belloni_cof.test.controller.actions;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.SendAnAssistantToElectACouncillorAction;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidActionException;
import it.polimi.ingsw.lm_belloni_cof.model.Councillor;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Region;

public class SendAnAssistantToElectACouncillorActionTest {
	private Game game;
	private Player p1;

	@Before
	public void initialize() {
		List<String> usernames = Arrays.asList("P1", "P2", "P3");
		this.game = new Game(new GameMapFactory().getGameMap("default"), usernames);
		this.p1 = game.getPlayerByUsername("P1");
	}

	@Test
	public void test() {
		int oldAssistants = p1.getNumberOfAssistants();
		Region region = Region.MOUNTAINS;
		Councillor c = null;
		try {
			c = this.game.getACouncillorByPoliticColor(this.game.getAvailableCouncillorColors().get(0));
		} catch (InvalidActionException e) {
			fail();
		}
		SendAnAssistantToElectACouncillorAction elect = new SendAnAssistantToElectACouncillorAction(this.game, this.p1,
				c, region);
		try {
			elect.execute();
		} catch (ActionExecutionException e) {
			fail();
		}
		assertTrue(elect.hasBeenExecuted());
		assertEquals(oldAssistants - 1, p1.getNumberOfAssistants());
		assertTrue(this.game.getCouncilByRegion(region).contains(c.getCouncillorColor()));
	}

}
