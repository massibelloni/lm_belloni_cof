package it.polimi.ingsw.lm_belloni_cof.test.controller;

import static org.junit.Assert.*;

import java.rmi.RemoteException;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.Broadcaster;
import it.polimi.ingsw.lm_belloni_cof.controller.GamesHandler;
import it.polimi.ingsw.lm_belloni_cof.messages.ChatRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ConnectionRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ConnectionResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.InvalidRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayersTableRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayersTableResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ResponseMessage;

public class GamesHandlerTest {
	private GamesHandler gamesHandler;

	@Before
	public void initialize() {
		this.gamesHandler = new GamesHandler(new Broadcaster());
	}

	@Test
	public void testConnection() {
		ConnectionRequestMessage req = new ConnectionRequestMessage("test", "test");
		try {
			ResponseMessage res = gamesHandler.processRequest(req);
			assertTrue(res instanceof ConnectionResponseMessage);
			PlayerToken token = ((ConnectionResponseMessage) res).getPlayerToken();
			assertNotNull(token);
			this.gamesHandler.processRequest(new ChatRequestMessage(token,"test"));
			/* trying to send a chat message */
			
		} catch (RemoteException e) {
			fail();
		}
	}

	@Test
	public void testFailedConnection() {
		ConnectionRequestMessage req = new ConnectionRequestMessage("test", "test");
		try {
			gamesHandler.processRequest(req); // first successful login attempt
		} catch (RemoteException e) {
			fail();
		}
		// second login attempt with wrong password
		ConnectionRequestMessage req2 = new ConnectionRequestMessage("test", "test1");
		try {
			ResponseMessage res2 = gamesHandler.processRequest(req2);
			assertTrue(res2 instanceof ConnectionResponseMessage);
			assertNull(((ConnectionResponseMessage) res2).getPlayerToken());
		} catch (RemoteException e) {
			fail();
		}
	}
	
	@Test
	public void testAlreadyLoggedConnection(){
		ConnectionRequestMessage req = new ConnectionRequestMessage("test", "test");
		String gameId = "";
		try {
			ConnectionResponseMessage res = (ConnectionResponseMessage) gamesHandler.processRequest(req); // first successful login attempt
			gameId = res.getMessage().substring(res.getMessage().indexOf("GAME"));
		} catch (RemoteException e) {
			fail();
		}
		/* ... */
		// second login attempt with same username and password
		ConnectionRequestMessage req2 = new ConnectionRequestMessage("test", "test");
		try {
			ConnectionResponseMessage res2 = (ConnectionResponseMessage)gamesHandler.processRequest(req2);
			/* no others players involved, subscribed to same game */
			assertEquals(gameId,res2.getMessage().substring(res2.getMessage().indexOf("GAME")));
		} catch (RemoteException e) {
			fail();
		}
	}

	@Test
	public void testClientNotRecognizable() {
		PlayerToken fake = new PlayerToken("fake");
		try {
			ResponseMessage res = gamesHandler.processRequest(new ChatRequestMessage(fake, null));

			assertTrue(res instanceof InvalidRequestMessage);
		} catch (RemoteException e) {
			fail();
		}
	}
	
	@Test
	public void testPlayersTable(){
		ConnectionRequestMessage req = new ConnectionRequestMessage("test", "test");
		ConnectionRequestMessage req2 = new ConnectionRequestMessage("test1", "test1");
		try {
			/* a player has been created */
			gamesHandler.processRequest(req);
			/* another player has been created */
			gamesHandler.processRequest(req2);
			PlayersTableResponseMessage msg = (PlayersTableResponseMessage) gamesHandler.processRequest(new PlayersTableRequestMessage());
			assertEquals(msg.getGamesPlayed().get("test"),new Integer(0));
			assertEquals(msg.getGamesWon().get("test"),new Integer(0));
			assertEquals(msg.getGamesWon().get("test1"),new Integer(0));
			assertEquals(msg.getGameTimeInMinutes().get("test1"),new Integer(0));
			assertEquals(msg.getGamesWon().containsKey("fake"),false);
		} catch (RemoteException e) {
			fail();
		}
	}

}
