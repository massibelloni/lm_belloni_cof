package it.polimi.ingsw.lm_belloni_cof.test.controller.actions;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.BuildAnEmporiumWithTheHelpOfTheKingAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.DrawAPoliticAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.MainAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.PerformAnAdditionalMainActionAction;
import it.polimi.ingsw.lm_belloni_cof.controller.states.TurnMachine;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.GameFinishedException;
import it.polimi.ingsw.lm_belloni_cof.model.Assistant;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.Player;

public class PerformAnAdditionalMainActionActionTest {
	private Game game;
	private Player p1;
	private TurnMachine turnMachine;

	@Before
	public void initialize() throws GameFinishedException {
		List<String> usernames = Arrays.asList("P1", "P2", "P3");
		this.game = new Game(new GameMapFactory().getGameMap("default"), usernames);
		this.p1 = game.getPlayerByUsername("P1");
		this.turnMachine = new TurnMachine(this.game);
		this.turnMachine.changeState("P1", new DrawAPoliticAction(null, null));
		MainAction main = new BuildAnEmporiumWithTheHelpOfTheKingAction(null, null, null, null, turnMachine);
		this.turnMachine.changeState("P1", main);
		// no more mains
		assertFalse(this.turnMachine.getState("P1").isAllowedToExecute(main));
	}

	@Test
	public void test() throws GameFinishedException {
		PerformAnAdditionalMainActionAction anotherMain = new PerformAnAdditionalMainActionAction(game, p1,
				turnMachine);
		p1.giveNAssistants(Arrays.asList(new Assistant(),new Assistant(),new Assistant()));
		try {
			anotherMain.execute();
		} catch (ActionExecutionException e) {
			fail();
		}
		assertTrue(anotherMain.hasBeenExecuted());
		this.turnMachine.changeState("P1", anotherMain);
		assertTrue(this.turnMachine.getState("P1").isAllowedToExecute(
				new BuildAnEmporiumWithTheHelpOfTheKingAction(null, null, null, null, turnMachine)));
	}

}
