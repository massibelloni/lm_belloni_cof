package it.polimi.ingsw.lm_belloni_cof.test.model;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.exceptions.EmptyDeckException;
import it.polimi.ingsw.lm_belloni_cof.model.GameMap;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.KingRewardTile;
import it.polimi.ingsw.lm_belloni_cof.model.KingRewardsPile;

public class KingRewardsPileTest {

	@Test
	public void test() {
		GameMap gameMap = new GameMapFactory().getGameMap("default");
		KingRewardsPile pile = KingRewardsPile.getKingRewardPile(gameMap.getKingRewardTileContents());
		try {
			assertEquals(pile.peekTopTile().getPickingOrder(), 1);
			KingRewardTile k = pile.pickUpATile();
			assertNull(k.getOwner());
		} catch (EmptyDeckException e) {
			fail();
		}
		try {
			KingRewardsPile pile2 = KingRewardsPile.getKingRewardPile(gameMap.getKingRewardTileContents());
			int prev = pile2.pickUpATile().getPickingOrder();
			while (true) {
				if (prev >= pile2.pickUpATile().getPickingOrder())
					fail();
			}

		} catch (EmptyDeckException e) {
			assertNotNull(e);
		}

	}

}
