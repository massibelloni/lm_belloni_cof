package it.polimi.ingsw.lm_belloni_cof.test.model;

import static org.junit.Assert.*;

import java.security.InvalidParameterException;
import java.util.ArrayList;

import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.model.Balcony;
import it.polimi.ingsw.lm_belloni_cof.model.Councillor;
import it.polimi.ingsw.lm_belloni_cof.model.PoliticColor;

public class BalconyTest {

	@Test
	public void testShift() {
		Councillor[] councillors = { new Councillor(PoliticColor.BLACK), new Councillor(PoliticColor.LIGHT_BLUE),
				new Councillor(PoliticColor.ORANGE), new Councillor(PoliticColor.PINK) };
		Balcony balcony = new Balcony(councillors);
		Councillor shiftedCouncillor = balcony.shift(new Councillor(PoliticColor.WHITE));
		ArrayList<PoliticColor> colorsOnBalcony = new ArrayList<PoliticColor>();
		colorsOnBalcony.add(balcony.getBalconyColors().get(0));
		colorsOnBalcony.add(balcony.getBalconyColors().get(1));
		colorsOnBalcony.add(balcony.getBalconyColors().get(2));
		colorsOnBalcony.add(balcony.getBalconyColors().get(3));
		if (colorsOnBalcony.get(0) == PoliticColor.LIGHT_BLUE && colorsOnBalcony.get(1) == PoliticColor.ORANGE
				&& colorsOnBalcony.get(2) == PoliticColor.PINK && colorsOnBalcony.get(3) == PoliticColor.WHITE)
			assertEquals(shiftedCouncillor.getCouncillorColor(), PoliticColor.BLACK);

		else {
			fail();
		}
	}

	@Test
	public void testBalconySize() {
		Councillor[] councillors = { new Councillor(PoliticColor.BLACK), new Councillor(PoliticColor.LIGHT_BLUE),
				new Councillor(PoliticColor.ORANGE), new Councillor(PoliticColor.PINK),
				new Councillor(PoliticColor.WHITE) };

		try {
			Balcony balcony = new Balcony(councillors);
			assertNotNull(balcony);
		} catch (InvalidParameterException ex) {
			// ok!
		}

	}

}
