package it.polimi.ingsw.lm_belloni_cof.test.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.exceptions.EmptyDeckException;
import it.polimi.ingsw.lm_belloni_cof.model.GameMap;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.Permit;
import it.polimi.ingsw.lm_belloni_cof.model.PermitContent;
import it.polimi.ingsw.lm_belloni_cof.model.PermitDeck;
import it.polimi.ingsw.lm_belloni_cof.model.Region;

public class PermitDeckTest {

	@Test
	public void test() {
		GameMap gameMap = new GameMapFactory().getGameMap("default");
		PermitDeck pd1 = PermitDeck.getPermitDeckInstance(gameMap.getPermitContentsByRegion(Region.COAST), 3);
		int counter = 0;
		try {
			while (true) {
				assertEquals(pd1.draw().getRegion(), Region.COAST);
				counter++;
			}
		} catch (EmptyDeckException e) {
			assertEquals(counter, PermitDeck.FOUR_PLAYERS_PERMIT);
		}
		int numberOfPlayers = 7;
		PermitDeck pd2 = PermitDeck.getPermitDeckInstance(gameMap.getPermitContentsByRegion(Region.COAST), numberOfPlayers);
		counter = 0;
		try {
			while (true) {
				assertEquals(pd2.draw().getRegion(), Region.COAST);
				counter++;
			}
		} catch (EmptyDeckException e) {
			assertEquals(counter, numberOfPlayers*PermitDeck.PER_PLAYER_PERMIT);
		}

	}

	@Test
	public void testContents() {
		GameMap gameMap = new GameMapFactory().getGameMap("default");
		PermitDeck pd1 = PermitDeck.getPermitDeckInstance(gameMap.getPermitContentsByRegion(Region.COAST), 3);
		List<PermitContent> contents = gameMap.getPermitContentsByRegion(Region.COAST);
		List<Permit> deckCopy = new ArrayList<Permit>();
		for (int i = 0; i < PermitDeck.FOUR_PLAYERS_PERMIT; i++)
			try {
				deckCopy.add(pd1.draw());
			} catch (EmptyDeckException e) {
				fail();
			}
		for (Permit p1 : deckCopy)
			assertNull(p1.getOwner());
		for(Permit p2: deckCopy){
			boolean found = false;
			for(PermitContent content:contents){
				if(p2.getCities() == content.getCities())
					found = true;
			}
			assertTrue(found);
		}
	}
}
