package it.polimi.ingsw.lm_belloni_cof.test.controller.actions;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.EndMarketBuyAction;
import it.polimi.ingsw.lm_belloni_cof.controller.states.ActionToken;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.Player;

public class EndMarketBuyActionTest {
	private Game game;

	@Before
	public void initialize() {
		List<String> usernames = Arrays.asList("P1", "P2", "P3");
		this.game = new Game(new GameMapFactory().getGameMap("default"), usernames);
	}

	@Test
	public void test() {
		Player p1 = game.getPlayerByUsername("P1");
		EndMarketBuyAction endBuy = new EndMarketBuyAction(game, p1);
		endBuy.execute();
		assertTrue(endBuy.hasBeenExecuted());
		assertEquals(endBuy.getActionToken(), ActionToken.MARKET_BUY);
	}

}
