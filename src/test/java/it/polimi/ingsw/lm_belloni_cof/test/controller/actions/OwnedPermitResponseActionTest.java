package it.polimi.ingsw.lm_belloni_cof.test.controller.actions;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.BuildAnEmporiumUsingAPermitTileAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.DrawAPoliticAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.EmptyQuickAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.OwnedPermitResponseAction;
import it.polimi.ingsw.lm_belloni_cof.controller.states.TurnMachine;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.GameFinishedException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidActionException;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.Permit;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Politic;
import it.polimi.ingsw.lm_belloni_cof.model.PoliticColor;
import it.polimi.ingsw.lm_belloni_cof.model.Region;

public class OwnedPermitResponseActionTest {
	private Game game;
	private Permit ownedAndUsed;
	private Player p1;
	private TurnMachine turnMachine;

	@Before
	public void initialize() throws GameFinishedException {
		List<String> usernames = Arrays.asList("P1", "P2", "P3");
		this.game = new Game(new GameMapFactory().getGameMap("default"), usernames);
		this.turnMachine = new TurnMachine(this.game);
		this.p1 = this.game.getCurrentPlayer();
		this.turnMachine.changeState("P1", new DrawAPoliticAction(null, null));
		List<Politic> multiColoredToUse = new ArrayList<>();
		for (int i = 0; i < 4; i++) {
			Politic cheat = new Politic(PoliticColor.MULTI_COLORED);
			p1.giveAPolitic(cheat);
			multiColoredToUse.add(cheat);
		}
		Region region = Region.COAST;
		try {
			game.acquireAPermitTile(p1, region, 1);
		} catch (InvalidActionException e) {
			fail();
		}
		this.ownedAndUsed = p1.getPermitsAcquired().get(0);
		// using it
		BuildAnEmporiumUsingAPermitTileAction action = new BuildAnEmporiumUsingAPermitTileAction(this.game, this.p1,
				this.ownedAndUsed.getCities().get(0), this.ownedAndUsed, this.turnMachine);
		try {
			action.execute();
		} catch (ActionExecutionException e) {
			fail();
		}

		// ... gameflow ...
		assertEquals(this.game.getCurrentPlayer().getUsername(), "P1");
		this.turnMachine.askForOwnedPermitResponseWaitingState(1);
	}

	@Test
	public void test() throws GameFinishedException {
		assertFalse(this.turnMachine.getState("P1").isAllowedToExecute(new EmptyQuickAction(null, null)));
		// player is in ownedAndUsedPermitResponseWaitingState
		OwnedPermitResponseAction response = new OwnedPermitResponseAction(this.game, this.p1, this.ownedAndUsed,
				this.turnMachine);
		response.execute();
		assertTrue(response.hasBeenExecuted());
		this.turnMachine.changeState("P1", response);
		assertTrue(this.turnMachine.getState("P1").isAllowedToExecute(new EmptyQuickAction(null, null)));
	}

}
