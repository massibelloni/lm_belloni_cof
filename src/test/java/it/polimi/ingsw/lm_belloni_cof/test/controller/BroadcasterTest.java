package it.polimi.ingsw.lm_belloni_cof.test.controller;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.Broadcaster;
import it.polimi.ingsw.lm_belloni_cof.messages.BroadcastMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;
import it.polimi.ingsw.lm_belloni_cof.view.shared.SubscriberInterface;

public class BroadcasterTest {
	private Broadcaster broadcaster;
	private TestSubscriber[] subscribers;
	private PlayerToken[] tokens;

	@Before
	public void initialize() {

		/* initializing 4 players subscribed to two different games */
		this.subscribers = new TestSubscriber[4];
		this.tokens = new PlayerToken[4];
		this.broadcaster = new Broadcaster();
		for (int i = 0; i < 4; i++) {
			subscribers[i] = new TestSubscriber();
			tokens[i] = new PlayerToken(Integer.toString(i));
		}
		this.broadcaster.addGame("GAME0");
		this.broadcaster.addGame("GAME1");
		/* subscribing 0 and 2 to GAME0 and 1 and 3 to GAME1 */
		for (int i = 0; i < 4; i++) {
			String game = "GAME" + Integer.toString(i % 2);
			this.broadcaster.subscribeTokenToGame(tokens[i], game);
		}
	}

	@Test
	public void testBroadcastOK() {
		// subscribing
		try {
			for (int i = 0; i < 4; i++) {
				this.broadcaster.subscribe(subscribers[i], tokens[i].getUsername());
				assertEquals(subscribers[i].getReceivedMessages(), 0);
			}
		} catch (Exception e) {
			fail();
		}
		/* GAME0 receives broadcast, GAME1 doesn't */
		this.broadcaster.broadcast(new TestMessage(), "GAME0");
		for (int i = 0; i < 4; i++)
			assertEquals(subscribers[i].getReceivedMessages(), (i % 2 == 0) ? 1 : 0);
		/* both receives */
		this.broadcaster.broadcast(new TestMessage(), "GAME1");
		for (int i = 0; i < 4; i++)
			assertEquals(subscribers[i].getReceivedMessages(), 1);
	}
	
	@Test
	public void testBroadcastToUnsubscribed() {
		/* no one of the subscribed tokens has subscribed with its interface */
		this.broadcaster.broadcast(new TestMessage(), "GAME0");
		/* no exceptions thrown */
		this.broadcaster.broadcast(new TestMessage(), "GAME1");
		
	}

	@Test
	public void testBroadcastToSelected() {
		try{
		for(int i=0;i<4;i++)
			this.broadcaster.subscribe(subscribers[i], tokens[i].getUsername());
		} catch(Exception e){
			fail();
		}
		this.broadcaster.broadcast(new TestMessage(), Arrays.asList(tokens[0], tokens[1]));
		for (int i = 0; i < 4; i++)
			assertEquals(subscribers[i].getReceivedMessages(), (i < 2) ? 1 : 0);
	}
	
	@Test
	public void testBroadcastToSelectedUnsubscribed() {
		this.broadcaster.broadcast(new TestMessage(), Arrays.asList(tokens[0]));
		/* no exceptions thrown */
	}

	@Test
	public void testRemoveGamesFromToken() {
		try {
			for (int i = 0; i < 4; i++) {
				this.broadcaster.subscribe(subscribers[i], tokens[i].getUsername());
				assertEquals(subscribers[i].getReceivedMessages(), 0);
			}
		} catch (Exception e) {
			fail();
		}
		this.broadcaster.removeGamesFromToken(this.tokens[0]);
		/* GAME0 receives broadcast, GAME1 doesn't */
		this.broadcaster.broadcast(new TestMessage(), "GAME0");
		for (int i = 0; i < 4; i++) {
			assertEquals(subscribers[i].getReceivedMessages(), (i == 2) ? 1 : 0);
		}
	}

	@Test
	public void testRemoveGameFromToken() {
		try {
			for (int i = 0; i < 4; i++) {
				this.broadcaster.subscribe(subscribers[i], tokens[i].getUsername());
				assertEquals(subscribers[i].getReceivedMessages(), 0);
			}
		} catch (Exception e) {
			fail();
		}
		this.broadcaster.removeGameFromToken(this.tokens[0], "GAME0");
		/* GAME0 receives broadcast, GAME1 doesn't */
		this.broadcaster.broadcast(new TestMessage(), "GAME0");
		for (int i = 0; i < 4; i++) {
			assertEquals(subscribers[i].getReceivedMessages(), (i == 2) ? 1 : 0);
		}
	}

}

class TestMessage extends BroadcastMessage {
	private static final long serialVersionUID = 1L;

	@Override
	public void visit(MessageVisitor visitor) {
		/**/

	}

}

class TestSubscriber implements SubscriberInterface {
	private int receivedMessagesCounter = 0;

	@Override
	public void update(BroadcastMessage msg) throws RemoteException {
		this.receivedMessagesCounter++;

	}

	public int getReceivedMessages() {
		return this.receivedMessagesCounter;
	}

}