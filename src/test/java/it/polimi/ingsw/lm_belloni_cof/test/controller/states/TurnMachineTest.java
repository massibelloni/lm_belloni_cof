package it.polimi.ingsw.lm_belloni_cof.test.controller.states;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.BonusResponseAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.BuildAnEmporiumUsingAPermitTileAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.CityTokenResponseAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.DrawAPoliticAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.EmptyQuickAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.EndMarketBuyAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.EndMarketOfferAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.MainAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.MarketBuyAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.MarketOfferAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.OwnedPermitResponseAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.QuickAction;
import it.polimi.ingsw.lm_belloni_cof.controller.states.StateChangingBonusHandler;
import it.polimi.ingsw.lm_belloni_cof.controller.states.StatesManager;
import it.polimi.ingsw.lm_belloni_cof.controller.states.TurnMachine;
import it.polimi.ingsw.lm_belloni_cof.exceptions.GameFinishedException;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;

public class TurnMachineTest {

	@Test
	public void testGameFlow() throws GameFinishedException {
		List<String> usernames = Arrays.asList("P1", "P2");
		Collections.shuffle(usernames);
		Game game = new Game(new GameMapFactory().getGameMap("default"),usernames);
		StatesManager states = new TurnMachine(game);
		assertTrue(states.getCurrentPlayer().equals("P1") || states.getCurrentPlayer().equals("P2"));
		String playing = states.getCurrentPlayer();
		String waiting = (playing.equals("P1")) ? ("P2") : ("P1");
		assertNotEquals(playing, waiting);
		assertEquals(states.getTurnId(), 1);
		// preparing some fake actions to exec
		MainAction main = new BuildAnEmporiumUsingAPermitTileAction(null, null, null, null, null);
		DrawAPoliticAction draw = new DrawAPoliticAction(null, null);
		QuickAction quick = new EmptyQuickAction(null, null);
		MarketOfferAction offer = new MarketOfferAction(null, null, null);
		MarketBuyAction buy = new MarketBuyAction(null, null, null,null);
		// waiting can't do anything
		assertFalse(states.getState(waiting).isAllowedToExecute(main));
		assertFalse(states.getState(waiting).isAllowedToExecute(draw));
		assertFalse(states.getState(waiting).isAllowedToExecute(quick));
		assertFalse(states.getState(waiting).isAllowedToExecute(offer));
		assertFalse(states.getState(waiting).isAllowedToExecute(buy));
		// what playing can do
		assertFalse(states.getState(playing).isAllowedToExecute(main));
		assertTrue(states.getState(playing).isAllowedToExecute(draw));
		assertFalse(states.getState(playing).isAllowedToExecute(quick));
		assertFalse(states.getState(playing).isAllowedToExecute(offer));
		assertFalse(states.getState(playing).isAllowedToExecute(buy));
		states.changeState(playing, draw);
		assertTrue(states.getState(playing).isAllowedToExecute(main));
		assertFalse(states.getState(playing).isAllowedToExecute(draw));
		assertTrue(states.getState(playing).isAllowedToExecute(quick));
		assertFalse(states.getState(playing).isAllowedToExecute(offer));
		assertFalse(states.getState(playing).isAllowedToExecute(buy));
		states.changeState(playing, quick);
		assertTrue(states.getState(playing).isAllowedToExecute(main));
		assertFalse(states.getState(playing).isAllowedToExecute(draw));
		assertFalse(states.getState(playing).isAllowedToExecute(quick));
		assertFalse(states.getState(playing).isAllowedToExecute(offer));
		assertFalse(states.getState(playing).isAllowedToExecute(buy));
		states.changeState(playing, main);
		assertFalse(states.getState(playing).isAllowedToExecute(main));
		assertFalse(states.getState(playing).isAllowedToExecute(draw));
		assertFalse(states.getState(playing).isAllowedToExecute(quick));
		assertFalse(states.getState(playing).isAllowedToExecute(offer));
		assertFalse(states.getState(playing).isAllowedToExecute(buy));
		// new turn, waiting is starting its turn
		// what playing can do
		assertFalse(states.getState(waiting).isAllowedToExecute(main));
		assertTrue(states.getState(waiting).isAllowedToExecute(draw));
		assertFalse(states.getState(waiting).isAllowedToExecute(quick));
		assertFalse(states.getState(waiting).isAllowedToExecute(offer));
		assertFalse(states.getState(waiting).isAllowedToExecute(buy));
		states.changeState(waiting, draw);
		states.changeState(waiting, quick);
		states.changeState(waiting, main);
		// waiting can't do anything
		assertFalse(states.getState(waiting).isAllowedToExecute(main));
		assertFalse(states.getState(waiting).isAllowedToExecute(draw));
		assertFalse(states.getState(waiting).isAllowedToExecute(quick));
		assertFalse(states.getState(waiting).isAllowedToExecute(offer));
		assertFalse(states.getState(waiting).isAllowedToExecute(buy));
		// playing is in market offer
		assertFalse(states.getState(playing).isAllowedToExecute(main));
		assertFalse(states.getState(playing).isAllowedToExecute(draw));
		assertFalse(states.getState(playing).isAllowedToExecute(quick));
		assertTrue(states.getState(playing).isAllowedToExecute(offer));
		assertFalse(states.getState(playing).isAllowedToExecute(buy));
		states.changeState(playing, offer);
		assertTrue(states.getState(playing).isAllowedToExecute(new EndMarketOfferAction(null,null)));
		states.changeState(playing, new EndMarketOfferAction(null,null));
		// waiting is in market offer
		assertFalse(states.getState(waiting).isAllowedToExecute(main));
		assertFalse(states.getState(waiting).isAllowedToExecute(draw));
		assertFalse(states.getState(waiting).isAllowedToExecute(quick));
		assertTrue(states.getState(waiting).isAllowedToExecute(offer));
		assertFalse(states.getState(waiting).isAllowedToExecute(buy));
		states.changeState(waiting, offer);
		states.changeState(waiting, new EndMarketOfferAction(null,null));
		// someone is in market buy
		String buy1 = states.getCurrentPlayer();
		String buy2 = (buy1.equals("P1")) ? ("P2") : ("P1");
		assertNotEquals(buy1, buy2);
		assertFalse(states.getState(buy1).isAllowedToExecute(main));
		assertFalse(states.getState(buy1).isAllowedToExecute(draw));
		assertFalse(states.getState(buy1).isAllowedToExecute(quick));
		assertFalse(states.getState(buy1).isAllowedToExecute(offer));
		assertTrue(states.getState(buy1).isAllowedToExecute(buy));
		states.changeState(buy1, buy);
		// buy1 has performed a market buy, it can perform another
		assertFalse(states.getState(buy1).isAllowedToExecute(main));
		assertFalse(states.getState(buy1).isAllowedToExecute(draw));
		assertFalse(states.getState(buy1).isAllowedToExecute(quick));
		assertFalse(states.getState(buy1).isAllowedToExecute(offer));
		assertTrue(states.getState(buy1).isAllowedToExecute(buy));
		// buy1 ends its market buy phase
		MarketBuyAction end = new EndMarketBuyAction(null,null);
		states.changeState(buy1, end);
		// now it's time for buy2 to perform market buy
		assertFalse(states.getState(buy2).isAllowedToExecute(main));
		assertFalse(states.getState(buy2).isAllowedToExecute(draw));
		assertFalse(states.getState(buy2).isAllowedToExecute(quick));
		assertFalse(states.getState(buy2).isAllowedToExecute(offer));
		assertTrue(states.getState(buy2).isAllowedToExecute(buy));
		states.changeState(buy2, buy);
		// buy 2 ends its market buy
		states.changeState(buy2, end);
		// now back to start
		assertEquals(states.getTurnId(), 2);
		// waiting can't do anything
		assertFalse(states.getState(waiting).isAllowedToExecute(main));
		assertFalse(states.getState(waiting).isAllowedToExecute(draw));
		assertFalse(states.getState(waiting).isAllowedToExecute(quick));
		assertFalse(states.getState(waiting).isAllowedToExecute(offer));
		assertFalse(states.getState(waiting).isAllowedToExecute(buy));
		// what playing can do
		assertFalse(states.getState(playing).isAllowedToExecute(main));
		assertTrue(states.getState(playing).isAllowedToExecute(draw));
		assertFalse(states.getState(playing).isAllowedToExecute(quick));
		assertFalse(states.getState(playing).isAllowedToExecute(offer));
		assertFalse(states.getState(playing).isAllowedToExecute(buy));
	}

	@Test
	public void testBonusResponseWaitingState() throws GameFinishedException {
		List<String> usernames = Arrays.asList("P1", "P2");
		Game game = new Game(new GameMapFactory().getGameMap("default"),usernames);
		TurnMachine t = new TurnMachine(game);
		StatesManager states = t;
		StateChangingBonusHandler bonusHandler = t;
		assertTrue(states.getCurrentPlayer().equals("P1") || states.getCurrentPlayer().equals("P2"));
		String playing = states.getCurrentPlayer();
		String waiting = (playing.equals("P1")) ? ("P2") : ("P1");
		assertNotEquals(playing, waiting);
		assertEquals(states.getTurnId(), 1);
		// preparing some fake actions to exec
		MainAction main = new BuildAnEmporiumUsingAPermitTileAction(null, null, null, null, null);
		DrawAPoliticAction draw = new DrawAPoliticAction(null, null);
		BonusResponseAction response = new CityTokenResponseAction(null, null, null, null);
		BonusResponseAction anotherTypeResponse = new OwnedPermitResponseAction(null, null, null, null);
		QuickAction quick = new EmptyQuickAction(null, null);
		// player does a main action
		states.changeState(playing, draw);
		assertTrue(states.getState(playing).isAllowedToExecute(main));
		// during the exec of the main action it got a city token bonus
		bonusHandler.askForCityTokenResponseWaitingState(1);
		states.changeState(playing, main); // called after every action exec
		assertFalse(states.getState(playing).isAllowedToExecute(main));
		assertFalse(states.getState(playing).isAllowedToExecute(quick));
		assertTrue(states.getState(playing).isAllowedToExecute(response));
		assertFalse(states.getState(playing).isAllowedToExecute(anotherTypeResponse));
		states.changeState(playing, response);
		assertTrue(states.getState(playing).isAllowedToExecute(quick));
	}

	@Test
	public void testMultiBonusResponseWaitingState() throws GameFinishedException {
		List<String> usernames = Arrays.asList("P1", "P2");
		Game game = new Game(new GameMapFactory().getGameMap("default"),usernames);
		TurnMachine t = new TurnMachine(game);
		StatesManager states = t;
		StateChangingBonusHandler bonusHandler = t;
		assertTrue(states.getCurrentPlayer().equals("P1") || states.getCurrentPlayer().equals("P2"));
		String playing = states.getCurrentPlayer();
		String waiting = (playing.equals("P1")) ? ("P2") : ("P1");
		assertNotEquals(playing, waiting);
		assertEquals(states.getTurnId(), 1);
		// preparing some fake actions to exec
		MainAction main = new BuildAnEmporiumUsingAPermitTileAction(null, null, null, null, null);
		DrawAPoliticAction draw = new DrawAPoliticAction(null, null);
		BonusResponseAction response = new CityTokenResponseAction(null, null, null, null);
		BonusResponseAction anotherTypeResponse = new OwnedPermitResponseAction(null, null, null, null);
		QuickAction quick = new EmptyQuickAction(null, null);
		// player does a main action
		states.changeState(playing, draw);
		assertTrue(states.getState(playing).isAllowedToExecute(main));
		// during the exec of the main action it got a city token bonus
		bonusHandler.askForCityTokenResponseWaitingState(1);
		// and a owned permit response bonus
		bonusHandler.askForOwnedPermitResponseWaitingState(1);
		states.changeState(playing, main);
		assertFalse(states.getState(playing).isAllowedToExecute(quick));
		assertTrue(states.getState(playing).isAllowedToExecute(response));
		assertTrue(states.getState(playing).isAllowedToExecute(anotherTypeResponse));
		states.changeState(playing, response);
		assertFalse(states.getState(playing).isAllowedToExecute(response));
		assertTrue(states.getState(playing).isAllowedToExecute(anotherTypeResponse));
		assertFalse(states.getState(playing).isAllowedToExecute(quick));
		states.changeState(playing, anotherTypeResponse);
		assertTrue(states.getState(playing).isAllowedToExecute(quick));
		states.changeState(playing, quick);
		// waiting turn
		assertTrue(states.getState(waiting).isAllowedToExecute(draw));
	}
	
	@Test
	public void testDisconnect() throws GameFinishedException {
		List<String> usernames = Arrays.asList("P1", "P2");
		Collections.shuffle(usernames);
		Game game = new Game(new GameMapFactory().getGameMap("default"),usernames);
		StatesManager states = new TurnMachine(game);
		assertTrue(states.getCurrentPlayer().equals("P1") || states.getCurrentPlayer().equals("P2"));
		String playing = states.getCurrentPlayer();
		String waiting = (playing.equals("P1")) ? ("P2") : ("P1");
		assertNotEquals(playing, waiting);
		assertEquals(states.getTurnId(), 1);
		// preparing some fake actions to exec
		MainAction main = new BuildAnEmporiumUsingAPermitTileAction(null, null, null, null, null);
		DrawAPoliticAction draw = new DrawAPoliticAction(null, null);
		QuickAction quick = new EmptyQuickAction(null, null);
		MarketOfferAction offer = new MarketOfferAction(null, null, null);
		MarketBuyAction buy = new MarketBuyAction(null, null, null,null);
		// waiting can't do anything
		assertFalse(states.getState(waiting).isAllowedToExecute(main));
		assertFalse(states.getState(waiting).isAllowedToExecute(draw));
		assertFalse(states.getState(waiting).isAllowedToExecute(quick));
		assertFalse(states.getState(waiting).isAllowedToExecute(offer));
		assertFalse(states.getState(waiting).isAllowedToExecute(buy));
		// what playing can do
		assertFalse(states.getState(playing).isAllowedToExecute(main));
		assertTrue(states.getState(playing).isAllowedToExecute(draw));
		assertFalse(states.getState(playing).isAllowedToExecute(quick));
		assertFalse(states.getState(playing).isAllowedToExecute(offer));
		assertFalse(states.getState(playing).isAllowedToExecute(buy));
		states.changeState(playing, draw);
		assertTrue(states.getState(playing).isAllowedToExecute(main));
		assertFalse(states.getState(playing).isAllowedToExecute(draw));
		assertTrue(states.getState(playing).isAllowedToExecute(quick));
		assertFalse(states.getState(playing).isAllowedToExecute(offer));
		assertFalse(states.getState(playing).isAllowedToExecute(buy));
		states.changeState(playing, quick);
		assertTrue(states.getState(playing).isAllowedToExecute(main));
		assertFalse(states.getState(playing).isAllowedToExecute(draw));
		assertFalse(states.getState(playing).isAllowedToExecute(quick));
		assertFalse(states.getState(playing).isAllowedToExecute(offer));
		assertFalse(states.getState(playing).isAllowedToExecute(buy));
		// playing disconnects
		states.setDisconnected(playing);
		assertEquals(states.getCurrentPlayer(),waiting);
		states.changeState(waiting, draw);
		states.changeState(waiting, quick);
		states.changeState(waiting, main);
		assertEquals(states.getCurrentPlayer(),waiting);
		assertTrue(states.getState(waiting).isAllowedToExecute(offer));
		assertFalse(states.getState(waiting).isAllowedToExecute(buy));
		states.changeState(waiting, new EndMarketOfferAction(null,null));
		assertFalse(states.getState(waiting).isAllowedToExecute(offer));
		assertTrue(states.getState(waiting).isAllowedToExecute(buy));
		states.changeState(waiting, new EndMarketBuyAction(null,null));
		assertFalse(states.getState(waiting).isAllowedToExecute(offer));
		assertFalse(states.getState(waiting).isAllowedToExecute(buy));
		assertEquals(states.getCurrentPlayer(),waiting);
		assertFalse(states.getState(playing).isAllowedToExecute(draw));
		assertTrue(states.getState(waiting).isAllowedToExecute(draw));
		assertFalse(states.getState(waiting).isAllowedToExecute(quick));
		assertFalse(states.getState(waiting).isAllowedToExecute(main));
		/* it's the only player */
		states.changeState(waiting, draw);
		states.changeState(waiting, quick);
		states.changeState(waiting, main);
		assertEquals(states.getCurrentPlayer(),waiting);
		assertTrue(states.getState(waiting).isAllowedToExecute(offer));
		assertFalse(states.getState(waiting).isAllowedToExecute(buy));
		states.changeState(waiting, new EndMarketOfferAction(null,null));
		assertFalse(states.getState(waiting).isAllowedToExecute(offer));
		assertTrue(states.getState(waiting).isAllowedToExecute(buy));
		states.changeState(waiting, new EndMarketBuyAction(null,null));
		assertFalse(states.getState(waiting).isAllowedToExecute(offer));
		assertFalse(states.getState(waiting).isAllowedToExecute(buy));
		assertEquals(states.getCurrentPlayer(),waiting);
		assertFalse(states.getState(playing).isAllowedToExecute(draw));
		assertTrue(states.getState(waiting).isAllowedToExecute(draw));
		assertFalse(states.getState(waiting).isAllowedToExecute(quick));
		assertFalse(states.getState(waiting).isAllowedToExecute(main));
		try{
			states.setDisconnected(waiting);
			fail();
		} catch(GameFinishedException e){
			assertNotNull(e);
		}
	}

}
