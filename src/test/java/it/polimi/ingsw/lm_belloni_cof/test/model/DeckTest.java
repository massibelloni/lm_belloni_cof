package it.polimi.ingsw.lm_belloni_cof.test.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.LinkedList;

import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.exceptions.EmptyDeckException;
import it.polimi.ingsw.lm_belloni_cof.model.Assistant;
import it.polimi.ingsw.lm_belloni_cof.model.Deck;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Politic;
import it.polimi.ingsw.lm_belloni_cof.model.PoliticColor;

public class DeckTest {

	@Test
	public void testShuffle() throws EmptyDeckException {
		Deck<Politic> deck = new Deck<Politic>();
		for (PoliticColor color : PoliticColor.values()) {
			deck.discard(new Politic(color));
		}
		deck.shuffle();
		ArrayList<PoliticColor> deckOfColors = new ArrayList<PoliticColor>();
		for (int i = 0; i < PoliticColor.values().length - 1; i++)
			deckOfColors.add(((Politic) deck.draw()).getPoliticColor());
		for (PoliticColor color : PoliticColor.values()) {
			if (deckOfColors.contains(color)) {
				deckOfColors.remove(color);
			}

		}
		assertEquals(deckOfColors.size(), 0);
	}

	@Test
	public void testDiscard() throws EmptyDeckException {
		Deck<Politic> politicDeck = new Deck<Politic>();
		Politic politicCard = new Politic(PoliticColor.BLACK);
		ArrayList<Politic> politicsHand = new ArrayList<Politic>();
		LinkedList<Assistant> assistantsOwned = new LinkedList<Assistant>();
		Player playerOwner = new Player("", 0, 0, politicsHand, assistantsOwned);
		politicsHand.add(politicCard);
		if (politicsHand.get(0).getOwner() == playerOwner) {
			politicDeck.discard(politicsHand.remove(0));
			assertEquals(politicDeck.draw().getOwner(), null);
		}
	}

}
