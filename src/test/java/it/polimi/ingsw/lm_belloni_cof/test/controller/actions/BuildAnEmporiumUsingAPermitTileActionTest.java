package it.polimi.ingsw.lm_belloni_cof.test.controller.actions;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.AcquireABusinessPermitTileAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.BuildAnEmporiumUsingAPermitTileAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.DrawAPoliticAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.EmptyQuickAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.MainAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.QuickAction;
import it.polimi.ingsw.lm_belloni_cof.controller.states.TurnMachine;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.GameFinishedException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughAssistantsException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughCoinsException;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
import it.polimi.ingsw.lm_belloni_cof.model.Assistant;
import it.polimi.ingsw.lm_belloni_cof.model.Bonus;
import it.polimi.ingsw.lm_belloni_cof.model.BonusType;
import it.polimi.ingsw.lm_belloni_cof.model.City;
import it.polimi.ingsw.lm_belloni_cof.model.FakePlayer;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMap;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.Permit;
import it.polimi.ingsw.lm_belloni_cof.model.PermitContent;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Politic;
import it.polimi.ingsw.lm_belloni_cof.model.PoliticColor;
import it.polimi.ingsw.lm_belloni_cof.model.Region;
import it.polimi.ingsw.lm_belloni_cof.model.RewardToken;

public class BuildAnEmporiumUsingAPermitTileActionTest {
	private GameMap defaultGameMap;
	private Game game;
	private Player p1;
	private List<Politic> cheatPolitics;
	@Before
	public void initialize() throws GameFinishedException {
		/* defining PlayerTokens */
		int numberOfPlayers = 2;
		List<String> usernames = new ArrayList<String>();
		for (int i = 1; i <= numberOfPlayers; i++) {
			usernames.add(Integer.toString(i));
		}
		this.defaultGameMap = new GameMapFactory().getGameMap("default");
		this.game = new Game(defaultGameMap, usernames);
		this.p1 = game.getPlayerByUsername("1");
		this.cheatPolitics = new ArrayList<Politic>();
		for (int i = 0; i < 4; i++)
			cheatPolitics.add(new Politic(PoliticColor.MULTI_COLORED));
		p1.getPoliticsHand().addAll(cheatPolitics);
	}

	@Test
	public void testBonusNoRecursion() {
		/*
		 * Before actually testing the action i've to create an "environment" in
		 * which the action could be executed (ex. permits,politics, ecc). Then
		 * i will try to build an emporium not directly calling the
		 * "model methods" but creating and executing the action
		 */
		/*
		 * player 1 has somehow to have a permit tile in his hands, for the
		 * purpose of this test it's not useful to going trough the whole
		 * procedure (satisfy a council...)
		 */
		try {
			p1.moveOnCoinsTrack(4, defaultGameMap.getMaxCoins());
			p1.giveNAssistants(Arrays.asList(new Assistant(),new Assistant(),new Assistant()));
		} catch (NotEnoughCoinsException e) {
			fail();
		}
		Region region = Region.COAST;
		Permit wanted = game.showFaceUpPermitTiles(region).get(0);
		assertEquals(p1.getPermitsAcquired().size(), 0);
		try {
			game.acquireAPermitTile(p1, region, 1, cheatPolitics);
		} catch (Exception e) {
			fail();
		}
		assertEquals(p1.getPermitsAcquired().size(), 1);
		Permit owned = p1.getPermitsAcquired().get(0);
		assertEquals(wanted, owned);
		List<City> cities = owned.getCities();
		List<Bonus> bonuses = new ArrayList<>();
		for (RewardToken token : game.getRewardTokensOn(cities.get(0)))
			bonuses.addAll(token.getBonuses());
		int coinsBefore = p1.getCoins();
		int assistantsBefore = p1.getNumberOfAssistants();
		int victoriesBefore = p1.getVictoryPoints();
		int politicsBefore = p1.getPoliticsHand().size();
		// int nobilityBefore = p1.getNobilityTrackPosition();
		// what behaviour am i expecting?
		int moreAssistants = 0;
		int moreCoins = 0;
		int moreVictories = 0;
		int morePolitics = 0;
		int moreNobilities = 0;
		for (Bonus b : bonuses) {
			switch (b.getBonusType()) {
			case ASSISTANT:
				moreAssistants += b.getNumberOfTimes();
				break;

			case COIN:
				moreCoins += b.getNumberOfTimes();
				break;

			case NOBILITY:
				moreNobilities += b.getNumberOfTimes();
				break;

			case POLITIC:
				morePolitics += b.getNumberOfTimes();
				break;

			case VICTORY_POINT:
				moreVictories += b.getNumberOfTimes();
				break;
			default:
				break;

			}
		}
		if (moreNobilities == 0) {
			// in the defaultMap there aren't state changing bonus on permits
			MainAction a = new BuildAnEmporiumUsingAPermitTileAction(game, p1, cities.get(0), owned, null);
			try {
				a.execute();
				assertTrue(a.hasBeenExecuted());
				assertTrue(game.getPlayersWithAnEmporiumOn(cities.get(0)).contains(p1));
				// checking bonuses
				assertEquals(p1.getCoins(), coinsBefore + moreCoins);
				int usedToBuild =  (this.game.getPlayersWithAnEmporiumOn(cities.get(0)).size() ==2)?1:0;
				assertEquals(p1.getNumberOfAssistants(), assistantsBefore + moreAssistants - usedToBuild);
				assertEquals(p1.getVictoryPoints(), victoriesBefore + moreVictories);
				// assertEquals(p1.getNobilityTrackPosition(),nobilityBefore+moreNobilities);
				assertEquals(p1.getPoliticsHand().size(), politicsBefore + morePolitics);
			} catch (ActionExecutionException e) {
				fail();
			}
		} else
			assertTrue("Has moved on nobility track, too complex to test here, but already tested in model.", true);
	}
	
	@Test
	public void testKOAssistant(){
		/* i've to look for a city on which it's already built the "Council of Four" emporium */
		Player p1 = this.game.getPlayerByUsername("1");
		try {
			//surely has one
			p1.getAnAssistant();
		} catch (NotEnoughAssistantsException e) {
			fail();
		} 
		City chosen = null;
		for(City c:this.game.getCityList()){
			for(Player p:this.game.getPlayersWithAnEmporiumOn(c)){
				if(p instanceof FakePlayer){
					chosen = c;
					break;
				}
			}
			if (chosen != null)
				break;
		}
		/* creating a permit that can build on that city */
		PermitContent c = new PermitContent(chosen.getCityRegion());
		c.addCity(chosen);
		Permit p = new Permit(c);
		/* giving it to the player */
		p1.giveAPermit(p);
		assertEquals(p.getOwner(),p1);
		/* trying to execute the action */
		MainAction a = new BuildAnEmporiumUsingAPermitTileAction(game, p1, chosen, p, null);
		try {
			a.execute();
			fail();
		} catch(ActionExecutionException e){
			assertEquals(e.getMessage(),"Not enough assistants to complete this action.");
			assertTrue(e.getCause() instanceof NotEnoughAssistantsException);
		}
		
					
	}
	@Test
	public void testBonusWithRecursion() {
		/*
		 * This is a test built to simulate the behaviour of the action when it
		 * needs to handle a bonus with a main action. This test will work on
		 * the default nobility track: supposing the player is in nobility
		 * position 5 i'll try to find a bonus that moves along the player of 1
		 * position on the nobility track to get that bonus in the only way it
		 * can be obtained in the default map
		 */
		/*
		 * player 1 has somehow to have a permit tile in his hands, for the
		 * purpose of this test it's not useful to going trough the whole
		 * procedure (satisfy a council...)
		 */
		try {
			p1.moveOnCoinsTrack(4, defaultGameMap.getMaxCoins());
			p1.giveNAssistants(Arrays.asList(new Assistant(),new Assistant(),new Assistant()));
		} catch (NotEnoughCoinsException e) {
			fail();
		}
		p1.moveOnNobilityTrackPosition(7, defaultGameMap.getNobilityTrackSize());
		assertEquals(p1.getNobilityTrackPosition(), 7);
		assertTrue(defaultGameMap.getBonusesForANobilityTrackPosition(p1.getNobilityTrackPosition()).isEmpty());
		// iteration in order to find a city with a "nobility track boost" on it
		// then i will choose the permit accordingly

		City cityToBuildOn = null;
		for (City city : game.getCityList()) {
			for (RewardToken reward : game.getRewardTokensOn(city)) {
				for (Bonus bonus : reward.getBonuses())
					if (bonus.getBonusType() == BonusType.NOBILITY && bonus.getNumberOfTimes() == 1) {
						cityToBuildOn = city;
						break;
					}
			}
		}
		// looking for a permit tile that can build on that city
		Region region = cityToBuildOn.getCityRegion();
		List<Permit> faceUps = game.showFaceUpPermitTiles(region);
		while (!faceUps.get(0).getCities().contains(cityToBuildOn)
				&& !faceUps.get(1).getCities().contains(cityToBuildOn)) {
			try {
				game.changeBusinessPermitTiles(p1, region);
			} catch (NotEnoughAssistantsException e) {
				fail();
			}
			faceUps = game.showFaceUpPermitTiles(region);
		}
		int index = (faceUps.get(0).getCities().contains(cityToBuildOn)) ? 1 : 2;
		assertTrue(index == 1 || index == 2);
		try {
			game.acquireAPermitTile(p1, region, index, cheatPolitics);
		} catch (Exception e) {
			fail();
		}
		// int oldPolitics = p1.getPoliticsHand().size();
		List<Politic> oldPolitics = new ArrayList<>(p1.getPoliticsHand());
		int oldVictories = p1.getVictoryPoints();
		MainAction a = new BuildAnEmporiumUsingAPermitTileAction(game, p1, cityToBuildOn,
				p1.getPermitsAcquired().get(0), null);
		try {
			a.execute();
		} catch (ActionExecutionException e) {
			fail();
		}
		assertTrue(game.getPlayersWithAnEmporiumOn(cityToBuildOn).contains(p1));
		assertTrue(a.hasBeenExecuted());
		// surely on that nobilitytrack position there was 3 victory and 1
		// politic, but what else
		int newPolitics = 1;
		int newVictories = 3;
		List<Bonus> bonuses = new ArrayList<>();
		for (RewardToken r : game.getRewardTokensOn(cityToBuildOn))
			bonuses.addAll(r.getBonuses());
		for (Bonus b : bonuses) {
			if (b.getBonusType() == BonusType.POLITIC)
				newPolitics += b.getNumberOfTimes();
			else if (b.getBonusType() == BonusType.VICTORY_POINT)
				newVictories += b.getNumberOfTimes();
		}
		assertEquals(p1.getNobilityTrackPosition(), 8);
		assertEquals(p1.getPoliticsHand().size(), oldPolitics.size() + newPolitics);
		assertEquals(p1.getVictoryPoints(), oldVictories + newVictories);
		assertTrue(p1.getPoliticsHand().containsAll(oldPolitics));
		assertFalse(oldPolitics.containsAll(p1.getPoliticsHand()));
	}

	@Test
	public void testBonusWithMainAction() throws GameFinishedException {
		int numberOfPlayers = 4;
		List<PlayerToken> tokens = new ArrayList<>();
		List<String> usernames = new ArrayList<String>();
		for (int i = 1; i <= numberOfPlayers; i++) {
			usernames.add(Integer.toString(i));
			tokens.add(new PlayerToken(Integer.toString(i)));
		}
		GameMap defaultGameMap = new GameMapFactory().getGameMap("default");
		Game game = new Game(defaultGameMap, usernames);
		TurnMachine states = new TurnMachine(game);
		/*
		 * player 1 has somehow to have a permit tile in his hands, for the
		 * purpose of this test it's not useful to going trough the whole
		 * procedure (satisfy a council...)
		 */
		// only for statesmanager
		DrawAPoliticAction draw = new DrawAPoliticAction(null, null);
		states.changeState(states.getCurrentPlayer(), draw);
		Player p1 = game.getPlayerByUsername(states.getCurrentPlayer());
		List<Politic> cheatPolitics = new ArrayList<Politic>();
		for (int i = 0; i < 4; i++)
			cheatPolitics.add(new Politic(PoliticColor.MULTI_COLORED));
		p1.getPoliticsHand().addAll(cheatPolitics);
		try {
			p1.moveOnCoinsTrack(4, defaultGameMap.getMaxCoins());
		} catch (NotEnoughCoinsException e) {
			fail();
		}
		p1.moveOnNobilityTrackPosition(5, defaultGameMap.getNobilityTrackSize());
		assertEquals(p1.getNobilityTrackPosition(), 5);
		assertTrue(defaultGameMap.getBonusesForANobilityTrackPosition(p1.getNobilityTrackPosition()).isEmpty());
		// iteration in order to find a city with a "nobility track boost" on it
		// then i will choose the permit accordingly

		City cityToBuildOn = null;
		for (City city : game.getCityList()) {
			for (RewardToken reward : game.getRewardTokensOn(city)) {
				for (Bonus bonus : reward.getBonuses())
					if (bonus.getBonusType() == BonusType.NOBILITY && bonus.getNumberOfTimes() == 1) {
						cityToBuildOn = city;
						break;
					}
			}
		}
		// looking for a permit tile that can build on that city
		Region region = cityToBuildOn.getCityRegion();

		List<Permit> faceUps = game.showFaceUpPermitTiles(region);
		while (!faceUps.get(0).getCities().contains(cityToBuildOn)
				&& !faceUps.get(1).getCities().contains(cityToBuildOn)) {
			try {
				game.changeBusinessPermitTiles(p1, region);
				p1.giveAnAssistant(new Assistant());
			} catch (NotEnoughAssistantsException e) {
				fail();
			}
			faceUps = game.showFaceUpPermitTiles(region);
		}
		int index = (faceUps.get(0).getCities().contains(cityToBuildOn)) ? 1 : 2;
		assertTrue(index == 1 || index == 2);
		try {
			game.acquireAPermitTile(p1, region, index, cheatPolitics);
		} catch (Exception e) {
			fail();
		}
		MainAction main = new AcquireABusinessPermitTileAction(null, null, null, 0, null, null);
		QuickAction quick = new EmptyQuickAction(null, null);
		assertTrue(states.getState(states.getCurrentPlayer()).isAllowedToExecute(main));
		assertTrue(states.getState(states.getCurrentPlayer()).isAllowedToExecute(quick));
		MainAction a = new BuildAnEmporiumUsingAPermitTileAction(game, p1, cityToBuildOn,
				p1.getPermitsAcquired().get(0), states);
		try {
			a.execute();
		} catch (ActionExecutionException e) {
			fail();
		}
		assertTrue(a.hasBeenExecuted());
		states.changeState(states.getCurrentPlayer(), a);
		assertTrue(states.getState(states.getCurrentPlayer()).isAllowedToExecute(main));
		assertTrue(states.getState(states.getCurrentPlayer()).isAllowedToExecute(quick));

	}
}
