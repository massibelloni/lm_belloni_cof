package it.polimi.ingsw.lm_belloni_cof.test.model;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.exceptions.CitiesNotConnectedException;
import it.polimi.ingsw.lm_belloni_cof.model.Bonus;
import it.polimi.ingsw.lm_belloni_cof.model.BonusType;
import it.polimi.ingsw.lm_belloni_cof.model.City;
import it.polimi.ingsw.lm_belloni_cof.model.CityColor;
import it.polimi.ingsw.lm_belloni_cof.model.GameMap;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.KingRewardTileContent;
import it.polimi.ingsw.lm_belloni_cof.model.PermitContent;
import it.polimi.ingsw.lm_belloni_cof.model.Region;

public class GameMapTest {

	@Test
	public void testCity() {
		GameMap defaultMap = new GameMapFactory().getGameMap("default");
		assertEquals(defaultMap.getCityList().size(), 15);
		List<City> cities = defaultMap.getCityList();
		City kingStart = defaultMap.getStartingKingLocation();
		assertEquals(kingStart.getName(), "Juvelar");
		City juvelar = kingStart;
		City castrum = cities.get(2);
		assertEquals(castrum.getName(), "Castrum");
		assertFalse(defaultMap.areCitiesDirectlyConnected(castrum, kingStart));
		try {
			assertEquals(defaultMap.getRoadsBetweenCities(castrum, juvelar), 3);
		} catch (CitiesNotConnectedException e) {
			fail();
		}
		List<City> yellowCities = defaultMap.getCitiesByCityColor(CityColor.YELLOW);
		assertTrue(cities.containsAll(yellowCities));
		List<City> coastCities = defaultMap.getCitiesByRegion(Region.COAST);
		assertTrue(cities.containsAll(coastCities));
	}

	@Test
	public void testRewardTokens() {
		GameMap defaultMap = new GameMapFactory().getGameMap("default");
		int totalRewardsNeeded = 0;
		for (City city : defaultMap.getCityList())
			totalRewardsNeeded += city.getBonusNumber();
		assertEquals(totalRewardsNeeded, defaultMap.getRewardTokens().size());
	}

	@Test
	public void testKingRewardsOrder() {
		GameMap defaultMap = new GameMapFactory().getGameMap("default");
		int previous = 0;
		List<KingRewardTileContent> kingRewardTileContents = defaultMap.getKingRewardTileContents();
		for (KingRewardTileContent kingRewardTileContent : kingRewardTileContents)
			assertEquals(kingRewardTileContent.getPickingOrder(), ++previous);
	}

	@Test
	public void testPermitContent() {
		GameMap defaultMap = new GameMapFactory().getGameMap("default");
		List<PermitContent> coastPermitContents = defaultMap.getPermitContentsByRegion(Region.COAST);
		List<City> coastCities = defaultMap.getCitiesByRegion(Region.COAST);
		for (PermitContent permitContent : coastPermitContents)
			assertTrue(coastCities.containsAll(permitContent.getCities()));
	}

	@Test
	public void nobilityTrackTest() {
		GameMap defaultMap = new GameMapFactory().getGameMap("default");
		for (int i = 0; i < defaultMap.getNobilityTrackSize(); i++) {
			try {
				List<Bonus> bonuses = defaultMap.getBonusesForANobilityTrackPosition(i);
				bonuses.add(new Bonus(BonusType.ASSISTANT, 2));
				fail();
			} catch (UnsupportedOperationException e) {

			}
		}
	}

}
