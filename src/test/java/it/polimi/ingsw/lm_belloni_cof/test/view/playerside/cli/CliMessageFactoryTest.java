package it.polimi.ingsw.lm_belloni_cof.test.view.playerside.cli;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidCommandException;
import it.polimi.ingsw.lm_belloni_cof.messages.ChatRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.PoliticsRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.RequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.AcquireABusinessPermitTileActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.BuildAnEmporiumUsingAPermitTileActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.ChangeBusinessPermitTilesActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.CityTokenResponseActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.DrawAPoliticActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.ElectACouncillorActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.EmptyQuickActionActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.EndMarketBuyActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.EndMarketOfferActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.EngageAnAssistantActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.FaceUpPermitResponseActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.MarketBuyActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.MarketOfferActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.OwnedPermitResponseActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.PerformAnAdditionalMainActionActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.SendAnAssistantToElectACouncillorActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.cli.CliMessageFactory;

public class CliMessageFactoryTest {
	
	@Test
	public void testChat() {
		String cmd = "chat test";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof ChatRequestMessage);
			assertEquals(((ChatRequestMessage) req).getMessage(), "test");
		} catch (InvalidCommandException e) {
			fail();
		}
	}

	@Test
	public void testAcquireOK() {
		String cmd = "acquire permit coast 1 orange black";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof AcquireABusinessPermitTileActRqstMsg);
			assertEquals(((AcquireABusinessPermitTileActRqstMsg) req).getRegion(), "coast");
			assertEquals(((AcquireABusinessPermitTileActRqstMsg) req).getIndexInRegion(), 1);
			assertTrue(((AcquireABusinessPermitTileActRqstMsg) req).getPoliticColors().contains("orange"));
		} catch (InvalidCommandException e) {
			fail();
		}
	}

	@Test
	public void testAcquireInvalidPosition() {
		String cmd = "acquire permit coast 3 orange black";
		try {
			CliMessageFactory.getRequestMessage(cmd, null);
			fail();
		} catch (InvalidCommandException e) {
			assertNotNull(e);
		}
	}

	@Test
	public void testAcquireInvalidPolitics() {
		String cmd = "acquire permit coast 3 orange black black black black black";
		try {
			CliMessageFactory.getRequestMessage(cmd, null);
			fail();
		} catch (InvalidCommandException e) {
			assertNotNull(e);
		}
	}

	@Test
	public void testBuildPermitOK() {
		String cmd = "build emporium permit arkon 1";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof BuildAnEmporiumUsingAPermitTileActRqstMsg);
			assertEquals(((BuildAnEmporiumUsingAPermitTileActRqstMsg) req).getCity(), "arkon");
			assertEquals(((BuildAnEmporiumUsingAPermitTileActRqstMsg) req).getPermit(), "1");
		} catch (InvalidCommandException e) {
			fail();
		}
	}

	@Test
	public void testBuildPermitKO() {
		String cmd = "build emporium permit arkon 1 9";
		try {
			CliMessageFactory.getRequestMessage(cmd, null);
			fail();
		} catch (InvalidCommandException e) {
			assertNotNull(e);
		}
	}
	
	@Test
	public void testBuildKingOK() {
		String cmd = "build emporium KING arkon black orange";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg);
			assertEquals(((BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg) req).getCity(), "arkon");
			assertTrue(((BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg) req).getPoliticColors().contains("black"));
		} catch (InvalidCommandException e) {
			fail();
		}
	}
	
	@Test
	public void testBuildKingKO() {
		String cmd = "build emporium KING arkon black orange black black black";
		try {
			CliMessageFactory.getRequestMessage(cmd, null);
			fail();
		} catch (InvalidCommandException e) {
			assertNotNull(e);
		}
	}
	
	@Test
	public void testChangeTilesOK(){
		String cmd = "change permit tiles mountains";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof ChangeBusinessPermitTilesActRqstMsg);
			assertEquals(((ChangeBusinessPermitTilesActRqstMsg) req).getRegion(), "mountains");
		} catch (InvalidCommandException e) {
			fail();
		}
	}
	
	@Test
	public void testChangeTilesKO(){
		String cmd = "change permit tiles mountains error";
		try {
			CliMessageFactory.getRequestMessage(cmd, null);
			fail();
		} catch (InvalidCommandException e) {
			assertNotNull(e);
		}
	}
	
	@Test
	public void testElectCouncillorOK(){
		String cmd = "elect councillor coast orange";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof ElectACouncillorActRqstMsg);
			assertEquals(((ElectACouncillorActRqstMsg) req).getRegion(), "coast");
			assertEquals(((ElectACouncillorActRqstMsg) req).getCouncillorColor(), "orange");
		} catch (InvalidCommandException e) {
			fail();
		}
	}
	
	@Test
	public void testElectCouncillorKO(){
		String cmd = "elect councillor coast";
		try {
			CliMessageFactory.getRequestMessage(cmd, null);
			fail();
		} catch (InvalidCommandException e) {
			assertNotNull(e);
		}
	}
	
	@Test
	public void testDrawPolitic(){
		String cmd = "draw politic";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof DrawAPoliticActRqstMsg);
		} catch (InvalidCommandException e) {
			fail();
		}
	}
	
	@Test
	public void testBonusesFromCityTokenOK(){
		String cmd = "bonuses FROM city TOKEN arkon";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof CityTokenResponseActRqstMsg);
			assertEquals(((CityTokenResponseActRqstMsg) req).getCity(), "arkon");
		} catch (InvalidCommandException e) {
			fail();
		}
	}
	
	@Test
	public void testBonusesFromCityTokenKO(){
		String cmd = "bonuses FROM city TOKEN arkon hesti";
		try {
			CliMessageFactory.getRequestMessage(cmd, null);
			fail();
		} catch (InvalidCommandException e) {
			assertNotNull(e);
		}
	}
	
	@Test
	public void testEmptyQuick(){
		String cmd = "drop";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof EmptyQuickActionActRqstMsg);
		} catch (InvalidCommandException e) {
			fail();
		}
	}
	
	@Test
	public void testEndMarketOffer(){
		String cmd = "end market offer";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof EndMarketOfferActRqstMsg);
		} catch (InvalidCommandException e) {
			fail();
		}
	}
	
	@Test
	public void testEndMarketBuy(){
		String cmd = "end market buy";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof EndMarketBuyActRqstMsg);
		} catch (InvalidCommandException e) {
			fail();
		}
	}
	
	@Test
	public void testEndMarketKO(){
		String cmd = "end market";
		try {
			CliMessageFactory.getRequestMessage(cmd, null);
			fail();
		} catch (InvalidCommandException e) {
			assertNotNull(e);
		}
	}
	
	@Test
	public void testEngageAssistant(){
		String cmd = "engage assistant";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof EngageAnAssistantActRqstMsg);
		} catch (InvalidCommandException e) {
			fail();
		}
	}
	
	@Test
	public void testOwnedPermitOK(){
		String cmd = "bonuses from owned permit 1";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof OwnedPermitResponseActRqstMsg);
			assertEquals(((OwnedPermitResponseActRqstMsg) req).getPermit(), "1");
		} catch (InvalidCommandException e) {
			fail();
		}
	}
	
	@Test
	public void testFaceUPPermitOK(){
		String cmd = "bonuses from faceUP permit coast 1";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof FaceUpPermitResponseActRqstMsg);
			assertEquals(((FaceUpPermitResponseActRqstMsg) req).getRegion(), "coast");
			assertEquals(((FaceUpPermitResponseActRqstMsg) req).getIndexInRegion(), 1);
		} catch (InvalidCommandException e) {
			fail();
		}
	}
	
	@Test
	public void testFaceUPPermitKO1(){
		String cmd = "bonuses from faceUP permit coast";
		try {
			CliMessageFactory.getRequestMessage(cmd, null);
			fail();
		} catch (InvalidCommandException e) {
			assertNotNull(e);
		}
	}
	
	@Test
	public void testFaceUPPermitKO2(){
		String cmd = "bonuses from faceUP permit coast one";
		try {
			CliMessageFactory.getRequestMessage(cmd, null);
			fail();
		} catch (InvalidCommandException e) {
			assertNotNull(e);
		}
	}
	
	@Test
	public void testSendAssistantToElectOK(){
		String cmd = "send assistant elect councillor coast orange";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof SendAnAssistantToElectACouncillorActRqstMsg);
			assertEquals(((SendAnAssistantToElectACouncillorActRqstMsg) req).getRegion(), "coast");
			assertEquals(((SendAnAssistantToElectACouncillorActRqstMsg) req).getCouncillorColor(), "orange");
		} catch (InvalidCommandException e) {
			fail();
		}
	}
	
	@Test
	public void testOfferPolitic(){
		String cmd = "offer politic black 1";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof MarketOfferActRqstMsg);
			assertEquals(((MarketOfferActRqstMsg) req).getForSale(), "politic black");
			assertEquals(((MarketOfferActRqstMsg) req).getPrice(), 1);
		} catch (InvalidCommandException e) {
			fail();
		}
	}
	
	@Test
	public void testOfferAssistant(){
		String cmd = "offer assistant 7";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof MarketOfferActRqstMsg);
			assertEquals(((MarketOfferActRqstMsg) req).getForSale(), "assistant");
			assertEquals(((MarketOfferActRqstMsg) req).getPrice(), 7);
		} catch (InvalidCommandException e) {
			fail();
		}
	}
	
	@Test
	public void testOfferPermit(){
		String cmd = "offer permit 1 7";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof MarketOfferActRqstMsg);
			assertEquals(((MarketOfferActRqstMsg) req).getForSale(), "permit 1");
			assertEquals(((MarketOfferActRqstMsg) req).getPrice(), 7);
		} catch (InvalidCommandException e) {
			fail();
		}
	}
	
	@Test
	public void testOfferPriceKO(){
		String cmd = "offer permit 1 seven";
		try {
			CliMessageFactory.getRequestMessage(cmd, null);
			fail();
		} catch (InvalidCommandException e) {
			assertNotNull(e);
		}
	}


	@Test
	public void testBuyOK(){
		String cmd = "buy user 1";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof MarketBuyActRqstMsg);
			assertEquals(((MarketBuyActRqstMsg) req).getSeller(), "user");
			assertEquals(((MarketBuyActRqstMsg) req).getIndexInSellerOffers(), 1);
		} catch (InvalidCommandException e) {
			fail();
		}
	}
	
	@Test
	public void testBuyKOPrice(){
		String cmd = "buy user one";
		try {
			CliMessageFactory.getRequestMessage(cmd, null);
			fail();
		} catch (InvalidCommandException e) {
			assertNotNull(e);
		}
	}
	
	@Test
	public void testBuyKOParam(){
		String cmd = "buy user";
		try {
			CliMessageFactory.getRequestMessage(cmd, null);
			fail();
		} catch (InvalidCommandException e) {
			assertNotNull(e);
		}
	}
	
	@Test
	public void testSendAssistantToElectKO(){
		String cmd = "send assistant elect councillor coast";
		try {
			CliMessageFactory.getRequestMessage(cmd, null);
			fail();
		} catch (InvalidCommandException e) {
			assertNotNull(e);
		}
	}
	
	
	@Test
	public void testAdditionalMain(){
		String cmd = "additional main action";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof PerformAnAdditionalMainActionActRqstMsg);
		} catch (InvalidCommandException e) {
			fail();
		}
	}
	
	@Test
	public void testPoliticHand(){
		String cmd = "politic hand";
		try {
			RequestMessage req = CliMessageFactory.getRequestMessage(cmd, null);
			assertTrue(req instanceof PoliticsRequestMessage);
		} catch (InvalidCommandException e) {
			fail();
		}
	}
	
	
	
	@Test
	public void testInvalidCommand(){
		String cmd = "do something you don't know";
		try {
			CliMessageFactory.getRequestMessage(cmd, null);
			fail();
		} catch (InvalidCommandException e) {
			assertNotNull(e);
		}
	}
	
	
	

}
