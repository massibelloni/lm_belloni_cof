package it.polimi.ingsw.lm_belloni_cof.exceptions;
/**
 * Exceptions thrown if a {@link Player} try to re-use an already used {@link Usable}
 *
 */
public class UsableAlreadyUsedException extends InvalidActionException{
	private static final long serialVersionUID = 1L;

}
