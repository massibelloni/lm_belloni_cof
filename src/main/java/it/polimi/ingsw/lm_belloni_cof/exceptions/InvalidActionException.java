package it.polimi.ingsw.lm_belloni_cof.exceptions;
/**
 * Generic exception thrown if an action can't be performed for any reason
 * @author fabioballabio
 *
 */
public class InvalidActionException extends ActionException {
	private static final long serialVersionUID = 1L;

	public InvalidActionException() {
		super();
	}

	public InvalidActionException(Exception e) {
		super(e);
	}
	
	public InvalidActionException(String msg){
		super(msg);
	}
}
