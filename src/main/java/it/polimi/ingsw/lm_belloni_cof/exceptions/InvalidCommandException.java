package it.polimi.ingsw.lm_belloni_cof.exceptions;
/**
 * Exception thrown if a {@link Player} submit a wrong command during a {@link Game} played via cli
 *
 */
public class InvalidCommandException extends Exception{
	private static final long serialVersionUID = 1L;
	public InvalidCommandException(String msg){
		super(msg);
	}
	public InvalidCommandException(String msg,Exception e){
		super(msg,e);
	}
}
