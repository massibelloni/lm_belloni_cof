package it.polimi.ingsw.lm_belloni_cof.exceptions;
/**
 * Exception thrown if someone submit a new {@link GameMap} configuration file not well formatted
 *
 */
public class GameMapFormatException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public GameMapFormatException(String msg, Exception e) {
		super(msg, e);
	}

	public GameMapFormatException(String msg) {
		super(msg);
	}
}
