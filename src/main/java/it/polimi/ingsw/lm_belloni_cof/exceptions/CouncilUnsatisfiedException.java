package it.polimi.ingsw.lm_belloni_cof.exceptions;
/**
 * Exception thrown if the submitted {@link PoliticCard}s to satisfied the council don't match the {@link PoliticColor}s 
 * on the Ç@link Balcony}
 *
 */
public class CouncilUnsatisfiedException extends ActionException {
	private static final long serialVersionUID = 1L;
}
