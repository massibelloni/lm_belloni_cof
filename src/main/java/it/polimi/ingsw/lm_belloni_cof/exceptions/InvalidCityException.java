package it.polimi.ingsw.lm_belloni_cof.exceptions;
/**
 * Exception thrown for any reason regarding {@link City}
 *
 */
public class InvalidCityException extends InvalidActionException {
	private static final long serialVersionUID = 1L;

	public InvalidCityException() {
		super();
	}

	public InvalidCityException(CitiesNotConnectedException e) {
		super(e);
	}
}
