package it.polimi.ingsw.lm_belloni_cof.exceptions;
/**
 *  Exception thrown if a {@link Player} hasn't enough coins to complete an action
 *
 */
public class NotEnoughCoinsException extends ActionException{
	private static final long serialVersionUID = 1L;
}
