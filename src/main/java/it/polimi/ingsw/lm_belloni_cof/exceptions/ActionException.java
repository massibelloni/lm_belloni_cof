package it.polimi.ingsw.lm_belloni_cof.exceptions;
/**
 * Exception related to a problem occured during the attempt of perform an action
 *
 */
public class ActionException extends Exception {
	private static final long serialVersionUID = 1L;

	public ActionException() {
		super();
	}

	public ActionException(Exception e) {
		super(e);
	}

	public ActionException(String msg) {
		super(msg);
	}
}
