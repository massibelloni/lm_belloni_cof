package it.polimi.ingsw.lm_belloni_cof.exceptions;
/**
 * Exception thrown if a {@link Player} indicated in any request is not in this {@link Game}
 *
 */
public class NoPlayerFoundException extends ActionException {
	private static final long serialVersionUID = 1L;

}
