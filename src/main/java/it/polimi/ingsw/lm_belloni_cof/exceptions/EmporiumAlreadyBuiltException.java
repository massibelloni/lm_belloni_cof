package it.polimi.ingsw.lm_belloni_cof.exceptions;
/**
 * Exception thrown if a {@link Player} tries to build an emporium on a {@link City} where he has already built
 * @author fabioballabio
 *
 */
public class EmporiumAlreadyBuiltException extends InvalidActionException {

	private static final long serialVersionUID = 1L;

}
