package it.polimi.ingsw.lm_belloni_cof.exceptions;
/**
 * Exception thrown if there's no path between two cities of the currently loaded {@link GameMap} 
 * @author fabioballabio
 *
 */
public class CitiesNotConnectedException extends ActionException {
	private static final long serialVersionUID = 1L;

}
