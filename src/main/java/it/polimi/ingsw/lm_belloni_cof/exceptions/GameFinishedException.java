package it.polimi.ingsw.lm_belloni_cof.exceptions;
/**
 * Exception thrown if a {@link Player} finishes his number of emporiums to built.
 * After this exception is thrown each {@link Player} completes his own turn and than the {@link Game} ends 
 * with the calculation of the final scores
 *
 */
public class GameFinishedException extends ActionException{
	private static final long serialVersionUID = 1L;
}
