package it.polimi.ingsw.lm_belloni_cof.exceptions;
/**
 * Exception related to a problem occured during the attempt of perform an action
 *
 */
public class ActionExecutionException extends Exception {
	private static final long serialVersionUID = 1L;

	public ActionExecutionException(String msg, ActionException e) {
		super(msg, e);
	}

	public ActionExecutionException(String msg) {
		super(msg);
	}
}
