package it.polimi.ingsw.lm_belloni_cof.exceptions;
/**
 * Exception thrown if a {@link Player} hasn't enough {@link Assistant}s to complete an action
 *
 */
public class NotEnoughAssistantsException extends ActionException{
	private static final long serialVersionUID = 1L;
}
