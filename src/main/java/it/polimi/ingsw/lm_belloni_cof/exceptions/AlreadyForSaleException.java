package it.polimi.ingsw.lm_belloni_cof.exceptions;
/**
 * Exception thrown when a {@link Player} try to offer an already for sale {@link Ownable}
 *
 */
public class AlreadyForSaleException extends InvalidActionException {
	private static final long serialVersionUID = 1L;

}
