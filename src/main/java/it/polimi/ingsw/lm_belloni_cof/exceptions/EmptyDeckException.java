package it.polimi.ingsw.lm_belloni_cof.exceptions;
/**
 * Exception thrown if a {@link Player} try to draw a card from an empty deck
 *
 */
public class EmptyDeckException extends ActionException{
	private static final long serialVersionUID = 1L;
}
