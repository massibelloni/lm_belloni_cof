package it.polimi.ingsw.lm_belloni_cof.exceptions;
/**
 * Exception thrown if a message is invalid or not correctly formatted
 *
 */
public class InvalidMessageException extends Exception {
	private static final long serialVersionUID = 1L;

	public InvalidMessageException(String msg, Exception e) {
		super(msg, e);
	}

	public InvalidMessageException(String msg) {
		super(msg);
	}
}
