package it.polimi.ingsw.lm_belloni_cof.exceptions;
/**
 * Exception thrown if an {@link Ownable} and his own {@link Player} owner don't coincide
 *
 */
public class OwnableNotOwnedException extends InvalidActionException{
	private static final long serialVersionUID = 1L;

}
