package it.polimi.ingsw.lm_belloni_cof.view.gameside;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.view.shared.BrokerInterface;

/** Socket server handling the Published-Subscriber protocol. */
public class BrokerSocketServer extends Thread {
	private static final Logger logger = Logger.getLogger(BrokerSocketServer.class.getName());
	private int port;
	private final BrokerInterface broker;
	private ExecutorService executor;
	private boolean isRunning;

	/**
	 * Initialize the server.
	 * 
	 * @param port
	 *            the port where this server will run
	 * @param executor
	 *            a pool of threads that will handle the clients connections
	 * @param broker
	 *            the {@link BrokerInterface} used to manage subscriptions.
	 */
	public BrokerSocketServer(int port, ExecutorService executor, BrokerInterface broker) {
		this.port = port;
		this.broker = broker;
		this.executor = executor;
		this.isRunning = true;
	}

	public void stopServer() {
		this.isRunning = false;
	}

	/**
	 * Core method of the server that accepts connections creating new
	 * {@link SubscriberHandler} to handle them.
	 */
	@Override
	public void run() {

		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(this.port); //NOSONAR
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error during broker socket server initialization.", e);
			return;
		}
		try {
			while (isRunning) {
				Socket socket = serverSocket.accept();
				this.executor.execute(new SubscriberHandler(socket, this.broker));

			}
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error during broker socket server execution.", e);
		} finally {
			try {
				serverSocket.close();
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Error while trying to close broker socket.", e);
			}
		}

	}
}
