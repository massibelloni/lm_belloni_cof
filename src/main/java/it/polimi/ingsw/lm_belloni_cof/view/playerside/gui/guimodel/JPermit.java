package it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.guimodel;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

import org.json.JSONObject;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.GUIMessageVisitor;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.OverlapLayout;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.StretchIcon;

/**
 * Swing element representing a permit tile dinamically created based on cities
 * and bonuses on it.
 */
public class JPermit extends JPanel {
	private static final long serialVersionUID = 1L;
	private JLabel emptyCard = null;
	private JPanel overlay = null;

	/**
	 * Public constructor of a JPermit
	 * 
	 * @param permit
	 *            a json formatted permit
	 */
	public JPermit(JSONObject permit) {
		super(new OverlapLayout(new Point(0, 0)));
		this.emptyCard = new JLabel(new StretchIcon("graphics/permits/empty-permit.png"));
		this.overlay = new JPanel(new GridLayout(4, 1));
		JPanel padding = new JPanel();
		padding.setOpaque(false);
		overlay.add(padding);
		overlay.setOpaque(false);
		/* retrieving data from the json */
		String cityNames = "";
		for (Object city : permit.getJSONArray("cities"))
			cityNames += Character.toUpperCase(((String) city).charAt(0)) + "/";
		/* removing last / */
		cityNames = cityNames.substring(0, cityNames.length() - 1);
		// generating bonuses map
		Map<String, Integer> bonuses = new HashMap<>();
		for (Object jsonBonus : permit.getJSONArray("bonuses")) {
			JSONObject bonus = (JSONObject) jsonBonus;
			bonuses.put(bonus.getString("bonus"), bonus.getInt("times"));
		}
		/* actually building permit */
		JLabel citiesList = new JLabel(cityNames, SwingConstants.CENTER);
		citiesList.setForeground(Color.BLACK);
		if (permit.getJSONArray("cities").length() < 3)
			citiesList.setFont(GUIMessageVisitor.LUCIDA_BLACKLETTER.deriveFont(Font.BOLD, 20));
		else
			citiesList.setFont(GUIMessageVisitor.LUCIDA_BLACKLETTER.deriveFont(Font.BOLD, 15));
		overlay.add(citiesList);
		JPanel bonusesPanel = new JPanel(new GridLayout(1, 0));
		bonusesPanel.setOpaque(false);
		for (Map.Entry<String, Integer> entry : bonuses.entrySet())
			bonusesPanel.add(new JBonus(entry.getKey(), entry.getValue()));
		overlay.add(bonusesPanel);
		padding = new JPanel();
		padding.setOpaque(false);
		overlay.add(padding);
		super.add(emptyCard);
		super.add(overlay);
	}
}
