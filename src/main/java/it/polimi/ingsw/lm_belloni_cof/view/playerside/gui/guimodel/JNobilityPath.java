package it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.guimodel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.CirclePane;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.JPadding;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.OverlapLayout;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.StretchIcon;

/**
 * Swing element representing the nobility track.
 */
public class JNobilityPath extends JFrame {
	private final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	private CirclePane[] markers;
	private static final long serialVersionUID = 1L;

	public JNobilityPath(JSONArray nobilityPath) {
		super("Nobility Track");
		this.markers = new CirclePane[nobilityPath.length()];
		super.setLayout(new GridLayout(1, 0));
		super.setBackground(Color.DARK_GRAY);
		int nobilityPosition = 0;
		for (Object jsonPosition : nobilityPath) {
			JSONArray position = (JSONArray) jsonPosition;
			this.markers[nobilityPosition] = new CirclePane();
			this.markers[nobilityPosition].setVisible(false);
			super.add(new JNobilityPosition(position, this.markers[nobilityPosition]));
			nobilityPosition++;
		}
		super.setPreferredSize(new Dimension(screenSize.width * 14 / 16, screenSize.height * 1 / 5));
		super.pack();
		super.setLocationRelativeTo(null);
		super.setResizable(false);
	}

	/**
	 * Color the position passed as an argument with the color passed. This
	 * methods tries to graphically represent a player placed in a certain
	 * position on the track.
	 * 
	 * @param color
	 *            the color of the player
	 * @param pos
	 *            the position that has to be colored
	 */
	public void setPlayerPosition(Color color, int pos) {
		/*
		 * looking for that color (if there) and erasing it, then color the new
		 * pos
		 */
		for (int i = 0; i < this.markers.length; i++)
			if (this.markers[i].getBackground() == color) {
				this.markers[i].setBackground(null);
				this.markers[i].setVisible(false);
				break;
			}
		this.markers[pos].setBackground(color);
		this.markers[pos].setVisible(true);
	}
}

/** Swing object representing a single cell in the track. */
class JNobilityPosition extends JPanel {
	private static final long serialVersionUID = 1L;

	public JNobilityPosition(JSONArray bonuses, CirclePane marker) {
		super(new OverlapLayout(new Point(0, 0)));
		super.setBackground(Color.darkGray);
		super.add(new JLabel(new StretchIcon("graphics/nobility-empty.png")));
		JPanel bonusesPanel = new JPanel(new GridLayout(0, 1));
		bonusesPanel.setOpaque(false);
		bonusesPanel.add(new JPadding());
		for (Object jsonBonus : bonuses) {
			JSONObject bonus = (JSONObject) jsonBonus;
			bonusesPanel.add(new JBonus(bonus.getString("bonus"), bonus.getInt("times")));
		}
		bonusesPanel.add(new JPadding());
		super.add(bonusesPanel);
		super.add(marker);
	}
}
