package it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.listeners;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import it.polimi.ingsw.lm_belloni_cof.messages.PoliticsResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.EmptyMessageVisitor;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.StretchIcon;
import net.miginfocom.swing.MigLayout;

/**
 * Abstract class that helps a listener in retrieving additional informations
 * about politics to use for specifying an action request.
 */
public class CouncilSatisfierVisitor extends EmptyMessageVisitor {
	/** Text to show in the main button of the frame. */
	private String textToShow;
	private ActionRequestProcessor processor;
	/* GRAPHICS */
	private final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	private JFrame politicsFrame;
	private JCheckBox[] politicsCheckBoxes;

	public CouncilSatisfierVisitor(String textToShow, ActionRequestProcessor processor) {
		this.textToShow = textToShow;
		this.processor = processor;
	}

	/**
	 * Visits a {@link PoliticsResponseMessage} letting the user to choose which
	 * politics to use in a game action.
	 */
	@Override
	public void display(PoliticsResponseMessage msg) {
		// showing up a frame
		this.politicsFrame = new JFrame("Choose politic cards");
		this.politicsFrame.setLayout(new MigLayout("fill"));
		JPanel politics = new JPanel(new GridLayout(1, 0));
		this.politicsCheckBoxes = new JCheckBox[msg.getPoliticColors().size()];
		for (int i = 0; i < msg.getPoliticColors().size(); i++) {
			this.politicsCheckBoxes[i] = new JCheckBox();
			this.politicsCheckBoxes[i].setName(msg.getPoliticColors().get(i));
			this.politicsCheckBoxes[i]
					.setIcon(new StretchIcon("graphics/politics/" + msg.getPoliticColors().get(i) + "-politic.png"));
			this.politicsCheckBoxes[i].setSelectedIcon(new StretchIcon(
					"graphics/politics/" + msg.getPoliticColors().get(i) + "-politic.png", (float) 0.5));
			politics.add(this.politicsCheckBoxes[i]);
		}
		politicsFrame.add(politics, "w 100%,h 90%,wrap");
		JButton acquire = new JButton(textToShow);
		acquire.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				/* retrieving politic colors by checkboxes */
				List<String> politicColors = new ArrayList<>();
				for (int i = 0; i < politicsCheckBoxes.length; i++) {
					if (politicsCheckBoxes[i].isSelected())
						politicColors.add(politicsCheckBoxes[i].getName());
				}
				politicsFrame.setVisible(false);
				processor.processActionRequest(politicColors);
			}
		});
		politicsFrame.add(acquire, "w 100%,h 10%,wrap");
		politicsFrame.setPreferredSize(new Dimension(screenSize.width * 1 / 2, screenSize.height * 1 / 3));
		politicsFrame.pack();
		politicsFrame.setLocationRelativeTo(null);
		politicsFrame.setVisible(true);
	}
}
