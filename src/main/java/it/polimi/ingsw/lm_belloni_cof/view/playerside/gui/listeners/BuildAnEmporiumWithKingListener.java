package it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.listeners;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.rmi.RemoteException;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
import it.polimi.ingsw.lm_belloni_cof.messages.PoliticsRequestMessage;

import it.polimi.ingsw.lm_belloni_cof.messages.ResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;

import it.polimi.ingsw.lm_belloni_cof.view.shared.RequestProcessor;

/**
 * The {@link MouseListener} that handles a
 * {@link BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg}.
 */
public class BuildAnEmporiumWithKingListener implements MouseListener, ActionRequestProcessor {
	private static final Logger logger = Logger.getLogger(BuildAnEmporiumWithKingListener.class.getName());
	private String city;
	private PlayerToken playerToken;
	private RequestProcessor requestProcessor;
	private MessageVisitor messageVisitor;

	public BuildAnEmporiumWithKingListener(String city, PlayerToken playerToken, RequestProcessor requestProcessor,
			MessageVisitor messageVisitor) {
		this.city = city;
		this.playerToken = playerToken;
		this.requestProcessor = requestProcessor;
		this.messageVisitor = messageVisitor;
	}

	/**
	 * When the mouse event is triggered the listener makes a server request and
	 * delegates its visit to obtain other informations from the user.
	 * 
	 * @see #processActionRequest(List)
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		/*
		 * Making a politics request to get politics hand. Then showing a frame
		 * with politics letting the user to choose.
		 */
		try {
			/*
			 * THIS MESSAGE WON'T BE VISITED BY THE VISITOR PASSED AS AN
			 * ARGUMENT!
			 */
			this.requestProcessor.processRequest(new PoliticsRequestMessage(this.playerToken))
					.visit(new CouncilSatisfierVisitor("Build", this));
		} catch (RemoteException e1) {
			logger.log(Level.SEVERE, "Error during in-build-king politics request.", e1);
		}
	}

	/**
	 * The method that actually makes the request to the server
	 * 
	 * @param colors-a
	 *            list of the politic colors needed and retrieved by
	 *            {@link CouncilSatisfierVisitor}.
	 */
	@Override
	public void processActionRequest(List<String> colors) {
		try {
			BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg msg = new BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg(
					playerToken, city, colors);
			ResponseMessage r = requestProcessor.processRequest(msg);
			/*
			 * THIS MESSAGE WILL BE VISITED BY THE VISITOR PASSED AS AN
			 * ARGUMENT!
			 */
			r.visit(messageVisitor);
		} catch (RemoteException e1) {
			logger.log(Level.SEVERE, "Error during build an emp with help of the king GUI request.", e1);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		/**/

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		/**/

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		/**/

	}

	@Override
	public void mouseExited(MouseEvent e) {
		/**/

	}

}
