package it.polimi.ingsw.lm_belloni_cof.view.playerside;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.messages.BroadcastMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.RequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.SubscribeRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.SubscribeResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.view.shared.BrokerInterface;
import it.polimi.ingsw.lm_belloni_cof.view.shared.ConnectionPorts;
import it.polimi.ingsw.lm_belloni_cof.view.shared.RequestProcessor;
import it.polimi.ingsw.lm_belloni_cof.view.shared.SubscriberInterface;

/**
 * Main client side socket class that works as a stub getting hands dirty with
 * actual socket code hiding details to the user.
 */
public class SocketClient extends Thread implements RequestProcessor, BrokerInterface {
	private static final Logger logger = Logger.getLogger(SocketClient.class.getName());
	/** The address of the socket server */
	private String address;
	private SubscriberInterface subscriberInterface;
	private Socket brokerSocket;
	/**
	 * The "always opened" socket input stream where the
	 * {@link BroadcastMessage}s are sent.
	 */
	private ObjectInputStream broadcasterInputStream;
	/** Thread-safe boolean variable to keep track of the subscription status */
	private AtomicBoolean isSubscribed;

	public SocketClient(String address, SubscriberInterface subscriberInterface)
			throws UnknownHostException, IOException {
		this.address = address;
		this.subscriberInterface = subscriberInterface;
		// Setup the connection to the publisher
		this.brokerSocket = new Socket(address, ConnectionPorts.BROKER_SERVER_PORT);
		this.isSubscribed = new AtomicBoolean(false);
	}

	@Override
	public void subscribe(SubscriberInterface observer, String username) throws RemoteException {
		Object response = null;
		try {
			/* Sending a subscription request to the server waiting for it */
			try {
				synchronized (brokerSocket) {
					ObjectOutputStream broadcasterOutputStream = new ObjectOutputStream(brokerSocket.getOutputStream());
					broadcasterOutputStream.writeObject(new SubscribeRequestMessage(username));
					broadcasterOutputStream.flush();
				}
			} catch (IOException e) {
				throw new RemoteException("Error while writing to the socket.", e);
			}
			/* Waiting for the subscription ack */
			try {

				synchronized (brokerSocket) {
					broadcasterInputStream = new ObjectInputStream(brokerSocket.getInputStream());
					response = broadcasterInputStream.readObject();
					brokerSocket.setSoTimeout(0); // forever
				}
			} catch (IOException e) {
				throw new RemoteException("Error while reading from the socket.", e);
			} catch (ClassNotFoundException e) {
				throw new RemoteException("An invalid message has been returned from the server.", e);
			}
			/* Shutting down the output in a one-way connection */
			try {
				brokerSocket.shutdownOutput();
			} catch (IOException e) {
				throw new RemoteException("Unable to shutdown output of the broker socket connection.", e);
			}
		} catch (RemoteException remoteException) {
			try {
				synchronized (brokerSocket) {
					brokerSocket.close();
				}
			} catch (IOException e) {
				logger.log(Level.SEVERE, "An error occurred while trying to close the broker socket after an error.",
						e);
			}
			throw remoteException;
		}

		if (!(response instanceof SubscribeResponseMessage)) {
			logger.log(Level.SEVERE, "A subscription response hasn't been received after a subscription request.");
		} else
			this.setSubscribed(true);
	}

	private void setSubscribed(boolean value) {
		synchronized (this.isSubscribed) {
			this.isSubscribed.set(value);
			this.isSubscribed.notifyAll();
		}
	}

	/**
	 * Method that waits for a successful subscription by the user
	 * 
	 * @see #subscribe
	 */
	private void waitForSubscription() {
		synchronized (isSubscribed) {
			while (!isSubscribed.get()) {
				try {
					isSubscribed.wait();
				} catch (InterruptedException e) {
					logger.log(Level.SEVERE, "InterruptedException occurred while waiting for a subscription.", e);
				}
			}
		}

	}

	@Override
	public ResponseMessage processRequest(RequestMessage request) {
		try {
			Object response = sendServerRequest(request);
			if (response instanceof ResponseMessage) {
				return (ResponseMessage) response;
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "There was a problem with the ReqProc server request.", e);
		}
		return null;
	}

	/** Send a request to a socket server */
	private Object sendServerRequest(RequestMessage request)
			throws UnknownHostException, IOException, ClassNotFoundException {
		/* Creates a one time connection to the ReqProc socket server */
		Socket server = new Socket(this.address, ConnectionPorts.REQPROC_SERVER_PORT);
		ObjectOutputStream serverOutputStream = new ObjectOutputStream(server.getOutputStream());
		serverOutputStream.writeObject(request);
		serverOutputStream.flush();
		ObjectInputStream serverInputStream = new ObjectInputStream(server.getInputStream());
		Object response = serverInputStream.readObject();
		/* Closing the one time connection to the ReqProc socket server */
		server.close();
		return response;
	}

	/**
	 * Life-cycle of this thread that waits for the subscription ack and then
	 * waits forever for new {@link BroadcastMessage}s.
	 */
	@Override
	public void run() {
		try {
			while (true) {

				waitForSubscription(); // blocking call
				Object received = null;
				synchronized (brokerSocket) {
					// this is a broadcast message
					received = broadcasterInputStream.readObject();
				}
				// only to avoid exceptions
				if (received instanceof BroadcastMessage) {
					subscriberInterface.update((BroadcastMessage) received);
				}
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error during broker/broadcaster socket connection.", e);
		}

	}
}
