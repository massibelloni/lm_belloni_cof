package it.polimi.ingsw.lm_belloni_cof.view.playerside.cli;

import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.*;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidCommandException;
import it.polimi.ingsw.lm_belloni_cof.messages.*;

/**
 * Utility class that generates messages for the server depending on the
 * formatted command received.
 */
public class CliMessageFactory {
	private CliMessageFactory() {
	}

	/**
	 * Main method.
	 * 
	 * @param cmd
	 *            the command that has to be parsed
	 * @param token
	 *            the {@link PlayerToken} that has to be inserted into the
	 *            {@link RequestMessage}.
	 * @return a well formed {@link RequestMessage}
	 * @throws InvalidCommandException
	 *             if the cmd passed as an argument hasn't been recognized as an
	 *             accepted one.
	 */
	public static RequestMessage getRequestMessage(String cmd, PlayerToken token) throws InvalidCommandException {

		String lowerCommand = cmd.toLowerCase();

		if (cmd.matches("(CHAT|Chat|chat)+.*")) {
			String msg = cmd.substring(cmd.indexOf(" ") + 1);
			return new ChatRequestMessage(token, msg);
		}

		else if (cmd.matches("(ACQUIRE|Acquire|acquire)+[ ]*+(PERMIT|Permit|permit)+.*")) { // Regex
																							// string
			String[] cmdSliced = lowerCommand.split("permit+[ ]*");
			// here i have the parameters related to this kind of command
			// (region, indexInRegion, politics(between 1 and 4))
			if(cmdSliced.length<2)
				throw new InvalidCommandException("Uncorrect parameters, IndexInRegion must be a number.");
			String[] parametersField = cmdSliced[1].split("\\s+");
			String region = parametersField[0];
			int indexInRegion;
			try {
				indexInRegion = Integer.parseInt(parametersField[1]);
			} catch (NumberFormatException e) {
				throw new InvalidCommandException("Uncorrect parameters, IndexInRegion must be a number.");
			}
			if (indexInRegion == 1 || indexInRegion == 2) {
				int numberOfPoliticsUsed = parametersField.length - 2;
				if (0 < numberOfPoliticsUsed && numberOfPoliticsUsed <= 4) {
					List<String> politicColors = new ArrayList<>();
					for (int i = 2; i < parametersField.length; i++)
						politicColors.add(parametersField[i]);

					return new AcquireABusinessPermitTileActRqstMsg(token, region, indexInRegion, politicColors);
				} else
					throw new InvalidCommandException(
							"Uncorrect parameters, they must be: Region   IndexInRegion(1 or 2)   PoliticColors(between 1 and 4)");
			} else
				throw new InvalidCommandException(
						"Uncorrect parameters, they must be: Region   IndexInRegion(1 or 2)   PoliticColors(between 1 and 4)");
		}

		// this regex accepts command with or without spaces, with or without
		// "using a"
		// the only important rule is that there must be a space(s) between
		// command and parameters
		else if (cmd
				.matches("(BUILD|Build|build)+[ ]*+(EMPORIUM|Emporium|emporium)+[^(P|p)]*+(PERMIT|Permit|permit)+.*")) {
			String[] cmdSliced = lowerCommand.split("permit+[ ]*");
			// here i have the parameters related to this kind of command (city,
			// permitIndex)
			if(cmdSliced.length<2)
				throw new InvalidCommandException("Uncorrect parameters, they must be: City   PermitIndexInOwnerHand");
			String[] parametersField = cmdSliced[1].split("\\s+");
			if (parametersField.length == 2) {
				String city = parametersField[0];
				String permitIndex = parametersField[1];
				return new BuildAnEmporiumUsingAPermitTileActRqstMsg(token, city, permitIndex);
			} else
				throw new InvalidCommandException("Uncorrect parameters, they must be: City   PermitIndexInOwnerHand");
		}

		// this regex accepts command with or without spaces, with or without
		// "with the help of the"
		// the only important rule is that there must be a space(s) between
		// command and parameters
		else if (cmd.matches("(BUILD|Build|build)+[ ]*+(EMPORIUM|Emporium|emporium)+[^(K|k)]*+(KING||King||king)+.*")) {
			String[] cmdSliced = lowerCommand.split("king+[ ]*");
			// here i have the parameters related to this kind of command (city,
			// politics(between 1 and 4))
			if(cmdSliced.length<2)
				throw new InvalidCommandException(
						"Uncorrect parameters, they must be: City   PoliticColors(between 1 and 4)");
			String[] parametersField = cmdSliced[1].split("\\s+");
			String city = parametersField[0];
			int numberOfPoliticsUsed = parametersField.length - 1;
			if (numberOfPoliticsUsed > 0 && numberOfPoliticsUsed <= 4) {
				List<String> politicColors = new ArrayList<>();
				for (int i = 1; i < parametersField.length; i++)
					politicColors.add(parametersField[i]);
				return new BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg(token, city, politicColors);
			} else
				throw new InvalidCommandException(
						"Uncorrect parameters, they must be: City   PoliticColors(between 1 and 4)");
		}

		else if (cmd.matches("(CHANGE|Change|change)+[ ]*+(PERMIT|Permit|permit)+[ ]*+(TILES|Tiles|tiles)+.*")) {
			String[] cmdSliced = lowerCommand.split("tiles+[ ]*");
			// here i have the parameters related to this kind of command
			// (region)
			if(cmdSliced.length<2)
				throw new InvalidCommandException("Uncorrect parameters, they must be: Region");
			String[] parametersField = cmdSliced[1].split("\\s+");
			if (parametersField.length == 1) {
				String region = parametersField[0];
				return new ChangeBusinessPermitTilesActRqstMsg(token, region);
			} else
				throw new InvalidCommandException("Uncorrect parameters, they must be: Region");
		}

		else if (cmd.matches("(ELECT|Elect|elect)+[ ]*+(COUNCILLOR|Councillor|councillor)+.*")) {
			String[] cmdSliced = lowerCommand.split("councillor+[ ]*");
			// here i have the parameters related to this kind of command
			// (region, councillorColor)
			if(cmdSliced.length<2)
				throw new InvalidCommandException("Uncorrect parameters, they must be: Region  CouncillorColor");
			String[] parametersField = cmdSliced[1].split("\\s+");
			if (parametersField.length == 2) {
				String region = parametersField[0];
				String councillorColor = parametersField[1];
				return new ElectACouncillorActRqstMsg(token, region, councillorColor);
			} else
				throw new InvalidCommandException("Uncorrect parameters, they must be: Region  CouncillorColor");
		}

		else if (cmd.matches("(DRAW|Draw|draw)+[ ]*+(POLITIC|Politic|politic)+.*")) {
			return new DrawAPoliticActRqstMsg(token);
		}

		else if (cmd.matches(
				"(BONUSES|Bonuses|bonuses)+[ ]*+(FROM|From|from)+[ ]*+(CITY|City|city)+[ ]*+(TOKEN|Token|token)+.*")) {
			String[] cmdSliced = lowerCommand.split("token+[ ]*");
			// here i have the parameters related to this kind of command (city)
			if(cmdSliced.length<2)
				throw new InvalidCommandException("Uncorrect parameters, they must be: City");
			String[] parametersField = cmdSliced[1].split("\\s+");
			if (parametersField.length == 1) {
				String city = parametersField[0];
				return new CityTokenResponseActRqstMsg(token, city);
			} else
				throw new InvalidCommandException("Uncorrect parameters, they must be: City");
		}

		else if (cmd.matches("(DROP|Drop|drop)+.*")) {
			return new EmptyQuickActionActRqstMsg(token);
		}

		else if (cmd.matches("(END|End|end)+[ ]*+(MARKET|Market|market)+.*")) {
			if (lowerCommand.contains("offer"))
				return new EndMarketOfferActRqstMsg(token);
			else if (lowerCommand.contains("buy"))
				return new EndMarketBuyActRqstMsg(token);
			else
				throw new InvalidCommandException("You can close only a Market Offer or a Market Buy");
		}

		else if (cmd.matches("(ENGAGE|Engage|engage)+[ ]*+(ASSISTANT|Assistant|assistant)+.*")) {
			return new EngageAnAssistantActRqstMsg(token);
		}

		else if (cmd.matches("(OFFER|Offer|offer)+.*")) {
			String[] cmdSliced = lowerCommand.split("offer+[ ]*");
			// here i have the parameters related to this kind of command
			// (item,price)
			if(cmdSliced.length<2)
				throw new InvalidCommandException("Uncorrect parameters");
			String[] parametersField = cmdSliced[1].split("\\s+");
			String sellableItem = parametersField[0];
			if (sellableItem.equals("assistant") && parametersField.length == 2) {
				try {
					int price = Integer.parseInt(parametersField[1]);
					return new MarketOfferActRqstMsg(token, sellableItem, price);
				} catch (NumberFormatException e) {
					throw new InvalidCommandException("Uncorrect parameters, Price must be a number.");
				}
			} else if (sellableItem.equals("politic") && parametersField.length == 3) {
				String sellablePoliticColor = parametersField[1];
				try {
					int price = Integer.parseInt(parametersField[2]);
					return new MarketOfferActRqstMsg(token, sellableItem + " " + sellablePoliticColor, price);
				} catch (NumberFormatException e) {

					throw new InvalidCommandException("Uncorrect parameters, Price must be a number.");

				}
			} else if (sellableItem.equals("permit") && parametersField.length == 3) {
				String sellablePermitIndex = parametersField[1];
				try {
					int price = Integer.parseInt(parametersField[2]);
					return new MarketOfferActRqstMsg(token, sellableItem + " " + sellablePermitIndex, price);
				} catch (NumberFormatException e) {
					throw new InvalidCommandException("Uncorrect parameters, Price must be a number.");
				}
			} else
				throw new InvalidCommandException(
						"Uncorrect parameters, they must be: SellableItem(only one per offer)  Price");
		}

		else if (cmd.matches("(BUY|Buy|buy)+.*")) {
			String[] cmdSliced = lowerCommand.split("buy+[ ]*");
			// here i have the parameters related to this kind of command
			// (seller, indexInSellerOffers)
			if(cmdSliced.length<2)
				throw new InvalidCommandException(
						"Uncorrect parameters, they must be: SellerOfTheChosenItem  IndexInSellerOffersList");
			String[] parametersField = cmdSliced[1].split("\\s+");
			if (parametersField.length == 2) {
				String seller = parametersField[0];
				try {
					int indexInSellerOffers = Integer.parseInt(parametersField[1]);
					return new MarketBuyActRqstMsg(token, seller, indexInSellerOffers);
				} catch (NumberFormatException e) {
					throw new InvalidCommandException(
							"Uncorrect parameters, IndexInSellerOffersList must be a number.");
				}
			} else
				throw new InvalidCommandException(
						"Uncorrect parameters, they must be: SellerOfTheChosenItem  IndexInSellerOffersList");
		}

		else if (cmd
				.matches("(ADDITIONAL|Additional|additional)+[ ]*+(MAIN|Main|main)+[ ]*+(ACTION|Action|action)+.*")) {
			return new PerformAnAdditionalMainActionActRqstMsg(token);
		}

		else if (cmd.matches(
				"(SEND|Send|send)+[ ]*+(ASSISTANT|Assistant|assistant)+[^(E|e)]*+(ELECT|Elect|elect)+[^c]*+(COUNCILLOR|Councillor|councillor)+.*")) {
			String[] cmdSliced = lowerCommand.split("councillor+[ ]*");
			// here i have the parameters related to this kind of command
			// (region, councillorColor)
			if(cmdSliced.length<2)
				throw new InvalidCommandException("Uncorrect parameters, they must be: Region  CouncillorColor");
			String[] parametersField = cmdSliced[1].split("\\s+");
			if (parametersField.length == 2) {
				String region = parametersField[0];
				String councillorColor = parametersField[1];
				return new SendAnAssistantToElectACouncillorActRqstMsg(token, region, councillorColor);
			} else
				throw new InvalidCommandException("Uncorrect parameters, they must be: Region  CouncillorColor");
		}

		else if (cmd.matches("(POLITIC|Politic|politic)+[ ]*+(HAND|Hand|hand)+[ ]*")) {
			return new PoliticsRequestMessage(token);
		}

		else if (cmd.matches(
				"(BONUSES|Bonuses|bonuses)+[ ]*+(FROM|From|from)+[^(O|o)]*+(OWNED|Owned|owned)+[ ]*+(PERMIT|Permit|permit)+.*")) {
			String[] cmdSliced = lowerCommand.split("permit+[ ]*");
			if(cmdSliced.length<2)
				throw new InvalidCommandException("Uncorrect parameters, they must be: Index");
			String[] parametersField = cmdSliced[1].split("\\s+");
			if (parametersField.length == 1) {
				String indexInOwnerHand = parametersField[0];
				return new OwnedPermitResponseActRqstMsg(token, indexInOwnerHand);
			} else
				throw new InvalidCommandException("Uncorrect parameters, they must be: Index");
		}

		else if (cmd.matches(
				"(BONUSES|Bonuses|bonuses)+[ ]*+(FROM|From|from)+[ ]*+(FACE|Face|face)+[ ]*+(UP|Up|up)+[ ]*+(PERMIT|Permit|permit)+.*")) {
			String[] cmdSliced = lowerCommand.split("permit+[ ]*");
			if(cmdSliced.length<2)
				throw new InvalidCommandException("Uncorrect parameters, they must be: Region  IndexInRegion");
			String[] parametersField = cmdSliced[1].split("\\s+");
			if (parametersField.length == 2) {
				String region = parametersField[0];
				try {
					int indexInRegion = Integer.parseInt(parametersField[1]);
					return new FaceUpPermitResponseActRqstMsg(token, region, indexInRegion);
				} catch (NumberFormatException e) {
					throw new InvalidCommandException("Uncorrect parameters, IndexInRegion must be a number", e);
				}
			} else
				throw new InvalidCommandException("Uncorrect parameters, they must be: Region  IndexInRegion");
		}

		throw new InvalidCommandException("Command not recognized.\nWrite 'help' to see available commands.");
	}
}
