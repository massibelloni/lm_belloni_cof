package it.polimi.ingsw.lm_belloni_cof.view.shared;

import java.rmi.Remote;
import java.rmi.RemoteException;

/** Interface seen by clients that permit them to subscribe to game updates. */
public interface BrokerInterface extends Remote {
	/**
	 * Subscribe a client to updates game updates.
	 * 
	 * @param subscriber
	 *            a {@link SubscriberInterface} where {@link BroadcastMessage}s
	 *            have to be sent.
	 * @param username
	 *            the username of the user that wants to be subscribed
	 * @throws RemoteException
	 *             if something goes wrong with network technology
	 */
	public void subscribe(SubscriberInterface subscriber, String username) throws RemoteException;
}
