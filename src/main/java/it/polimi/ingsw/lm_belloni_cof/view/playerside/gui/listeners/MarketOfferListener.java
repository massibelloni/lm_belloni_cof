package it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.listeners;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.MarketOfferActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;
import it.polimi.ingsw.lm_belloni_cof.view.shared.RequestProcessor;
import net.miginfocom.swing.MigLayout;

/**
 * The listener used to ask the price of an object during the market phase. This
 * is the object that actually sends the request to the server
 */
public class MarketOfferListener extends MouseAdapter {
	private static final Logger logger = Logger.getLogger(MarketOfferListener.class.getName());
	private String forSale;
	private PlayerToken playerToken;
	private RequestProcessor requestProcessor;
	private MessageVisitor messageVisitor;

	public MarketOfferListener(String forSale, PlayerToken playerToken, RequestProcessor requestProcessor,
			MessageVisitor messageVisitor) {
		this.forSale = forSale;
		this.playerToken = playerToken;
		this.requestProcessor = requestProcessor;
		this.messageVisitor = messageVisitor;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		/* showing a frame for the asked price */
		JFrame priceFrame = new JFrame("Set price");
		priceFrame.setLayout(new MigLayout("fill"));
		JTextField priceField = new JTextField("");
		priceFrame.add(priceField, "w 100%,h 90%,grow,wrap");
		JButton offer = new JButton("Offer");
		offer.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					int price = Integer.parseInt(priceField.getText());
					try {
						priceFrame.setVisible(false);
						requestProcessor.processRequest(new MarketOfferActRqstMsg(playerToken, forSale, price))
								.visit(messageVisitor);
					} catch (RemoteException e1) {
						logger.log(Level.SEVERE, "Error during GUI market offer.", e1);
						JOptionPane.showMessageDialog(new JFrame(), "The action request hasn't been sent.",
								"Request not sent", JOptionPane.ERROR_MESSAGE);
					}
				} catch (Exception ex) {
					logger.log(Level.SEVERE, "Error during GUI market offer: wrong price format.", ex);
					JOptionPane.showMessageDialog(new JFrame(), "Price is uncorrect.", "Request not sent",
							JOptionPane.ERROR_MESSAGE);
					priceField.setText("");
				}
			}
		});
		priceFrame.add(offer, "w 100%,h 10%,grow");
		priceFrame.pack();
		priceFrame.setLocationRelativeTo(null);
		priceFrame.setAlwaysOnTop(true);
		priceFrame.setVisible(true);

	}
}
