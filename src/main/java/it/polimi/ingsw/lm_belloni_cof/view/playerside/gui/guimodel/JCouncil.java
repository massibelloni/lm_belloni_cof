package it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.guimodel;

import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.BackgroundPanel;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.StretchIcon;

/**
 * Swing element representing a council.
 */
public class JCouncil extends BackgroundPanel {
	private static final long serialVersionUID = 1L;
	private String[] colors;

	public JCouncil(String[] colors) {
		super(new ImageIcon("graphics/councillors/balcony-bg.png").getImage(), BackgroundPanel.TILED);
		super.setLayout(new GridLayout(1, 4));
		this.colors = colors;
		for (String color : colors) {
			super.add(new JLabel(new StretchIcon("graphics/councillors/councillor-" + color + ".png")));
		}
	}

	/**
	 * Update (and repaint only if needed) the council
	 * 
	 * @param colors
	 *            the colors of the new councillors.
	 */
	public void update(String[] colors) {
		// avoiding useless repaint
		boolean edited = false;
		for (int i = 0; i < colors.length; i++) {
			if (!this.colors[i].equals(colors[i])) {
				edited = true;
				break;
			}
		}
		if (edited) {
			this.colors = colors;
			super.removeAll();
			for (String color : colors) {
				super.add(new JLabel(new StretchIcon("graphics/councillors/councillor-" + color + ".png")));
			}
			super.repaint();
		}
	}
}
