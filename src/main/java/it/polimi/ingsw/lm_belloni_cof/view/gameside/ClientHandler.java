package it.polimi.ingsw.lm_belloni_cof.view.gameside;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.messages.ResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.InvalidRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.RequestMessage;
import it.polimi.ingsw.lm_belloni_cof.view.shared.RequestProcessor;

/**
 * Thread that actually handles an incoming request, retrieves the response and
 * send it using socket technology. Then closes the connection.
 */
public class ClientHandler extends Thread {
	private static final Logger logger = Logger.getLogger(ClientHandler.class.getName());
	/** The socket connection to the client. */
	private final Socket client;
	/**
	 * The {@link RequestProcessor} used generate responses based on user
	 * requests.
	 */
	private final RequestProcessor requestProcessor;

	public ClientHandler(Socket client, RequestProcessor requestProcessor) {

		this.client = client;
		this.requestProcessor = requestProcessor;

	}

	/**
	 * Read the incoming message, retrieves the response using a
	 * {@link RequestProcessor} and send it back to the client. Then closes the
	 * connection.
	 */
	@Override
	public void run() {

		ResponseMessage response;
		try {
			ObjectInputStream inputStream = new ObjectInputStream(this.client.getInputStream());
			Object request = inputStream.readObject();
			if (request instanceof RequestMessage) {
				response = this.requestProcessor.processRequest((RequestMessage) request);
			} else {
				response = new InvalidRequestMessage("The request is not well formed.");
			}
			ObjectOutputStream outputStream = new ObjectOutputStream(this.client.getOutputStream());
			outputStream.writeObject(response);
			outputStream.flush();
		} catch (IOException | ClassNotFoundException e) {
			logger.log(Level.SEVERE, "Error during RequestProcessor communication with a specific client", e);
		} finally {
			try {
				this.client.close();
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Error trying to close a specific client connection.", e);
			}

		}

	}

}
