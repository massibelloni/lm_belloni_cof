package it.polimi.ingsw.lm_belloni_cof.view.playerside.cli;

import java.io.PrintStream;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidCommandException;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
import it.polimi.ingsw.lm_belloni_cof.messages.RequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.GameStatusKeeper;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;
import it.polimi.ingsw.lm_belloni_cof.view.shared.RequestProcessor;

/**
 * Class managing commands: both local and remote. Directly outputs on console
 * if the requested command can be locally dispatched or makes the call to the
 * server if further processes are needed.
 */
public class CliCommandManager {
	private static final Logger logger = Logger.getLogger(CliCommandManager.class.getName());
	private PrintStream out;
	private PlayerToken playerToken;
	private RequestProcessor requestProcessor;
	private GameStatusKeeper gameStatusKeeper;
	private MessageVisitor messageVisitor;

	public CliCommandManager(GameStatusKeeper gameStatusKeeper, RequestProcessor requestProcessor,
			MessageVisitor messageVisitor, PlayerToken playerToken) {
		this.gameStatusKeeper = gameStatusKeeper;
		this.requestProcessor = requestProcessor;
		this.playerToken = playerToken;
		this.messageVisitor = messageVisitor;
		this.out = System.out;
	}

	/** Main method of the class */
	public void executeCommand(String cmd) {
		/*
		 * some commands may be locally achieved, others may need a server call
		 */

		// Checking the command using Regex string in order to have more input
		// possibilities for the user

		if (cmd.matches("(CITY|City|city)+[ ]*+(LIST|List|list)+[ ]*")) {
			JSONArray cities = ((JSONObject) this.gameStatusKeeper.getData().get("map")).getJSONArray("cities");
			for (int i = 0; i < cities.length(); i++) {
				String cityName = cities.getJSONObject(i).getString("name").toUpperCase();
				String cityRegion = cities.getJSONObject(i).getString("region");
				String cityColor = cities.getJSONObject(i).getString("color");
				JSONArray cityConnections = cities.getJSONObject(i).getJSONArray("connections");
				String connectedTo = "Connected to: ";
				for (int j = 0; j < cityConnections.length(); j++) {
					if (j == 0)
						connectedTo = connectedTo + " " + cityConnections.getString(j);
					else
						connectedTo = connectedTo + ", " + cityConnections.getString(j);
				}
				cli(cityName + "(" + cityRegion + ", " + cityColor + ")" + ":" + "  " + connectedTo);
			}
		} else if (cmd.matches(
				"(COLOR|Color|color)+[ ]*+(REWARD|Reward|reward)+[ ]*+(TILE|Tile|tile)+[ ]*+(USED|Used|used)+[ ]*")) {
			JSONArray alreadyUsedRwdTiles = ((JSONObject) this.gameStatusKeeper.getStatus())
					.getJSONArray("colorRewardTileUsed");
			for (int i = 0; i < alreadyUsedRwdTiles.length(); i++)
				cli(alreadyUsedRwdTiles.getString(i));
		} else if (cmd.matches(
				"(REGION|Region|region)+[ ]*+(REWARD|Reward|reward)+[ ]*+(TILE|Tile|tile)+[ ]*+(USED|Used|used)+[ ]*")) {
			JSONArray alreadyUsedRwdTiles = ((JSONObject) this.gameStatusKeeper.getStatus())
					.getJSONArray("regionRewardTileUsed");
			for (int i = 0; i < alreadyUsedRwdTiles.length(); i++)
				cli(alreadyUsedRwdTiles.getString(i));
		} else if (cmd.matches("(KING|King|king)+[ ]*+(LOCATION|Location|location)+[ ]*")) {
			cli(((JSONObject) this.gameStatusKeeper.getStatus()).getString("kingsLocation"));
		} else if (cmd.matches("(AVAILABLE|Available|available)+[ ]*+(COUNCILLORS|Councillors|councillors)+[ ]*")) {
			JSONArray availableCouncillors = ((JSONObject) this.gameStatusKeeper.getStatus())
					.getJSONArray("councillors");
			String avCouncillors = "";
			for (int i = 0; i < availableCouncillors.length(); i++) {
				if (i != availableCouncillors.length() - 1)
					avCouncillors = avCouncillors + availableCouncillors.getString(i) + ", ";
				else
					avCouncillors = avCouncillors + availableCouncillors.getString(i);
			}
			cli(avCouncillors);
		} else if (cmd.matches("(OPPONENTS|Opponents|opponents)+[ ]*") || cmd.matches("(RECAP|Recap|recap)+[ ]*")) {
			JSONArray players = ((JSONObject) this.gameStatusKeeper.getStatus()).getJSONArray("players");
			Boolean cmdChecker = cmd.toLowerCase().equals("recap");
			for (Object obj : players) {
				if ((!((JSONObject) obj).getString("username").equals(this.playerToken.getUsername()) && !cmdChecker)
						|| ((JSONObject) obj).getString("username").equals(this.playerToken.getUsername())
								&& cmdChecker) {
					cli("Username: " + ((JSONObject) obj).getString("username"));
					cli("Number of assistants: " + ((JSONObject) obj).getInt("assistants"));
					cli("Coins: " + ((JSONObject) obj).getInt("coins"));
					cli("Nobility track position: " + ((JSONObject) obj).getInt("nobilityTrackPosition"));
					cli("Victory points: " + ((JSONObject) obj).getInt("victoryPoints"));
					cli("Left emporiums: " + ((JSONObject) obj).getInt("emporiums"));
					JSONArray opponentPermits = ((JSONObject) obj).getJSONArray("permits");
					int permitIndex = 1;
					for (Object permitObj : opponentPermits) {
						JSONArray permitCities = ((JSONObject) permitObj).getJSONArray("cities");
						JSONArray permitBonuses = ((JSONObject) permitObj).getJSONArray("bonuses");
						Boolean usage = ((JSONObject) permitObj).getBoolean("used");
						cli("Permit " + permitIndex + ":");
						String citiesHere = "";
						for (int count = 0; count < permitCities.length(); count++)
							if (count != 0)
								citiesHere = citiesHere + ", " + permitCities.getString(count);
							else
								citiesHere = citiesHere + permitCities.getString(count);
						String bonusesHere = "";
						int comma = 0;
						for (Object permitBonusesObject : permitBonuses)
							if (comma != 0)
								bonusesHere = bonusesHere + ", " + ((JSONObject) permitBonusesObject).getInt("times")
										+ " " + ((JSONObject) permitBonusesObject).getString("bonus");
							else {
								bonusesHere = bonusesHere + ((JSONObject) permitBonusesObject).getInt("times") + " "
										+ ((JSONObject) permitBonusesObject).getString("bonus");
								comma++;
							}

						cli("	Cities: " + citiesHere);
						cli("	Bonuses: " + bonusesHere);
						cli("	Used: " + usage);
						permitIndex++;
					}
				}
			}
		}
		// show for sale object (player/s)
		// shows sellable objects for each players
		// if market phases are not active, shows an 'error message'
		else if (cmd.matches("(SHOW|Show|show)+[ ]*+(FOR|For|for)+[ ]*+(SALE|Sale|sale)+[ ]*+(ITEMS|Items|items)+.*")) {
			boolean inMarketBuy = ((JSONObject) (this.gameStatusKeeper.getStatus())).getJSONObject("turn")
					.getBoolean("marketbuy");
			boolean inMarketOffer = ((JSONObject) (this.gameStatusKeeper.getStatus())).getJSONObject("turn")
					.getBoolean("marketoffer");
			if (inMarketBuy || inMarketOffer) {
				JSONArray players = ((JSONObject) this.gameStatusKeeper.getStatus()).getJSONArray("players");
				for (Object playerObject : players) {
					String currentPlayer = ((JSONObject) playerObject).getString("username");
					if (!currentPlayer.equals(this.playerToken.getUsername())) {
						int indexInSellerHand = 1;
						// List<String> forSale=new ArrayList<>();
						// List<String> forSalePrice=new ArrayList<>();
						JSONArray offers = ((JSONObject) playerObject).getJSONArray("offered");
						cli(currentPlayer.toUpperCase() + " for sale items are:");
						for (Object offersObj : offers) {
							Object forSaleObjectByPlayer = ((JSONObject) offersObj).get("forSale");
							if (forSaleObjectByPlayer instanceof String) {
								int itemPrice = ((JSONObject) offersObj).getInt("price");
								// forSale.add(forSaleObjectByPlayer);
								// forSalePrice.add(itemPrice);
								cli(indexInSellerHand + "-" + forSaleObjectByPlayer + " Price:" + itemPrice);
								indexInSellerHand++;
							}
							// If is not a politic card and an assistant
							// neither, it necessary is a permit
							else {
								Object sellablePermit = ((JSONObject) offersObj).get("forSale");
								int itemPrice = ((JSONObject) offersObj).getInt("price");
								JSONArray citiesOnThisPermit = ((JSONObject) sellablePermit).getJSONArray("cities");
								JSONArray bonusesOnThisPermit = ((JSONObject) sellablePermit).getJSONArray("bonuses");
								Boolean usage = ((JSONObject) sellablePermit).getBoolean("used");
								String citiesHere = "";
								for (int count = 0; count < citiesOnThisPermit.length(); count++)
									if (count != 0)
										citiesHere = citiesHere + ", " + citiesOnThisPermit.getString(count);
									else
										citiesHere = citiesHere + citiesOnThisPermit.getString(count);
								int comma = 0;
								String bonusesHere = "";
								for (Object permitBonusesObject : bonusesOnThisPermit)
									if (comma != 0)
										bonusesHere = bonusesHere + ", "
												+ ((JSONObject) permitBonusesObject).getInt("times") + " "
												+ ((JSONObject) permitBonusesObject).getString("bonus");
									else {
										bonusesHere = bonusesHere + ((JSONObject) permitBonusesObject).getInt("times")
												+ " " + ((JSONObject) permitBonusesObject).getString("bonus");
										comma++;
									}
								cli(indexInSellerHand + "-" + "Permit " + "Price:" + itemPrice);
								cli("	Cities: " + citiesHere);
								cli("	Bonuses: " + bonusesHere);
								cli("	Used: " + usage);
								indexInSellerHand++;
							}
						}
					}
				}
			} else
				cli("Nothing to display, Market phase not yet started.");
		}

		else if (cmd.matches("(SHOW|Show|show)+[ ]*+(FACE|Face|face)+[ ]*(UP|Up|up)+[ ]*+(PERMIT|Permit|permit)+.*")) {
			JSONArray regions = ((JSONObject) this.gameStatusKeeper.getStatus()).getJSONArray("regions");
			String[] splittedString = cmd.split("\\s+"); // "\\s" regex string
															// that represents
															// space character.
			int numberOfCmdComponents = splittedString.length;
			// if player doesn't specify from which region, i show to him all
			// face up permits from all regions
			if (splittedString[numberOfCmdComponents - 1].matches("(PERMIT|Permit|permit)")) {
				for (Object objRegion : regions) {
					int indexInRegion = 1;
					String currentRegion = ((JSONObject) objRegion).getString("region");
					if (!currentRegion.equals("king")) {
						JSONArray permitsObject = ((JSONObject) objRegion).getJSONArray("permits");
						cli("Region: " + currentRegion.toUpperCase());
						for (Object permitObject : permitsObject) {
							JSONArray citiesOnPermit = ((JSONObject) permitObject).getJSONArray("cities");
							JSONArray bonusesOnPermit = ((JSONObject) permitObject).getJSONArray("bonuses");
							cli("	Permit " + indexInRegion);
							String citiesHere = "";
							for (int count = 0; count < citiesOnPermit.length(); count++)
								if (count != 0)
									citiesHere = citiesHere + ", " + citiesOnPermit.getString(count);
								else
									citiesHere = citiesHere + citiesOnPermit.getString(count);
							int comma = 0;
							String bonusesHere = "";
							for (Object bonusObject : bonusesOnPermit)
								if (comma != 0)
									bonusesHere = bonusesHere + ", " + ((JSONObject) bonusObject).getInt("times") + " "
											+ ((JSONObject) bonusObject).getString("bonus");
								else {
									bonusesHere = bonusesHere + ((JSONObject) bonusObject).getInt("times") + " "
											+ ((JSONObject) bonusObject).getString("bonus");
									comma++;
								}
							cli("		Cities: " + citiesHere);
							cli("		Bonuses: " + bonusesHere);
							indexInRegion++;
						}
					}

				}
			}
			// player has specify a region
			else {
				String chosenRegion = splittedString[numberOfCmdComponents - 1].toLowerCase();
				for (Object regionSearcher : regions) {
					if (((JSONObject) regionSearcher).getString("region").toLowerCase().equals(chosenRegion)) {
						JSONArray regionPermitsObject = ((JSONObject) regionSearcher).getJSONArray("permits");
						int indexInRegion = 1;
						cli("Region: " + chosenRegion.toUpperCase());
						for (Object regionPermitObject : regionPermitsObject) {
							JSONArray regionCitiesOnPermit = ((JSONObject) regionPermitObject).getJSONArray("cities");
							JSONArray regionBonusesOnPermit = ((JSONObject) regionPermitObject).getJSONArray("bonuses");
							cli("	Permit " + indexInRegion);
							String citiesHere = "";
							for (int count = 0; count < regionCitiesOnPermit.length(); count++)
								if (count != 0)
									citiesHere = citiesHere + ", " + regionCitiesOnPermit.getString(count);
								else
									citiesHere = citiesHere + regionCitiesOnPermit.getString(count);
							String bonusesHere = "";
							int comma = 0;
							for (Object bonusObject : regionBonusesOnPermit)
								if (comma != 0)
									bonusesHere = bonusesHere + ", " + ((JSONObject) bonusObject).getInt("times") + " "
											+ ((JSONObject) bonusObject).getString("bonus");
								else {
									bonusesHere = bonusesHere + ((JSONObject) bonusObject).getInt("times") + " "
											+ ((JSONObject) bonusObject).getString("bonus");
									comma++;
								}
							cli("		Cities: " + citiesHere);
							cli("		Bonuses: " + bonusesHere);
							indexInRegion++;
						}
					}
				}

			}
		} else if (cmd.matches("(COUNCIL|Council|council)+[ ]*+(STATUS|Status|status)+.*")) {
			JSONArray regions = (((JSONObject) this.gameStatusKeeper.getStatus())).getJSONArray("regions");
			for (Object regionObject : regions) {
				String currentRegion = ((JSONObject) regionObject).getString("region");
				JSONArray councilByRegion = ((JSONObject) regionObject).getJSONArray("council");
				String councilStatus = " (";
				for (int i = 0; i < councilByRegion.length(); i++)
					if (i != councilByRegion.length() - 1)
						councilStatus = councilStatus + councilByRegion.getString(i) + ", ";
					else
						councilStatus = councilStatus + councilByRegion.getString(i);
				cli(currentRegion.toUpperCase() + councilStatus + ")");
			}
		} else if (cmd.matches("(PERMIT|Permit|permit)+[ ]*+(HAND|Hand|hand)+.*")) {
			JSONArray players = ((JSONObject) this.gameStatusKeeper.getStatus()).getJSONArray("players");
			String lowerCmd = cmd.toLowerCase();
			Boolean all = lowerCmd.contains("all");
			for (Object playerObj : players) {
				String username = ((JSONObject) playerObj).getString("username");
				if (all || (!all && username.equals(this.playerToken.getUsername()))) {
					JSONArray playerPermits = ((JSONObject) playerObj).getJSONArray("permits");
					int i = 1;
					cli("Player: " + username);
					for (Object playerPermitObj : playerPermits) {
						JSONArray permitCities = ((JSONObject) playerPermitObj).getJSONArray("cities");
						JSONArray permitBonuses = ((JSONObject) playerPermitObj).getJSONArray("bonuses");
						String citiesPermit = "";
						for (int count = 0; count < permitCities.length(); count++)
							if (count != 0)
								citiesPermit = citiesPermit + ", " + permitCities.getString(count);
							else
								citiesPermit = citiesPermit + permitCities.getString(count);
						int commaBonus = 0;
						String bonusesOnPermit = "";
						for (Object permitBonusObj : permitBonuses) {
							if (commaBonus != 0)
								bonusesOnPermit = bonusesOnPermit + ", " + ((JSONObject) permitBonusObj).getInt("times")
										+ " " + ((JSONObject) permitBonusObj).getString("bonus");
							else {
								bonusesOnPermit = bonusesOnPermit + ((JSONObject) permitBonusObj).getInt("times") + " "
										+ ((JSONObject) permitBonusObj).getString("bonus");
								commaBonus++;
							}
						}
						Boolean usage = ((JSONObject) playerPermitObj).getBoolean("used");
						cli("	Permit" + i);
						cli("		Cities: " + citiesPermit);
						cli("		Bonuses: " + bonusesOnPermit);
						cli("		Usage: " + usage);
						i++;
					}
				}
			}
		}

		else if (cmd.matches("(EMPORIUMS|Emporiums|emporiums)+(BUILT|Built|built)+.*")) {
			JSONArray cities = ((JSONObject) this.gameStatusKeeper.getStatus()).getJSONArray("cities");
			for (Object cityObj : cities) {
				JSONArray emporiumsInThisCity = ((JSONObject) cityObj).getJSONArray("emporiums");
				String cityEmporiumsStatus = ((JSONObject) cityObj).getString("city").toUpperCase() + " (";
				for (int i = 0; i < emporiumsInThisCity.length(); i++) {
					if (i == emporiumsInThisCity.length() - 1) {
						cityEmporiumsStatus = cityEmporiumsStatus + emporiumsInThisCity.get(i) + ")";
					} else
						cityEmporiumsStatus = cityEmporiumsStatus + emporiumsInThisCity.get(i) + ", ";
					cli(cityEmporiumsStatus);
				}
			}
		}

		else if (cmd.matches("(HELP|Help|help)+[ ]*")) {
			// Cmd list with parameters needed and effects of the command.
			cli("LIST OF AVAILABLE COMMANDS:");
			System.out.printf("%-50.50s  %-55.55s%n", "-Acquire permit",
					"/parameters: Region  Index  PoliticColors(min1,max 4)");
			System.out.printf("%-50.50s  %-50.50s%n", "-Available councillors", "/parameters: ");
			System.out.printf("%-50.50s  %-50.50s%n", "-Bonuses from city token", "/parameters: City");
			System.out.printf("%-50.50s  %-50.50s%n", "-Bonuses from face up permit",
					"/parameters: Region  IndexInRegion");
			System.out.printf("%-50.50s  %-50.50s%n", "-Bonuses from (an) owned permit",
					"/parameters: City  PermitIndex");
			System.out.printf("%-50.50s  %-50.50s%n", "-Build emporium (using a) permit",
					"/parameters: City  PermitIndex");
			System.out.printf("%-50.50s  %-50.50s%n", "-Build emporium (with the help of the) king",
					"/parameters: City  PoliticColors(min1,max 4)");
			System.out.printf("%-50.50s  %-50.50s%n", "-Buy", "/parameters: Seller  Seller'sItemIndex");
			System.out.printf("%-50.50s  %-50.50s%n", "-Change permit tiles", "/parameters: Region");
			System.out.printf("%-50.50s  %-50.50s%n", "-Color reward tile used", "/parameters: ");
			System.out.printf("%-50.50s  %-50.50s%n", "-Council status", "/parameters: ");
			System.out.printf("%-50.50s  %-50.50s%n", "-City list", "/parameters: ");
			System.out.printf("%-50.50s  %-50.50s%n", "-Chat", "/parameters: Message");
			System.out.printf("%-50.50s  %-50.50s%n", "-Draw politic", "/parameters: ");
			System.out.printf("%-50.50s  %-50.50s%n", "-Drop", "/parameters: ");
			System.out.printf("%-50.50s  %-50.50s%n", "-Elect councillor", "/parameters: Region  CouncillorColor");
			System.out.printf("%-50.50s  %-50.50s%n", "-Emporiums built", "/parameters: ");
			System.out.printf("%-50.50s  %-50.50s%n", "-End market offer", "/parameters: ");
			System.out.printf("%-50.50s  %-50.50s%n", "-End market buy", "/parameters: ");
			System.out.printf("%-50.50s  %-50.50s%n", "-Engage assistant", "/parameters: ");
			System.out.printf("%-50.50s  %-50.50s%n", "-King location", "/parameters: ");
			System.out.printf("%-50.50s  %-50.50s%n", "-Offer", "/parameters: SellableItem  Price");
			System.out.printf("%-50.50s  %-50.50s%n", "-Opponents", "/parameters: ");
			System.out.printf("%-50.50s  %-50.50s%n", "-Permit hand", "/parameters: ");
			System.out.printf("%-50.50s  %-50.50s%n", "-Permit hand all", "/parameters: ");
			System.out.printf("%-50.50s  %-50.50s%n", "-Politic hand", "/parameters: ");
			System.out.printf("%-50.50s  %-50.50s%n", "-Recap", "/parameters: ");
			System.out.printf("%-50.50s  %-50.50s%n", "-Region reward tile used", "/parameters: ");
			System.out.printf("%-50.50s  %-50.50s%n", "-Region reward tile used", "/parameters: ");
			System.out.printf("%-50.50s  %-50.50s%n", "-Send assistant (to) elect (a) councillor",
					"/parameters: Region  CouncillorColor");
			System.out.printf("%-50.50s  %-50.50s%n", "-Show face up permit", "/parameters: ");
			System.out.printf("%-50.50s  %-50.50s%n", "-Show for sale items", "/parameters: ");
			cli("\n");
			System.out.printf("%-5.5s  %-90.90s%n", "N.B:",
					"ALL COMMANDS CAN BE WRITTEN WITH OR WITHOUT SPACES AND WITH OR WITHOUT CAPITAL LETTERS. ");
			System.out.printf("%-5.5s  %-30.30s%n", " ", "BETWEEN COMMAND AND ITS PARAMETERS THERE MUST BE A SPACE.");
			System.out.printf("%-5.5s  %-30.30s%n", " ", "EXPRESSIONS BETWEEN PARETHESIS ARE OPTIONAL. ");
		} else {
			try {
				RequestMessage req = CliMessageFactory.getRequestMessage(cmd, this.playerToken);
				ResponseMessage r = requestProcessor.processRequest(req);
				r.visit(this.messageVisitor);
			} catch (RemoteException e) {
				logger.log(Level.SEVERE, "Unable to send a remote request.", e);
			} catch (InvalidCommandException ex) {
				logger.log(Level.FINE, this.playerToken.getUsername() + " has submitted a wrong formatted command.",
						ex);
				cli(ex.getMessage());
			}
		}
	}

	private void cli(String string) {
		out.println(string);
	}

}
