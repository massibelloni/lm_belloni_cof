package it.polimi.ingsw.lm_belloni_cof.view.playerside.gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.PlayerSideConnectionFactory;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.RMIConnection;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.SocketConnection;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.JPadding;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.StretchIcon;

import net.miginfocom.swing.MigLayout;

/** Main thread for the graphical user interface */
public class GUI implements Runnable {
	private static final Logger logger = Logger.getLogger(GUI.class.getName());
	private JFrame frame;
	private JTextField username;
	private JPasswordField password;
	private JRadioButton rmi;
	private JRadioButton socket;

	/** Asks for the connection method and then waits for user's interaction */
	@Override
	public void run() {
		final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		final GUIMessageVisitor guiVisitor = new GUIMessageVisitor();
		final GUIUpdater updater = new GUIUpdater(guiVisitor);
		frame = new JFrame("Council of Four - Login");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new MigLayout("fill"));
		frame.add(new JLabel(new StretchIcon("graphics/cof_logo.png"), SwingConstants.CENTER),
				"w 100%, h 25%,grow,wrap");
		frame.add(new JLabel("Username", SwingConstants.CENTER), "wrap,grow");
		this.username = new JTextField();
		frame.add(username, "wrap,grow");
		frame.add(new JLabel("Password", SwingConstants.CENTER), "wrap,grow");
		this.password = new JPasswordField();
		frame.add(password, "wrap,grow");
		this.rmi = new JRadioButton("RMI", true);
		this.socket = new JRadioButton("Socket");
		ButtonGroup group = new ButtonGroup();
		group.add(this.rmi);
		group.add(this.socket);
		JPanel connectionButtons = new JPanel(new GridLayout(1, 2));
		connectionButtons.setBorder(new TitledBorder("Connection type"));
		connectionButtons.add(rmi);
		connectionButtons.add(socket);
		frame.add(new JPadding(), "grow,wrap");
		frame.add(connectionButtons, "grow,wrap");
		JButton connect = new JButton("Connect");
		connect.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				try {
					PlayerSideConnectionFactory connection;
					if (rmi.isSelected())
						connection = new RMIConnection("localhost", updater);
					else
						connection = new SocketConnection("localhost", updater);
					if (connection.connect(username.getText(), new String(password.getPassword()))) {
						guiVisitor.setRequestsEndPoints(connection.getToken(), connection.getRequestProcessor());
						frame.setVisible(false);
					} else
						JOptionPane.showMessageDialog(frame, "Wrong password", "Error", JOptionPane.ERROR_MESSAGE);
				} catch (Exception ex) {
					logger.log(Level.INFO, "Connection error.", ex);
					JOptionPane.showMessageDialog(frame, "Connection error", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		frame.add(connect, "grow");
		frame.setSize(new Dimension(screenSize.width * 1 / 4, screenSize.height * 4 / 8));
		// frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setAlwaysOnTop(true);
		frame.setVisible(true);
	}
}
