package it.polimi.ingsw.lm_belloni_cof.view.playerside;

import org.json.JSONObject;

public abstract class GameStatusKeeper {
	private String username;
	private JSONObject data;
	private JSONObject status;

	public JSONObject getStatus() {
		return this.status;
	}

	public JSONObject getData() {
		return this.data;
	}
	
	public void setUsername(String username){
		this.username = username;
	}
	
	public String getUsername(){
		return this.username;
	}

	public void updateStatus(JSONObject status) {
		this.status = status;
	}

	public void setData(JSONObject data) {
		this.data = data;
	}
}
