package it.polimi.ingsw.lm_belloni_cof.view.gameside;

import it.polimi.ingsw.lm_belloni_cof.controller.GamesHandler;

/** Main server class that starts the servers */
public class GameSide {

	public static void main(String[] args) {
		GameSideConnectionFactory server = new GameSideConnectionFactory();
		GamesHandler gamesHandler = new GamesHandler(server.getBroadcasterInterface());
		server.setRequestProcessor(gamesHandler);
		server.startServers();
	}

}
