package it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.guimodel;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Swing element representing the top tile in the king region.
 */
public class JKingRewardTile extends JPanel {
	private static final long serialVersionUID = 1L;
	private int pickingOrder;
	private final static Color BG_COLOR = new Color(149, 115, 149);

	public JKingRewardTile(int pickingOrder, Map<String, Integer> bonuses) {
		super(new GridLayout(1, 2));
		super.setBorder(new LineBorder(Color.YELLOW, 1));
		super.setBackground(BG_COLOR);
		this.pickingOrder = pickingOrder;
		JLabel pick = new JLabel(Integer.toString(pickingOrder), SwingConstants.CENTER);
		pick.setFont(pick.getFont().deriveFont(25f));
		pick.setForeground(Color.white);
		super.add(pick);
		for (Map.Entry<String, Integer> entry : bonuses.entrySet())
			super.add(new JBonus(entry.getKey(), entry.getValue()));
	}

	/**
	 * Method that updates the painted tile only if it actually changes.
	 * 
	 * @param kingRewardTopTile
	 *            a json formatted KingRewardTile
	 */
	public void update(JSONObject kingRewardTopTile) {
		// otherwise no updates needed
		int newPickingOrder = kingRewardTopTile.getInt("pickingOrder");
		if (newPickingOrder != this.pickingOrder) {
			super.removeAll();
			super.setLayout(new GridLayout(1, 2));
			super.setBorder(new LineBorder(Color.YELLOW, 1));
			super.setBackground(BG_COLOR);
			this.pickingOrder = newPickingOrder;
			JLabel pick = new JLabel(Integer.toString(pickingOrder), SwingConstants.CENTER);
			pick.setFont(pick.getFont().deriveFont(25f));
			pick.setForeground(Color.white);
			super.add(pick);
			JSONArray bonuses = kingRewardTopTile.getJSONArray("bonuses");
			for (Object jsonBonus : bonuses) {
				JSONObject bonus = (JSONObject) jsonBonus;
				super.add(new JBonus(bonus.getString("bonus"), bonus.getInt("times")));
			}
			super.repaint();
		}
	}

	public void empty() {
		super.removeAll();
		super.setBackground(BG_COLOR);
	}
}
