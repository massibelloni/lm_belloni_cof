package it.polimi.ingsw.lm_belloni_cof.view.gameside;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.view.shared.RequestProcessor;

/** Socket server handling the request-response protocol. */
public class RequestProcessorSocketServer extends Thread {
	private static final Logger logger = Logger.getLogger(RequestProcessorSocketServer.class.getName());
	private final int port;
	private RequestProcessor requestProcessor;
	private ExecutorService executor;
	private boolean isRunning;

	/**
	 * Initialize the server.
	 * 
	 * @param port
	 *            the port where this server will run
	 * @param RequestProcessor
	 *            the {@link RequestProcessor} used generate responses based on
	 *            user requests.
	 */
	public RequestProcessorSocketServer(int port, RequestProcessor requestProcessor) {

		this.port = port;
		this.requestProcessor = requestProcessor;
		this.executor = Executors.newCachedThreadPool();
		this.isRunning = true;
	}

	public void stopServer() {
		this.isRunning = false;
	}

	/**
	 * Core method of the server that accepts connections creating new
	 * {@link ClientHandler} to handle them.
	 */
	@Override
	public void run() {

		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(this.port); //NOSONAR
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error during request processor socket server initialization.", e);
			return;
		}
		try {
			while (isRunning) {
				Socket socket;
				socket = serverSocket.accept();
				this.executor.execute(new ClientHandler(socket, this.requestProcessor));

			}
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error during request processor socket server execution", e);
		} finally {
			try {
				serverSocket.close();
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Error while trying to close request processor socket.", e);
			}
		}

	}

}
