package it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils;
import javax.swing.JPanel;

public class JPadding extends JPanel {

	private static final long serialVersionUID = 1L;

	public JPadding() {
		super();
		super.setOpaque(false);
	}
}
