package it.polimi.ingsw.lm_belloni_cof.view.playerside;

import it.polimi.ingsw.lm_belloni_cof.messages.*;

/**
 * Simple interface to actually visit message content using Visitor Pattern. An
 * implementation of this will have to provide some logic based on messages
 * content.
 */
public interface MessageVisitor {
	public void display(ChatBroadcastMessage msg);

	public void display(GameStartMessage msg);

	public void display(GameUpdateMessage msg);

	public void display(GameFinishedMessage msg);

	public void display(ConnectionResponseMessage msg);

	public void display(ChatResponseMessage msg);

	public void display(ActionExecutedMessage msg);

	public void display(ActionNotExecutedMessage msg);

	public void display(InvalidRequestMessage msg);

	public void display(PoliticsResponseMessage msg);

	public void display(SubscribeResponseMessage msg);

	public void display(DisconnectedPlayerBroadcastMessage msg);

	public void display(ReconnectedPlayerBroadcastMessage msg);
	
	public void display(PlayersTableResponseMessage msg);
}
