package it.polimi.ingsw.lm_belloni_cof.view.playerside.cli;

import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.*;

import it.polimi.ingsw.lm_belloni_cof.messages.*;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.DrawAPoliticActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.GameStatusKeeper;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;
import it.polimi.ingsw.lm_belloni_cof.view.shared.RequestProcessor;

/**
 * Cli implementation that actually visits message content using Visitor
 * Pattern.
 */
public class CliMessageVisitor extends GameStatusKeeper implements MessageVisitor {
	private static final Logger logger = Logger.getLogger(CliMessageVisitor.class.getName());
	private PrintStream out;
	private boolean autoDraw = false;
	private RequestProcessor requestProcessor;
	private PlayerToken playerToken;

	public CliMessageVisitor() {
		this.out = System.out;
	}

	public void cli(String string) {
		this.out.println(string);
	}

	/**
	 * Set the "autodraw" behaviour: automatize the process of drawing a politic
	 * card when the player's turn begins.
	 * 
	 * @param requestProcessor
	 *            a {@link RequestProcessor} to which the request has to be sent
	 * @param playerToken
	 *            the {@link PlayerToken} to authenticate.
	 */
	public void setAutoDraw(RequestProcessor requestProcessor, PlayerToken playerToken) {
		this.autoDraw = true;
		this.requestProcessor = requestProcessor;
		this.playerToken = playerToken;
	}

	@Override
	public void display(ChatBroadcastMessage msg) {
		cli("[" + msg.getSender() + "]" + msg.getMessage());
	}

	@Override
	public void display(ConnectionResponseMessage msg) {
		cli(msg.getMessage());
	}

	@Override
	public void display(PoliticsResponseMessage msg) {
		cli("Your politics are:");
		String politics = "";
		for (String color : msg.getPoliticColors())
			politics += color + " ";
		cli(politics);
	}

	@Override
	public void display(GameStartMessage msg) {
		super.setData(new JSONObject(msg.getData()));
		super.updateStatus(new JSONObject(msg.getUpdate()));
		cli(msg.getLastActionDescription());
		cli("Connected players are:");
		JSONObject data = new JSONObject(msg.getData());
		String playersStr = "";
		for (Object obj : (JSONArray) data.getJSONArray("players"))
			playersStr += (String) obj + " ";
		cli(playersStr);
		String currentPlayer = ((JSONObject) super.getStatus().getJSONObject("turn")).getString("username");
		/* if autodraw is set and i'm the first to play */
		if (autoDraw && currentPlayer.equals(super.getUsername())) {
			this.drawAPoliticRequest();
		}
	}

	@Override
	public void display(ActionExecutedMessage msg) {
		/**/
	}

	@Override
	public void display(ActionNotExecutedMessage msg) {
		cli("Your action hasn't been executed due to:");
		cli(msg.getReason());
	}

	@Override
	public void display(GameUpdateMessage msg) {
		String oldPlayer = ((JSONObject) super.getStatus().getJSONObject("turn")).getString("username");
		boolean wasMarketBuy = ((JSONObject) super.getStatus().getJSONObject("turn")).getBoolean("marketbuy");
		super.updateStatus(new JSONObject(msg.getUpdate()));
		cli(msg.getLastActionDescription());
		String currentPlayer = ((JSONObject) super.getStatus().getJSONObject("turn")).getString("username");
		boolean isMarketBuy = ((JSONObject) super.getStatus().getJSONObject("turn")).getBoolean("marketbuy");
		boolean isMarketOffer = ((JSONObject) super.getStatus().getJSONObject("turn")).getBoolean("marketoffer");
		boolean isMarket = isMarketBuy || isMarketOffer;
		boolean isMyTurn = currentPlayer.equals(super.getUsername());
		boolean wasMyTurn = oldPlayer.equals(super.getUsername());
		if (autoDraw) {
			if ((isMyTurn && !wasMyTurn && !wasMarketBuy && !isMarket) || (isMyTurn && wasMarketBuy && !isMarket)) {
				this.drawAPoliticRequest();
			}
		}
	}

	/** Actually performs a request to the server for a politic card draw */
	private void drawAPoliticRequest() {
		MessageVisitor visitor = this;
		new Thread() {
			public void run() {
				DrawAPoliticActRqstMsg rqst = new DrawAPoliticActRqstMsg(playerToken);
				try {
					requestProcessor.processRequest(rqst).visit(visitor);
				} catch (Exception e) {
					logger.log(Level.SEVERE, "Unable to connect to the server", e);
				}
			}
		}.start();
	}

	@Override
	public void display(ChatResponseMessage msg) {
		/* actually nothing to display */

	}

	@Override
	public void display(InvalidRequestMessage msg) {
		cli("The request sent is invalid due to " + msg.getError());
	}

	@Override
	public void display(SubscribeResponseMessage msg) {
		/* actually nothing to display */

	}

	@Override
	public void display(DisconnectedPlayerBroadcastMessage msg) {
		cli(msg.getDisconnected() + " has been disconnected.");
	}

	@Override
	public void display(GameFinishedMessage msg) {
		if (this.playerToken.getUsername().equals(msg.getWinner()))
			cli("You have won this game with the score of: " + msg.getScore(this.playerToken.getUsername()));
		else {
			cli(msg.getWinner() + " has won this game with the score of: " + msg.getScore(msg.getWinner()));
			cli("\nYour final score is: " + msg.getScore(this.playerToken.getUsername()));
		}

	}

	@Override
	public void display(ReconnectedPlayerBroadcastMessage msg) {
		cli(msg.getReconnected() + " has reconnected.");
	}

	@Override
	public void display(PlayersTableResponseMessage msg) {
		/**/
		
	}
}
