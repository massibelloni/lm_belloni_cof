package it.polimi.ingsw.lm_belloni_cof.view.playerside;

import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.messages.ConnectionRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ConnectionResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
import it.polimi.ingsw.lm_belloni_cof.view.shared.BrokerInterface;
import it.polimi.ingsw.lm_belloni_cof.view.shared.RequestProcessor;
import it.polimi.ingsw.lm_belloni_cof.view.shared.SubscriberInterface;

/**
 * Client connection class that provides connection methods independent from the
 * connection type chosen (RMI or Socket). The details of the connection used
 * are left to the implementations {@link RMIConnection} and
 * {@link SocketConnection}.
 */
public abstract class PlayerSideConnectionFactory {
	private static final Logger logger = Logger.getLogger(PlayerSideConnectionFactory.class.getName());
	private ViewUpdater viewUpdater;
	private Subscriber subscriber;
	private PlayerToken playerToken;

	/**
	 * Protected constructor of a {@link PlayerSideConnectionFactory.
	 * 
	 * @param viewUpdater
	 *            a {@link ViewUpdater} used to notify the view about incoming
	 *            messages in order to display them.
	 */
	protected PlayerSideConnectionFactory(ViewUpdater viewUpdater) {
		this.viewUpdater = viewUpdater;
		this.subscriber = new Subscriber(this.viewUpdater);
		this.playerToken = null; // set only after established connection
	}

	/**
	 * Login to the server using the specified username and password
	 * 
	 * @param username
	 * @param password
	 * @return true if the connection has been established, false otherwise.
	 */
	public boolean connect(String username, String password) {
		if (this.playerToken == null) {
			ConnectionRequestMessage msg = new ConnectionRequestMessage(username, password);
			try {
				this.getBrokerInterface().subscribe(getSubscriberInterface(), username);
				ConnectionResponseMessage response = (ConnectionResponseMessage) getRequestProcessor()
						.processRequest(msg);
				if (response.getPlayerToken() != null) {
					this.playerToken = response.getPlayerToken();
				} else
					return false;
				this.viewUpdater.updateView(response);
				return true;
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Network error during authentication", e);
			}
			return false;
		}
		// already connected
		return true;
	}

	public PlayerToken getToken() {
		return this.playerToken;
	}

	public Subscriber getSubscriber() {
		return this.subscriber;
	}

	public SubscriberInterface getSubscriberInterface() {
		return this.getSubscriber();
	}

	/**
	 * Abstract method. The implementation of the {@link RequestProcessor} is
	 * protocol dependent
	 */
	public abstract RequestProcessor getRequestProcessor();

	/**
	 * Abstract method. The implementation of the {@link BrokerInterface} is
	 * protocol dependent
	 */
	public abstract BrokerInterface getBrokerInterface();

}
