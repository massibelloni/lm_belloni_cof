package it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.guimodel;

import java.awt.Color;
import java.awt.Point;

import javax.swing.*;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.OverlapLayout;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.StretchIcon;

/**
 * Swing element representing a bonus that may be painted actually everywhere in
 * the game
 */
public class JBonus extends JPanel {
	private static final long serialVersionUID = 1L;

	/**
	 * Public constructor
	 * 
	 * @param bonus
	 *            a String representing the bonus type
	 * @param times
	 *            the number of times the bonus is on the tile
	 */
	public JBonus(String bonus, int times) {
		super(new OverlapLayout(new Point(0, 0)));
		super.setOpaque(false);
		super.add(new JLabel(new StretchIcon("graphics/bonuses/" + bonus + ".png")));
		JLabel timesLabel = new JLabel(Integer.toString(times), SwingConstants.CENTER);
		timesLabel.setForeground(Color.white);
		super.add(timesLabel);
	}
}
