package it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.guimodel;

import java.awt.*;
import javax.swing.*;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.CirclePane;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.OverlapLayout;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.StretchIcon;

/** Swing element representing a position in the victory path. */
public class JVictory extends JPanel {
	private static final long serialVersionUID = 1L;
	private Color occupiedBy = null;
	private JLabel posLabel = null;
	private CirclePane marker = null;

	/**
	 * Public constructor that creates the element
	 * 
	 * @param position
	 *            the position in the victory path of the JVictory created.
	 */
	public JVictory(int position) {
		super(new OverlapLayout(new Point(0, 0)));
		super.setOpaque(false);
		super.add(new JLabel(new StretchIcon("graphics/bonuses/victory_point.png")));
		this.posLabel = new JLabel(String.format("%02d", position), SwingConstants.CENTER);
		posLabel.setFont(posLabel.getFont().deriveFont(15f));
		posLabel.setForeground(Color.white);
		super.add(posLabel);
		marker = new CirclePane();
		marker.setVisible(false);
		super.add(marker);
	}

	/** Set the JVictory as occupied by a specific color. */
	public void setOccupiedBy(Color color) {
		this.occupiedBy = color;
		if (this.occupiedBy != null) {
			this.marker.setBackground(color);
			this.marker.setVisible(true);
		} else
			this.marker.setVisible(false);
	}

}
