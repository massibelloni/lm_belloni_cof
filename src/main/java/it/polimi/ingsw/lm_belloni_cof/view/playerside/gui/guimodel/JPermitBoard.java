package it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.guimodel;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.listeners.AcquireAPermitListener;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.listeners.ChangePermitTilesListener;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.StretchIcon;

/**
 * Swing element representing a permit board with a back permit tile and two
 * face up tiles.
 */
public class JPermitBoard extends JPanel {
	private static final long serialVersionUID = 1L;
	private int[] hashes;
	private transient AcquireAPermitListener[] acquireListeners;

	/**
	 * Public constructor of a JPermitBoard.
	 * 
	 * @param region
	 *            the region of this permit board
	 * @param permits
	 *            the permits that have to be placed (in JSON format)
	 * @param changeTilesListener
	 *            the {@link MouseListener} used for the quick action
	 *            "Change permit tiles"
	 * @param acquireListeners
	 *            the {@link MouseListener} used for the "Acquire a permit tile"
	 *            main action.
	 */
	public JPermitBoard(String region, JSONArray permits, ChangePermitTilesListener changeTilesListener,
			AcquireAPermitListener[] acquireListeners) {
		super(new GridLayout(1, 3));
		this.hashes = new int[permits.length()];
		this.acquireListeners = acquireListeners;
		JLabel back = new JLabel(new StretchIcon("graphics/permits/" + region.toLowerCase() + "-back.png"));
		back.addMouseListener(changeTilesListener);
		super.add(back);
		int index = 0;
		for (Object jsonPermit : permits) { // 2 times
			JSONObject permit = (JSONObject) jsonPermit;
			this.hashes[index] = this.permitHash(permit);
			JPermit jPermit = new JPermit(permit);
			jPermit.addMouseListener(this.acquireListeners[index]);
			super.add(jPermit);
			index++;
		}
	}

	/**
	 * Update the {@link JPermit}s on this permitboard only if they need to be
	 * updated.
	 * 
	 * @see #permitHash(JSONObject)
	 */
	public void update(JSONArray permits) {
		boolean updated = false;
		int index = 0;
		for (Object jsonPermit : permits) { // 2 times
			JSONObject permit = (JSONObject) jsonPermit;
			if (this.permitHash(permit) != this.hashes[index]) {
				super.remove(index + 1); // back
				JPermit jPermit = new JPermit(permit);
				jPermit.addMouseListener(this.acquireListeners[index]);
				super.add(jPermit, index + 1); // back!
				this.hashes[index] = this.permitHash(permit);
				updated = true;
			}
			index++;
		}
		if (updated)
			super.repaint();
	}

	/**
	 * Calculates a "unique" id associated to a json formatted permit based on
	 * its content. This number will be used to avoid useless gui updates.
	 * 
	 * @param permit
	 *            a json formatted permit
	 * @return a unique hash based on its content
	 */
	private int permitHash(JSONObject permit) {
		int hash = 0;
		JSONArray cities = permit.getJSONArray("cities");
		JSONArray bonuses = permit.getJSONArray("bonuses");
		for (Object city : cities)
			hash += ((String) city).hashCode();
		for (Object jsonBonus : bonuses) {
			JSONObject bonus = (JSONObject) jsonBonus;
			hash += (bonus.get("bonus")).hashCode() + bonus.getInt("times");
		}
		return hash;
	}
}
