package it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.guimodel;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Point;
import java.util.Map;

import javax.swing.*;
import javax.swing.border.LineBorder;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.GUIMessageVisitor;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.JPadding;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.OverlapLayout;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.StretchIcon;
import net.miginfocom.swing.MigLayout;

/**
 * Swing element representing a city painted in a {@link JGameMap}.
 */
public class JCity extends JPanel {
	private static final long serialVersionUID = 1L;
	private static final Color KING_COLOR = Color.yellow;
	private JPanel emporiums;
	private JLabel nameLabel;

	public JCity(String name, String color, Map<String, Integer> bonuses, int playersNumber) {
		super(new OverlapLayout(new Point(0, 0)));
		super.setOpaque(false);
		JPanel firstLayer = new JPanel(new MigLayout("fill"));
		firstLayer.setOpaque(false);
		firstLayer.add(new JLabel(new StretchIcon("graphics/cities/" + color + "-city.png")), "w 100%,h 99%,grow,wrap");
		this.emporiums = new JPanel(new GridLayout(1, playersNumber));
		for (int i = 0; i < playersNumber; i++) {
			JPanel emp = new JPanel();
			emp.setBackground(Color.darkGray);
			emp.setBorder(new LineBorder(Color.black, 1));
			this.emporiums.add(emp);
		}
		firstLayer.add(this.emporiums, "w 100%,grow");
		JPanel secondLayer = new JPanel(new GridLayout(3, 1));
		secondLayer.setOpaque(false);
		String toPrintName = Character.toUpperCase(name.charAt(0)) + name.substring(1).toLowerCase();
		this.nameLabel = new JLabel(toPrintName, SwingConstants.CENTER);
		nameLabel.setForeground(Color.white);
		nameLabel.setFont(GUIMessageVisitor.LUCIDA_BLACKLETTER.deriveFont(Font.BOLD, 15));
		secondLayer.add(nameLabel);
		JPanel bonusesPanel = new JPanel(new GridLayout(1, 0));
		bonusesPanel.setOpaque(false);
		for (Map.Entry<String, Integer> entry : bonuses.entrySet()) {
			JBonus bonus = new JBonus(entry.getKey(), entry.getValue().intValue());
			bonusesPanel.add(bonus);
		}
		secondLayer.add(bonusesPanel);
		secondLayer.add(new JPadding());
		super.add(firstLayer);
		super.add(secondLayer);
	}

	/**
	 * Add an emporium (represented by the color of the player) to this list
	 * 
	 * @param color
	 *            the color of the emporium to be added
	 * @return true if the emporium has actually been added, false if was
	 *         already there
	 */
	public boolean addEmporium(Color color) {
		if (!this.hasEmporium(color)) {
			for (int i = 0; i < this.emporiums.getComponentCount(); i++) {
				JPanel emp = (JPanel) this.emporiums.getComponent(i);
				if (emp.getBackground() == Color.darkGray) { // first available
					emp.setBackground(color);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Check if an emporium of the color passed as an argument has already been
	 * painted
	 * 
	 * @param color
	 *            the color to be checked
	 * @return true if an emporium of the color passed as an argument has
	 *         already been painted.
	 */
	public boolean hasEmporium(Color color) {
		for (int i = 0; i < this.emporiums.getComponentCount(); i++) {
			JPanel emp = (JPanel) this.emporiums.getComponent(i);
			if (emp.getBackground() == color) {
				return true;
			}
		}
		return false;
	}

	/** Set the city as the city with the king on */
	public void hasKingOn() {
		this.nameLabel.setForeground(KING_COLOR);
	}

	/** Restore the JCity to default condition */
	public void hasntKingOn() {
		this.nameLabel.setForeground(Color.white);
	}

}
