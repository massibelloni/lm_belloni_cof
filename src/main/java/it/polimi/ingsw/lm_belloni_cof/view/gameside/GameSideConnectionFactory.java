package it.polimi.ingsw.lm_belloni_cof.view.gameside;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.controller.Broadcaster;
import it.polimi.ingsw.lm_belloni_cof.controller.BroadcasterInterface;
import it.polimi.ingsw.lm_belloni_cof.view.shared.*;;

/** Class that manages the server side connection creation phase. */
public class GameSideConnectionFactory {
	private static final Logger logger = Logger.getLogger(GameSideConnectionFactory.class.getName());
	/**
	 * Maximum number of subscribers that can be managed at the same time from
	 * the {@link BrokerSocketServer} instance.
	 */
	private static final int MAX_SUBSCRIBERS = 50;
	private RequestProcessor requestProcessor;
	private Broadcaster broadcaster;
	private RequestProcessorSocketServer requestProcessorSocketServer;
	private BrokerSocketServer brokerSocketServer;

	public GameSideConnectionFactory() {
		this.broadcaster = new Broadcaster();
	}

	/** Get the object used as {@link BroadcasterInterface}. */
	public BroadcasterInterface getBroadcasterInterface() {
		return this.broadcaster;
	}

	/**
	 * Set the {@link RequestProcessor} in charge to hold the request-response
	 * protocol.
	 */
	public void setRequestProcessor(RequestProcessor requestProcessor) {
		this.requestProcessor = requestProcessor;
	}

	/** Start the servers: both RMIs and sockets. */
	public void startServers() {
		if (requestProcessor == null)
			throw new IllegalStateException("Request processor must be not null.");
		startRMI();
		startSocket();
	}

	/** Starting the RMI server publishing the objects on the registry. */
	private void startRMI() {
		System.setProperty("java.rmi.server.hostname", "localhost");
		try {
			Registry registry = LocateRegistry.createRegistry(ConnectionPorts.RMI_PORT);
			registry.bind("RequestProcessor",
					(RequestProcessor) UnicastRemoteObject.exportObject(this.requestProcessor, 0));
			registry.bind("Broker", (BrokerInterface) UnicastRemoteObject.exportObject(this.broadcaster, 0));
			logger.log(Level.INFO, "RMI objects correctly bound.");
		} catch (RemoteException | AlreadyBoundException e) {
			logger.log(Level.SEVERE, "Error during RMI initialization.", e);
		}
	}

	/**
	 * Starting the two socket servers: one for the request-response protocol
	 * (RequestProcessorSocketServer) one for the publisher-subscriber protocol
	 * (BrokerSocketServer).
	 */
	private void startSocket() {
		/* Request processor server */
		this.requestProcessorSocketServer = new RequestProcessorSocketServer(ConnectionPorts.REQPROC_SERVER_PORT,
				this.requestProcessor);
		this.requestProcessorSocketServer.start();
		/* Broadcaster server */
		this.brokerSocketServer = new BrokerSocketServer(ConnectionPorts.BROKER_SERVER_PORT,
				Executors.newFixedThreadPool(MAX_SUBSCRIBERS), this.broadcaster);
		this.brokerSocketServer.start();
		logger.log(Level.INFO, "Socket objects correctly started.");
	}
}
