package it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.listeners;

import java.util.List;

public interface ActionRequestProcessor {
	public void processActionRequest(List<String> params);
}
