package it.polimi.ingsw.lm_belloni_cof.view.playerside;

import it.polimi.ingsw.lm_belloni_cof.messages.BroadcastMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ResponseMessage;

/**
 * Simple interface that allows an external process to update the view using
 * messages without getting hand dirty with view details.
 */
public interface ViewUpdater {
	public void updateView(BroadcastMessage message);

	public void updateView(ResponseMessage message);
}
