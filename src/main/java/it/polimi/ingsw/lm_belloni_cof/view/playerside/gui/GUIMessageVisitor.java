package it.polimi.ingsw.lm_belloni_cof.view.playerside.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.lm_belloni_cof.messages.ActionExecutedMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ActionNotExecutedMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ChatBroadcastMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ChatRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ConnectionResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.DisconnectedPlayerBroadcastMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.GameFinishedMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.GameStartMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.GameUpdateMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.InvalidRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayersTableRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayersTableResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.PoliticsRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.PoliticsResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ReconnectedPlayerBroadcastMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.*;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.EmptyMessageVisitor;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.guimodel.*;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.listeners.AcquireAPermitListener;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.listeners.BuildAnEmporiumWithKingListener;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.listeners.ChangePermitTilesListener;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.listeners.CityTokenResponseListener;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.listeners.MarketOfferListener;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.JPadding;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.OverlapLayout;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.StretchIcon;
import it.polimi.ingsw.lm_belloni_cof.view.shared.RequestProcessor;
import net.miginfocom.swing.MigLayout;

/**
 * GUI implementation that actually visits message content using Visitor Pattern
 * creating and updating a graphical interface.
 */
public class GUIMessageVisitor extends EmptyMessageVisitor {
	private static final Logger logger = Logger.getLogger(GUIMessageVisitor.class.getName());
	private final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	private RequestProcessor requestProcessor;
	private PlayerToken playerToken;
	/* CONSTANTS */
	public static Font LUCIDA_BLACKLETTER;
	private final static Color VICTORY_PATH_BG = new Color(181, 144, 114);
	private final static Color REGION_REWARD_BG = new Color(194, 165, 136);
	private static final Map<String, Color> CITY_COLORS;

	static {
		CITY_COLORS = new HashMap<>();
		CITY_COLORS.put("gray", new Color(127, 133, 142));
		CITY_COLORS.put("red", new Color(183, 109, 81));
		CITY_COLORS.put("cyan", new Color(47, 148, 178));
		CITY_COLORS.put("yellow", new Color(195, 160, 74));
		CITY_COLORS.put("purple", new Color(93, 70, 93));
	}

	static {
		try {
			LUCIDA_BLACKLETTER = Font.createFont(Font.TRUETYPE_FONT,
					new FileInputStream(new File("graphics/lucida_blackletter.ttf")));
		} catch (FontFormatException | IOException e) {
			LUCIDA_BLACKLETTER = new Font("Lucida Blackletter", Font.BOLD, 15);
			logger.log(Level.SEVERE, "Error during font initialization.", e);
		}
	}

	/* GRAPHICS */
	private JFrame waitingFrame;
	private JFrame frame;
	private JFrame marketOfferFrame;
	private JFrame marketBuyFrame;
	private Map<String, Color> players;
	private JGameMap jGameMap;
	private JNobilityPath jNobilityPath;
	private JVictory[] victories;
	private Map<String, JPanel> colorTiles;
	private Map<String, JPanel> regionTiles;
	private Map<String, JCouncil> councils;
	private Map<String, JPermitBoard> permitBoards;
	private JKingRewardTile jKingRewardTile;
	private JPanel availableCouncillors;
	private JTextArea textArea = new JTextArea("");
	private JScrollPane scroll = new JScrollPane(textArea);
	private JTextField inputText;
	private JTextPane statuses;

	/* LISTENERS */
	private List<AcquireAPermitListener> acquireListeners;

	/* GAME STATUS */
	private List<String> politicColors; // avoiding useless requests
	private List<JSONObject> ownedPermits;
	private int cityTokenResponsesNeeded;
	private int faceUpPermitResponsesNeeded;
	private int ownedPermitResponsesNeeded;

	public void setRequestsEndPoints(PlayerToken playerToken, RequestProcessor requestProcessor) {
		this.requestProcessor = requestProcessor;
		this.playerToken = playerToken;
	}

	@Override
	public void display(ChatBroadcastMessage msg) {
		String old = this.textArea.getText();
		String append = "[" + msg.getSender() + "]" + msg.getMessage() + "\n";
		this.textArea.setText(old + append);
		JScrollBar vertical = scroll.getVerticalScrollBar();
		vertical.setValue(vertical.getMaximum());
	}

	@Override
	public void display(GameStartMessage msg) {
		new Thread() {
			public void run() {
				initGUI(new JSONObject(msg.getData()), new JSONObject(msg.getUpdate()));
			}
		}.start();
	}

	@Override
	public void display(GameFinishedMessage msg) {
		this.frame.setVisible(false);
		new Thread() {
			public void run() {
				JOptionPane.showMessageDialog(new JFrame(), msg.getWinner() + " has won the game!");
			}
		}.start();
	}

	@Override
	public void display(GameUpdateMessage msg) {
		JSONObject update = new JSONObject(msg.getUpdate());
		String old = this.textArea.getText();
		this.textArea.setText(old + msg.getLastActionDescription() + "\n");
		JScrollBar vertical = scroll.getVerticalScrollBar();
		vertical.setValue(vertical.getMaximum());
		/* backupping owned permits */
		this.ownedPermits = new ArrayList<>();
		JSONArray players = update.getJSONArray("players");
		for (Object jsonPlayer : players) {
			JSONObject player = (JSONObject) jsonPlayer;
			if (player.get("username").equals(this.playerToken.getUsername())) { // mine
				JSONArray permits = player.getJSONArray("permits");
				for (Object jsonPermit : permits)
					this.ownedPermits.add((JSONObject) jsonPermit);
				break;
			}
		}
		/* always updating gui */
		new Thread() {
			public void run() {
				updateGUI(update);
			}
		}.start();

		/*
		 * checking if is user market offer/buy to show a different input frame
		 */
		if (update.getJSONObject("turn").getString("username").equals(playerToken.getUsername()))
			if (update.getJSONObject("turn").getBoolean("marketoffer")) {
				if (marketOfferFrame != null && marketOfferFrame.isVisible())
					marketOfferFrame.setVisible(false);
				new Thread() {
					public void run() {
						// Dimension oldDim = (marketOfferFrame != null) ?
						// (marketOfferFrame.getSize()) : null;
						showMarketOfferFrame(update);
						/*
						 * if (oldDim != null) {
						 * marketOfferFrame.setSize(oldDim);
						 * marketOfferFrame.setLocationRelativeTo(null); }
						 */
					}
				}.start();
			} else if (update.getJSONObject("turn").getBoolean("marketbuy")) {
				if (marketBuyFrame != null && marketBuyFrame.isVisible())
					marketBuyFrame.setVisible(false);
				new Thread() {
					public void run() {
						// Dimension oldDim = (marketBuyFrame != null) ?
						// (marketBuyFrame.getSize()) : null;
						showMarketBuyFrame(update);
						/*
						 * if (oldDim != null) { marketBuyFrame.setSize(oldDim);
						 * marketBuyFrame.setLocationRelativeTo(null); }
						 */
					}
				}.start();
			}
	}

	@Override
	public void display(ConnectionResponseMessage msg) {
		new Thread() {
			public void run() {
				if (frame == null) {
					waitingFrame = new JFrame("Connection established");
					waitingFrame.setUndecorated(true);
					waitingFrame.setLayout(new MigLayout("fill"));
					waitingFrame.add(new JLabel(msg.getMessage(), SwingConstants.CENTER), "grow,wrap");
					waitingFrame.add(new JLabel(new ImageIcon("graphics/loader.gif")), "grow");
					waitingFrame.pack();
					waitingFrame.setLocationRelativeTo(null);
					waitingFrame.setVisible(true);
				}
			}
		}.start();
	}

	@Override
	public void display(PlayersTableResponseMessage msg){
		new Thread() {
			public void run() {
				JFrame tableFrame = new JFrame("Council of Four - Table");
				Map<String,Integer> played = msg.getGamesPlayed();
				Map<String,Integer> won = msg.getGamesWon();
				Map<String, Integer> minutes = msg.getGameTimeInMinutes();
				JTabbedPane mainPane = new JTabbedPane();
				String[] header = new String[]{"",""};
				/* played */
				int cont = 0;
				String[][] playedContent = new String[played.keySet().size()][2];
				for(Map.Entry<String,Integer> entry:played.entrySet()){
					playedContent[cont][0] = entry.getKey();
					playedContent[cont++][1] = entry.getValue().toString();
				}
				JPanel playedPanel = new JPanel(new GridLayout());
				JTable playedTable = new JTable(playedContent,header);
				playedTable.setEnabled(false);
				playedPanel.add(playedTable);
				mainPane.add("Played",playedPanel);
				/* won */
				String[][] wonContent = new String[won.keySet().size()][2];
				cont = 0;
				for(Map.Entry<String,Integer> entry:won.entrySet()){
					wonContent[cont][0] = entry.getKey();
					wonContent[cont++][1] = entry.getValue().toString();
				}
				JPanel wonPanel = new JPanel(new GridLayout());
				JTable wonTable = new JTable(wonContent,header);
				wonTable.setEnabled(false);
				wonPanel.add(wonTable);
				mainPane.add("Won",wonPanel);
				/* minutes */
				String[][] minutesContent = new String[minutes.keySet().size()][2];
				cont = 0;
				for(Map.Entry<String,Integer> entry:minutes.entrySet()){
					minutesContent[cont][0] = entry.getKey();
					minutesContent[cont++][1] = entry.getValue().toString();
				}
				JPanel minutesPanel = new JPanel(new GridLayout());
				JTable minutesTable = new JTable(minutesContent,header);
				minutesTable.setEnabled(false);
				minutesPanel.add(minutesTable);
				mainPane.add("Minutes played",minutesPanel);
				tableFrame.add(mainPane);
				tableFrame.setPreferredSize(new Dimension(screenSize.width/4, screenSize.height/5));
				tableFrame.pack();
				tableFrame.setLocationRelativeTo(null);
				tableFrame.setVisible(true);
			}
		}.start();
	}
	
	@Override
	public void display(ActionExecutedMessage msg) {
		this.cityTokenResponsesNeeded += msg.getCityTokenResponsesAdded();
		this.ownedPermitResponsesNeeded += msg.getOwnedPermitResponsesAdded();
		this.faceUpPermitResponsesNeeded += msg.getFaceUpPermitResponsesAdded();
		/* handling dialogs on another thread */
		new Thread() {
			public void run() {
				if (cityTokenResponsesNeeded > 0) {
					JOptionPane.showMessageDialog(new JFrame(), "You have to select a city to get bonuses from it.");
				}
				if (ownedPermitResponsesNeeded > 0) {
					JOptionPane.showMessageDialog(new JFrame(),
							"You have to select an owned permit to get bonuses from it.");
					showPermitsHand();
				}
				if (faceUpPermitResponsesNeeded > 0) {
					for (int i = 0; i < acquireListeners.size(); i++)
						acquireListeners.get(i).setWaitingState(true);
					JOptionPane.showMessageDialog(new JFrame(),
							"You have to select a face up permit to get as a bonus.");
				} else {
					for (int i = 0; i < acquireListeners.size(); i++)
						acquireListeners.get(i).setWaitingState(false);
				}
			}
		}.start();
	}

	@Override
	public void display(ActionNotExecutedMessage msg) {
		new Thread() {
			public void run() {
				JOptionPane.showMessageDialog(frame, msg.getReason(), "Action not executed", JOptionPane.ERROR_MESSAGE);
			}
		}.start();
	}

	@Override
	public void display(InvalidRequestMessage msg) {
		new Thread() {
			public void run() {
				JOptionPane.showMessageDialog(frame, msg.getError(), "Invalid request", JOptionPane.ERROR_MESSAGE);
			}
		}.start();

	}

	@Override
	public void display(PoliticsResponseMessage msg) {
		this.politicColors = msg.getPoliticColors();
	}

	@Override
	public void display(DisconnectedPlayerBroadcastMessage msg) {
		if (!msg.getDisconnected().equals(this.playerToken.getUsername())) {
			/* updating graphics */
			new Thread() {
				@Override
				public void run() {
					String old = textArea.getText();
					String append = "[" + msg.getDisconnected() + "]has disconnected\n";
					textArea.setText(old + append);
					JScrollBar vertical = scroll.getVerticalScrollBar();
					vertical.setValue(vertical.getMaximum());
				}
			}.start();
		} else {
			frame.setVisible(false);
			if (this.marketOfferFrame != null && this.marketOfferFrame.isVisible())
				this.marketOfferFrame.setVisible(false);
			if (this.marketBuyFrame != null && this.marketBuyFrame.isVisible())
				this.marketBuyFrame.setVisible(false);
			new Thread() {
				public void run() {
					JOptionPane.showMessageDialog(new JFrame(),
							"You have been disconnected, restart the game to reconnect.", "Disconnected",
							JOptionPane.ERROR_MESSAGE);
				}
			}.start();
		}
	}

	@Override
	public void display(ReconnectedPlayerBroadcastMessage msg) {
		String old = this.textArea.getText();
		String append = "[" + msg.getReconnected() + "] has reconnected\n";
		this.textArea.setText(old + append);
		JScrollBar vertical = scroll.getVerticalScrollBar();
		vertical.setValue(vertical.getMaximum());
	}

	private synchronized void initGUI(JSONObject data, JSONObject status) {
		if (this.waitingFrame != null)
			this.waitingFrame.setVisible(false);
		this.ownedPermits = new ArrayList<>(); // empty when game starts
		/* PLAYERS TO COLOR MAPPING */
		this.players = new HashMap<>();
		JSONArray jsonPlayers = data.getJSONArray("players");
		for (Object player : jsonPlayers) {
			players.put((String) player, new Color((int) (Math.random() * 0x1000000)));
		}
		this.frame = new JFrame("Council of Four - " + this.playerToken.getUsername());
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new MigLayout("fill"));
		// frame.getContentPane().setBackground(VICTORY_PATH_BG.brighter());
		JPanel map = new JPanel(new MigLayout("fill"));
		/* inner map */
		JPanel mapBody = new JPanel(new MigLayout());
		/* creating a set of listeners */
		JSONArray cities = data.getJSONObject("map").getJSONArray("cities");
		BuildAnEmporiumWithKingListener[] buildListeners = new BuildAnEmporiumWithKingListener[cities.length()];
		for (int i = 0; i < cities.length(); i++)
			buildListeners[i] = new BuildAnEmporiumWithKingListener(((JSONObject) cities.get(i)).getString("name"),
					playerToken, requestProcessor, this);
		this.jGameMap = new JGameMap(cities, status.getString("kingsLocation"),
				(this.players.keySet().size() == 2) ? 3 : (this.players.keySet().size()), buildListeners);
		/* emporiums on cities -> 2 players game? */
		for (Object jsonCity : status.getJSONArray("cities")) {
			JSONObject city = (JSONObject) jsonCity;
			String name = city.getString("city");
			for (Object player : city.getJSONArray("emporiums"))
				this.jGameMap.drawAnEmporium(name,
						(this.players.get(player) != null) ? (this.players.get(player)) : (Color.MAGENTA));
		}
		mapBody.add(jGameMap, "w 100%,h 70%,wrap");
		this.permitBoards = new HashMap<>();
		this.councils = new HashMap<>();
		this.regionTiles = new HashMap<>();
		Map<String, JPanel> regions = new HashMap<>();
		JSONArray jsonRegionTiles = data.getJSONObject("map").getJSONArray("regionRewardTiles");
		/* region box with permit tiles and council */
		JSONObject kingRegion = null;
		this.acquireListeners = new ArrayList<>();
		for (Object jsonRegion : status.getJSONArray("regions")) {
			JSONObject region = (JSONObject) jsonRegion;
			if (!region.getString("region").equals("king")) {
				JPanel regionPanel = new JPanel(new GridLayout(2, 1));
				regionPanel.setBorder(new TitledBorder(Character.toUpperCase(region.getString("region").charAt(0))
						+ region.getString("region").substring(1)));
				/* preparing the two cards listeners */
				JSONArray permits = region.getJSONArray("permits");
				AcquireAPermitListener[] regionListeners = new AcquireAPermitListener[permits.length()];
				for (int i = 0; i < regionListeners.length; i++) {
					regionListeners[i] = new AcquireAPermitListener(region.getString("region"), i + 1, this.playerToken,
							this.requestProcessor, this);
					this.acquireListeners.add(regionListeners[i]);
				}
				JPermitBoard permitBoard = new JPermitBoard(region.getString("region"), permits,
						new ChangePermitTilesListener(region.getString("region"), this.playerToken,
								this.requestProcessor, this),
						regionListeners);
				this.permitBoards.put(region.getString("region"), permitBoard);
				JSONArray council = region.getJSONArray("council");
				String[] colors = new String[4];
				/*
				 * not from 0 to 3 but from 3 to 0 to have councillors ordered
				 * as in the board game.
				 */
				for (int i = 3, j = 0; i >= 0; i--, j++)
					colors[j] = council.getString(i);
				JCouncil jcouncil = new JCouncil(colors);
				this.councils.put(region.getString("region"), jcouncil);
				/* looking for the correct region tile */
				JSONObject tile = null;
				for (Object jsonTile : jsonRegionTiles) {
					if (((JSONObject) jsonTile).getString("region").equals(region.getString("region"))) {
						tile = (JSONObject) jsonTile;
						break;
					}
				}
				JPanel regionTile = new JPanel(new GridLayout(1, 0));
				regionTile.setBackground(REGION_REWARD_BG);
				regionTile.setBorder(new LineBorder(Color.black, 1));
				regionTile.add(new JLabel(new StretchIcon("graphics/regions/" + region.getString("region") + ".png")));
				for (Object jsonBonus : tile.getJSONArray("bonuses")) {
					JSONObject bonus = (JSONObject) jsonBonus;
					regionTile.add(new JBonus(bonus.getString("bonus"), bonus.getInt("times")));
				}
				this.regionTiles.put(region.getString("region"), regionTile);
				regionPanel.add(permitBoard, "w 100%,h 10%,wrap,grow");
				JPanel padding = new JPanel(new MigLayout("fill"));
				padding.add(jcouncil, "w 100%,h 50%,wrap,grow");
				padding.add(regionTile, "w 100%,h 50%,grow");
				regionPanel.add(padding);
				regions.put(region.getString("region"), regionPanel);
			} else
				kingRegion = region; // useful later, avoiding extra cycle
		}

		JPanel mapBottom = new JPanel(new MigLayout("fill"));
		/* KING REGION */
		JPanel king = new JPanel(new MigLayout("fill"));
		king.setBorder(new TitledBorder("King"));
		JSONArray council = kingRegion.getJSONArray("council");
		String[] colors = new String[4];
		/*
		 * not from 0 to 3 but from 3 to 0 to have councillors ordered as in the
		 * board game.
		 */
		for (int i = 3, j = 0; i >= 0; i--, j++)
			colors[j] = council.getString(i);
		JCouncil kingCouncil = new JCouncil(colors);
		this.councils.put("king", kingCouncil);
		king.add(kingCouncil, "w 100%,h 25%,wrap");

		/* all the councils has been placed, i can now add listeners */
		for (Map.Entry<String, JCouncil> entry : this.councils.entrySet()) {
			entry.getValue().addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					JFrame howToElectFrame = new JFrame("How?");
					howToElectFrame.setLayout(new MigLayout("fill"));
					JButton electACouncillor = new JButton("Elect a councillor");
					electACouncillor.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							howToElectFrame.setVisible(false);
							showWhichCouncillorToElect(entry.getKey(), true);
						}
					});
					JButton sendAnAssistant = new JButton("Send an assistant");
					sendAnAssistant.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							howToElectFrame.setVisible(false);
							showWhichCouncillorToElect(entry.getKey(), false);
						}
					});
					howToElectFrame.add(electACouncillor, "wrap,grow");
					howToElectFrame.add(sendAnAssistant, "grow");
					electACouncillor.setFocusPainted(false);
					sendAnAssistant.setFocusPainted(false);
					howToElectFrame.pack();
					howToElectFrame.setLocationRelativeTo(null);
					howToElectFrame.setVisible(true);
				}
			});

		}
		/* king reward tile */
		/* creating bonuses map */
		Map<String, Integer> kingBonuses = new HashMap<>();
		JSONArray kingRewardTopTile = status.getJSONObject("kingRewardTopTile").getJSONArray("bonuses");
		for (Object bonus : kingRewardTopTile)
			kingBonuses.put(((JSONObject) bonus).getString("bonus"), ((JSONObject) bonus).getInt("times"));
		this.jKingRewardTile = new JKingRewardTile(status.getJSONObject("kingRewardTopTile").getInt("pickingOrder"),
				kingBonuses);
		king.add(jKingRewardTile, "w 100%,h 25%,wrap");
		/* color reward tiles */
		this.colorTiles = new HashMap<>();
		JPanel colorTilesPanel = new JPanel(new GridLayout(4, 1));
		JSONArray colorRewardTiles = data.getJSONObject("map").getJSONArray("colorRewardTiles");
		for (Object jsonColorTile : colorRewardTiles) {
			JSONObject colorTile = (JSONObject) jsonColorTile;
			JPanel colorTilePanel = new JPanel(new GridLayout());
			for (Object bonus : colorTile.getJSONArray("bonuses")) {
				colorTilePanel
						.add(new JBonus(((JSONObject) bonus).getString("bonus"), ((JSONObject) bonus).getInt("times")));
			}
			colorTilePanel.setBackground(CITY_COLORS.get(colorTile.getString("color")));
			colorTilesPanel.add(colorTilePanel);
			this.colorTiles.put(colorTile.getString("color"), colorTilePanel);
		}
		colorTilesPanel.setBorder(new LineBorder(Color.black, 1));
		king.add(colorTilesPanel, "w 100%,h 50%");

		/* ADDING REGIONS */
		// hard coded because order is important
		JPanel regionBottom = new JPanel(new GridLayout(1, 4));
		regionBottom.add(regions.get("coast"), "w 25%,grow");
		regionBottom.add(regions.get("hills"), "w 25%,grow");
		regionBottom.add(regions.get("mountains"), "w 25%,grow");
		regionBottom.add(king, "w 25%,grow,wrap");
		mapBottom.add(regionBottom, "w 100%,h 90%,grow,wrap");
		/* NOBILITY PATH BUTTON */
		this.jNobilityPath = new JNobilityPath(data.getJSONObject("map").getJSONArray("nobilityPath"));
		JButton nobility = new JButton("Nobility track");
		nobility.setBackground(Color.darkGray);
		nobility.setOpaque(true);
		nobility.setBorderPainted(false);
		nobility.setForeground(Color.white);
		nobility.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if (!jNobilityPath.isVisible())
					jNobilityPath.setVisible(true);
				else
					jNobilityPath.repaint();
			}
		});
		mapBottom.add(nobility, "span 4,growx");
		mapBody.add(mapBottom, "w 100%,h 30%");

		/* VICTORY PATH */
		int maxVictoryPoints = data.getJSONObject("map").getInt("maxVictoryPoints");
		int victoryPathSize = maxVictoryPoints + 1; // from 0!
		this.victories = new JVictory[victoryPathSize];
		/* TOP BAR */
		JPanel top = new JPanel();
		top.setBackground(VICTORY_PATH_BG);
		top.setLayout(new BoxLayout(top, BoxLayout.X_AXIS));
		int quarter = (int) victoryPathSize / 4;
		for (int i = 0; i < quarter; i++) {
			victories[i] = new JVictory(i);
			top.add(victories[i]);
		}
		/* RIGHT BAR */
		JPanel right = new JPanel(new GridLayout(0, 1));
		right.setLayout(new BoxLayout(right, BoxLayout.Y_AXIS));
		right.setBackground(VICTORY_PATH_BG);
		for (int i = quarter; i < 2 * quarter; i++) {
			victories[i] = new JVictory(i);
			right.add(victories[i]);
		}
		/* BOTTOM BAR */
		JPanel bottom = new JPanel(new GridLayout(1, 0));
		bottom.setBackground(VICTORY_PATH_BG);
		for (int i = 3 * quarter - 1; i >= 2 * quarter; i--) {
			victories[i] = new JVictory(i);
			bottom.add(victories[i]);
		}
		/* LEFT BAR */
		JPanel left = new JPanel(new GridLayout(0, 1));
		left.setBackground(VICTORY_PATH_BG);
		for (int i = 4 * quarter - 1; i >= 3 * quarter; i--) {
			victories[i] = new JVictory(i);
			left.add(victories[i]);
		}
		map.add(top, "dock north");
		map.add(bottom, "dock south");
		map.add(right, "dock east");
		map.add(left, "dock west");

		/* ADDING BODY */
		map.add(mapBody, "w 100%,h 100%,grow");

		/* ADDING MAP */
		frame.add(map, "w 80%,grow");

		/* CREATING SIDE */
		JPanel side = new JPanel(new GridLayout(2, 1));
		/* GAME SIDE */
		JPanel gameSide = new JPanel(new GridLayout(2, 1));
		JPanel personalButtons = new JPanel(new GridLayout(0, 1));
		JButton showPolitics = new JButton("Politics hand");
		showPolitics.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					requestProcessor.processRequest(new PoliticsRequestMessage(playerToken))
							.visit(GUIMessageVisitor.this);
					showPoliticsHand();
				} catch (RemoteException e1) {
					logger.log(Level.SEVERE, "Error during GUI politics request.", e1);
					JOptionPane.showMessageDialog(frame, "The request hasn't been sent.", "Request not sent",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		JButton permitsAcquired = new JButton("Permits acquired");
		permitsAcquired.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				showPermitsHand();
			}
		});
		JPanel quicks = new JPanel(new GridLayout(2, 1));
		JButton main = new JButton("Perform another main action");
		main.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					requestProcessor.processRequest(new PerformAnAdditionalMainActionActRqstMsg(playerToken))
							.visit(GUIMessageVisitor.this);
				} catch (RemoteException e1) {
					logger.log(Level.SEVERE, "Error during GUI additional main.", e1);
					JOptionPane.showMessageDialog(frame, "The action request hasn't been sent.", "Request not sent",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		JButton endTurn = new JButton("No quick action");
		endTurn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					requestProcessor.processRequest(new EmptyQuickActionActRqstMsg(playerToken))
							.visit(GUIMessageVisitor.this);
				} catch (RemoteException e1) {
					logger.log(Level.SEVERE, "Error during GUI empty quick.", e1);
					JOptionPane.showMessageDialog(frame, "The action request hasn't been sent.", "Request not sent",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		quicks.add(main);
		quicks.add(endTurn);
		personalButtons.add(showPolitics);
		personalButtons.add(permitsAcquired);
		personalButtons.add(quicks);
		gameSide.add(personalButtons);
		JPanel availables = new JPanel(new GridLayout(1, 2));
		JPanel availablesLeft = new JPanel(new MigLayout("fill"));
		/* AVAILABLE COUNCILLORS */
		this.availableCouncillors = new JPanel(new GridLayout(0, 4));
		this.availableCouncillors.setBorder(new LineBorder(Color.black, 1));
		JSONArray councillors = status.getJSONArray("councillors");
		for (int i = 0; i < councillors.length(); i++) {
			JLabel councillor = new JLabel(
					new StretchIcon("graphics/councillors/councillor-" + councillors.getString(i) + ".png"));
			councillor.setName(councillors.getString(i));
			this.availableCouncillors.add(councillor);
		}
		availablesLeft.add(availableCouncillors, "grow,wrap");
		JLabel assistantsPool = new JLabel(new StretchIcon("graphics/assistants-pool.png"));
		assistantsPool.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					requestProcessor.processRequest(new EngageAnAssistantActRqstMsg(playerToken))
							.visit(GUIMessageVisitor.this);
				} catch (RemoteException e1) {
					logger.log(Level.SEVERE, "Error during GUI engage an assistant.", e1);
					JOptionPane.showMessageDialog(frame, "The action request hasn't been sent.", "Request not sent",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		availablesLeft.add(assistantsPool, "grow");
		availables.add(availablesLeft);
		JLabel politicDeck = new JLabel(new StretchIcon("graphics/politics/back-politic.png"));
		politicDeck.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					requestProcessor.processRequest(new DrawAPoliticActRqstMsg(playerToken))
							.visit(GUIMessageVisitor.this);
				} catch (RemoteException e1) {
					logger.log(Level.SEVERE, "Error during GUI draw a politic.", e1);
					JOptionPane.showMessageDialog(frame, "The action request hasn't been sent.", "Request not sent",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		availables.add(politicDeck);
		gameSide.add(availables);
		side.add(gameSide);
		/* OTHER PLAYER SIDE */
		/* PLAYERS STATUS */
		JPanel playersSide = new JPanel(new MigLayout("fill"));
		JPanel playerStatuses = new JPanel();
		playerStatuses.setBackground(Color.white);
		this.statuses = new JTextPane();
		/* a click on the player's list sends a request for the players' table */
		this.statuses.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				try {
					requestProcessor.processRequest(new PlayersTableRequestMessage())
							.visit(GUIMessageVisitor.this);
				} catch (RemoteException e1) {
					logger.log(Level.SEVERE, "Error during GUI players table request.", e1);
					JOptionPane.showMessageDialog(frame, "The request hasn't been sent.", "Request not sent",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		statuses.setEditable(false);
		statuses.setOpaque(false);
		StyledDocument doc = statuses.getStyledDocument();
		Style style = statuses.addStyle("CoF", null);
		for (Object jsonPlayer : status.getJSONArray("players")) {
			try {
				JSONObject player = (JSONObject) jsonPlayer;
				StyleConstants.setForeground(style, this.players.get(player.getString("username")));
				/* current player */
				if (status.getJSONObject("turn").getString("username").equals(player.getString("username")))
					StyleConstants.setUnderline(style, true);
				else
					StyleConstants.setUnderline(style, false);
				doc.insertString(doc.getLength(), player.getString("username"), style);
				StyleConstants.setForeground(style, Color.black);
				doc.insertString(doc.getLength(), " " + player.getInt("assistants") + " A ", style);
				StyleConstants.setForeground(style, Color.ORANGE);
				doc.insertString(doc.getLength(), player.getInt("coins") + " C ", style);
				StyleConstants.setForeground(style, Color.BLACK);
				doc.insertString(doc.getLength(), player.getInt("emporiums") + " E\n", style);
				/* avoiding extracycles */
				this.victories[player.getInt("victoryPoints")]
						.setOccupiedBy(this.players.get(player.getString("username")));
				this.jNobilityPath.setPlayerPosition(this.players.get(player.getString("username")),
						player.getInt("nobilityTrackPosition"));
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Error during GUI init", e);
			}
		}
		playerStatuses.add(statuses);
		playerStatuses.setBorder(new TitledBorder("Players"));
		// playersStatus.setBackground(Color.white);
		playersSide.add(playerStatuses, "w 100%,h 25%,wrap");
		JPanel chatbox = new JPanel(new MigLayout("fill"));
		// this.textArea = new JTextArea("");
		textArea.setEditable(false);
		chatbox.add(scroll, "span,w 100%,h 90%,wrap,grow");
		this.inputText = new JTextField();
		chatbox.add(this.inputText, "w 75%,h 10%,grow");
		JButton send = new JButton("Send");
		send.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					requestProcessor.processRequest(new ChatRequestMessage(playerToken, inputText.getText()));
				} catch (RemoteException e1) {
					logger.log(Level.SEVERE, "Error during GUI chat request.", e1);
					JOptionPane.showMessageDialog(frame, "The chat message hasn't been sent.", "Message not sent",
							JOptionPane.ERROR_MESSAGE);
				} finally {
					inputText.setText("");
				}
			}
		});
		chatbox.add(send, "w 25%,h 10%,grow");
		playersSide.add(chatbox, "w 100%,h 75%,grow");
		side.add(playersSide);
		frame.add(side, "w 20%,grow");
		frame.setPreferredSize(new Dimension(screenSize.width, screenSize.height));
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		notify();
	}

	private synchronized void updateGUI(JSONObject status) {
		while (this.frame == null)
			try {
				wait();
			} catch (InterruptedException e1) {
				logger.log(Level.SEVERE, "While waiting for frame to be created before update.", e1);
			}
		/* region reward tile used */
		JSONArray regionRewardTilesUsed = status.getJSONArray("regionRewardTilesUsed");
		for (Object regionTile : regionRewardTilesUsed) {
			this.regionTiles.get((String) regionTile).removeAll();
			this.regionTiles.get((String) regionTile).repaint();
		}
		/* color reward tile used */
		JSONArray colorRewardTilesUsed = status.getJSONArray("colorRewardTilesUsed");
		for (Object colorTile : colorRewardTilesUsed) {
			this.colorTiles.get((String) colorTile).removeAll();
			this.colorTiles.get((String) colorTile).repaint();
		}
		/* updating king reward tile */
		JSONObject kingRewardTopTile = status.getJSONObject("kingRewardTopTile");
		this.jKingRewardTile.update(kingRewardTopTile);
		/* updating councils and permits */
		JSONArray regions = status.getJSONArray("regions");
		for (Object jsonRegion : regions) {
			JSONObject region = (JSONObject) jsonRegion;
			if (this.permitBoards.get(region.get("region")) != null) // not king
				this.permitBoards.get(region.get("region")).update(region.getJSONArray("permits"));
			// retrieving updated council
			JSONArray council = region.getJSONArray("council");
			String[] colors = new String[4];
			/*
			 * not from 0 to 3 but from 3 to 0 to have councillors ordered as in
			 * the board game.
			 */
			for (int i = 3, j = 0; i >= 0; i--, j++)
				colors[j] = council.getString(i);
			this.councils.get(region.get("region")).update(colors);
		}
		/* emporiums on cities */
		JSONArray cities = status.getJSONArray("cities");
		for (Object jsonCity : cities) {
			JSONObject city = (JSONObject) jsonCity;
			String name = city.getString("city");
			for (Object player : city.getJSONArray("emporiums")) {
				/*
				 * changing listener if the emporium is from the player. The
				 * click on that city doesn't build an emp anymore but is used
				 * for citytoken response
				 */
				if (this.jGameMap.drawAnEmporium(name,
						(this.players.get(player) != null) ? (this.players.get(player)) : (Color.MAGENTA))
						&& player.equals(playerToken.getUsername())) {
					this.jGameMap.changeClickBehaviour(name,
							new CityTokenResponseListener(name, playerToken, requestProcessor, this));
				}
			}
		}
		/* king's location */
		this.jGameMap.setKingLocation(status.getString("kingsLocation"));
		/* emptying victory path */
		for (int i = 0; i < this.victories.length; i++)
			this.victories[i].setOccupiedBy(null);
		/* rebuilding players statuses */
		this.statuses.setText("");
		StyledDocument doc = statuses.getStyledDocument();
		Style style = statuses.addStyle("CoF", null);
		for (Object jsonPlayer : status.getJSONArray("players")) {
			try {
				JSONObject player = (JSONObject) jsonPlayer;
				StyleConstants.setForeground(style, this.players.get(player.getString("username")));
				/* current player */
				if (status.getJSONObject("turn").getString("username").equals(player.getString("username")))
					StyleConstants.setUnderline(style, true);
				else
					StyleConstants.setUnderline(style, false);
				doc.insertString(doc.getLength(), player.getString("username"), style);
				StyleConstants.setForeground(style, Color.black);
				doc.insertString(doc.getLength(), " " + player.getInt("assistants") + " A ", style);
				StyleConstants.setForeground(style, Color.ORANGE);
				doc.insertString(doc.getLength(), player.getInt("coins") + " C ", style);
				StyleConstants.setForeground(style, Color.BLACK);
				doc.insertString(doc.getLength(), player.getInt("emporiums") + " E\n", style);
				/* avoiding extracycles */
				this.victories[player.getInt("victoryPoints")]
						.setOccupiedBy(this.players.get(player.getString("username")));
				this.jNobilityPath.setPlayerPosition(this.players.get(player.getString("username")),
						player.getInt("nobilityTrackPosition"));
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Error during GUI init", e);
			}
		}
		/* rebuilding available councillors */
		this.availableCouncillors.removeAll();
		JSONArray councillors = status.getJSONArray("councillors");
		for (int i = 0; i < councillors.length(); i++) {
			JLabel councillor = new JLabel(
					new StretchIcon("graphics/councillors/councillor-" + councillors.getString(i) + ".png"));
			councillor.setName(councillors.getString(i));
			this.availableCouncillors.add(councillor);
		}
	}

	private void showPermitsHand() {
		JFrame permitsFrame = new JFrame("Permits hand");
		permitsFrame.setLayout(new GridLayout(1, 0));
		int permitIndex = 1;
		for (JSONObject permit : this.ownedPermits) {
			JPanel permitPanel = new JPanel(new OverlapLayout((new Point(0, 0))));
			/* i've to create the jpermit obj */
			String[] cities = new String[permit.getJSONArray("cities").length()];
			for (int i = 0; i < cities.length; i++)
				cities[i] = permit.getJSONArray("cities").getString(i);
			JPermit jPermit = new JPermit(permit);
			jPermit.setName(Integer.toString(permitIndex));
			if (this.ownedPermitResponsesNeeded == 0) {
				if (!permit.getBoolean("used")) {
					jPermit.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							/*
							 * i've to check if the chosen permit contains only
							 * one city (if so i can move on with the action) or
							 * more than one city (if so i've to make the player
							 * pick one).
							 */
							if (cities.length == 1) {
								try {
									requestProcessor
											.processRequest(new BuildAnEmporiumUsingAPermitTileActRqstMsg(playerToken,
													cities[0], jPermit.getName()))
											.visit(GUIMessageVisitor.this);
								} catch (RemoteException e1) {
									logger.log(Level.SEVERE, "Error during GUI acquire a permit request.", e1);
									JOptionPane.showMessageDialog(frame, "The action request hasn't been sent.",
											"Message not sent", JOptionPane.ERROR_MESSAGE);
								}
								permitsFrame.setVisible(false);
							} else {
								/* another frame is needed */
								JFrame chooseCityFrame = new JFrame("Where?");
								chooseCityFrame.setLayout(new MigLayout("fill"));
								JComboBox<String> cityList = new JComboBox<>();
								for (String city : cities)
									cityList.addItem(Character.toUpperCase(city.charAt(0)) + city.substring(1));
								chooseCityFrame.add(cityList, "w 100%,h 90%,wrap,grow");
								JButton build = new JButton("Build");
								build.addMouseListener(new MouseAdapter() {
									@Override
									public void mouseClicked(MouseEvent e) {
										String selectedCity = ((String) cityList.getSelectedItem()).toLowerCase();
										try {
											requestProcessor
													.processRequest(new BuildAnEmporiumUsingAPermitTileActRqstMsg(
															playerToken, selectedCity, jPermit.getName()))
													.visit(GUIMessageVisitor.this);
										} catch (RemoteException e1) {
											logger.log(Level.SEVERE, "Error during GUI acquire a permit request.", e1);
											JOptionPane.showMessageDialog(frame, "The action request hasn't been sent.",
													"Message not sent", JOptionPane.ERROR_MESSAGE);
										}
										chooseCityFrame.setVisible(false);
										permitsFrame.setVisible(false);
									}
								});
								chooseCityFrame.add(build, "w 100%,h 10%,grow");
								chooseCityFrame.pack();
								chooseCityFrame.setLocationRelativeTo(null);
								chooseCityFrame.setVisible(true);
							}

						}
					});
				}
			} else {
				jPermit.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						try {
							requestProcessor
									.processRequest(new OwnedPermitResponseActRqstMsg(playerToken, jPermit.getName()))
									.visit(GUIMessageVisitor.this);
						} catch (RemoteException e1) {
							logger.log(Level.SEVERE, "Error during GUI owned permit response request.", e1);
							JOptionPane.showMessageDialog(frame, "The action request hasn't been sent.",
									"Message not sent", JOptionPane.ERROR_MESSAGE);
						}
						permitsFrame.setVisible(false);
					}
				});
			}
			permitPanel.add(jPermit);
			if (permit.getBoolean("used")) {
				JPanel usedPanel = new JPanel();
				usedPanel.setBackground(new Color(255, 255, 255, 100));
				permitPanel.add(usedPanel);
			}
			permitsFrame.add(permitPanel);
			permitIndex++;
		}
		if (this.ownedPermits.size() == 0)
			permitsFrame.add(new JLabel("You don't own any permit yet."));
		permitsFrame.pack();
		permitsFrame.setLocationRelativeTo(null);
		permitsFrame.setVisible(true);
		permitsFrame.setResizable(false);

	}

	private void showPoliticsHand() {
		/* opening a jframe with politics - read only */
		JFrame politicsFrame = new JFrame("Politics hand");
		politicsFrame.setLayout(new GridLayout(1, 0));
		for (String color : politicColors) {
			politicsFrame.add(new JLabel(new StretchIcon("graphics/politics/" + color + "-politic.png")));
		}
		politicsFrame.setPreferredSize(new Dimension(screenSize.width * 1 / 2, screenSize.height * 1 / 3));
		politicsFrame.pack();
		politicsFrame.setLocationRelativeTo(null);
		politicsFrame.setVisible(true);
	}

	/*
	 * the objects to show and the "things" to do are exactly the same apart
	 * from the ActionRequestMessage to deliver
	 */
	private void showWhichCouncillorToElect(String region, boolean main) {
		/*
		 * now actually showing a frame that's a copy of
		 * this.availableCouncillors
		 */
		JFrame availablesForElectionFrame = new JFrame("Pick one");
		availablesForElectionFrame.setLayout(new GridLayout(2, 4));
		for (int i = 0; i < this.availableCouncillors.getComponentCount(); i++) {
			/* making a copy to don't add listener to original obj */
			String councillorColor = this.availableCouncillors.getComponent(i).getName();
			JLabel councillor = new JLabel(
					new StretchIcon("graphics/councillors/councillor-" + councillorColor + ".png"));
			councillor.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					/* processing the action request */
					availablesForElectionFrame.setVisible(false);
					ActionRequestMessage req;
					if (main) {
						req = new ElectACouncillorActRqstMsg(playerToken, region, councillorColor);
					} else {
						req = new SendAnAssistantToElectACouncillorActRqstMsg(playerToken, region, councillorColor);
					}
					try {
						requestProcessor.processRequest(req).visit(GUIMessageVisitor.this);
					} catch (RemoteException e1) {
						logger.log(Level.SEVERE, "Error during GUI elect a councillor request.", e1);
						JOptionPane.showMessageDialog(frame, "The chat message hasn't been sent.", "Message not sent",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			});
			availablesForElectionFrame.add(councillor);
		}
		availablesForElectionFrame.setPreferredSize(new Dimension(screenSize.width * 1 / 3, screenSize.height * 1 / 5));
		availablesForElectionFrame.pack();
		availablesForElectionFrame.setLocationRelativeTo(null);
		availablesForElectionFrame.setVisible(true);
	}

	private void showMarketOfferFrame(JSONObject update) {
		marketOfferFrame = new JFrame("Market offer");
		marketOfferFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		marketOfferFrame.setLayout(new MigLayout("fill"));
		JPanel sellablePanel = new JPanel(new GridLayout(3, 1));
		JPanel politics = new JPanel(new GridLayout(1, 0));
		politics.setBorder(new TitledBorder("Politics"));
		/* I NEED THE MOST UPDATED POLITICS */
		try {
			requestProcessor.processRequest(new PoliticsRequestMessage(playerToken)).visit(GUIMessageVisitor.this);
		} catch (RemoteException e1) {
			logger.log(Level.SEVERE, "Error during GUI politics request.", e1);
			JOptionPane.showMessageDialog(frame, "The request hasn't been sent.", "Request not sent",
					JOptionPane.ERROR_MESSAGE);
		}
		/* politicColors is surely not null */
		for (String color : politicColors) {
			JLabel politic = new JLabel(new StretchIcon("graphics/politics/" + color + "-politic.png"));
			politic.addMouseListener(new MarketOfferListener("politic " + color, playerToken, requestProcessor, this));
			politics.add(politic);
		}
		if (politicColors.isEmpty())
			politics.add(new JLabel("You don't own any politics."));
		JPanel assistants = new JPanel(new GridLayout(1, 0));
		assistants.setBorder(new TitledBorder("Assistants"));
		int numberOfAssistants = 0;
		for (Object jsonPlayer : update.getJSONArray("players")) {
			JSONObject player = (JSONObject) jsonPlayer;
			if (player.getString("username").equals(playerToken.getUsername())) {
				numberOfAssistants = player.getInt("assistants");
				break;
			}
		}
		if (numberOfAssistants == 0)
			assistants.add(new JLabel("You don't own any assistants."));
		/* one for all is ok */
		MarketOfferListener assistantListener = new MarketOfferListener("assistant", playerToken, requestProcessor,
				this);
		for (int i = 0; i < numberOfAssistants; i++) {
			JLabel assistant = new JLabel(new StretchIcon("graphics/bonuses/assistant.png"));
			assistant.addMouseListener(assistantListener);
			assistants.add(assistant);
		}
		JPanel permits = new JPanel(new GridLayout(1, 0));
		permits.setBorder(new TitledBorder("Permits"));
		int permitIndex = 1;
		if (this.ownedPermits.size() > 0 && this.ownedPermits.size() < 3)
			permits.add(new JPadding());
		for (JSONObject permit : this.ownedPermits) {
			JPanel permitPanel = new JPanel(new OverlapLayout((new Point(0, 0))));
			/* i've to create the jpermit obj */
			JPermit jPermit = new JPermit(permit);
			jPermit.setName(Integer.toString(permitIndex));
			permitPanel.add(jPermit);
			if (permit.getBoolean("used")) {
				JPanel usedPanel = new JPanel();
				usedPanel.setBackground(new Color(255, 255, 255, 100));
				permitPanel.add(usedPanel);
			}
			permits.add(permitPanel);
			permitPanel.addMouseListener(
					new MarketOfferListener("permit " + permitIndex, playerToken, requestProcessor, this));
			permitIndex++;
		}
		if (this.ownedPermits.size() > 0 && this.ownedPermits.size() < 3)
			permits.add(new JPadding());
		if (this.ownedPermits.isEmpty())
			permits.add(new JLabel("You don't own any permit yet."));
		sellablePanel.add(politics);
		sellablePanel.add(assistants);
		sellablePanel.add(permits);
		marketOfferFrame.add(sellablePanel, "w 100%,h 90%,grow,wrap");
		JButton endMarketOffer = new JButton("End market offer");
		endMarketOffer.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					requestProcessor.processRequest(new EndMarketOfferActRqstMsg(playerToken))
							.visit(GUIMessageVisitor.this);
					marketOfferFrame.setVisible(false);
					marketOfferFrame = null;
				} catch (RemoteException e1) {
					logger.log(Level.SEVERE, "Error during GUI end market offer.", e1);
					JOptionPane.showMessageDialog(frame, "The action request hasn't been sent.", "Message not sent",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		endMarketOffer.setFocusPainted(false);
		marketOfferFrame.add(endMarketOffer, "w 100%,h 10%");
		// marketOfferFrame.setPreferredSize(new Dimension(screenSize.width * 1
		// / 3, screenSize.height * 1 / 3));
		marketOfferFrame.pack();
		marketOfferFrame.setLocationRelativeTo(null);
		marketOfferFrame.setVisible(true);
		marketOfferFrame.setAlwaysOnTop(true);
	}

	private void showMarketBuyFrame(JSONObject update) {
		/* fetching the array i need */
		JSONArray players = update.getJSONArray("players");
		marketBuyFrame = new JFrame("Market buy");
		marketBuyFrame.setLayout(new MigLayout("fill"));
		JPanel offers = new JPanel(new GridLayout(0, 1));
		for (Object jsonPlayer : players) {
			JSONObject player = (JSONObject) jsonPlayer;
			/* not showing player's offers */
			if (!player.getString("username").equals(playerToken.getUsername())) {
				JPanel playerOffersPanel = new JPanel(new GridLayout(1, 0));
				playerOffersPanel.setBorder(new TitledBorder(player.getString("username")));
				JSONArray offered = player.getJSONArray("offered");
				int indexInPlayerList = 1;
				for (Object jsonOffer : offered) {
					JPanel playerOffer = new JPanel(new GridLayout(1, 2));
					playerOffer.setBorder(new LineBorder(Color.black, 1));
					JSONObject offer = (JSONObject) jsonOffer;
					int price = offer.getInt("price");
					if (offer.get("forSale").equals("assistant")) {
						playerOffer.add(new JLabel(new StretchIcon("graphics/bonuses/assistant.png")));
					} else if (offer.get("forSale") instanceof String) {
						String color = offer.getString("forSale").split("\\s")[1];
						playerOffer.add(new JLabel(new StretchIcon("graphics/politics/" + color + "-politic.png")));
					} else {
						playerOffer.add(new JPermit(offer.getJSONObject("forSale")));
					}
					playerOffer.add(new JLabel(Integer.toString(price), SwingConstants.CENTER));
					playerOffer.setName(Integer.toString(indexInPlayerList));
					playerOffer.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							try {
								requestProcessor
										.processRequest(new MarketBuyActRqstMsg(playerToken,
												player.getString("username"), Integer.parseInt(playerOffer.getName())))
										.visit(GUIMessageVisitor.this);
							} catch (RemoteException e1) {
								logger.log(Level.SEVERE, "Error during GUI market buy action.", e1);
								JOptionPane.showMessageDialog(frame, "The action request hasn't been sent.",
										"Message not sent", JOptionPane.ERROR_MESSAGE);
							}
						}
					});
					playerOffersPanel.add(playerOffer);
					indexInPlayerList++;
				}
				if (indexInPlayerList == 1) // nothing added
					playerOffersPanel.add(new JLabel("No offers from " + player.getString("username")));
				offers.add(playerOffersPanel);
			}
		}
		marketBuyFrame.add(offers, "w 100%,h 90%,wrap,grow");
		JButton endMarketBuy = new JButton("End market buy");
		endMarketBuy.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					requestProcessor.processRequest(new EndMarketBuyActRqstMsg(playerToken))
							.visit(GUIMessageVisitor.this);
					marketBuyFrame.setVisible(false);
					marketBuyFrame = null;
				} catch (RemoteException e1) {
					logger.log(Level.SEVERE, "Error during GUI end market buy.", e1);
					JOptionPane.showMessageDialog(frame, "The action request hasn't been sent.", "Message not sent",
							JOptionPane.ERROR_MESSAGE);
				}
			}

		});
		endMarketBuy.setFocusPainted(false);
		marketBuyFrame.add(endMarketBuy, "w 100%,h 10%,grow");
		// marketBuyFrame.setPreferredSize(new Dimension(screenSize.width * 1 /
		// 3, screenSize.height * 1 / 3));
		marketBuyFrame.pack();
		marketBuyFrame.setLocationRelativeTo(null);
		marketBuyFrame.setVisible(true);
		marketBuyFrame.setAlwaysOnTop(true);
	}
}
