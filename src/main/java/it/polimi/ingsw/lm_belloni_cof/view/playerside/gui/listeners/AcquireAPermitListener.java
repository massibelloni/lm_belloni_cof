package it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.listeners;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.rmi.RemoteException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
import it.polimi.ingsw.lm_belloni_cof.messages.PoliticsRequestMessage;

import it.polimi.ingsw.lm_belloni_cof.messages.ResponseMessage;

import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.AcquireABusinessPermitTileActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.FaceUpPermitResponseActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;

import it.polimi.ingsw.lm_belloni_cof.view.shared.RequestProcessor;

/**
 * The {@link MouseListener} that handles an
 * {@link AcquireABusinessPermitTileActRqstMsg} or
 * {@link FaceUpPermitResponseActRqstMsg} request.
 */
public class AcquireAPermitListener  implements MouseListener,ActionRequestProcessor {
	private static final Logger logger = Logger.getLogger(AcquireAPermitListener.class.getName());

	private String region;
	private int indexInRegion;
	private PlayerToken playerToken;
	private RequestProcessor requestProcessor;
	private MessageVisitor messageVisitor;
	/** The parameter used to determine which request has to be sent */
	private boolean waitingState;

	public AcquireAPermitListener(String region, int indexInRegion, PlayerToken playerToken,
			RequestProcessor requestProcessor, MessageVisitor messageVisitor) {
		this.region = region;
		this.indexInRegion = indexInRegion;
		this.playerToken = playerToken;
		this.requestProcessor = requestProcessor;
		this.messageVisitor = messageVisitor;
	}

	public void setWaitingState(boolean state) {
		this.waitingState = state;
	}

	/**
	 * When the mouse event is triggered the listener decides which request to
	 * send. May be needed to visit a {@link PoliticsRequestMessage} to obtain
	 * other informations from the user.
	 * 
	 * @see AcquireAPermitListener#processActionRequest(List)
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		if (waitingState) {
			/* Directly getting it */
			try {
				FaceUpPermitResponseActRqstMsg msg = new FaceUpPermitResponseActRqstMsg(playerToken, region,
						indexInRegion);
				ResponseMessage r = requestProcessor.processRequest(msg);
				r.visit(messageVisitor);
			} catch (RemoteException e1) {
				logger.log(Level.SEVERE, "Error during face up permit response request.", e1);
			}
		} else {
			/*
			 * Making a politics request to get politics hand. Then showing a
			 * frame with politics letting the user to choose.
			 */
			try {
				/*
				 * THIS MESSAGE WON'T BE VISITED BY THE VISITOR PASSED AS AN
				 * ARGUMENT BUT FROM COUNCILSATISFIERVISITOR
				 */
				this.requestProcessor.processRequest(new PoliticsRequestMessage(this.playerToken)).visit(new CouncilSatisfierVisitor("Acquire",this));
			} catch (RemoteException e1) {
				logger.log(Level.SEVERE, "Error during in-acquire-a-permit politics request.", e1);
			}
		}
	}

	/**
	 * The method that actually makes the request to the server
	 * 
	 * @param colors-a
	 *            list of the politic colors needed and retrieved by
	 *            {@link CouncilSatisfierVisitor}.
	 */
	@Override
	public void processActionRequest(List<String> colors) {
		try {
			AcquireABusinessPermitTileActRqstMsg msg = new AcquireABusinessPermitTileActRqstMsg(playerToken, region,
					indexInRegion, colors);
			ResponseMessage r = requestProcessor.processRequest(msg);
			/*
			 * THIS MESSAGE WILL BE VISITED BY THE VISITOR PASSED AS AN
			 * ARGUMENT!
			 */
			r.visit(messageVisitor);
		} catch (RemoteException e1) {
			logger.log(Level.SEVERE, "Error during in-acquire-a-permit politics request.", e1);
		}
	}
	@Override
	public void mousePressed(MouseEvent e) {
		/**/

	}
	@Override
	public void mouseReleased(MouseEvent e) {
		/**/

	}
	@Override
	public void mouseEntered(MouseEvent e) {
		/**/

	}
	@Override
	public void mouseExited(MouseEvent e) {
		/**/

	}
}
