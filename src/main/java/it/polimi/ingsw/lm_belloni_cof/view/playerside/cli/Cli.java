package it.polimi.ingsw.lm_belloni_cof.view.playerside.cli;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.PlayerSideConnectionFactory;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.RMIConnection;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.SocketConnection;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.GameStatusKeeper;

/** Main thread for the command line interface */
public class Cli implements Runnable {
	private static final Logger logger = Logger.getLogger(Cli.class.getName());
	private Scanner in;

	public Cli(Scanner in) {
		this.in = in;
	}

	/**
	 * Asks for the connection protocol and then continues asking for commands
	 */
	@Override
	public void run() {
		CliMessageVisitor cliVisitor = new CliMessageVisitor();
		CliUpdater updater = new CliUpdater(cliVisitor);
		PlayerSideConnectionFactory connection = null;
		// CHOOSES BETWEEN RMI AND SOCKET
		String connStr;
		do {
			System.out.println("RMI(1) or Socket(2)");
			connStr = in.nextLine();
		} while (!"1".equals(connStr) && !"2".equals(connStr));
		try {
			if (connStr.equals("1"))
				connection = new RMIConnection("localhost", updater);
			else
				connection = new SocketConnection("localhost", updater);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error during server connection.", e);
			System.out.println("Error during server connection.");
			return;
		}
		String username;
		String password;
		do {
			System.out.println("Username:");
			username = in.nextLine();
			System.out.println("Password:");
			password = in.nextLine();
		} while (!connection.connect(username, password));
		cliVisitor.setUsername(username);
		cliVisitor.setAutoDraw(connection.getRequestProcessor(), connection.getToken());
		CliCommandManager cmdManager = new CliCommandManager((GameStatusKeeper) cliVisitor,
				connection.getRequestProcessor(), cliVisitor, connection.getToken());
		String cmd;
		while (!"stop".equals(cmd = in.nextLine())) {
			cmdManager.executeCommand(cmd);
		}
	}
}
