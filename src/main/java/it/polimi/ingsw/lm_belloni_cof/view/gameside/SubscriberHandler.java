package it.polimi.ingsw.lm_belloni_cof.view.gameside;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.rmi.RemoteException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.messages.BroadcastMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.InvalidRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.SubscribeRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.SubscribeResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.view.shared.BrokerInterface;
import it.polimi.ingsw.lm_belloni_cof.view.shared.SubscriberInterface;

/**
 * Thread that actually handles an incoming subscription request behaving as a
 * "stub" between the server and the client. All the incoming broadcast messages
 * sent from the server are queued and sent using socket technology.
 */
public class SubscriberHandler extends Thread implements SubscriberInterface {
	private static final Logger logger = Logger.getLogger(SubscriberHandler.class.getName());
	/** The socket connection to the subscriber. */
	private Socket client;
	/**
	 * The {@link BrokerInterface} to actyally subscribe the subscriber to
	 * updates.
	 */
	private BrokerInterface broker;
	/** A queue of all the broadcast message not sent yet. */
	private final Queue<BroadcastMessage> broadcastBuffer;
	/** The always-opened output stream */
	private ObjectOutputStream outputStream;

	public SubscriberHandler(Socket client, BrokerInterface broker) {
		this.client = client;
		this.broker = broker;
		this.broadcastBuffer = new ConcurrentLinkedQueue<>();
	}

	/**
	 * Waits for a subscription request, handles it subscribing this object to
	 * updates working as a stub between other processes and the client
	 * communicating using socket technology.
	 */
	private SubscribeRequestMessage receiveSubscriptionRequest() {
		SubscribeRequestMessage subscribeMessage = null;
		try {
			ObjectInputStream inputStream = new ObjectInputStream(this.client.getInputStream());
			Object object = inputStream.readObject();
			if (object instanceof SubscribeRequestMessage)
				subscribeMessage = (SubscribeRequestMessage) object;
			try {
				this.client.shutdownInput(); // closing incoming requests
			} catch (IOException e) {
				logger.log(Level.WARNING, "Error while shutting down input.", e);
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error during socket connection.", e);
		}
		return subscribeMessage;
	}

	/**
	 * Send a message using socket technology
	 * 
	 * @param message
	 *            the message that has to be sent
	 * @throws IOException
	 *             if something wrong happens during message delivery
	 */
	private void sendMsg(Object message) throws IOException {
		if (outputStream == null) {
			outputStream = new ObjectOutputStream(this.client.getOutputStream());
		}
		outputStream.writeObject(message);
		outputStream.flush();
	}

	/**
	 * Run life-cycle of this object: waits for a subscription request, handles
	 * it and waits for new {@link BroadcastMessage}s that other processes may
	 * send trough this object.
	 */
	@Override
	public void run() {
		/*
		 * i need to wait for a subscription request, the only legit way to
		 * start this socket communication
		 */
		try {
			ResponseMessage response = null;
			try {
				SubscribeRequestMessage subscribeRequest = receiveSubscriptionRequest(); // blocking
				this.broker.subscribe(this, subscribeRequest.getUsername());
				response = new SubscribeResponseMessage();
			} catch (RemoteException e) {
				logger.log(Level.SEVERE, "Remote exception during subscription.", e);
				response = new InvalidRequestMessage("Something wrong happened during subscription.");
			}
			sendMsg(response);
			while (true) {
				broadcastBuffer(); // waiting for someone to fill the queue
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error in message dispatching.", e);
		} finally {
			try {
				this.client.close();
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Error in closing socket connection.", e);
			}
		}
	}

	/** Empties the buffer when a new {@link BroadcastMessage} is queued. */
	private void broadcastBuffer() throws IOException, InterruptedException {
		synchronized (broadcastBuffer) {
			while (broadcastBuffer.isEmpty()) {
				try {
					broadcastBuffer.wait();
				} catch (InterruptedException e) {
					throw e;
				}
			}
			BroadcastMessage broadcastMessage;
			do {
				broadcastMessage = broadcastBuffer.poll();
				if (broadcastMessage != null)
					sendMsg(broadcastMessage);
			} while (broadcastMessage != null);
		}
	}

	/**
	 * Filling the broadcast message queue with a new message and notifying the
	 * message dispatcher to dispatch it, according to the queue order.
	 * 
	 * @param msg
	 *            the message to send to the subscriber
	 * @throws RemoteException
	 */
	@Override
	public void update(BroadcastMessage msg) {
		broadcastBuffer.add(msg);
		synchronized (broadcastBuffer) {
			/**
			 * @see #broadcastBuffer
			 */
			broadcastBuffer.notify();
		}
	}

}
