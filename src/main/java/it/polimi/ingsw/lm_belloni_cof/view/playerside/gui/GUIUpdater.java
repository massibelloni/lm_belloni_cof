package it.polimi.ingsw.lm_belloni_cof.view.playerside.gui;

import it.polimi.ingsw.lm_belloni_cof.messages.BroadcastMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.ViewUpdater;

/**
 * GUI implementation of the interface that simply let the message be visited by
 * a {@link GUIMessageVisitor}.
 */
public class GUIUpdater implements ViewUpdater {
	private GUIMessageVisitor guiMessageVisitor;

	public GUIUpdater(GUIMessageVisitor guiMessageVisitor) {
		this.guiMessageVisitor = guiMessageVisitor;
	}

	@Override
	public void updateView(BroadcastMessage message) {
		message.visit(this.guiMessageVisitor);

	}

	@Override
	public void updateView(ResponseMessage message) {
		message.visit(this.guiMessageVisitor);

	}

}
