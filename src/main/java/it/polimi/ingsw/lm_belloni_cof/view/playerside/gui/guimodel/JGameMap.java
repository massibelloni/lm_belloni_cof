package it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.guimodel;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.listeners.BuildAnEmporiumWithKingListener;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.BackgroundPanel;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.swingutils.JPadding;
import net.miginfocom.swing.MigLayout;

/**
 * Swing element representing the whole CoF map with cities and connections in
 * it.
 */
public class JGameMap extends BackgroundPanel {
	private static final long serialVersionUID = 1L;
	private final Color ROAD_BORDER_COLOR = new Color(96, 85, 55/* ,200 */);
	private final Color ROAD_COLOR = new Color(175, 146, 103, 200);
	private int ROAD_SIZE;
	private transient JSONArray cities;
	private Map<String, JPanel> regions;
	private Map<String, JCity> jcities;
	private String hasKingOn;

	public JGameMap(JSONArray cities, String kingStartingCity, int playersNumber,
			BuildAnEmporiumWithKingListener[] listeners) {
		super(new ImageIcon("graphics/map-bg.png").getImage(), BackgroundPanel.TILED);
		this.cities = cities;
		super.setLayout(new GridLayout(1, 3));
		this.regions = new HashMap<>();
		/* 3 default regions */
		this.regions.put("coast", new JPanel(new MigLayout("fill")));
		this.regions.put("hills", new JPanel(new MigLayout("fill")));
		this.regions.put("mountains", new JPanel(new MigLayout("fill")));
		for (Map.Entry<String, JPanel> entry : this.regions.entrySet())
			entry.getValue().setOpaque(false);
		this.jcities = new HashMap<>();
		int listenerIndex = 0;
		for (Object jsonCity : this.cities) {
			JSONObject city = (JSONObject) jsonCity;
			// creating a bonuses map
			Map<String, Integer> bonuses = new HashMap<>();
			JSONArray tokens = city.getJSONArray("tokens");
			for (Object jsonToken : tokens) {
				JSONArray token = (JSONArray) jsonToken;
				for (Object jsonBonus : token) {
					JSONObject bonus = (JSONObject) jsonBonus;
					if (bonuses.get(bonus.getString("bonus")) == null)
						bonuses.put(bonus.getString("bonus"), 0);
					bonuses.put(bonus.getString("bonus"),
							Integer.sum(bonuses.get(bonus.getString("bonus")), bonus.getInt("times")));
				}
			}
			JCity jCity = new JCity(city.getString("name"), city.getString("color"), bonuses, playersNumber);
			jCity.addMouseListener(listeners[listenerIndex++]);
			this.jcities.put(city.getString("name"), jCity);
		}
		this.regions.get("coast").add(this.jcities.get("arkon"), "w 50%,h 100%");
		this.regions.get("coast").add(this.jcities.get("castrum"), "w 50%,h 100%,wrap");
		this.regions.get("coast").add(this.jcities.get("burgen"), "w 50%,h 100%");
		this.regions.get("coast").add(this.jcities.get("dorful"), "w 50%,h 100%,wrap");
		this.regions.get("coast").add(this.jcities.get("esti"), "w 50%,h 100%");
		this.regions.get("coast").add(new JPadding(), "w 50%");
		super.add(this.regions.get("coast"));
		this.regions.get("hills").add(this.jcities.get("framek"), "w 50%,h 100%");
		this.regions.get("hills").add(this.jcities.get("indur"), "w 50%,h 100%,wrap");
		this.regions.get("hills").add(this.jcities.get("graden"), "w 50%,h 100%");
		this.regions.get("hills").add(this.jcities.get("juvelar"), "w 50%,h 100%,wrap");
		this.regions.get("hills").add(new JPadding(), "w 50%");
		this.regions.get("hills").add(this.jcities.get("hellar"), "w 50%,h 100%");
		super.add(this.regions.get("hills"));
		this.regions.get("mountains").add(this.jcities.get("kultos"), "w 50%,h 100%");
		this.regions.get("mountains").add(this.jcities.get("naris"), "w 50%,h 100%,wrap");
		this.regions.get("mountains").add(this.jcities.get("lyram"), "w 50%,h 100%");
		this.regions.get("mountains").add(this.jcities.get("osium"), "w 50%,h 100%,wrap");
		this.regions.get("mountains").add(this.jcities.get("merkatim"), "w 50%,h 100%");
		this.regions.get("mountains").add(new JPadding(), "w 50%");
		super.add(this.regions.get("mountains"));
		/* king city */
		this.hasKingOn = kingStartingCity;
		this.jcities.get(this.hasKingOn).hasKingOn();
	}

	/**
	 * Methods that paints a "road" between two "cities" represented by their
	 * center points
	 */
	private void paintRoad(Graphics g, Point a, Point b) {
		if (a.getX() > b.getX()) {
			Point aux = b;
			b = a;
			a = aux;
		}
		Color original = g.getColor();
		Graphics2D g2 = (Graphics2D) g;
		Stroke s = g2.getStroke();
		g2.setStroke(new BasicStroke(5));
		int length = (int) Math.sqrt(Math.pow((b.getX() - a.getX()), 2) + Math.pow(b.getY() - a.getY(), 2));
		Rectangle rect = new Rectangle((int) a.getX(), (int) a.getY(), length, this.ROAD_SIZE);
		double teta = Math.atan((b.getY() - a.getY()) / (b.getX() - a.getX()));
		g2.rotate(teta, a.getX(), a.getY());
		g2.setColor(ROAD_BORDER_COLOR);
		g2.draw(rect);
		g2.setColor(ROAD_COLOR);
		g2.fill(rect);
		g2.rotate(-teta, a.getX(), a.getY());
		g2.setStroke(s);
		g.setColor(original);
	}

	@Override
	public void paintComponent(Graphics g) {
		this.ROAD_SIZE = (int) (this.jcities.get("arkon").getHeight() * 0.05);
		super.paintComponent(g);
		for (Object jsonCity : this.cities) {
			JSONObject city = (JSONObject) jsonCity;
			String startName = city.getString("name");
			JSONArray connections = city.getJSONArray("connections");
			for (Object endName : connections) {
				if (startName.compareTo((String) endName) < 0) { // avoiding
																	// duplicate
																	// roads
					// endCity region is needed
					JPanel endRegion = null;
					for (Map.Entry<String, JPanel> entry : this.regions.entrySet()) {
						if (this.jcities.get(endName).getParent() == entry.getValue()) {
							endRegion = entry.getValue();
							break;
						}
					}
					if (endRegion != null) {
						Point startingLoc = new Point(
								(int) (this.regions.get(city.get("region")).getX() + this.jcities.get(startName).getX()
										+ this.jcities.get(startName).getWidth() / 2),
								(int) (this.jcities.get(startName).getY()
										+ this.jcities.get(startName).getHeight() / 2));
						Point endingLoc = new Point(
								(int) (endRegion.getX() + this.jcities.get(endName).getX()
										+ this.jcities.get(endName).getWidth() / 2),
								(int) (this.jcities.get(endName).getY() + this.jcities.get(endName).getHeight() / 2));
						this.paintRoad(g, startingLoc, endingLoc);
					}
				}
			}
		}
	}

	/**
	 * Draws an emporium under the {@link JCity} which name is cityName.
	 * 
	 * @param cityName
	 *            the {@link JCity} under which draw an emporium.
	 * @param color
	 *            the {@link Color} of the emporium built.
	 * @return true if the emporium has been drawn, false if was already there.
	 */
	public boolean drawAnEmporium(String cityName, Color color) {
		return this.jcities.get(cityName).addEmporium(color);
	}

	/** Method that changes the click behavior of the JCity named as the argument passed with the listener passed.
	 * 
	 * @param cityName the name of the JCity which click behavior will be changed
	 * @param listener the listener representing the new behaviour
	 */
	public void changeClickBehaviour(String cityName, MouseListener listener) {
		/* removing old listener */
		this.jcities.get(cityName)
				.removeMouseListener((this.jcities.get(cityName).getListeners(MouseListener.class))[0]);
		this.jcities.get(cityName).addMouseListener(listener);
	}

	public void setKingLocation(String cityName) {
		if (!cityName.equals(this.hasKingOn)) {
			this.jcities.get(cityName).hasKingOn();
			this.jcities.get(this.hasKingOn).hasntKingOn();
			this.hasKingOn = cityName;
		}
	}
}