package it.polimi.ingsw.lm_belloni_cof.view.shared;

import java.rmi.Remote;
import java.rmi.RemoteException;

import it.polimi.ingsw.lm_belloni_cof.messages.RequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ResponseMessage;

/**
 * Interface that has to be implemented by a server in a client-server
 * environment.
 */
public interface RequestProcessor extends Remote {
	/**
	 * Process a request contained in the {@link RequestMessage} passed as an
	 * argument answering with a {@link ResponseMessage}.
	 * 
	 * @param request
	 *            the {@link RequestMessage} containing a request.
	 * @return the {@link ResponseMessage} processed for the request passed.
	 * @throws RemoteException
	 *             if something goes wrong with network technology
	 */
	public ResponseMessage processRequest(RequestMessage request) throws RemoteException;
}
