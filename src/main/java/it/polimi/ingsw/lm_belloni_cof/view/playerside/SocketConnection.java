package it.polimi.ingsw.lm_belloni_cof.view.playerside;

import java.io.IOException;
import java.net.UnknownHostException;

import it.polimi.ingsw.lm_belloni_cof.view.shared.BrokerInterface;
import it.polimi.ingsw.lm_belloni_cof.view.shared.RequestProcessor;

/** Implementation of the client-side socket connection to the game */
public class SocketConnection extends PlayerSideConnectionFactory {
	private SocketClient socketClient;

	public SocketConnection(String address, ViewUpdater viewUpdater) throws UnknownHostException, IOException {
		super(viewUpdater);
		this.socketClient = new SocketClient(address, getSubscriber());
		socketClient.start();

	}

	@Override
	public RequestProcessor getRequestProcessor() {
		return this.socketClient;
	}

	@Override
	public BrokerInterface getBrokerInterface() {
		return this.socketClient;
	}

}
