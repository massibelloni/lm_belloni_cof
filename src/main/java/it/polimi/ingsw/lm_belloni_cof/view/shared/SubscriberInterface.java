package it.polimi.ingsw.lm_belloni_cof.view.shared;

import java.rmi.Remote;
import java.rmi.RemoteException;

import it.polimi.ingsw.lm_belloni_cof.messages.BroadcastMessage;

/**
 * Interface that has to be implemented by clients that want to be updated by a
 * {@link BrokerInterface}.
 */
public interface SubscriberInterface extends Remote {
	public void update(BroadcastMessage msg) throws RemoteException;
}
