package it.polimi.ingsw.lm_belloni_cof.view.playerside;

import java.util.Scanner;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.cli.Cli;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.GUI;

/** Client starting point that asks to the user which I/O interface to use */
public class PlayerSide {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String outputStr;
		do {
			System.out.println("GUI(1) or CLI(2)?");
			outputStr = scanner.nextLine();
		} while (!"1".equals(outputStr) && !"2".equals(outputStr));
		switch (outputStr) {
		case "1":
			GUI gui = new GUI();
			new Thread(gui).start();
			break;
		default:
			Cli cli = new Cli(scanner);
			new Thread(cli).start();
			break;
		}
	}

}
