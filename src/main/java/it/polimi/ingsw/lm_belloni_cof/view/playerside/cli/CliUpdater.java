package it.polimi.ingsw.lm_belloni_cof.view.playerside.cli;

import it.polimi.ingsw.lm_belloni_cof.messages.BroadcastMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.ViewUpdater;

/**
 * Cli implementation of the interface that simply let the message be visited by
 * a {@link CliMessageVisitor}.
 */
public class CliUpdater implements ViewUpdater {
	private CliMessageVisitor cliMessageVisitor;

	public CliUpdater(CliMessageVisitor cliMessageVisitor) {
		this.cliMessageVisitor = cliMessageVisitor;
	}

	@Override
	public void updateView(BroadcastMessage message) {
		message.visit(cliMessageVisitor);
	}

	@Override
	public void updateView(ResponseMessage message) {
		message.visit(cliMessageVisitor);
	}

}
