package it.polimi.ingsw.lm_belloni_cof.view.playerside;

import it.polimi.ingsw.lm_belloni_cof.messages.BroadcastMessage;
import it.polimi.ingsw.lm_belloni_cof.view.shared.SubscriberInterface;

/**
 * Simple interface of a subscriber in a Publisher-Subscriber protocol. It
 * allows a publisher to notify the subscriber about {@link BroadcastMessage}s.
 */
public class Subscriber implements SubscriberInterface {
	private ViewUpdater viewUpdater;

	public Subscriber(ViewUpdater viewUpdater) {
		this.viewUpdater = viewUpdater;
	}

	@Override
	public void update(BroadcastMessage msg) {
		this.viewUpdater.updateView(msg);
	}

}
