package it.polimi.ingsw.lm_belloni_cof.view.shared;

/** Connection ports used in the client-server environment. */
public class ConnectionPorts {
	public static final int RMI_PORT = 1099;
	public static final int REQPROC_SERVER_PORT = 1100;
	public static final int BROKER_SERVER_PORT = 1101;
}
