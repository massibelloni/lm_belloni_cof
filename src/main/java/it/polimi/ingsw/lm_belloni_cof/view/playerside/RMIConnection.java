package it.polimi.ingsw.lm_belloni_cof.view.playerside;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import it.polimi.ingsw.lm_belloni_cof.view.shared.BrokerInterface;
import it.polimi.ingsw.lm_belloni_cof.view.shared.ConnectionPorts;
import it.polimi.ingsw.lm_belloni_cof.view.shared.RequestProcessor;
import it.polimi.ingsw.lm_belloni_cof.view.shared.SubscriberInterface;

/** Implementation of the client-side RMI connection to the game */
public class RMIConnection extends PlayerSideConnectionFactory {
	private RequestProcessor requestProcessor;
	private BrokerInterface broker;
	private SubscriberInterface subscriberInterface;

	/**
	 * Public constructor of a RMI connection
	 * 
	 * @param address
	 *            the address of the RMI server
	 * @param viewUpdater
	 *            a {@link ViewUpdater} used to notify the view about incoming
	 *            messages in order to display them.
	 * @throws RemoteException
	 *             when RMI problems occur
	 * @throws NotBoundException
	 *             when RMI problems occur
	 */
	public RMIConnection(String address, ViewUpdater viewUpdater) throws RemoteException, NotBoundException {
		super(viewUpdater);
		// INIT RMI
		System.setProperty("java.rmi.server.hostname", address);
		Registry registry = LocateRegistry.getRegistry(address, ConnectionPorts.RMI_PORT);
		this.requestProcessor = (RequestProcessor) registry.lookup("RequestProcessor");
		this.broker = (BrokerInterface) registry.lookup("Broker");
		this.subscriberInterface = (SubscriberInterface) UnicastRemoteObject.exportObject(getSubscriber(), 0);
		// OK RMI
	}

	/**
	 * Returns a working implementation of a {@link RequestProcessor}. In the
	 * RMI implementation is simply a remote object running on server's JVM.
	 */
	@Override
	public RequestProcessor getRequestProcessor() {
		return this.requestProcessor;
	}

	/**
	 * Returns a working implementation of a {@link BrokerInterface}. In the RMI
	 * implementation is simply a remote object running on server's JVM.
	 */
	@Override
	public BrokerInterface getBrokerInterface() {
		return this.broker;
	}

	@Override
	public SubscriberInterface getSubscriberInterface() {
		return this.subscriberInterface;
	}

}
