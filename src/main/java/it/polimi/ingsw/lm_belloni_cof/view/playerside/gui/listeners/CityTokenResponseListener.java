package it.polimi.ingsw.lm_belloni_cof.view.playerside.gui.listeners;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.CityTokenResponseActRqstMsg;
import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;
import it.polimi.ingsw.lm_belloni_cof.view.shared.RequestProcessor;

/**
 * The {@link MouseListener} that handles a {@link CityTokenResponseActRqstMsg}
 * for the {@link JCity} it's attached to.
 */
public class CityTokenResponseListener extends MouseAdapter {
	private static final Logger logger = Logger.getLogger(CityTokenResponseListener.class.getName());
	private String city;
	private PlayerToken playerToken;
	private RequestProcessor requestProcessor;
	private MessageVisitor messageVisitor;

	public CityTokenResponseListener(String city, PlayerToken playerToken, RequestProcessor requestProcessor,
			MessageVisitor messageVisitor) {
		this.city = city;
		this.playerToken = playerToken;
		this.requestProcessor = requestProcessor;
		this.messageVisitor = messageVisitor;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		try {
			requestProcessor.processRequest(new CityTokenResponseActRqstMsg(this.playerToken, this.city))
					.visit(this.messageVisitor);
		} catch (RemoteException e1) {
			logger.log(Level.SEVERE, "Error during GUI city token response.", e1);
			JOptionPane.showMessageDialog(new JFrame(), "The action request hasn't been sent.", "Request not sent",
					JOptionPane.ERROR_MESSAGE);
		}
	}
}
