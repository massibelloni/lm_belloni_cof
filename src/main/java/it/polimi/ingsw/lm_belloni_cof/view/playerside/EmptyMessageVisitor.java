package it.polimi.ingsw.lm_belloni_cof.view.playerside;

import it.polimi.ingsw.lm_belloni_cof.messages.ActionExecutedMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ActionNotExecutedMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ChatBroadcastMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ChatResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ConnectionResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.DisconnectedPlayerBroadcastMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.GameFinishedMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.GameStartMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.GameUpdateMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.InvalidRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayersTableResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.PoliticsResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ReconnectedPlayerBroadcastMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.SubscribeResponseMessage;

/**
 * Super basic {@link MessageVisitor} implementation that visits messages
 * without doing anything.
 */
public class EmptyMessageVisitor implements MessageVisitor {

	@Override
	public void display(ChatBroadcastMessage msg) {
		/**/

	}

	@Override
	public void display(GameStartMessage msg) {
		/**/

	}

	@Override
	public void display(GameUpdateMessage msg) {
		/**/

	}

	@Override
	public void display(GameFinishedMessage msg) {
		/**/

	}

	@Override
	public void display(ConnectionResponseMessage msg) {
		/**/

	}

	@Override
	public void display(ChatResponseMessage msg) {
		/**/

	}

	@Override
	public void display(ActionExecutedMessage msg) {
		/**/

	}

	@Override
	public void display(ActionNotExecutedMessage msg) {
		/**/

	}

	@Override
	public void display(InvalidRequestMessage msg) {
		/**/

	}

	@Override
	public void display(PoliticsResponseMessage msg) {
		/**/

	}

	@Override
	public void display(SubscribeResponseMessage msg) {
		/**/

	}

	@Override
	public void display(DisconnectedPlayerBroadcastMessage msg) {
		/**/

	}

	@Override
	public void display(ReconnectedPlayerBroadcastMessage msg) {
		// TODO Auto-generated method stub

	}

	@Override
	public void display(PlayersTableResponseMessage msg) {
		/**/

	}

}
