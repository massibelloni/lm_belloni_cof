package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import it.polimi.ingsw.lm_belloni_cof.controller.states.StateChangingBonusHandler;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Player;

/**
 * The most abstract concept of a response that a {@link Player} needs to send
 * in order to better specify a bonus that it has obtained. This {@link Action}
 * is intended to be executed after a
 * {@link it.polimi.ingsw.lm_belloni_cof.model.BonusType} CITY_REWARD,
 * PERMIT_BONUS or TAKE_PERMIT.
 */
public abstract class BonusResponseAction extends MainAction {
	/**
	 * Protected constructor for the BonusResponseAction instances.
	 * 
	 * @param game
	 *            the {@link Game} on which the action will generate changes.
	 * @param player
	 *            the {@link Player} responsible for the changes on the
	 *            {@link Game}.
	 * @param bonusHandler
	 *            {@link StateChangingBonusHandler} instance to edit the
	 *            {@link PlayerState} of the {@link Player}.
	 */
	protected BonusResponseAction(Game game, Player player, StateChangingBonusHandler bonusHandler) {
		super(game, player, bonusHandler);
	}

}
