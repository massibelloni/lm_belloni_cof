package it.polimi.ingsw.lm_belloni_cof.controller.states;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.exceptions.GameFinishedException;
import it.polimi.ingsw.lm_belloni_cof.model.Game;

/**
 * This is the class intended to manage the turns and the states of the players
 * in a game.
 */
public class TurnMachine implements StatesManager, StateChangingBonusHandler {
	/** The {@link Game} of which this object manages the turns. */
	private Game game;
	private int turnId;
	private boolean isLastTurn;
	private Map<String, PlayerState> currentStates;
	private String currentPlayer;
	/** The order that the players follow during the regular game flow. */
	private List<String> playersOrder;
	/** Whether the game is in market offer. */
	private boolean inMarketOffer;
	/** Whether the game is in market buy. */
	private boolean inMarketBuy;
	/** The order that the players follow during the market buy phase. */
	private List<String> marketBuyOrder;
	/** The list of disconnected players. */
	private List<String> disconnected;

	/**
	 * Initialize the a new turn machine and set the first player of the list
	 * passed as the current player.
	 */
	public TurnMachine(Game game) {
		this.game = game;
		this.currentStates = new HashMap<>();
		this.disconnected = new ArrayList<>(); // empty
		this.inMarketOffer = false;
		this.inMarketBuy = false;
		List<String> players = game.getPlayerUsernames();
		if (!players.isEmpty()) {
			this.playersOrder = new ArrayList<>(players);
			// Collections.shuffle(this.playersOrder); // random order
			for (String player : playersOrder)
				currentStates.put(player, new PlayerState());
			// initializing turn for the first player
			this.currentPlayer = this.playersOrder.get(0);
			this.game.setCurrentPlayer(this.game.getPlayerByUsername(this.currentPlayer));
			currentStates.get(currentPlayer).addToken(ActionToken.DRAW_A_POLITIC);
			currentStates.get(currentPlayer).addToken(ActionToken.MAIN);
			currentStates.get(currentPlayer).addToken(ActionToken.QUICK);
			this.turnId = 1;
			this.isLastTurn = false;
		}
	}

	@Override
	public PlayerState getState(String player) {
		return this.currentStates.get(player);
	}

	@Override
	public void changeState(String player, Action actionPerformed) throws GameFinishedException {
		this.currentStates.get(player).removeToken(actionPerformed.getActionToken());
		if (this.currentStates.get(player).isEmpty())
			changeTurn();
	}

	/**
	 * Decide who is the next player of a list starting from a designated one.
	 * The designated one will be returned if it's not disconnected. If the
	 * designated it's disconnected the next one will be tried, until a non
	 * disconnected player is found or no players are found.
	 * 
	 * @param order
	 *            the list of usernames in a given order
	 * @param designated
	 *            the position of the player that will play if it's not
	 *            disconnected
	 * @return the surely non disconnected player that will play
	 * @throws GameFinishedException
	 *             when no connected players are found
	 */
	private String next(List<String> order, int designated) throws GameFinishedException {
		int next = (designated == order.size()) ? 0 : designated;
		int origin = next;
		while (this.disconnected.contains(order.get(next))) {
			next = ((next + 1) == order.size()) ? 0 : (next + 1);
			if (next == origin) // no connected players
				throw new GameFinishedException();
		}
		return order.get(next);
	}

	/**
	 * Change the turn making a new player the current one and, if necessary,
	 * changing the game phase (regular,market offer,market buy).
	 * 
	 * @throws GameFinishedException
	 *             if the current game is finished changing the game's turn.
	 */
	private void changeTurn() throws GameFinishedException {
		if (!this.inMarketBuy) {
			// regular gameflow/market offer
			if (this.playersOrder
					.indexOf(next(this.playersOrder, this.playersOrder.indexOf(currentPlayer) + 1)) <= this.playersOrder
							.indexOf(this.currentPlayer)) {
				if (!this.inMarketOffer) {
					// market offer starts
					currentPlayer = next(this.playersOrder, 0);
					this.game.setCurrentPlayer(this.game.getPlayerByUsername(this.currentPlayer));
					this.game.startMarketOffer();
					this.inMarketOffer = true;
					this.currentStates.get(currentPlayer).addToken(ActionToken.MARKET_OFFER);
				} else {
					// market buy starts
					this.marketBuyOrder = new ArrayList<>(this.playersOrder);
					// new order
					Collections.shuffle(this.marketBuyOrder);
					this.game.startMarketBuy();
					this.inMarketBuy = true;
					this.inMarketOffer = false;
					this.currentPlayer = next(this.marketBuyOrder, 0);
					this.game.setCurrentPlayer(this.game.getPlayerByUsername(this.currentPlayer));
					this.currentStates.get(currentPlayer).addToken(ActionToken.MARKET_BUY);
				}
			} else {
				currentPlayer = next(this.playersOrder, this.playersOrder.indexOf(currentPlayer) + 1);
				if (!this.inMarketOffer) {
					// next player regular gameflow
					this.game.setCurrentPlayer(this.game.getPlayerByUsername(this.currentPlayer));
					this.currentStates.get(currentPlayer).addToken(ActionToken.DRAW_A_POLITIC);
					this.currentStates.get(currentPlayer).addToken(ActionToken.MAIN);
					this.currentStates.get(currentPlayer).addToken(ActionToken.QUICK);
				} else {
					// next player market offer
					this.currentStates.get(currentPlayer).addToken(ActionToken.MARKET_OFFER);
					this.game.setCurrentPlayer(this.game.getPlayerByUsername(this.currentPlayer));
				}
			}

		} else {
			// marketbuy
			if (this.marketBuyOrder.indexOf(
					next(this.marketBuyOrder, this.marketBuyOrder.indexOf(currentPlayer) + 1)) <= this.marketBuyOrder
							.indexOf(this.currentPlayer)) {
				// back to gameflow
				this.inMarketBuy = false;
				game.endMarketSession();
				this.currentPlayer = next(this.playersOrder, 0);
				this.game.setCurrentPlayer(this.game.getPlayerByUsername(this.currentPlayer));
				this.currentStates.get(currentPlayer).addToken(ActionToken.DRAW_A_POLITIC);
				this.currentStates.get(currentPlayer).addToken(ActionToken.MAIN);
				this.currentStates.get(currentPlayer).addToken(ActionToken.QUICK);
				this.turnId++;
				if (isLastTurn) {
					for (Map.Entry<String, PlayerState> entry : this.currentStates.entrySet()) {
						String username = entry.getKey();
						this.currentStates.get(username).addToken(ActionToken.GAME_FINISHED);
					}
					throw new GameFinishedException();
				}
			} else {
				// next buy
				this.currentPlayer = next(this.marketBuyOrder, this.marketBuyOrder.indexOf(currentPlayer) + 1);
				this.game.setCurrentPlayer(this.game.getPlayerByUsername(this.currentPlayer));
				this.currentStates.get(currentPlayer).addToken(ActionToken.MARKET_BUY);
			}
		}
	}

	@Override
	public int getTurnId() {
		return this.turnId;
	}

	@Override
	public void setLastTurn() {
		this.isLastTurn = true;
	}

	@Override
	public String getCurrentPlayer() {
		return this.currentPlayer;
	}

	@Override
	public void setDisconnected(String player) throws GameFinishedException {
		/* if a player is disconnected it means it was current player */
		this.disconnected.add(player);
		/* i've to decide who plays next */
		changeTurn();
		/* putting the disconnected player in a waiting state */
		this.currentStates.get(player).removeAllTokens();
	}

	@Override
	public void reconnect(String player) {
		this.disconnected.remove(player);
	}
	
	/**
	 * Utility method that adds an arbitrary number of tokens of the same type
	 * to the current player state.
	 * 
	 * @param actionToken
	 *            the {@link ActionToken} that has to be added.
	 * @param numberOfTokens
	 *            the number of actionTokens that has to be added.
	 */
	private void addNTokensToCurrentPlayer(int numberOfTokens, ActionToken actionToken) {
		for (int i = 0; i < numberOfTokens; i++) {
			this.currentStates.get(currentPlayer).addToken(actionToken);
		}
	}

	@Override
	public void addNMainActions(int numberOfMainActions) {
		this.addNTokensToCurrentPlayer(numberOfMainActions, ActionToken.MAIN);
	}

	@Override
	public void askForOwnedPermitResponseWaitingState(int numberOfPermitsWaited) {
		this.addNTokensToCurrentPlayer(numberOfPermitsWaited, ActionToken.PERMIT_BONUS_RESPONSE);
	}

	@Override
	public void askForFaceUpPermitResponseWaitingState(int numberOfFaceUpsWaited) {
		this.addNTokensToCurrentPlayer(numberOfFaceUpsWaited, ActionToken.FACE_UP_PERMIT_RESPONSE);

	}

	@Override
	public void askForCityTokenResponseWaitingState(int numberOfCitiesWaited) {
		this.addNTokensToCurrentPlayer(numberOfCitiesWaited, ActionToken.CITY_RESPONSE);

	}

}
