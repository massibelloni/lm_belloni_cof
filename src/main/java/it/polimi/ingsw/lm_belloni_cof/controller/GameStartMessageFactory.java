package it.polimi.ingsw.lm_belloni_cof.controller;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.lm_belloni_cof.messages.GameStartMessage;
import it.polimi.ingsw.lm_belloni_cof.model.Bonus;
import it.polimi.ingsw.lm_belloni_cof.model.City;
import it.polimi.ingsw.lm_belloni_cof.model.CityColor;
import it.polimi.ingsw.lm_belloni_cof.model.ColorRewardTileContent;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMap;
import it.polimi.ingsw.lm_belloni_cof.model.Region;
import it.polimi.ingsw.lm_belloni_cof.model.RegionRewardTileContent;
import it.polimi.ingsw.lm_belloni_cof.model.RewardToken;

public class GameStartMessageFactory {

	/** This class cannot be instantiated */
	private GameStartMessageFactory() {
	}

	public static GameStartMessage getGameStartMessage(Game game, GameMap gameMap) {
		JSONObject update = GameUpdateMessageFactory.getGameStatus(game);
		GameStartMessage message = new GameStartMessage(getGameStartData(game, gameMap).toString(), update.toString());
		return message;
	}

	private static JSONObject getGameStartData(Game game, GameMap gameMap) {
		JSONObject data = new JSONObject();
		/* map section */
		JSONObject map = new JSONObject();
		JSONArray cities = new JSONArray();
		List<City> gameMapCities = gameMap.getCityList();
		for (City gameMapCity : gameMapCities) {
			JSONObject city = new JSONObject();
			city.put("name", gameMapCity.getName().toLowerCase());
			city.put("color", gameMapCity.getCityColor().toString().toLowerCase());
			city.put("region", gameMapCity.getCityRegion().toString().toLowerCase());
			JSONArray tokens = new JSONArray();
			for (RewardToken cityToken : game.getRewardTokensOn(gameMapCity)) {
				JSONArray token = new JSONArray();
				for (Bonus tokenBonus : cityToken.getBonuses()) {
					JSONObject bonus = new JSONObject();
					bonus.put("bonus", tokenBonus.getBonusType().toString().toLowerCase());
					bonus.put("times", tokenBonus.getNumberOfTimes());
					token.put(bonus);
				}
				tokens.put(token);
			}
			city.put("tokens", tokens);
			JSONArray connections = new JSONArray();
			for (City cityConnected : gameMap.getConnectedCities(gameMapCity)) {
				connections.put(cityConnected.getName().toLowerCase());
			}
			city.put("connections", connections);
			cities.put(city);
		}
		map.put("cities", cities);
		/* nobilityPath section */
		JSONArray nobilityPath = new JSONArray();
		for (int i = 0; i < gameMap.getNobilityTrackSize(); i++) {
			JSONArray nobPosition = new JSONArray();
			for (Bonus nobBonus : gameMap.getBonusesForANobilityTrackPosition(i)) {
				JSONObject bonus = new JSONObject();
				bonus.put("bonus", nobBonus.getBonusType().toString().toLowerCase());
				bonus.put("times", nobBonus.getNumberOfTimes());
				nobPosition.put(bonus);
			}
			nobilityPath.put(nobPosition);
		}
		map.put("nobilityPath", nobilityPath);
		/* regionRewardTiles */
		JSONArray regionRewardTiles = new JSONArray();
		for (Region region : Region.values()) {
			if (region != Region.KING) {
				RegionRewardTileContent regionContent = gameMap.getRewardTileContentByRegion(region);
				JSONObject rewardTile = new JSONObject();
				rewardTile.put("region", region.toString().toLowerCase());
				JSONArray bonuses = new JSONArray();
				for (Bonus tileBonus : regionContent.getBonuses()) {
					JSONObject bonus = new JSONObject();
					bonus.put("bonus", tileBonus.getBonusType().toString().toLowerCase());
					bonus.put("times", tileBonus.getNumberOfTimes());
					bonuses.put(bonus);
				}
				rewardTile.put("bonuses", bonuses);
				regionRewardTiles.put(rewardTile);
			}
		}
		map.put("regionRewardTiles", regionRewardTiles);

		/* colorRewardTile */
		JSONArray colorRewardTiles = new JSONArray();
		for (CityColor color : CityColor.values()) {
			if (color != CityColor.PURPLE) {
				ColorRewardTileContent colorContent = gameMap.getRewardTileContentByCityColor(color);
				JSONObject rewardTile = new JSONObject();
				rewardTile.put("color", color.toString().toLowerCase());
				JSONArray bonuses = new JSONArray();
				for (Bonus tileBonus : colorContent.getBonuses()) {
					JSONObject bonus = new JSONObject();
					bonus.put("bonus", tileBonus.getBonusType().toString().toLowerCase());
					bonus.put("times", tileBonus.getNumberOfTimes());
					bonuses.put(bonus);
				}
				rewardTile.put("bonuses", bonuses);
				colorRewardTiles.put(rewardTile);
			}
		}
		map.put("colorRewardTiles", colorRewardTiles);
		map.put("maxCoins", gameMap.getMaxCoins());
		map.put("maxVictoryPoints", gameMap.getMaxVictoryPoints());
		data.put("map", map);
		/* players section */
		JSONArray players = new JSONArray();
		for (String username : game.getPlayerUsernames())
			players.put(username);
		data.put("players", players);
		return data;
	}
}
