package it.polimi.ingsw.lm_belloni_cof.controller.states;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.exceptions.GameFinishedException;

/**
 * Interface that allows the controller to verify if a player is allowed to
 * execute the actions it wants to execute and to "change" the player states.
 */
public interface StatesManager {

	/**
	 * Get the {@link PlayerState} associated to the player passed as an
	 * argument.
	 */
	public PlayerState getState(String player);

	/**
	 * Change the state of the player passed as an argument after it has
	 * executed the action passed.
	 * 
	 * @param player
	 *            the player of which the state has to be changed
	 * @param actionPerformed
	 *            the action just performed
	 * @throws GameFinishedException
	 *             if the current game is finished changing the player's state.
	 */
	public void changeState(String player, Action actionPerformed) throws GameFinishedException;

	/** Get the incremental number of the turn reached by the game. */
	public int getTurnId();

	/** Set the current turn as the last turn of a game. */
	public void setLastTurn();

	/** Get the player that is expected to play. */
	public String getCurrentPlayer();

	/**
	 * Disconnect the player passed as an argument.
	 * 
	 * @param player
	 *            the player to disconnect
	 * @throws GameFinishedException
	 *             if the current game is finished due to last player
	 *             disconnection.
	 */
	public void setDisconnected(String player) throws GameFinishedException;

	/**
	 * Reconnect the player passed as an argument.
	 * 
	 * @param player
	 *            the player to reconnect
	 */
	public void reconnect(String player);
}
