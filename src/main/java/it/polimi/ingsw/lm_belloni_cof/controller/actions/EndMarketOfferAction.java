package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import it.polimi.ingsw.lm_belloni_cof.controller.states.ActionToken;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Player;

public class EndMarketOfferAction extends MarketOfferAction {

	public EndMarketOfferAction(Game game, Player player) {
		super(game, player, null);

	}

	/**
	 * The {@link Action#execute} instance method. This does actually nothing.
	 */
	@Override
	public boolean execute() {
		this.actionDescription = super.player.getUsername() + " has ended to offer on market.";
		super.setExecuted();
		return false;
	}

	@Override
	public ActionToken getActionToken() {
		return ActionToken.MARKET_OFFER;
	}

}
