package it.polimi.ingsw.lm_belloni_cof.controller.states;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.BonusResponseAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.CityTokenResponseAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.DrawAPoliticAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.FaceUpPermitResponseAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.MainAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.MarketBuyAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.MarketOfferAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.OwnedPermitResponseAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.QuickAction;

/**
 * Object representing the state in which a player is. This stores all the
 * {@link ActionToken} it has and based on these decides whether or not the
 * player can execute an action.
 */
public class PlayerState {
	/** List of all the tokens stored. */
	private List<ActionToken> tokens;

	/** Create an empty state without any token. */
	public PlayerState() {
		this.tokens = new ArrayList<>();
	}

	/**
	 * Core method of the class: based on the tokens stored decides if the
	 * action passed as an argument may be executed.
	 * 
	 * @param action
	 *            the {@link Action} that the player wants to execute
	 * @return true if the action passed as an argument can be executed, false
	 *         otherwise
	 */
	public boolean isAllowedToExecute(Action action) {
		if (!this.tokens.isEmpty() && !this.tokens.contains(ActionToken.GAME_FINISHED)) {
			// player needs to draw a politic to start its turn
			if (this.tokens.contains(ActionToken.DRAW_A_POLITIC)) {
				if (action instanceof DrawAPoliticAction)
					return true;
				return false;
			} else if (this.tokens.contains(ActionToken.FACE_UP_PERMIT_RESPONSE)
					|| this.tokens.contains(ActionToken.PERMIT_BONUS_RESPONSE)
					|| this.tokens.contains(ActionToken.CITY_RESPONSE)) {
				if (!(action instanceof BonusResponseAction))
					return false;
				if (this.tokens.contains(ActionToken.FACE_UP_PERMIT_RESPONSE)
						&& action instanceof FaceUpPermitResponseAction
						|| this.tokens.contains(ActionToken.PERMIT_BONUS_RESPONSE)
								&& action instanceof OwnedPermitResponseAction
						|| this.tokens.contains(ActionToken.CITY_RESPONSE) && action instanceof CityTokenResponseAction)
					return true;
				return false;
			} else if (this.tokens.contains(ActionToken.MAIN) && action instanceof MainAction
					&& !(action instanceof BonusResponseAction))
				return true;
			else if (this.tokens.contains(ActionToken.QUICK) && action instanceof QuickAction)
				return true;
			else if (this.tokens.contains(ActionToken.MARKET_OFFER) && action instanceof MarketOfferAction)
				return true;
			else if (this.tokens.contains(ActionToken.MARKET_BUY) && action instanceof MarketBuyAction)
				return true;
			else
				return false;
		}
		return false;
	}

	/**
	 * Remove a token from the list (= an action has been executed.)
	 * 
	 * @param tokenToRemove
	 *            the {@link ActionToken} to remove
	 * @see Action
	 */
	public void removeToken(ActionToken tokenToRemove) {
		this.tokens.remove(tokenToRemove);
	}

	/**
	 * Add a token to the list (= give to the associated player the right to
	 * execute a specific action)
	 * 
	 * @param tokenToAdd
	 *            the {@link ActionToken} to be added.
	 */
	public void addToken(ActionToken tokenToAdd) {
		this.tokens.add(tokenToAdd);
	}

	/**
	 * 
	 * @return true if the list of token is empty (= the player cannot execute
	 *         any action)
	 */
	public boolean isEmpty() {
		return tokens.isEmpty();
	}

	/**
	 * Remove all tokens from the list (= after this call the player won't be
	 * able to execute any action).
	 */
	public void removeAllTokens() {
		this.tokens.clear();
	}
}
