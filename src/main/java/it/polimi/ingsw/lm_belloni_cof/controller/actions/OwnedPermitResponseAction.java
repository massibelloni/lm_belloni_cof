package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.controller.states.ActionToken;
import it.polimi.ingsw.lm_belloni_cof.controller.states.StateChangingBonusHandler;
import it.polimi.ingsw.lm_belloni_cof.model.Bonus;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Permit;
import it.polimi.ingsw.lm_belloni_cof.model.Player;

/**
 * The {@link Action} generated (and executed) to specify the owned
 * {@link Permit} from which obtain the {@link Bonus}es on it (again).
 *
 */
public class OwnedPermitResponseAction extends BonusResponseAction {
	private static final Logger logger = Logger.getLogger(OwnedPermitResponseAction.class.getName());
	/** The permit from which the bonuses will be obtained */
	private Permit permit;

	/**
	 * Public constructor of this action.
	 * 
	 * @param game
	 *            the {@link Game} on which the action will generate changes.
	 * @param player
	 *            the {@link Player} responsible for the changes on the
	 *            {@link Game}
	 * @param permit
	 *            the permit from which the bonuses will be obtained
	 * @param bonusHandler
	 *            a {@link StateChangingBonusHandler} instance to edit the
	 *            {@link PlayerState} of the {@link Player}.
	 */
	public OwnedPermitResponseAction(Game game, Player player, Permit permit,
			StateChangingBonusHandler stateChangingBonusHandler) {
		super(game, player, stateChangingBonusHandler);
		this.permit = permit;
	}

	/**
	 * Obtain and handle the {@link Bonus}es on the permit passed as an
	 * argument.
	 * 
	 * @see MainAction#handleBonuses
	 */
	@Override
	public boolean execute() {
		List<Bonus> bonusesToBeHandled = new ArrayList<>();
		if (this.player.getPermitsAcquired().contains(permit)) {
			bonusesToBeHandled.addAll(permit.getBonuses());
			handleBonuses(bonusesToBeHandled);
			super.actionDescription += this.player.getUsername() + " has gained bonuses from his own permit tile.";
			super.actionDescription += super.getHandledBonusesDescriptions();
			super.ownedPermitResponsesNeeded-=1;
			super.setExecuted();
		} else
			logger.log(Level.SEVERE, "The MessageTranslator should avoid this.");
		return false;
	}

	@Override
	public ActionToken getActionToken() {
		return ActionToken.PERMIT_BONUS_RESPONSE;
	}

}
