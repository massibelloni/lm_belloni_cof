package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.controller.states.ActionToken;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.EmptyDeckException;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Player;

/**
 * The action representing the act of drawing a politic card at the beginning of
 * every turn,
 */
public class DrawAPoliticAction extends Action {
	private static final Logger logger = Logger.getLogger(DrawAPoliticAction.class.getName());

	/**
	 * Public constructor for the action.
	 * 
	 * @param game
	 *            the {@link Game} on which the action will generate changes.
	 * @param player
	 *            the {@link Player} responsible for the changes on the
	 *            {@link Game}.
	 */
	public DrawAPoliticAction(Game game, Player player) {
		super(game, player);
	}

	/**
	 * Calls the
	 * {@link it.polimi.ingsw.lm_belloni_cof.model.Game#drawAPolitic(Player)}
	 * method of the model and edits the {@link Action#actionDescription} field.
	 * 
	 * @throws ActionExecutionException
	 *             when the requested action hasn't been completely executed due
	 *             to a {@link EmptyDeckException}.
	 */
	@Override
	public boolean execute() throws ActionExecutionException {
		try {
			super.game.drawAPolitic(super.player);
			super.actionDescription += super.player.getUsername() + " has drawn a politic.";
		} catch (EmptyDeckException e) {
			logger.log(Level.INFO, "Politic deck is empty.", e);
			super.actionDescription += super.player.getUsername() + " has tried to draw, but the deck is empty.";
		}
		super.setExecuted();
		return false;
	}

	@Override
	public ActionToken getActionToken() {
		return ActionToken.DRAW_A_POLITIC;
	}

}
