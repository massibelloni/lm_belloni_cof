package it.polimi.ingsw.lm_belloni_cof.controller;

/** This class contains all the data that must be stored for a player. */

public class PlayerData {
	private String username;
	private String password;
	private int gamesPlayed;
	private int gamesWon;
	private long totalGameTimeInMillis;

	public PlayerData(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return this.username;
	}

	public String getPassword() {
		return this.password;
	}

	public int getGamesPlayed() {
		return this.gamesPlayed;
	}

	public int getGamesWon() {
		return this.gamesWon;
	}

	public long getTotalGameTime() {
		return this.totalGameTimeInMillis;
	}

	public void addAPlayedGame() {
		this.gamesPlayed++;
	}

	public void addAWonGame() {
		this.gamesWon++;
	}

	public void addGameTime(long gameTimeInMillis) {
		this.totalGameTimeInMillis += gameTimeInMillis;
	}
}
