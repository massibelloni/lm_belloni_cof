package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.controller.states.ActionToken;
import it.polimi.ingsw.lm_belloni_cof.controller.states.StateChangingBonusHandler;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidActionException;
import it.polimi.ingsw.lm_belloni_cof.model.Bonus;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Region;

/**
 * The {@link Action} generated (and executed) to specify a {@link Permit} after
 * a TAKE_PERMIT bonus.
 */
public class FaceUpPermitResponseAction extends BonusResponseAction {
	private static final Logger logger = Logger.getLogger(FaceUpPermitResponseAction.class.getName());
	/**
	 * The {@link Region} of the {@link Permit} that the {@link Player} wants to
	 * acquire.
	 */
	private Region region;
	/**
	 * The index on the permit board of the {@link Permit}: it may be 1 or 2.
	 * 
	 * @see it.polimi.ingsw.lm_belloni_cof.model.PermitBoard
	 **/
	private int indexInRegion;

	/**
	 * Public constructor of this action.
	 * 
	 * @param game
	 *            the {@link Game} on which the action will generate changes.
	 * @param player
	 *            the {@link Player} responsible for the changes on the
	 *            {@link Game}.
	 * @param region
	 *            the {@link Region} of the {@link Permit} that the
	 *            {@link Player} wants to acquire
	 * @param indexInRegion
	 *            the index on the permit board of the {@link Permit}: it may be
	 *            1 or 2
	 * @param bonusHandler
	 *            {@link StateChangingBonusHandler} instance to edit the
	 *            {@link PlayerState} of the {@link Player}.
	 * 
	 */
	public FaceUpPermitResponseAction(Game game, Player player, Region region, int indexInRegion,
			StateChangingBonusHandler bonusHandler) {
		super(game, player, bonusHandler);
		this.region = region;
		this.indexInRegion = indexInRegion;
	}

	/**
	 * Calls the
	 * {@link it.polimi.ingsw.lm_belloni_cof.model.Game#acquireAPermitTile(Player,Region,int)}
	 * of the model and handles the bonuses obtained. Then edits the
	 * {@link Action#actionDescription} field.
	 * 
	 * @see MainAction#handleBonuses
	 * @return true if the player has built all its emporiums, false otherwise
	 */
	@Override
	public boolean execute() {
		try {
			List<Bonus> bonuses = game.acquireAPermitTile(player, region, indexInRegion);
			super.actionDescription += player.getUsername() + " has obtained a permit tile in the region of "
					+ region.toString().toLowerCase() + " as a bonus.";
			super.handleBonuses(bonuses);
			super.actionDescription += super.getHandledBonusesDescriptions();
			super.faceUpPermitResponsesNeeded-=1;
			super.setExecuted();
		} catch (InvalidActionException e) {
			logger.log(Level.SEVERE, "The MessageTranslator should avoid this.", e);
		}
		return false;
	}

	@Override
	public ActionToken getActionToken() {
		return ActionToken.FACE_UP_PERMIT_RESPONSE;
	}

}
