package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughAssistantsException;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Region;

/**
 * The {@link QuickAction} representing the Council of Four
 * "Change business permit tiles" quick action.
 */
public class ChangeBusinessPermitTilesAction extends QuickAction {
	/** The region of which the permits on it will be changed. */
	private Region region;

	/**
	 * Public action constructor
	 * 
	 * @param game
	 *            the {@link Game} on which the action will generate changes.
	 * @param player
	 *            the {@link Player} responsible for the changes on the
	 *            {@link Game}.
	 * @param region
	 *            the region of which the permits on it will be changed.
	 */
	public ChangeBusinessPermitTilesAction(Game game, Player player, Region region) {
		super(game, player);
		this.region = region;
	}

	/**
	 * Calls the
	 * {@link it.polimi.ingsw.lm_belloni_cof.model.Game#changeBusinessPermitTiles(Player,Region)}
	 * of the model, then edits the {@link Action#actionDescription} field.
	 * 
	 * @return true if the player has built all its emporiums, false otherwise
	 * @throws ActionExecutionException
	 *             when the requested action hasn't been completely executed due
	 *             to a {@link NotEnoughAssistantsException}.
	 */
	@Override
	public boolean execute() throws ActionExecutionException {
		try {
			game.changeBusinessPermitTiles(this.player, this.region);
			super.actionDescription += player.getUsername() + " has changed the face up permit tiles in the region "
					+ region.toString().toLowerCase();
			super.setExecuted();
		} catch (NotEnoughAssistantsException e) {
			throw new ActionExecutionException("Not enough assistants to complete this action.", e);
		}
		return false;
	}

}
