package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.controller.states.ActionToken;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidActionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughCoinsException;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Ownable;
import it.polimi.ingsw.lm_belloni_cof.model.Player;

/**
 * The {@link MarketAction} representing the intent of a {@link Player} to buy
 * an {@link Ownable} during a market session.
 * 
 * @see Ownable
 */
public class MarketBuyAction extends MarketAction {
	private static final Logger logger = Logger.getLogger(MarketBuyAction.class.getName());
	/**
	 * The {@link Ownable} that the current {@link Player} is trying to
	 * purchase.
	 */
	private Ownable purchase;
	/** The {@link Player} that's selling the {@link Ownable}· */
	private Player seller;

	/**
	 * Public constructor.
	 * 
	 * @param game
	 *            a correctly instantiated {@link Game}
	 * @param player
	 *            a {@link Player} that is currently playing the game.
	 * @param seller
	 *            a {@link Player} that is currently playing the game and that
	 *            is selling the {@link Ownable} that player is trying to
	 *            purchase.
	 * @param purchase
	 *            the {@link Ownable} that the current {@link Player} is trying
	 *            to purchase.
	 */
	public MarketBuyAction(Game game, Player player, Player seller, Ownable purchase) {
		super(game, player);
		this.purchase = purchase;
		this.seller = seller;
	}

	/**
	 * The {@link Action#execute} instance method.
	 */
	@Override
	public boolean execute() throws ActionExecutionException {
		try {
			game.tryToBuy(super.player, this.seller, this.purchase);
			super.actionDescription += player.getUsername() + " has purchased a " + this.purchase.toString() + " from "
					+ seller.getUsername();
			super.setExecuted();
		} catch (NotEnoughCoinsException e) {
			throw new ActionExecutionException("Not enough coins to complete this action.", e);
		} catch (InvalidActionException e) {
			logger.log(Level.SEVERE, "The MessageTranslator should avoid this.", e);
		}
		return false;
	}

	/**
	 * Returns the {@link ActionToken} for the executed action.
	 * 
	 * @return null, a {@link Player} may execute an indefinite number of
	 *         {@link MarketBuyAction}.
	 */
	@Override
	public ActionToken getActionToken() {
		return null;
	}

}
