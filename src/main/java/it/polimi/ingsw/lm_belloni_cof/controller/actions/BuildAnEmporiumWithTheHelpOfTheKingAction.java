package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.controller.states.StateChangingBonusHandler;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.CitiesNotConnectedException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.CouncilUnsatisfiedException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.EmporiumAlreadyBuiltException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidActionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughAssistantsException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughCoinsException;
import it.polimi.ingsw.lm_belloni_cof.model.Bonus;
import it.polimi.ingsw.lm_belloni_cof.model.City;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Politic;

/**
 * The {@link MainAction} representing the Council of Four
 * "Build an emporium with the help of the king" main action
 */
public class BuildAnEmporiumWithTheHelpOfTheKingAction extends MainAction {
	private static final Logger logger = Logger.getLogger(BuildAnEmporiumWithTheHelpOfTheKingAction.class.getName());
	/**
	 * The city where the emporium will be built and where the king will be
	 * placed.
	 */
	private City city;
	/**
	 * The list of politic cards that the player intends to use to satisfy the
	 * council of the king.
	 */
	private List<Politic> politics;

	/**
	 * Public constructor of the action.
	 * 
	 * @param game
	 *            the {@link Game} on which the action will generate changes.
	 * @param player
	 *            the {@link Player} responsible for the changes on the
	 *            {@link Game}.
	 * @param city
	 *            the city where the emporium will be built and the king will be
	 *            placed.
	 * @param politics
	 *            the list of politic cards that the player intends to use to
	 *            satisfy the council of the king
	 * @param bonusHandler
	 *            a {@link StateChangingBonusHandler} instance to edit the
	 *            {@link PlayerState} of the {@link Player}.
	 */
	public BuildAnEmporiumWithTheHelpOfTheKingAction(Game game, Player player, City city, List<Politic> politics,
			StateChangingBonusHandler bonusHandler) {
		super(game, player, bonusHandler);
		this.politics = politics;
		this.city = city;
	}

	/**
	 * Calls the
	 * {@link it.polimi.ingsw.lm_belloni_cof.model.Game#buildAnEmporiumWithTheHelpOfTheKing(Player,City,List)}
	 * of the model and handles the bonuses obtained. Then edits the
	 * {@link Action#actionDescription} field.
	 * 
	 * @see MainAction#handleBonuses
	 * @return true if the player has built all its emporiums, false otherwise
	 * @throws ActionExecutionException
	 *             when the requested action hasn't been completely executed due
	 *             to a {@link NotEnoughCoinsException}, a
	 *             {@link NotEnoughAssistantsException} or a
	 *             {@link CouncilUnsatisfiedException}.
	 */
	@Override
	public boolean execute() throws ActionExecutionException {
		try {
			List<Bonus> bonuses = game.buildAnEmporiumWithTheHelpOfTheKing(player, city, politics);
			super.actionDescription += player.getUsername() + " has built an emporium on the city of " + city.getName()
					+ " with the help of the king.";
			super.handleBonuses(bonuses);
			super.actionDescription += super.getHandledBonusesDescriptions();
			super.setExecuted();
			if (player.getNumberOfEmporiums() == 0) {
				if (super.game.getFirstToFinish() == null)
					super.game.setFirstToFinish(super.player);
				return true;
			}
		} catch (NotEnoughCoinsException e) {
			throw new ActionExecutionException("Not enough coins to complete this action.", e);
		} catch (NotEnoughAssistantsException e) {
			throw new ActionExecutionException("Not enough assistants to complete this action.", e);
		} catch (CitiesNotConnectedException e) {
			throw new ActionExecutionException("Doesn't exist a road that connects the two cities.", e);
		} catch (CouncilUnsatisfiedException e) {
			throw new ActionExecutionException("The council isn't satisfied.", e);
		} catch (EmporiumAlreadyBuiltException e) {
			throw new ActionExecutionException("You already have an emporium on this city.", e);
		} catch (InvalidActionException e) {
			logger.log(Level.SEVERE, "The MessageTranslator should avoid this.", e);
		}
		return false;
	}

}
