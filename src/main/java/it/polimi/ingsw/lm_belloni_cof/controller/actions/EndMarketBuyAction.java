package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import it.polimi.ingsw.lm_belloni_cof.controller.states.ActionToken;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Ownable;
import it.polimi.ingsw.lm_belloni_cof.model.Player;

/**
 * The {@link MarketAction} representing the intent of a {@link Player} to close
 * its market buy session.
 * 
 * @see Ownable
 */
public class EndMarketBuyAction extends MarketBuyAction {
	/**
	 * Default constructor
	 * 
	 * @see Action#Action(Game,Player)
	 * @param game
	 *            the {@link Game} in which the {@link MarketAction} will be
	 *            executed.
	 * @param player
	 *            the {@link Player} this {@link Action} belongs to.
	 */
	public EndMarketBuyAction(Game game, Player player) {
		super(game, player, null, null);
	}

	/**
	 * The {@link Action#execute} instance method. This does actually nothing.
	 */
	@Override
	public boolean execute() {
		this.actionDescription = super.player.getUsername() + " has ended to buy on market.";
		super.setExecuted();
		return false;
	}
	
	/**
	 * This is the only {@link Action} instance that removes a MARKET_BUY of
	 * {@link ActionToken}
	 * 
	 * @return ActionToken.MARKET_BUY
	 */
	@Override
	public ActionToken getActionToken() {
		return ActionToken.MARKET_BUY;
	}

}
