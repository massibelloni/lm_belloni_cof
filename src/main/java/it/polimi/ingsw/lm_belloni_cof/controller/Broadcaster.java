package it.polimi.ingsw.lm_belloni_cof.controller;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.messages.BroadcastMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
import it.polimi.ingsw.lm_belloni_cof.view.shared.BrokerInterface;
import it.polimi.ingsw.lm_belloni_cof.view.shared.SubscriberInterface;

/**
 * Implementation of the {@link BroadcasterInterface} and the
 * {@link BrokerInterface} that handles the client subscriptions and sends
 * broadcast messages using the Publisher-Subscriber pattern.
 */
public class Broadcaster implements BroadcasterInterface, BrokerInterface {
	private static final Logger logger = Logger.getLogger(Broadcaster.class.getName());
	/**
	 * The list of the games (topics in the pattern) to which players the
	 * broadcaster is able to send messages.
	 */
	private List<String> gamesList;
	/**
	 * Collections that connects usernames to the interfaces that handles the
	 * {@link BroadcastMessage}s for them.
	 */
	private Map<String, SubscriberInterface> subscribers;
	/**
	 * Collections that stores for each game all the {@link PlayerToken}s
	 * interested in updates.
	 */
	private Map<String, List<PlayerToken>> subscribedTokens;

	public Broadcaster() {
		this.gamesList = new ArrayList<>();
		this.subscribers = new HashMap<>();
		this.subscribedTokens = new HashMap<>();
	}

	/**
	 * Broadcast a message to all the clients connect to a game.
	 * 
	 * @param msg
	 *            the message to broadcast
	 * @param gameId
	 *            the game of which subscribers have to receive the message
	 */
	@Override
	public synchronized void broadcast(BroadcastMessage msg, String gameId) {
		if (gamesList.contains(gameId)) {
			for (PlayerToken observerToken : subscribedTokens.get(gameId)) {
				try {
					subscribers.get(observerToken.getUsername()).update(msg);
				} catch (RemoteException e) {
					logger.log(Level.INFO, observerToken.getUsername() + " is unreachable.", e);
					subscribers.put(observerToken.getUsername(), null);
				} catch (NullPointerException e) {
					logger.log(Level.FINE, observerToken.getUsername() + " isn't subscribed.", e);
				}
			}

		}
	}

	/**
	 * Broadcast a message to a selected list of clients
	 * 
	 * @param msg
	 *            the message to broadcast
	 * @param tokens
	 *            a {@link List} of tokens that will receive the broadcast
	 *            message.
	 */
	@Override
	public synchronized void broadcast(BroadcastMessage msg, List<PlayerToken> tokens) {
		for (PlayerToken observerToken : tokens) {
			try {
				subscribers.get(observerToken.getUsername()).update(msg);
			} catch (RemoteException e) {
				logger.log(Level.INFO, observerToken.getUsername() + " is unreachable.", e);
			} catch (NullPointerException e) {
				logger.log(Level.FINE, observerToken.getUsername() + " isn't subscribed.", e);
			}
		}
	}

	/**
	 * Subscribe a player to the updates of a game.
	 */
	@Override
	public synchronized void subscribeTokenToGame(PlayerToken playerToken, String gameId) {
		if (this.gamesList.contains(gameId) && !subscribedTokens.get(gameId).contains(playerToken)) {
			subscribedTokens.get(gameId).add(playerToken);
		}
	}

	/** Add a new game to the list of known game. */
	@Override
	public void addGame(String gameId) {
		if (!this.gamesList.contains(gameId)) {
			this.gamesList.add(gameId);
			// no one is subscribed yet
			this.subscribedTokens.put(gameId, new ArrayList<>());
		}
	}

	/** Unsubscribe a player from all the updates. */
	@Override
	public void removeGamesFromToken(PlayerToken playerToken) {
		for (List<PlayerToken> subscribed : this.subscribedTokens.values())
			subscribed.remove(playerToken);
	}

	/**
	 * Unsubscribe a player from the updates of the game passed as an argument.
	 */
	@Override
	public void removeGameFromToken(PlayerToken playerToken, String gameId) {
		if (this.subscribedTokens.containsKey(gameId))
			this.subscribedTokens.get(gameId).remove(playerToken);
	}

	/**
	 * Associates the updates sent to a {@link PlayerToken} to the interface
	 * that actually handles them.
	 */
	@Override
	public void subscribe(SubscriberInterface subscriber, String username) throws RemoteException {
		this.subscribers.put(username, subscriber);
	}

}
