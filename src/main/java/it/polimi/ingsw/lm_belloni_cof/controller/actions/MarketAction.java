package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Player;

/**
 * The general superclass for an Action executed during a Market session, this
 * may be a {@link MarketBuyAction}. an {@link EndMarketBuyAction} or a
 * {@link MarketOfferAction}.
 */

public abstract class MarketAction extends Action {
	/**
	 * Default constructor
	 * 
	 * @see Action#Action(Game,Player)
	 * @param game
	 *            the {@link Game} in which the {@link MarketAction} will be
	 *            executed.
	 * @param player
	 *            the {@link Player} this {@link Action} belongs to.
	 */
	protected MarketAction(Game game, Player player) {
		super(game, player);
	}

}
