package it.polimi.ingsw.lm_belloni_cof.controller;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidMessageException;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.*;

/**
 * Interface describing the whole message to action process behind the scene of
 * this project implementation.
 */
public interface MessageTranslator {

	public Action translate(AcquireABusinessPermitTileActRqstMsg message) throws InvalidMessageException;

	public Action translate(BuildAnEmporiumUsingAPermitTileActRqstMsg message) throws InvalidMessageException;

	public Action translate(BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg message) throws InvalidMessageException;

	public Action translate(ChangeBusinessPermitTilesActRqstMsg message) throws InvalidMessageException;

	public Action translate(CityTokenResponseActRqstMsg message) throws InvalidMessageException;

	public Action translate(DrawAPoliticActRqstMsg message) throws InvalidMessageException;

	public Action translate(ElectACouncillorActRqstMsg message) throws InvalidMessageException;

	public Action translate(EmptyQuickActionActRqstMsg message) throws InvalidMessageException;

	public Action translate(EngageAnAssistantActRqstMsg message) throws InvalidMessageException;

	public Action translate(EndMarketBuyActRqstMsg message) throws InvalidMessageException;

	public Action translate(EndMarketOfferActRqstMsg message) throws InvalidMessageException;

	public Action translate(FaceUpPermitResponseActRqstMsg message) throws InvalidMessageException;

	public Action translate(MarketBuyActRqstMsg message) throws InvalidMessageException;

	public Action translate(MarketOfferActRqstMsg message) throws InvalidMessageException;

	public Action translate(OwnedPermitResponseActRqstMsg message) throws InvalidMessageException;

	public Action translate(PerformAnAdditionalMainActionActRqstMsg message) throws InvalidMessageException;

	public Action translate(SendAnAssistantToElectACouncillorActRqstMsg message) throws InvalidMessageException;

}
