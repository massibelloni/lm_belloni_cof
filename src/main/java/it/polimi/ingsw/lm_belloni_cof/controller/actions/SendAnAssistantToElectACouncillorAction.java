package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidActionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughAssistantsException;
import it.polimi.ingsw.lm_belloni_cof.model.Councillor;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Region;

/**
 * The {@link QuickAction} representing the Council of Four
 * "Send an assistant to elect a councillor" quick action.
 */
public class SendAnAssistantToElectACouncillorAction extends QuickAction {
	private static final Logger logger = Logger.getLogger(SendAnAssistantToElectACouncillorAction.class.getName());
	/** The region of the council that will be modified. */
	private Region region;
	/** The councillor that will be elected. */
	private Councillor councillor;

	/**
	 * Public constructor of this action.
	 * 
	 * @param game
	 *            the {@link Game} on which the action will generate changes.
	 * @param player
	 *            the {@link Player} responsible for the changes on the
	 *            {@link Game}.
	 * @param region
	 *            the region of the council that will be modified.
	 * @param councillor
	 *            the councillor that will be elected.
	 */
	public SendAnAssistantToElectACouncillorAction(Game game, Player player, Councillor councillor, Region region) {
		super(game, player);
		this.region = region;
		this.councillor = councillor;
	}

	/**
	 * Calls the
	 * {@link it.polimi.ingsw.lm_belloni_cof.model.Game#sendAnAssistantToElectACouncillor(Player, Region, Councillor)}
	 * then edits the {@link Action#actionDescription} field.
	 * 
	 * @throws ActionExecutionException
	 *             when the requested action hasn't been completely executed due
	 *             to a {@link NotEnoughAssistantsException}.
	 */
	@Override
	public boolean execute() throws ActionExecutionException {
		try {
			game.sendAnAssistantToElectACouncillor(player, region, councillor);
			super.actionDescription += player.getUsername() + " has sent an assistant to elect a councillor in region "
					+ region.toString().toLowerCase();
			super.setExecuted();
		} catch (NotEnoughAssistantsException e) {
			throw new ActionExecutionException("Not enough assistants to complete this action.", e);
		} catch (InvalidActionException e) {
			logger.log(Level.SEVERE, "The MessageTranslator should avoid this.", e);
		}
		return false;

	}

}
