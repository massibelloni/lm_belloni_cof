package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import it.polimi.ingsw.lm_belloni_cof.controller.GameUpdateMessageFactory;
import it.polimi.ingsw.lm_belloni_cof.controller.states.ActionToken;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.messages.ActionExecutedMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.GameUpdateMessage;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Player;

/**
 * The most abstract concept of an Action, the only possible way to edit a
 * {@link Game} object.
 */
public abstract class Action {
	/** The {@link Game} on which the action will generate changes. */
	protected Game game;
	/**
	 * The {@link Player} responsible for the changes on the {@link Game}.
	 */
	protected Player player;
	/** True if the current action has already been executed, false otherwise */
	protected boolean executed;
	/** An human readable {@link String} describing the action */
	protected String actionDescription;

	/**
	 * Default constructor for any Action.
	 * 
	 * @param game
	 *            the {@link Game} on which the action will generate changes.
	 * @param player
	 *            the {@link Player} responsible for the changes on the
	 *            {@link Game}.
	 */
	protected Action(Game game, Player player) {
		this.game = game;
		this.player = player;
		this.actionDescription = "";
	}

	/**
	 * The method that actually edits the model.
	 * 
	 * @return true if the player has built all its emporiums, false otherwise
	 * @throws ActionExecutionException
	 *             when the requested action hasn't been completely executed on
	 *             the model keeping it like it was before the {@link #execute}
	 */
	public abstract boolean execute() throws ActionExecutionException;

	/**
	 * 
	 * @return the {@link ActionToken} for the Action instance.
	 */
	public abstract ActionToken getActionToken();

	/**
	 * Return a {@link String} representing the description of the Action
	 * executed.
	 * 
	 * @return a {@link String} representing the description of the Action
	 *         executed
	 */
	public String getActionDescription() {
		return (!"".equals(this.actionDescription)) ? this.actionDescription : "Not executed yet.";
	};

	/**
	 * Set the action as executed.
	 */
	protected void setExecuted() {
		this.executed = true;
	}

	/**
	 * Returns whether the action has been executed or not.
	 * 
	 * @return true if the action has been executed, false otherwise.
	 */
	public boolean hasBeenExecuted() {
		return this.executed;
	}

	/**
	 * Get the {@link GameUpdateMessage} for the executed Action
	 * 
	 * @return a {@link GameUpdateMessage}
	 */
	public GameUpdateMessage getGameUpdateMessage() {
		return GameUpdateMessageFactory.getGameUpdateMessage(this.game, this);
	}

	public ActionExecutedMessage getActionExecutedMessage() {
		ActionExecutedMessage msg = new ActionExecutedMessage(this.actionDescription);
		return msg;
	}

}
