package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import it.polimi.ingsw.lm_belloni_cof.controller.states.ActionToken;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Player;

/**
 * The abstract concept of a Quick Action.
 */
public abstract class QuickAction extends Action {
	/**
	 * The protected constructor of a quick action.
	 * 
	 * @param game
	 *            the {@link Game} on which the action will generate changes.
	 * @param player
	 *            the {@link Player} responsible for the changes on the
	 *            {@link Game}.
	 */
	public QuickAction(Game game, Player player) {
		super(game, player);
	}

	@Override
	public ActionToken getActionToken() {
		return ActionToken.QUICK;
	}

}
