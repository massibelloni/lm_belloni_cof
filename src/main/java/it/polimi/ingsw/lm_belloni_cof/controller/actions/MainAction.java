package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.lm_belloni_cof.controller.states.ActionToken;
import it.polimi.ingsw.lm_belloni_cof.controller.states.StateChangingBonusHandler;
import it.polimi.ingsw.lm_belloni_cof.messages.ActionExecutedMessage;
import it.polimi.ingsw.lm_belloni_cof.model.Bonus;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Player;

/**
 * The abstract concept of a Main Action.
 */
public abstract class MainAction extends Action {
	/**
	 * A {@link StateChangingBonusHandler} instance to edit the
	 * {@link PlayerState} of the {@link Player}.
	 */
	protected StateChangingBonusHandler bonusHandler;
	/**
	 * The {@link Bonus}es correctly handled during the execution of this
	 * action.
	 */
	protected List<Bonus> handledBonuses;

	protected int cityTokenResponsesNeeded;
	protected int faceUpPermitResponsesNeeded;
	protected int ownedPermitResponsesNeeded;

	/**
	 * The protected constructor of a main action.
	 * 
	 * @param game
	 *            the {@link Game} on which the action will generate changes.
	 * @param player
	 *            the {@link Player} responsible for the changes on the
	 *            {@link Game}.
	 * @param bonusHandler
	 *            a {@link StateChangingBonusHandler} instance to edit the
	 *            {@link PlayerState} of the {@link Player}.
	 */
	protected MainAction(Game game, Player player, StateChangingBonusHandler bonusHandler) {
		super(game, player);
		this.bonusHandler = bonusHandler;
		this.handledBonuses = new ArrayList<>();
		this.cityTokenResponsesNeeded = 0;
		this.faceUpPermitResponsesNeeded = 0;
		this.ownedPermitResponsesNeeded = 0;
	}

	/**
	 * This method handles a list of bonuses giving them to the {@link Player}.
	 * The "model" bonuses ASSISTANT, COIN, VICTORY_POINT, POLITIC and NOBILITY
	 * are collected and passed to the correct method of {@link Game}, the
	 * others are directly handled using {@link #bonusHandler}
	 * 
	 * @param bonuses
	 *            the list of {@link Bonus}es to handle.
	 */
	protected void handleBonuses(List<Bonus> bonuses) {
		/*
		 * I need to create a list of the bonuses that can be directly handled
		 * by the model, during the handling process of this bonuses new bonuses
		 * may be obtained. The whole process must be recursively iterated.
		 */
		List<Bonus> directlyHandleables = new ArrayList<>();
		for (Bonus bonus : bonuses) {
			switch (bonus.getBonusType()) {

			case MAIN_ACTION:
				bonusHandler.addNMainActions(bonus.getNumberOfTimes());
				this.handledBonuses.add(bonus);
				break;

			case PERMIT_BONUS:
				bonusHandler.askForOwnedPermitResponseWaitingState(bonus.getNumberOfTimes());
				this.ownedPermitResponsesNeeded += bonus.getNumberOfTimes();
				this.handledBonuses.add(bonus);
				break;
			case TAKE_PERMIT:
				bonusHandler.askForFaceUpPermitResponseWaitingState(bonus.getNumberOfTimes());
				this.faceUpPermitResponsesNeeded += bonus.getNumberOfTimes();
				this.handledBonuses.add(bonus);
				break;
			case CITY_REWARD:
				bonusHandler.askForCityTokenResponseWaitingState(bonus.getNumberOfTimes());
				this.cityTokenResponsesNeeded += bonus.getNumberOfTimes();
				this.handledBonuses.add(bonus);
				break;

			default:
				directlyHandleables.add(bonus);
				break;
			}
		}

		List<Bonus> bonusesToBeHandled = game.obtainBonuses(player, directlyHandleables);
		this.handledBonuses.addAll(directlyHandleables);
		if (!bonusesToBeHandled.isEmpty())
			handleBonuses(bonusesToBeHandled);
	}

	/**
	 * Get a human readable description of all the handled bonuses
	 * 
	 * @return a String describing all the handled bonuses.
	 */
	protected String getHandledBonusesDescriptions() {
		String handledBonusDescr = "";
		for (int i = 0; i < this.handledBonuses.size(); i++) {
			Bonus bonus = this.handledBonuses.get(i);
			handledBonusDescr += "\nHas obtained a bonus of type " + bonus.getNumberOfTimes() + " "
					+ bonus.getBonusType().toString().toLowerCase();
			/*if (i < this.handledBonuses.size() - 1)
				handledBonusDescr += "\n";*/
		}
		return handledBonusDescr;
	}

	@Override
	public ActionToken getActionToken() {
		return ActionToken.MAIN;
	}

	@Override
	public ActionExecutedMessage getActionExecutedMessage() {
		ActionExecutedMessage msg = new ActionExecutedMessage(this.actionDescription, this.cityTokenResponsesNeeded,
				this.faceUpPermitResponsesNeeded, this.ownedPermitResponsesNeeded);
		return msg;
	}

}
