package it.polimi.ingsw.lm_belloni_cof.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.exceptions.EmptyDeckException;
import it.polimi.ingsw.lm_belloni_cof.messages.GameUpdateMessage;
import it.polimi.ingsw.lm_belloni_cof.model.Assistant;
import it.polimi.ingsw.lm_belloni_cof.model.Bonus;
import it.polimi.ingsw.lm_belloni_cof.model.City;
import it.polimi.ingsw.lm_belloni_cof.model.CityColor;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.KingRewardTile;
import it.polimi.ingsw.lm_belloni_cof.model.MarketOffer;
import it.polimi.ingsw.lm_belloni_cof.model.Permit;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Politic;
import it.polimi.ingsw.lm_belloni_cof.model.PoliticColor;
import it.polimi.ingsw.lm_belloni_cof.model.Region;

public class GameUpdateMessageFactory {
	private static final Logger logger = Logger.getLogger(GameUpdateMessageFactory.class.getName());

	/** This class cannot be instantiated */
	private GameUpdateMessageFactory() {
	}

	/**
	 * Static method called to get a {@link GameUpdateMessage} after that an
	 * action has been executed.
	 * 
	 * @param game
	 *            the {@link Game} of which an update has to be sent
	 * @param actionPerformed
	 *            the {@link Action} just performed
	 * @return a well formed {@link GameUpdateMessage}.
	 */
	public static GameUpdateMessage getGameUpdateMessage(Game game, Action actionPerformed) {
		GameUpdateMessage message = new GameUpdateMessage(getGameStatus(game).toString(),
				actionPerformed.getActionDescription());
		return message;
	}

	/**
	 * Static method called to get a {@link GameUpdateMessage} after with a
	 * string description in it.
	 * 
	 * @param game
	 *            the {@link Game} of which an update has to be sent
	 * @param description
	 *            a String describing the update sent.
	 * @return a well formed {@link GameUpdateMessage}.
	 */
	public static GameUpdateMessage getGameUpdateMessage(Game game, String description) {
		GameUpdateMessage message = new GameUpdateMessage(getGameStatus(game).toString(), description);
		return message;
	}

	protected static JSONObject getGameStatus(Game game) {
		JSONObject status = new JSONObject();
		/* turn section */
		JSONObject turn = new JSONObject();
		turn.put("username", game.getCurrentPlayer().getUsername());
		turn.put("marketoffer", game.inMarketOffer());
		turn.put("marketbuy", game.inMarketBuy());
		status.put("turn", turn);
		/* players section */
		JSONArray players = new JSONArray();
		for (String username : game.getPlayerUsernames()) {
			Player p = game.getPlayerByUsername(username);
			JSONObject player = new JSONObject();
			player.put("username", username);
			player.put("victoryPoints", p.getVictoryPoints());
			player.put("coins", p.getCoins());
			player.put("nobilityTrackPosition", p.getNobilityTrackPosition());
			player.put("emporiums", p.getNumberOfEmporiums());
			player.put("assistants", p.getNumberOfAssistants());
			JSONArray permits = new JSONArray();
			for (Permit permit : p.getPermitsAcquired()) {
				JSONObject obj = new JSONObject();
				/* permit cities */
				JSONArray cities = new JSONArray();
				for (City city : permit.getCities())
					cities.put(city.getName().toLowerCase());
				obj.put("cities", cities);
				/* permit bonuses */
				JSONArray bonuses = new JSONArray();
				for (Bonus bonus : permit.getBonuses()) {
					JSONObject bonusObj = new JSONObject();
					bonusObj.put("bonus", bonus.getBonusType().toString().toLowerCase());
					bonusObj.put("times", bonus.getNumberOfTimes());
					bonuses.put(bonusObj);
				}
				obj.put("bonuses", bonuses);
				obj.put("used", permit.hasBeenUsed());
				permits.put(obj);
			}
			player.put("permits", permits);
			/* OFFERS */
			if (game.inMarketOffer() || game.inMarketBuy()) {
				List<MarketOffer> offers = game.getOffersByPlayer(p);
				JSONArray offered = new JSONArray();
				for (MarketOffer offer : offers) {
					JSONObject obj = new JSONObject();
					if (offer.getObjectForSale() instanceof Assistant) {
						obj.put("forSale", "assistant");
					} else if (offer.getObjectForSale() instanceof Politic) {
						obj.put("forSale", "politic "
								+ ((Politic) offer.getObjectForSale()).getPoliticColor().toString().toLowerCase());
					} else if (offer.getObjectForSale() instanceof Permit) {
						Permit permit = (Permit) offer.getObjectForSale();
						JSONObject permitObj = new JSONObject();
						/* permit cities */
						JSONArray cities = new JSONArray();
						for (City city : permit.getCities())
							cities.put(city.getName().toLowerCase());
						permitObj.put("cities", cities);
						/* permit bonuses */
						JSONArray bonuses = new JSONArray();
						for (Bonus bonus : permit.getBonuses()) {
							JSONObject bonuspermitObj = new JSONObject();
							bonuspermitObj.put("bonus", bonus.getBonusType().toString().toLowerCase());
							bonuspermitObj.put("times", bonus.getNumberOfTimes());
							bonuses.put(bonuspermitObj);
						}
						permitObj.put("bonuses", bonuses);
						permitObj.put("used", permit.hasBeenUsed());
						obj.put("forSale", permitObj);
					}
					obj.put("price", offer.getPrice());
					offered.put(obj);
				}
				player.put("offered", offered);
			}
			players.put(player);
		}
		status.put("players", players);
		/* king's location section */
		status.put("kingsLocation", game.getKingsLocation().getName().toLowerCase());
		/* cities section */
		JSONArray cities = new JSONArray();
		for (City city : game.getCityList()) {
			JSONObject obj = new JSONObject();
			obj.put("city", city.getName().toLowerCase());
			JSONArray playersWithEmp = new JSONArray();
			for (Player player : game.getPlayersWithAnEmporiumOn(city)) {
				playersWithEmp.put(player.getUsername());
			}
			obj.put("emporiums", playersWithEmp);
			cities.put(obj);
		}
		status.put("cities", cities);
		/* regionRewardTilesUsed section */
		JSONArray regionRewardTilesUsed = new JSONArray();
		for (Region region : Region.values()) {
			if (game.hasRegionRewardTileAlreadyBeenUsed(region))
				regionRewardTilesUsed.put(region.toString().toLowerCase());
		}
		status.put("regionRewardTilesUsed", regionRewardTilesUsed);
		/* colorRewardTilesUsed section */
		JSONArray colorRewardTilesUsed = new JSONArray();
		for (CityColor color : CityColor.values()) {
			if (game.hasColorRewardTileAlreadyBeenUsed(color))
				colorRewardTilesUsed.put(color.toString().toLowerCase());
		}
		status.put("colorRewardTilesUsed", colorRewardTilesUsed);
		/* regions section */
		JSONArray regions = new JSONArray();
		for (Region region : Region.values()) {
			JSONObject obj = new JSONObject();
			obj.put("region", region.toString().toLowerCase());
			JSONArray council = new JSONArray();
			List<PoliticColor> councillors = game.getCouncilByRegion(region);
			for (PoliticColor color : councillors)
				council.put(color.toString().toLowerCase());
			obj.put("council", council);
			if (region != Region.KING) {
				JSONArray permits = new JSONArray();
				List<Permit> faceups = game.showFaceUpPermitTiles(region);
				for (Permit permit : faceups) {
					JSONObject permitObj = new JSONObject();
					/* permit cities */
					JSONArray permitCities = new JSONArray();
					for (City city : permit.getCities())
						permitCities.put(city.getName().toLowerCase());
					permitObj.put("cities", permitCities);
					/* permit bonuses */
					JSONArray bonuses = new JSONArray();
					for (Bonus bonus : permit.getBonuses()) {
						JSONObject bonusObj = new JSONObject();
						bonusObj.put("bonus", bonus.getBonusType().toString().toLowerCase());
						bonusObj.put("times", bonus.getNumberOfTimes());
						bonuses.put(bonusObj);
					}
					permitObj.put("bonuses", bonuses);
					permits.put(permitObj);
				}
				obj.put("permits", permits);
			}
			regions.put(obj);
		}
		status.put("regions", regions);
		/* kingRewardTopTile section */
		JSONObject kingRewardTopTile = new JSONObject();
		JSONArray kingRewardTopTileBonuses = new JSONArray();
		try {
			KingRewardTile tile = game.peekKingRewardTopTile();
			for (Bonus bonus : tile.getBonuses()) {
				JSONObject bonusObj = new JSONObject();
				bonusObj.put("bonus", bonus.getBonusType().toString().toLowerCase());
				bonusObj.put("times", bonus.getNumberOfTimes());
				kingRewardTopTileBonuses.put(bonusObj);
			}
			kingRewardTopTile.put("pickingOrder", tile.getPickingOrder());
			kingRewardTopTile.put("bonuses", kingRewardTopTileBonuses);
			status.put("kingRewardTopTile", kingRewardTopTile);
		} catch (EmptyDeckException e) {
			logger.log(Level.INFO, "King pile is empty.", e);
			status.put("kingRewardTopTile", kingRewardTopTile); // empty
		}
		/* councillors */
		JSONArray councillors = new JSONArray();
		for (PoliticColor color : game.getAvailableCouncillorColors())
			councillors.put(color.toString().toLowerCase());
		status.put("councillors", councillors);
		return status;
	}
}
