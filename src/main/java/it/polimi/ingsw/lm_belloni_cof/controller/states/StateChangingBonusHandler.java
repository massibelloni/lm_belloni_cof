package it.polimi.ingsw.lm_belloni_cof.controller.states;

/**
 * Interface that allows the user to modify the {@link PlayerState} of the
 * current player adding or removing tokens about bonuses that change the
 * "regular" game flow: MAIN_ACTION,TAKE_PERMIT, CITY_REWARD, PERMIT_BONUS
 */
public interface StateChangingBonusHandler {
	/** Add n main actions to the current player. */
	public void addNMainActions(int numberOfMainActions);

	/**
	 * Put the current player in a state in which an answer is waited about an
	 * already owned permit card.
	 */
	public void askForOwnedPermitResponseWaitingState(int numberOfPermitsWaited);

	/**
	 * Put the current player in a state in which an answer is waited abouta
	 * face up permit tile.
	 */
	public void askForFaceUpPermitResponseWaitingState(int numberOfFaceUpsWaited);

	/**
	 * Put the current player in a state in which an answer is waited about a
	 * city on which it already has an emporium.
	 */
	public void askForCityTokenResponseWaitingState(int numberOfCitiesWaited);

}
