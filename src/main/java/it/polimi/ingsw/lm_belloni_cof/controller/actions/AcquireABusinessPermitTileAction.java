package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.controller.states.StateChangingBonusHandler;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.CouncilUnsatisfiedException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidActionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughCoinsException;
import it.polimi.ingsw.lm_belloni_cof.model.Bonus;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Politic;
import it.polimi.ingsw.lm_belloni_cof.model.Region;

/**
 * The {@link MainAction} representing the Council of Four
 * "Acquire a business permit tile" main action
 */
public class AcquireABusinessPermitTileAction extends MainAction {
	private static final Logger logger = Logger.getLogger(AcquireABusinessPermitTileAction.class.getName());
	/**
	 * The {@link Region} of the {@link Permit} that the {@link Player} wants to
	 * acquire.
	 */
	private Region region;
	/**
	 * The index on the permit board of the {@link Permit}: it may be 1 or 2.
	 * 
	 * @see it.polimi.ingsw.lm_belloni_cof.model.PermitBoard
	 **/
	private int indexInRegion;
	/**
	 * The list of politic cards that the player intends to use to satisfy the
	 * council of the region.
	 */
	private List<Politic> politics;

	/**
	 * Public constructor for the action
	 * 
	 * @param game
	 *            the {@link Game} on which the action will generate changes.
	 * @param player
	 *            the {@link Player} responsible for the changes on the
	 *            {@link Game}.
	 * @param region
	 *            the {@link Region} of the {@link Permit} that the
	 *            {@link Player} wants to acquire.
	 * @param indexInRegion
	 *            the index on the permit board of the {@link Permit}: it may be
	 *            1 or 2.
	 * @param politics
	 *            the list of politic cards that the player intends to use to
	 *            satisfy the council of the region.
	 * @param bonusHandler
	 *            a {@link StateChangingBonusHandler} instance to edit the
	 *            {@link PlayerState} of the {@link Player}.
	 */
	public AcquireABusinessPermitTileAction(Game game, Player player, Region region, int indexInRegion,
			List<Politic> politics, StateChangingBonusHandler bonusHandler) {
		super(game, player, bonusHandler);
		this.indexInRegion = indexInRegion;
		this.region = region;
		this.politics = politics;
	}

	/**
	 * Calls the
	 * {@link it.polimi.ingsw.lm_belloni_cof.model.Game#acquireAPermitTile(Player,Region,int,List)}
	 * of the model and handles the bonuses obtained. Then edits the
	 * {@link Action#actionDescription} field.
	 * 
	 * @see MainAction#handleBonuses
	 * @return true if the player has built all its emporiums, false otherwise
	 * @throws ActionExecutionException
	 *             when the requested action hasn't been completely executed due
	 *             to a {@link NotEnoughCoinsException} or a {@link CouncilUnsatisfiedException}.
	 */
	@Override
	public boolean execute() throws ActionExecutionException {
		try {
			List<Bonus> bonuses = game.acquireAPermitTile(super.player, region, indexInRegion, politics);
			super.actionDescription += player.getUsername()
					+ " has acquired a permit tile satisfying the council in the region of "
					+ region.toString().toLowerCase()+".";
			super.handleBonuses(bonuses);
			super.actionDescription += super.getHandledBonusesDescriptions();
			super.setExecuted();
		} catch (NotEnoughCoinsException e) {
			throw new ActionExecutionException("Not enough coins to complete this action.", e);
		} catch (CouncilUnsatisfiedException e) {
			throw new ActionExecutionException("The council isn't satisfied.", e);
		} catch (InvalidActionException e) {
			logger.log(Level.SEVERE, "The MessageTranslator should avoid this.", e);
		}
		return false;
	}

}
