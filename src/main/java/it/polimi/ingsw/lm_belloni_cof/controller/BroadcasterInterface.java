package it.polimi.ingsw.lm_belloni_cof.controller;

import java.util.List;

import it.polimi.ingsw.lm_belloni_cof.messages.BroadcastMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;

public interface BroadcasterInterface {
	/**
	 * Broadcast a message to all the clients connected to a game.
	 * 
	 * @param msg
	 *            the message to broadcast
	 * @param gameId
	 *            the game of which subscribers have to receive the message
	 */
	public void broadcast(BroadcastMessage msg, String gameId);

	/**
	 * Broadcast a message to a selected list of clients
	 * 
	 * @param msg
	 *            the message to broadcast
	 * @param tokens
	 *            a {@link List} of tokens that will receive the broadcast
	 *            message.
	 */
	public void broadcast(BroadcastMessage msg, List<PlayerToken> tokens);

	/** Add a new game to the list of known game. */
	public void addGame(String gameId);

	/**
	 * Subscribe a player to the updates of a game.
	 */
	public void subscribeTokenToGame(PlayerToken playerToken, String gameId);

	/** Unsubscribe a player from all the updates. */
	public void removeGamesFromToken(PlayerToken playerToken);

	/**
	 * Unsubscribe a player from the updates of the game passed as an argument.
	 */
	public void removeGameFromToken(PlayerToken playerToken, String gameId);

}
