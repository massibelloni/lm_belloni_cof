package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.RewardToken;
import it.polimi.ingsw.lm_belloni_cof.model.Bonus;
import it.polimi.ingsw.lm_belloni_cof.model.BonusType;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.controller.states.ActionToken;
import it.polimi.ingsw.lm_belloni_cof.controller.states.StateChangingBonusHandler;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.model.City;

/**
 * The {@link Action} generated (and executed) to specify a {@link City} from
 * which obtain the {@link Bonus}es on its {@link RewardTile}s. Following the
 * rules the cities specified must have a {@link Player}'s emporium on them.
 *
 */
public class CityTokenResponseAction extends BonusResponseAction {
	private static final Logger logger = Logger.getLogger(CityTokenResponseAction.class.getName());
	/**
	 * The city of which the player wants to obtain the bonuses on its tokens.
	 */
	private City city;

	/**
	 * Public constructor of the action.
	 * 
	 * @param game
	 *            the {@link Game} on which the action will generate changes.
	 * @param player
	 *            the {@link Player} responsible for the changes on the
	 *            {@link Game}.
	 * @param city
	 *            the city of which the player wants to obtain the bonuses on
	 *            its tokens.
	 * @param bonusHandler
	 *            a {@link StateChangingBonusHandler} instance to edit the
	 *            {@link PlayerState} of the {@link Player}.
	 */
	public CityTokenResponseAction(Game game, Player player, City city, StateChangingBonusHandler bonusHandler) {
		super(game, player, bonusHandler);
		this.city = city;
	}

	/**
	 * Obtain and handle the {@link Bonus}es on {@link RewardToken}s and change
	 * the {@link PlayerState} of the {@link Player}.
	 * 
	 * @throws ActionExecutionException
	 *             when has been chosen a token that advances the player on the
	 *             nobility track.
	 * 
	 * @see MainAction#handleBonuses
	 */
	@Override
	public boolean execute() throws ActionExecutionException {
		List<Bonus> bonusesToBeHandled = new ArrayList<>();
		if (game.getPlayersWithAnEmporiumOn(city).contains(player)) {
			for (RewardToken rewardTokenOnCity : game.getRewardTokensOn(city)) {
				bonusesToBeHandled.addAll(rewardTokenOnCity.getBonuses());
			}
			// i've to check that none of the bonuses advance the player on the
			// nobility track
			boolean nobilityAdvance = false;
			for (Bonus bonus : bonusesToBeHandled)
				if (bonus.getBonusType() == BonusType.NOBILITY) {
					nobilityAdvance = true;
					break;
				}
			if (nobilityAdvance)
				throw new ActionExecutionException(
						"You cannot choose a token which advance you on the nobility track.");
			handleBonuses(bonusesToBeHandled);
			super.actionDescription += player.getUsername() + " has gained bonuses from city " + city.getName()
					+ " where he already had an emporium.";
			super.actionDescription += super.getHandledBonusesDescriptions();
			super.cityTokenResponsesNeeded-=1;
			super.setExecuted();
		} else {
			logger.log(Level.SEVERE, "The MessageTranslator should avoid this.");
		}
		return false;
	}

	@Override
	public ActionToken getActionToken() {
		return ActionToken.CITY_RESPONSE;
	}

}
