package it.polimi.ingsw.lm_belloni_cof.controller;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.AcquireABusinessPermitTileAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.BuildAnEmporiumUsingAPermitTileAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.BuildAnEmporiumWithTheHelpOfTheKingAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.ChangeBusinessPermitTilesAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.CityTokenResponseAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.DrawAPoliticAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.ElectACouncillorAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.EmptyQuickAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.EndMarketBuyAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.EndMarketOfferAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.EngageAnAssistantAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.FaceUpPermitResponseAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.MarketBuyAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.MarketOfferAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.OwnedPermitResponseAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.PerformAnAdditionalMainActionAction;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.SendAnAssistantToElectACouncillorAction;
import it.polimi.ingsw.lm_belloni_cof.controller.states.StateChangingBonusHandler;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidActionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidMessageException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughAssistantsException;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.*;
import it.polimi.ingsw.lm_belloni_cof.model.City;
import it.polimi.ingsw.lm_belloni_cof.model.Councillor;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.MarketOffer;
import it.polimi.ingsw.lm_belloni_cof.model.Ownable;
import it.polimi.ingsw.lm_belloni_cof.model.Permit;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Politic;
import it.polimi.ingsw.lm_belloni_cof.model.PoliticColor;
import it.polimi.ingsw.lm_belloni_cof.model.Region;

/**
 * Unique implementation of the {@link MessageToAction} that generates
 * {@link Action}s based on parameters passed as field in
 * {@ActionRequestMessage}s.
 */

public class MessageTranslatorImpl implements MessageTranslator {
	/** The {@link Game} where the actions generated will be executed. */
	private Game game;
	/**
	 * The {@link StateChangingBonusHandler} passed to some actions that will
	 * need to update the {@link PlayerState} of the player during action
	 * execution.
	 */
	private StateChangingBonusHandler stateChangingBonusHandler;

	public MessageTranslatorImpl(Game game, StateChangingBonusHandler stateChangingBonusHandler) {
		this.game = game;
		this.stateChangingBonusHandler = stateChangingBonusHandler;
	}

	@Override
	public Action translate(AcquireABusinessPermitTileActRqstMsg message) throws InvalidMessageException {
		Player player = getPlayerByUsername(message.getSender().getUsername());
		if (message.getIndexInRegion() != 1 && message.getIndexInRegion() != 2)
			throw new InvalidMessageException("indexInRegion must be 1 or 2.");
		Region region = getRegionByRegionName(message.getRegion());
		// parsing politics
		if (message.getPoliticColors().isEmpty() || message.getPoliticColors().size() > 4)
			throw new InvalidMessageException("You must specify from 1 to 4 politics.");
		List<Politic> politicsToUse = this.getPoliticsToUseByColorStrings(player, message.getPoliticColors());
		AcquireABusinessPermitTileAction action = new AcquireABusinessPermitTileAction(this.game, player, region,
				message.getIndexInRegion(), politicsToUse, this.stateChangingBonusHandler);
		return action;
	}

	@Override
	public Action translate(BuildAnEmporiumUsingAPermitTileActRqstMsg message) throws InvalidMessageException {
		Player player = getPlayerByUsername(message.getSender().getUsername());
		City city = this.getCityByCityName(message.getCity());
		Permit permit;
		try {
			permit = player.getPermitsAcquired().get(Integer.parseInt(message.getPermit()) - 1);
		} catch (Exception e) {
			throw new InvalidMessageException(message.getPermit() + " is not a correct permit.", e);
		}
		if (permit == null)
			throw new InvalidMessageException(message.getPermit() + " is not a correct permit.");
		if (!permit.getCities().contains(city))
			throw new InvalidMessageException(message.getPermit() + " doesn't contain " + message.getCity());
		BuildAnEmporiumUsingAPermitTileAction action = new BuildAnEmporiumUsingAPermitTileAction(this.game, player,
				city, permit, this.stateChangingBonusHandler);
		return action;
	}

	@Override
	public Action translate(BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg message) throws InvalidMessageException {
		Player player = getPlayerByUsername(message.getSender().getUsername());
		City city = this.getCityByCityName(message.getCity());
		List<Politic> politicsToUse = this.getPoliticsToUseByColorStrings(player, message.getPoliticColors());
		BuildAnEmporiumWithTheHelpOfTheKingAction action = new BuildAnEmporiumWithTheHelpOfTheKingAction(this.game,
				player, city, politicsToUse, this.stateChangingBonusHandler);
		return action;
	}

	@Override
	public Action translate(ChangeBusinessPermitTilesActRqstMsg message) throws InvalidMessageException {
		Player player = getPlayerByUsername(message.getSender().getUsername());
		Region region = getRegionByRegionName(message.getRegion());
		if (region == Region.KING)
			throw new InvalidMessageException("King region has no permit tiles on it.");
		ChangeBusinessPermitTilesAction action = new ChangeBusinessPermitTilesAction(this.game, player, region);
		return action;
	}

	@Override
	public Action translate(CityTokenResponseActRqstMsg message) throws InvalidMessageException {
		Player player = getPlayerByUsername(message.getSender().getUsername());
		City city = this.getCityByCityName(message.getCity());
		if (!game.getPlayersWithAnEmporiumOn(city).contains(player))
			throw new InvalidMessageException(
					"You must have an emporium on " + message.getCity() + " in order to collect the bonuses on it.");
		CityTokenResponseAction action = new CityTokenResponseAction(this.game, player, city,
				this.stateChangingBonusHandler);
		return action;
	}

	@Override
	public Action translate(DrawAPoliticActRqstMsg message) throws InvalidMessageException {
		Player player = getPlayerByUsername(message.getSender().getUsername());
		return new DrawAPoliticAction(this.game, player);
	}

	@Override
	public Action translate(ElectACouncillorActRqstMsg message) throws InvalidMessageException {
		Player player = getPlayerByUsername(message.getSender().getUsername());
		Region region = getRegionByRegionName(message.getRegion());
		Councillor councillor = this.getCouncillorByColorString(message.getCouncillorColor());
		ElectACouncillorAction action = new ElectACouncillorAction(this.game, player, region, councillor);
		return action;
	}

	@Override
	public Action translate(EmptyQuickActionActRqstMsg message) throws InvalidMessageException {
		Player player = getPlayerByUsername(message.getSender().getUsername());
		EmptyQuickAction action = new EmptyQuickAction(this.game, player);
		return action;
	}

	@Override
	public Action translate(EndMarketBuyActRqstMsg message) throws InvalidMessageException {
		Player player = getPlayerByUsername(message.getSender().getUsername());
		EndMarketBuyAction action = new EndMarketBuyAction(this.game, player);
		return action;
	}

	@Override
	public Action translate(EndMarketOfferActRqstMsg message) throws InvalidMessageException {
		Player player = getPlayerByUsername(message.getSender().getUsername());
		EndMarketOfferAction action = new EndMarketOfferAction(this.game, player);
		return action;
	}

	@Override
	public Action translate(EngageAnAssistantActRqstMsg message) throws InvalidMessageException {
		Player player = getPlayerByUsername(message.getSender().getUsername());
		EngageAnAssistantAction action = new EngageAnAssistantAction(this.game, player);
		return action;
	}

	@Override
	public Action translate(FaceUpPermitResponseActRqstMsg message) throws InvalidMessageException {
		Player player = getPlayerByUsername(message.getSender().getUsername());
		if (message.getIndexInRegion() != 1 && message.getIndexInRegion() != 2)
			throw new InvalidMessageException("indexInRegion must be 1 or 2.");
		Region region = getRegionByRegionName(message.getRegion());
		FaceUpPermitResponseAction action = new FaceUpPermitResponseAction(this.game, player, region,
				message.getIndexInRegion(), this.stateChangingBonusHandler);
		return action;
	}

	@Override
	public Action translate(MarketBuyActRqstMsg message) throws InvalidMessageException {
		Player player = getPlayerByUsername(message.getSender().getUsername());
		Player seller = this.game.getPlayerByUsername(message.getSeller());
		if (seller == null)
			throw new InvalidMessageException(message.getSeller() + " is not a correct username.");
		if (player == seller)
			throw new InvalidMessageException("You can't buy your own objects.");
		List<MarketOffer> offersBySeller = this.game.getOffersByPlayer(seller);
		MarketOffer chosen = null;
		try {
			chosen = offersBySeller.get(message.getIndexInSellerOffers() - 1);
		} catch (IndexOutOfBoundsException e) {
			throw new InvalidMessageException(message.getSeller() + " hasn't offered such object.", e);
		}
		MarketBuyAction action = new MarketBuyAction(this.game, player, seller, chosen.getObjectForSale());
		return action;
	}

	@Override
	/* Syntax permit #,assistant,politic color */
	public Action translate(MarketOfferActRqstMsg message) throws InvalidMessageException {
		Player player = getPlayerByUsername(message.getSender().getUsername());
		if (message.getPrice() < 0)
			throw new InvalidMessageException("Price must be positive.");
		// checking the object
		String forSaleString = message.getForSale();
		Ownable forSale;
		if ("assistant".equals(forSaleString)) {
			try {
				forSale = player.getAnAssistant();
			} catch (NotEnoughAssistantsException e) {
				throw new InvalidMessageException("You don't have enough assistants.", e);
			}
		} else if (forSaleString.contains("politic")) {
			String colorString = null;
			try {
				colorString = forSaleString.split("\\s")[1];
			} catch (IndexOutOfBoundsException e) {
				throw new InvalidMessageException(forSaleString + " is not a well formed politic card.", e);
			}
			if (colorString == null)
				throw new InvalidMessageException(forSaleString + " is not a well formed politic card.");
			PoliticColor color = null;
			try {
				color = PoliticColor.valueOf(colorString.toUpperCase());
			} catch (IllegalArgumentException e) {
				throw new InvalidMessageException(colorString + " is not a known color.", e);
			}
			Politic politicForSale = null;
			for (Politic politic : player.getPoliticsHand())
				if (politic.getPoliticColor() == color) {
					politicForSale = politic;
					break;
				}
			if (politicForSale == null)
				throw new InvalidMessageException("A " + colorString + " politic is not available in your hand.");
			player.getPoliticsHand().remove(politicForSale);
			forSale = politicForSale;
		} else if (forSaleString.contains("permit")) {
			int indexInPermit = -1;
			try {
				indexInPermit = Integer.parseInt(forSaleString.split("\\s")[1]);
			} catch (Exception e) {
				throw new InvalidMessageException(forSaleString + " is not a well formed permit.", e);
			}
			Permit permit; 
			try{
				permit = player.getPermitsAcquired().get(indexInPermit - 1);
			} catch(Exception e){
				throw new InvalidMessageException(indexInPermit + " is not available in your hand.",e);
			}	
			/*
			 * if (permit.hasBeenUsed()) throw new InvalidMessageException(
			 * "You can't sell an already used permit.");
			 */
			player.getPermitsAcquired().remove(permit);
			forSale = permit;
		} else
			throw new InvalidMessageException(forSaleString + " is not a correct object.");
		MarketOfferAction action = new MarketOfferAction(this.game, player,
				new MarketOffer(forSale, message.getPrice()));
		return action;
	}

	@Override
	public Action translate(OwnedPermitResponseActRqstMsg message) throws InvalidMessageException {
		Player player = getPlayerByUsername(message.getSender().getUsername());
		Permit permit;
		try {
			permit = player.getPermitsAcquired().get(Integer.parseInt(message.getPermit()) - 1);
		} catch (Exception e) {
			throw new InvalidMessageException(message.getPermit() + " is not a correct permit.", e);
		}
		if (permit == null)
			throw new InvalidMessageException(message.getPermit() + " is not a correct permit.");
		/*
		 * if (!permit.hasBeenUsed()) throw new
		 * InvalidMessageException(message.getPermit() +
		 * " hasn't been used yet.");
		 */
		OwnedPermitResponseAction action = new OwnedPermitResponseAction(this.game, player, permit,
				this.stateChangingBonusHandler);
		return action;
	}

	@Override
	public Action translate(PerformAnAdditionalMainActionActRqstMsg message) throws InvalidMessageException {
		Player player = getPlayerByUsername(message.getSender().getUsername());
		PerformAnAdditionalMainActionAction action = new PerformAnAdditionalMainActionAction(this.game, player,
				this.stateChangingBonusHandler);
		return action;
	}

	@Override
	public Action translate(SendAnAssistantToElectACouncillorActRqstMsg message) throws InvalidMessageException {
		Player player = getPlayerByUsername(message.getSender().getUsername());
		Region region = getRegionByRegionName(message.getRegion());
		Councillor councillor = this.getCouncillorByColorString(message.getCouncillorColor().toUpperCase());
		SendAnAssistantToElectACouncillorAction action = new SendAnAssistantToElectACouncillorAction(this.game, player,
				councillor, region);
		return action;
	}

	private City getCityByCityName(String name) throws InvalidMessageException {
		List<City> cities = this.game.getCityList();
		for (City city : cities)
			if (city.getName().equalsIgnoreCase(name))
				return city;
		throw new InvalidMessageException(name + " is not a known city.");
	}

	private List<Politic> getPoliticsToUseByColorStrings(Player player, List<String> colorStrings)
			throws InvalidMessageException {
		List<Politic> politicsToUse = new ArrayList<>();
		List<PoliticColor> colorsToUse = new ArrayList<>();
		for (String color : colorStrings)
			try {
				colorsToUse.add(PoliticColor.valueOf(color.toUpperCase()));
			} catch (IllegalArgumentException e) {
				throw new InvalidMessageException(color + " is not a known color.", e);
			}
		List<Politic> handCopy = new ArrayList<>(player.getPoliticsHand());
		for (PoliticColor colorToUse : colorsToUse) {
			Politic politicToUse = null;
			for (Politic politic : handCopy)
				if (politic.getPoliticColor() == colorToUse) {
					politicToUse = politic;
					break;
				}
			if (politicToUse != null) {
				politicsToUse.add(politicToUse);
				handCopy.remove(politicToUse);
			} else {
				throw new InvalidMessageException("A " + colorToUse + " politic is not available in your hand.");
			}
		}
		return politicsToUse;
	}

	private Councillor getCouncillorByColorString(String colorString) throws InvalidMessageException {
		PoliticColor color = null;
		try {
			color = PoliticColor.valueOf(colorString.toUpperCase());
		} catch (IllegalArgumentException e) {
			throw new InvalidMessageException(colorString + " is not a known color.", e);
		}
		Councillor councillor = null;
		try {
			councillor = game.getACouncillorByPoliticColor(color);
		} catch (InvalidActionException e) {
			throw new InvalidMessageException("A " + color + " councillor is not available.", e);
		}
		return councillor;
	}

	private Player getPlayerByUsername(String username) throws InvalidMessageException {
		Player player = this.game.getPlayerByUsername(username);
		if (player == null)
			throw new InvalidMessageException(username + " is not a correct username.");
		return player;

	}
	
	private Region getRegionByRegionName(String name) throws InvalidMessageException{
		Region region;
		try {
			region = Region.valueOf(name.toUpperCase());
			return region;
		} catch (IllegalArgumentException e) {
			throw new InvalidMessageException(name + " is not a known region.", e);
		}
	}

}
