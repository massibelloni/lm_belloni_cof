package it.polimi.ingsw.lm_belloni_cof.controller;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.SortedMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.messages.ConnectionRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ConnectionResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.InvalidRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayersTableRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayersTableResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.RequestMessage;
import it.polimi.ingsw.lm_belloni_cof.messages.ResponseMessage;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.view.shared.RequestProcessor;

/**
 * This is the object that holds all the ongoing games and handles all the
 * connection requests
 */

public class GamesHandler implements RequestProcessor {
	private static final Logger logger = Logger.getLogger(GamesHandler.class.getName());
	/**
	 * The timeout after that a game starts when it reaches at least two players
	 */
	private static final long WAITING_TIME = 20000;
	/**
	 * The {@link BroadcasterInterface} used to send {@link BroadcastMessage}s.
	 */
	private BroadcasterInterface broadcaster;
	/** The number of games created so far, used for gameId creation. */
	private int numberOfGamesCreated;
	/**
	 * The {@link GameMapFactory} instance used to generate {@link GameMap}s
	 * 
	 * @see GameMapFactory
	 */
	private GameMapFactory gameMapFactory;
	/** A list of all the players registered since the server was started. */
	private List<PlayerData> registeredPlayers;
	/** A list of all the connected players at the moment. */
	private List<PlayerToken> connectedPlayers;
	/**
	 * The game waiting to start.
	 * 
	 * @see GameController
	 * @see #WAITING_TIME
	 */
	private GameController waitingGame;
	/**
	 * The {@link Timer} used to start a game.
	 * 
	 * @see WAITING_TIME
	 */
	private Timer timer;
	/**
	 * A map that links {@link PlayerToken} to the relative
	 * {@link GameController}. This is the object that actually connects players
	 * to game.
	 */
	private Map<PlayerToken, GameController> games;

	/**
	 * Public and only constructor of a new GamesHandler.
	 * 
	 * @param broadcaster
	 *            the {@link BroadcasterInterface} used by this GamesHandler.
	 */
	public GamesHandler(BroadcasterInterface broadcaster) {
		this.broadcaster = broadcaster;
		this.numberOfGamesCreated = 0;
		this.registeredPlayers = new ArrayList<>();
		this.connectedPlayers = new ArrayList<>();
		this.games = new HashMap<>();
		this.gameMapFactory = new GameMapFactory();
		this.timer = new Timer();
	}

	/**
	 * Adds a player to the {@link #waitingGame} starting a new timer if two
	 * players have been reached.
	 * 
	 * @param token
	 *            the {@link PlayerToken} of the new player.
	 * @param playerData
	 *            the {@link PlayerData} of the new player.
	 */
	private void addPlayerToWaitingGame(PlayerToken token, PlayerData playerData) {
		if (this.waitingGame == null)
			this.waitingGame = new GameController("GAME" + (++this.numberOfGamesCreated), gameMapFactory,
					this.broadcaster);
		this.connectedPlayers.add(token);
		this.games.put(token, waitingGame);
		this.waitingGame.addPlayer(token, playerData);
		if (this.waitingGame.getNumberOfPlayers() >= 2) {
			timer.cancel();
			timer = new Timer();
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					waitingGame.startGame();
					waitingGame = null;
				}
			}, GamesHandler.WAITING_TIME);
		}
	}

	/**
	 * Verifies user credentials or creates a new user if no other users with
	 * this username exists, then adds the player to the waiting game. If this
	 * username is related to another game and the game isn't yet finished the
	 * player is reconnected, otherwise is removed from that game and added to
	 * the {@link #waitingGame}.
	 * 
	 * @param username
	 * @param password
	 * @return a {@link ConnectionResponseMessage} with the result of the
	 *         connection attempt.
	 */
	private ConnectionResponseMessage connect(String username, String password) {
		/* check if it is an existing player */
		for (PlayerData playerData : registeredPlayers) {
			if (playerData.getUsername().equals(username)) {
				if (playerData.getPassword().equals(password)) {
					/*
					 * player may be logged in only if it's not already logged
					 */
					for (PlayerToken playerToken : this.connectedPlayers) {
						if (playerToken.getUsername().equals(username)) {
							if (this.games.get(playerToken).isGameFinished()
									|| !this.games.get(playerToken).isGameStarted()) {
								/*
								 * the game it was connected to is finished (or
								 * not even started), creating a fresh token and
								 * adding it to the waiting one
								 */
								// adding it to a new game
								this.games.remove(playerToken);
								this.connectedPlayers.remove(playerToken);
								// no old updates
								this.broadcaster.removeGamesFromToken(playerToken);
								PlayerToken newToken = new PlayerToken(username);
								this.addPlayerToWaitingGame(newToken, playerData);
								return new ConnectionResponseMessage(newToken,
										"You are now logged in as " + username + " in " + this.waitingGame.getGameId());
							} else {
								this.games.get(playerToken).reconnectPlayer(playerToken.getUsername());
								return new ConnectionResponseMessage(playerToken,
										"You have been reconnected to " + this.games.get(playerToken).getGameId());
							}
						}
					}
					PlayerToken token = new PlayerToken(username);
					this.addPlayerToWaitingGame(token, playerData);
					return new ConnectionResponseMessage(token,
							"You are now logged in as " + username + " in " + this.waitingGame.getGameId());
				}
				return new ConnectionResponseMessage(null, "Wrong password for user " + username);
			}
		}
		/* no users with that username, i can create one */
		PlayerData newPlayerData = new PlayerData(username, password);
		this.registeredPlayers.add(newPlayerData);
		PlayerToken token = new PlayerToken(username);
		this.addPlayerToWaitingGame(token, newPlayerData);
		return new ConnectionResponseMessage(token, "You have been registered and you are now logged in as " + username
				+ " in " + this.waitingGame.getGameId());
	}

	/**
	 * Creates a {@link PlayersTableResponseMessage} with the table of the
	 * players based on games played, games won and total minutes played.
	 * 
	 * @return the {@link PlayersTableResponseMessage} created.
	 */
	
	private PlayersTableResponseMessage getPlayersTable() {
		/* generating tables */
		SortedMap<Integer, List<String>> wonUnsortedMap = new TreeMap<>();
		SortedMap<Integer, List<String>> playedUnsortedMap = new TreeMap<>();
		SortedMap<Integer, List<String>> minutesPlayedUnsortedMap = new TreeMap<>();
		/* filling maps */
		for (PlayerData data : this.registeredPlayers) {
			if (wonUnsortedMap.get(data.getGamesWon()) == null)
				wonUnsortedMap.put(data.getGamesWon(), new ArrayList<>());
			wonUnsortedMap.get(data.getGamesWon()).add(data.getUsername());
			if (playedUnsortedMap.get(data.getGamesPlayed()) == null)
				playedUnsortedMap.put(data.getGamesPlayed(), new ArrayList<>());
			playedUnsortedMap.get(data.getGamesPlayed()).add(data.getUsername());
			if (minutesPlayedUnsortedMap.get((int) data.getTotalGameTime() / 1000 / 60) == null)
				minutesPlayedUnsortedMap.put((int) data.getTotalGameTime() / 1000 / 60, new ArrayList<>());
			minutesPlayedUnsortedMap.get((int) data.getTotalGameTime() / 1000 / 60).add(data.getUsername());
		}
		Map<String, Integer> wonMap = new LinkedHashMap<>();
		Map<String, Integer> playedMap = new LinkedHashMap<>();
		Map<String, Integer> minutesPlayedMap = new LinkedHashMap<>();
		/* filling message maps with ordered data */
		int wonAdded = 0;
		int playedAdded = 0;
		int minutesAdded = 0;
		while (wonAdded < registeredPlayers.size() || playedAdded < registeredPlayers.size()
				|| minutesAdded < registeredPlayers.size()) {
			/* won */
			try {
				Integer won = wonUnsortedMap.lastKey();
				List<String> usernamesWon = wonUnsortedMap.get(won);
				for (String username : usernamesWon) {
					wonMap.put(username, won);
					wonAdded++;
				}
				wonUnsortedMap.remove(wonUnsortedMap.lastKey());
			} catch (NoSuchElementException e) {
				logger.log(Level.FINE, "Winner table is finished.", e);
			}
			/* played */
			try {
				Integer played = playedUnsortedMap.lastKey();
				List<String> usernamesPlayed = playedUnsortedMap.get(played);
				for (String username : usernamesPlayed) {
					playedMap.put(username, played);
					playedAdded++;
				}
				playedUnsortedMap.remove(playedUnsortedMap.lastKey());
			} catch (NoSuchElementException e) {
				logger.log(Level.FINE, "Played table is finished.", e);
			}
			/* minutes played */
			try {
				Integer minutes = minutesPlayedUnsortedMap.lastKey();
				List<String> usernamesMinutes = minutesPlayedUnsortedMap.get(minutes);
				for (String username : usernamesMinutes) {
					minutesPlayedMap.put(username, minutes);
					minutesAdded++;
				}
				minutesPlayedUnsortedMap.remove(minutesPlayedUnsortedMap.lastKey());
			} catch (NoSuchElementException e) {
				logger.log(Level.FINE, "Minutes table is finished.", e);
			}
		}
		return new PlayersTableResponseMessage(playedMap, wonMap, minutesPlayedMap);
	}

	/**
	 * Network exposed method that processes all the requests coming from users,
	 * it handles them itself if they're connection requests or passes them to
	 * the correct {@link GameController}.
	 * 
	 * @param RequestMessage
	 *            the message to be processed
	 * @return ResponseMessage the result of the processing
	 */
	@Override
	public synchronized ResponseMessage processRequest(RequestMessage request) throws RemoteException {
		if (request instanceof ConnectionRequestMessage) {
			return this.connect(((ConnectionRequestMessage) request).getUsername(),
					((ConnectionRequestMessage) request).getPassword());
		} else if (request instanceof PlayersTableRequestMessage) {
			return this.getPlayersTable();
		} else {
			if (this.connectedPlayers.contains(request.getSender()))
				return this.games.get(request.getSender()).handleRequest(request);
			else
				return new InvalidRequestMessage("Not recognizable client.");
		}
	}

}
