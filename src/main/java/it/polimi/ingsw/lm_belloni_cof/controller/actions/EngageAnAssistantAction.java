package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughCoinsException;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Player;

/**
 * The {@link QuickAction} representing the Council of Four
 * "Engage an assistant" quick action.
 */
public class EngageAnAssistantAction extends QuickAction {
	/**
	 * Public constructor of the action.
	 * 
	 * @param game
	 *            the {@link Game} on which the action will generate changes.
	 * @param player
	 *            the {@link Player} responsible for the changes on the
	 *            {@link Game}.
	 */
	public EngageAnAssistantAction(Game game, Player player) {
		super(game, player);
	}

	/**
	 * Calls the
	 * {@link it.polimi.ingsw.lm_belloni_cof.model.Game#engageAnAssistant(Player)}
	 * of the model, then edits the {@link Action#actionDescription} field.
	 * 
	 * @return true if the player has built all its emporiums, false otherwise
	 * @throws ActionExecutionException
	 *             when the requested action hasn't been executed due to a
	 *             {@link NotEnoughCoinsException}.
	 */
	@Override
	public boolean execute() throws ActionExecutionException {
		try {
			game.engageAnAssistant(this.player);
			super.actionDescription += player.getUsername() + " has engaged an assistant.";
			super.setExecuted();
		} catch (NotEnoughCoinsException e) {
			throw new ActionExecutionException("Not enough coins to complete this action.", e);
		}
		return false;
	}

}
