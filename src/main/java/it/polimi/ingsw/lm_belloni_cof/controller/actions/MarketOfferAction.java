package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.controller.states.ActionToken;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidActionException;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.MarketOffer;
import it.polimi.ingsw.lm_belloni_cof.model.Ownable;
import it.polimi.ingsw.lm_belloni_cof.model.Player;

/**
 * The {@link MarketAction} representing the intent of a {@link Player} to offer
 * some {@link Ownable}s during a market session.
 * 
 * @see MarketOffer
 */
public class MarketOfferAction extends MarketAction {
	private static final Logger logger = Logger.getLogger(MarketOfferAction.class.getName());
	/** A List of {@link MarketOffer}s for this market session. */
	private MarketOffer offer;

	/**
	 * Default constructor.
	 * 
	 * @see Action#Action(Game,Player)
	 * @param game
	 *            the {@link Game} in which the {@link MarketAction} will be
	 *            executed.
	 * @param player
	 *            the {@link Player} this {@link Action} belongs to.
	 * @param offers
	 *            a {@link List} of {@link MarketOffer}s for this market
	 *            session.
	 */
	public MarketOfferAction(Game game, Player player, MarketOffer offer) {
		super(game, player);
		this.offer = offer;
	}

	/** Add the player offers to the market session. */
	@Override
	public boolean execute() {
		try {
			game.addPlayerOffer(super.player, this.offer);
			super.actionDescription += player.getUsername() + " has offered a ";
			super.actionDescription += offer.getObjectForSale().toString() + " " + offer.getPrice() + " "
					+ ((offer.getPrice() > 1) ? "coins" : "coin");
			super.setExecuted();
		} catch (InvalidActionException e) {
			logger.log(Level.SEVERE, "The MessageTranslator should avoid this.", e);
		}
		return false;
	}

	/**
	 * This is the only {@link Action} instance that removes a MARKET_OFFER of
	 * {@link ActionToken}
	 * 
	 * @return null
	 */
	@Override
	public ActionToken getActionToken() {
		return null;
	}

}
