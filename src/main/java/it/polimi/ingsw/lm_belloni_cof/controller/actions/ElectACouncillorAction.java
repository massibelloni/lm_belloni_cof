package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidActionException;
import it.polimi.ingsw.lm_belloni_cof.model.Councillor;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Region;

/**
 * The {@link MainAction} representing the Council of Four "Elect a councillor"
 * main action.
 */
public class ElectACouncillorAction extends MainAction {
	private static final Logger logger = Logger.getLogger(ElectACouncillorAction.class.getName());
	/** The region of the council that will be modified. */
	private Region region;
	/** The councillor that will be elected. */
	private Councillor councillor;

	/**
	 * Public constructor of this action.
	 * 
	 * @param game
	 *            the {@link Game} on which the action will generate changes.
	 * @param player
	 *            the {@link Player} responsible for the changes on the
	 *            {@link Game}.
	 * @param region
	 *            the region of the council that will be modified.
	 * @param councillor
	 *            the councillor that will be elected.
	 */
	public ElectACouncillorAction(Game game, Player player, Region region, Councillor councillor) {
		// no StateChangingBonusHandler needed
		super(game, player, null);
		this.region = region;
		this.councillor = councillor;
	}

	/**
	 * Calls the
	 * {@link it.polimi.ingsw.lm_belloni_cof.model.Game#electACouncillor(Player, Region, Councillor)}
	 * then edits the {@link Action#actionDescription} field.
	 */
	@Override
	public boolean execute() {
		try {
			game.electACouncillor(player, region, councillor);
			super.actionDescription += player.getUsername() + " has elected a councillor in the region of " + region;
			super.setExecuted();
		} catch (InvalidActionException e) {
			logger.log(Level.SEVERE, "The MessageTranslator should avoid this.", e);
		}
		return false;

	}

}
