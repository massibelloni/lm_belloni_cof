package it.polimi.ingsw.lm_belloni_cof.controller.states;

/**
 * This enumeration describes the tokens that a player may have in its state.
 * Each token represents an action that it has to perform before closing its
 * turn.
 */
public enum ActionToken {
	MAIN, QUICK, DRAW_A_POLITIC, MARKET_OFFER, MARKET_BUY, FACE_UP_PERMIT_RESPONSE, PERMIT_BONUS_RESPONSE, CITY_RESPONSE, GAME_FINISHED
}
