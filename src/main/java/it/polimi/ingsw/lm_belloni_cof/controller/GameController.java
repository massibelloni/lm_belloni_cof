package it.polimi.ingsw.lm_belloni_cof.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.SortedMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.controller.states.StatesManager;
import it.polimi.ingsw.lm_belloni_cof.controller.states.TurnMachine;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.GameFinishedException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidMessageException;
import it.polimi.ingsw.lm_belloni_cof.messages.*;
import it.polimi.ingsw.lm_belloni_cof.messages.actionrequests.ActionRequestMessage;
import it.polimi.ingsw.lm_belloni_cof.model.FinalScoreRules;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.GameMapFactory;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Politic;

/**
 * The controller of a {@link Game}: it decides whether and how a player can
 * execute actions on the model
 */
public class GameController {
	private static final Logger logger = Logger.getLogger(GameController.class.getName());
	/** Time waited before disconnecting a player after its turn is begun */
	private static final int ACTION_TIMEOUT = 120000;
	private boolean started;
	private boolean finished;
	/**
	 * The {@link GameMapFactory} to be used to retrieve {@link GameMap}s.
	 * 
	 * @see GameMapFactory
	 */
	private GameMapFactory gameMapFactory;
	private String gameId;
	private String mapId;
	/** Time in milliseconds of when the game has started. */
	private long startingTime;
	/**
	 * Game status
	 * 
	 * @see Game
	 */
	private Game game;
	private List<PlayerToken> connectedPlayers;
	/**
	 * Collection (to be updated when this game will finish) of the
	 * {@link PlayerData} of connected players.
	 */
	private Map<String, PlayerData> connectedPlayersData;
	private StatesManager statesManager;
	private MessageTranslator messageTranslator;
	private BroadcasterInterface broadcaster;
	/** Timer object used during game life cycle */
	private Timer actionTimer;

	/**
	 * Public constructor that creates a new controller setting a random map.
	 */
	public GameController(String gameId, GameMapFactory gameMapFactory, BroadcasterInterface broadcaster) {
		this.actionTimer = new Timer();
		this.started = false;
		this.finished = false;
		this.gameId = gameId;
		int randomMap = (new Random()).nextInt(8) + 1;
		this.mapId = "maps/map" + Integer.toString(randomMap) + ".xml";
		this.gameMapFactory = gameMapFactory;
		this.broadcaster = broadcaster;
		this.connectedPlayers = new ArrayList<>();
		this.connectedPlayersData = new HashMap<>();
		this.broadcaster.addGame(this.gameId);
	}

	/**
	 * Public constructor that creates a new controller with the map passed as
	 * an argument, if the map doesn't exist the default map will be used.
	 */
	public GameController(String gameId, String mapId, GameMapFactory gameMapFactory,
			BroadcasterInterface broadcaster) {
		this.actionTimer = new Timer();
		this.started = false;
		this.finished = false;
		this.gameId = gameId;
		this.mapId = mapId;
		this.gameMapFactory = gameMapFactory;
		this.broadcaster = broadcaster;
		this.connectedPlayers = new ArrayList<>();
		this.connectedPlayersData = new HashMap<>();
		this.broadcaster.addGame(this.gameId);
	}

	public boolean isGameFinished() {
		return this.finished;
	}

	public boolean isGameStarted() {
		return this.started;
	}

	/**
	 * Add a player to this game.
	 * 
	 * @param token
	 *            the {@link PlayerToken} of the added player.
	 * @param playerData
	 *            the {@link PlayerData} of the added player.
	 */
	public void addPlayer(PlayerToken token, PlayerData playerData) {
		if (!this.started && !this.connectedPlayers.contains(token)) {
			/*
			 * checking if a player with this username had already connected
			 * with a different token
			 */
			for (PlayerToken alreadyConnectedToken : this.connectedPlayers)
				if (alreadyConnectedToken.getUsername().equals(token.getUsername())) {
					this.connectedPlayers.remove(alreadyConnectedToken);
					break;
				}
			this.connectedPlayers.add(token);
			this.connectedPlayersData.put(token.getUsername(), playerData);
			this.broadcaster.subscribeTokenToGame(token, this.gameId);
		}
	}

	public int getNumberOfPlayers() {
		return this.connectedPlayers.size();
	}

	public String getGameId() {
		return this.gameId;
	}

	/** Actually starts the game controlled by this controller. */
	public void startGame() {
		if (!this.started) {
			List<String> usernames = new ArrayList<>();
			for (PlayerToken playerToken : connectedPlayers)
				usernames.add(playerToken.getUsername());
			Collections.shuffle(usernames); // random order
			this.game = new Game(gameMapFactory.getGameMap(mapId), usernames);
			TurnMachine turnMachine = new TurnMachine(game);
			this.statesManager = turnMachine;
			this.messageTranslator = new MessageTranslatorImpl(this.game, turnMachine);
			this.started = true;
			this.startingTime = new Date().getTime();
			GameStartMessage gameStartMessage = GameStartMessageFactory.getGameStartMessage(this.game,
					gameMapFactory.getGameMap(this.mapId));
			this.broadcaster.broadcast(gameStartMessage, this.gameId);
			/* starting the timer for action timeout */
			this.actionTimer = new Timer();
			this.actionTimer.schedule(new TimerTask() {
				@Override
				public void run() {
					disconnectPlayer(statesManager.getCurrentPlayer());
				}
			}, ACTION_TIMEOUT);
		}
	}

	/**
	 * Disconnect from the game the player passed as an argument.
	 * 
	 * @param toDisconnect
	 *            username of the player to disconnect.
	 */
	public void disconnectPlayer(String toDisconnect) {
		try {
			this.statesManager.setDisconnected(toDisconnect);
			/* updating other players */
			DisconnectedPlayerBroadcastMessage msg = new DisconnectedPlayerBroadcastMessage(toDisconnect);
			this.broadcaster.broadcast(msg, this.gameId);
			this.broadcaster.broadcast(GameUpdateMessageFactory.getGameUpdateMessage(this.game,
					"Now it's " + this.getCurrentPlayer() + " turn."), this.gameId);
		} catch (GameFinishedException e) {
			logger.log(Level.FINE, this.gameId + " is finished.", e);
			this.broadcaster.broadcast(onGameFinished(), this.gameId);
		}
	}

	/**
	 * Reconnect to the game the player passed as an argument.
	 * 
	 * @param toReconnect
	 *            username of the player to reconnect.
	 */
	public void reconnectPlayer(String toReconnect) {
		/* looking for the associated token */
		PlayerToken token = null;
		for (PlayerToken t : this.connectedPlayers) {
			if (t.getUsername().equals(toReconnect)) {
				token = t;
				break;
			}
		}
		if (token != null) {
			this.statesManager.reconnect(toReconnect);
			/* updating other players about reconnection */
			this.broadcaster.broadcast(new ReconnectedPlayerBroadcastMessage(token.getUsername()), this.gameId);
			/* re-subscribing reconnected player to updates */
			this.broadcaster.subscribeTokenToGame(token, this.gameId);
			/* broadcasting full game status */
			this.broadcaster.broadcast(
					GameStartMessageFactory.getGameStartMessage(this.game, gameMapFactory.getGameMap(this.mapId)),
					Arrays.asList(token));
		} else {

		}
	}

	/**
	 * Get the current player of this game. In other words this is the player
	 * that will be disconnected if an action isn't executed in
	 * {@link #ACTION_TIMEOUT} milliseconds.
	 * 
	 * @return the username of the current player.
	 */
	public String getCurrentPlayer() {
		return this.statesManager.getCurrentPlayer();
	}

	/**
	 * Private method that decides who has won the game calculating points and
	 * updating all {@link PlayerData} objects.
	 * 
	 * @return a {@link GameFinishedMessage} with this game results.
	 */
	private GameFinishedMessage onGameFinished() {
		FinalScoreRules rules = this.gameMapFactory.getGameMap(mapId).getFinalRules();
		// for each player i've to calculate the score
		Map<String, Integer> scores = new HashMap<>();
		/*
		 * nobilityPathFinalPositions is intended as a MultiMap with the
		 * position as key and the usernames as values, the map will be
		 * eventually ordered by key showing the situation correctly.
		 */
		SortedMap<Integer, List<String>> nobilityPathFinalPositions = new TreeMap<>();
		String playerWithMorePermits = game.getCurrentPlayer().getUsername(); // initialization
		// getting all informations about players
		for (PlayerToken playerToken : this.connectedPlayers) {
			Player player = this.game.getPlayerByUsername(playerToken.getUsername());
			scores.put(player.getUsername(), player.getVictoryPoints());
			/* adding the nobility points in the special data structure */
			if (nobilityPathFinalPositions.get(player.getNobilityTrackPosition()) == null)
				nobilityPathFinalPositions.put(player.getNobilityTrackPosition(), new ArrayList<String>());
			nobilityPathFinalPositions.get(player.getNobilityTrackPosition()).add(player.getUsername());
			/*
			 * looking for the player with the maximum number of permits, the
			 * game rules are not precise about this (what about 2 players with
			 * the same number of permits?)
			 */
			if (player.getPermitsAcquired().size() > game.getPlayerByUsername(playerWithMorePermits)
					.getPermitsAcquired().size())
				playerWithMorePermits = player.getUsername();
			/* adding the emporium bonus */
			if (game.getFirstToFinish() == player)
				scores.put(player.getUsername(), scores.get(player.getUsername()) + rules.getLastEmporiumPoints());
		}
		// nobility track position handling
		Integer topNobilityPositionReached = nobilityPathFinalPositions.lastKey();
		List<String> playersInTopPosition = nobilityPathFinalPositions.get(topNobilityPositionReached);
		nobilityPathFinalPositions.remove(topNobilityPositionReached);
		// giving victory points to them
		for (String username : playersInTopPosition) {
			scores.put(username, scores.get(username) + rules.getFirstNobilityTrackPoints());
		}
		// giving victory points to second classified only if an ex aequo hasn't
		// occured
		if (playersInTopPosition.size() == 1) {
			Integer secondNobilityPositionReached = nobilityPathFinalPositions.lastKey();
			List<String> playersInSecondPosition = nobilityPathFinalPositions.get(secondNobilityPositionReached);
			// giving bonuses to them
			for (String username : playersInSecondPosition) {
				scores.put(username, scores.get(username) + rules.getSecondNobilityTrackPoints());
			}
		}
		// giving victory points to the player with more permits
		scores.put(playerWithMorePermits, scores.get(playerWithMorePermits) + rules.getPermitPoints());
		// who is the winner?
		String winner = null;
		int winnerPoints = 0;
		List<String> exaequos = new ArrayList<>();
		for (Map.Entry<String, Integer> entry : scores.entrySet()) {
			if (entry.getValue() > winnerPoints) {
				winner = entry.getKey();
				winnerPoints = entry.getValue();
				exaequos.clear();
			} else if (entry.getValue() == winnerPoints) {
				exaequos.add(winner);
				winner = entry.getKey();
			}
		}
		if (!exaequos.isEmpty()) {
			exaequos.add(winner);
			int maxPoliticsAssistants = 0;
			// the player that holds the most assistants and politics is the
			// winner
			for (String potentialWinner : exaequos) {
				Player player = game.getPlayerByUsername(potentialWinner);
				if ((player.getPoliticsHand().size() + player.getNumberOfAssistants()) > maxPoliticsAssistants) {
					maxPoliticsAssistants = player.getPoliticsHand().size() + player.getNumberOfAssistants();
					winner = potentialWinner;
				}
			}
		}
		long endingTime = new Date().getTime();
		/* updating players data */
		for (Map.Entry<String, PlayerData> entry : this.connectedPlayersData.entrySet()) {
			if (entry.getKey().equals(winner))
				entry.getValue().addAWonGame();
			entry.getValue().addAPlayedGame();
			entry.getValue().addGameTime(endingTime - this.startingTime);
		}
		this.finished = true;
		return new GameFinishedMessage(scores, winner);
	}

	/**
	 * Private methods that queries the {@link Game} object and returns the
	 * politics card of a given username.
	 * 
	 * @param username
	 *            the username of the player
	 * @return a {@link PoliticsResponseMessage} containing the politics
	 *         requested.
	 */
	private PoliticsResponseMessage getPoliticsResponseMessage(String username) {
		// creating the list
		List<String> politicColors = new ArrayList<>();
		Player player = this.game.getPlayerByUsername(username);
		for (Politic politic : player.getPoliticsHand())
			politicColors.add(politic.getPoliticColor().toString().toLowerCase());
		return new PoliticsResponseMessage(politicColors);
	}

	/**
	 * Execute an {@link Action} on the game object and returns the results
	 * 
	 * @param action
	 *            the {@link Action} that has to be executed.
	 * @return a {@link ResponseMessage} containing the results of the action.
	 */
	private ResponseMessage executeAction(Action action) {
		try {
			if (action.execute())
				this.statesManager.setLastTurn();
			return action.getActionExecutedMessage();
		} catch (ActionExecutionException e) {
			logger.log(Level.FINE, "Action " + action.toString() + " hasn't been executed.", e);
			return new ActionNotExecutedMessage(e.getMessage());
		}
	}

	/**
	 * Core of the controller, this is the door to the controller and the only
	 * way to interact with a game
	 * 
	 * @param request
	 *            the request that may contain an {@link ActionRequestMessage}
	 *            to modify the game status or not.
	 * @return a {@link ResponseMessage} containing the results of the requested
	 *         service.
	 */
	public ResponseMessage handleRequest(RequestMessage request) {
		if (this.started) {
			if (request instanceof ChatRequestMessage) {
				this.broadcaster.broadcast(new ChatBroadcastMessage(request.getSender().getUsername(),
						((ChatRequestMessage) request).getMessage()), this.gameId);
				return new ChatResponseMessage();
			} else if (request instanceof PoliticsRequestMessage) {
				return this.getPoliticsResponseMessage(request.getSender().getUsername());
			} else if (request instanceof ActionRequestMessage) {
				try {
					Action action = ((ActionRequestMessage) request).getAction(this.messageTranslator);
					if (this.statesManager.getState(request.getSender().getUsername()).isAllowedToExecute(action)) {
						ResponseMessage result = executeAction(action);
						if (result instanceof ActionExecutedMessage) { // OK
							// changing state
							if (action.hasBeenExecuted()) { // double check
								this.actionTimer.cancel();
								try {
									this.statesManager.changeState(request.getSender().getUsername(), action);
									/* starting a new action timer */
									this.actionTimer = new Timer();
									this.actionTimer.schedule(new TimerTask() {
										@Override
										public void run() {
											disconnectPlayer(statesManager.getCurrentPlayer());
										}
									}, ACTION_TIMEOUT);
									this.broadcaster.broadcast(action.getGameUpdateMessage(), this.gameId);
								} catch (GameFinishedException e) {
									logger.log(Level.FINE, this.gameId + " is finished.", e);
									this.broadcaster.broadcast(onGameFinished(), this.gameId);
								}
							}
						}
						return result;
					} else {
						return new ActionNotExecutedMessage("You can't do this right now.");
					}
				} catch (InvalidMessageException e) {
					ActionNotExecutedMessage message = new ActionNotExecutedMessage(e.getMessage());
					logger.log(Level.INFO, "Invalid message received.", e);
					return message;
				}
			}
			return null;
		} else
			return new ActionNotExecutedMessage("The game hasn't started yet.");
	}
}
