package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import it.polimi.ingsw.lm_belloni_cof.controller.states.StateChangingBonusHandler;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughAssistantsException;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Player;

/**
 * The {@link QuickAction} representing the Council of Four
 * "Perform an additional main action" quick action.
 */
public class PerformAnAdditionalMainActionAction extends QuickAction {
	/**
	 * A {@link StateChangingBonusHandler} instance to edit the
	 * {@link PlayerState} of the {@link Player}.
	 */
	private StateChangingBonusHandler stateChangingBonusHandler;

	/**
	 * Public constructor of this action.
	 * 
	 * @param game
	 *            the {@link Game} on which the action will generate changes.
	 * @param player
	 *            the {@link Player} responsible for the changes on the
	 *            {@link Game}.
	 * @param stateChangingBonusHandler
	 *            a {@link StateChangingBonusHandler} instance to edit the
	 *            {@link PlayerState} of the {@link Player}.
	 */
	public PerformAnAdditionalMainActionAction(Game game, Player player,
			StateChangingBonusHandler stateChangingBonusHandler) {
		super(game, player);
		this.stateChangingBonusHandler = stateChangingBonusHandler;
	}

	/**
	 * Add a main action to the current player calling the
	 * {@link StateChangingBonusHandler#addNMainActions(int)} method after
	 * returning three assistants to the pool, then edits the
	 * {@link Action#actionDescription} field.
	 * 
	 * @throws ActionExecutionException
	 *             when the requested action hasn't been completely executed due
	 *             to a {@link NotEnoughAssistantsException}.
	 */
	@Override
	public boolean execute() throws ActionExecutionException {
		try {
			super.game.returnNAssistantsToThePool(super.player.getNAssistants(3));
			this.stateChangingBonusHandler.addNMainActions(1);
			super.actionDescription += super.player.getUsername() + " will perform another main action.";
			super.setExecuted();
		} catch (NotEnoughAssistantsException e) {
			throw new ActionExecutionException("Not enough assistants to complete this action.", e);
		}
		return false;
	}

}
