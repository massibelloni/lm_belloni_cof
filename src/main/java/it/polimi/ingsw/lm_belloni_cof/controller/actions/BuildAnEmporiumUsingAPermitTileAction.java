package it.polimi.ingsw.lm_belloni_cof.controller.actions;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.controller.states.StateChangingBonusHandler;
import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionExecutionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.EmporiumAlreadyBuiltException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidActionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughAssistantsException;
import it.polimi.ingsw.lm_belloni_cof.model.City;
import it.polimi.ingsw.lm_belloni_cof.model.Game;
import it.polimi.ingsw.lm_belloni_cof.model.Permit;
import it.polimi.ingsw.lm_belloni_cof.model.Player;
import it.polimi.ingsw.lm_belloni_cof.model.Bonus;

/**
 * The {@link MainAction} representing the Council of Four
 * "Build an emporium using a permit tile" main action
 */
public class BuildAnEmporiumUsingAPermitTileAction extends MainAction {
	private static final Logger logger = Logger.getLogger(BuildAnEmporiumUsingAPermitTileAction.class.getName());
	/** The city where the emporium will be built. */
	private City city;
	/** The permit card containing the city that will be used. */
	private Permit permit;

	/**
	 * Public constructor for the action.
	 * 
	 * @param game
	 *            the {@link Game} on which the action will generate changes.
	 * @param player
	 *            the {@link Player} responsible for the changes on the
	 *            {@link Game}.
	 * @param city
	 *            the city where the emporium will be built.
	 * @param permit
	 *            the permit card containing the city that will be used.
	 * @param bonusHandler
	 *            a {@link StateChangingBonusHandler} instance to edit the
	 *            {@link PlayerState} of the {@link Player}.
	 */
	public BuildAnEmporiumUsingAPermitTileAction(Game game, Player player, City city, Permit permit,
			StateChangingBonusHandler bonusHandler) {
		super(game, player, bonusHandler);
		this.city = city;
		this.permit = permit;
	}

	/**
	 * Calls the
	 * {@link it.polimi.ingsw.lm_belloni_cof.model.Game#buildAnEmporiumUsingAPermitTile(Player,Permit,City)}
	 * of the model and handles the bonuses obtained. Then edits the
	 * {@link Action#actionDescription} field.
	 * 
	 * @see MainAction#handleBonuses
	 * @return true if the player has built all its emporiums, false otherwise
	 * @throws ActionExecutionException
	 *             when the requested action hasn't been completely executed due
	 *             to a {@link NotEnoughAssistantsException}.
	 */
	@Override
	public boolean execute() throws ActionExecutionException {
		try {
			List<Bonus> bonuses = game.buildAnEmporiumUsingAPermitTile(player, permit, city);
			super.actionDescription += player.getUsername() + " has built an emporium on the city of " + city.getName()+".";
			super.handleBonuses(bonuses);
			super.actionDescription += super.getHandledBonusesDescriptions();
			super.setExecuted();
			if (player.getNumberOfEmporiums() == 0) {
				if (super.game.getFirstToFinish() == null)
					super.game.setFirstToFinish(super.player);
				return true;
			}
		} catch (NotEnoughAssistantsException e) {
			throw new ActionExecutionException("Not enough assistants to complete this action.", e);
		} catch(EmporiumAlreadyBuiltException e){
			throw new ActionExecutionException("You already have an emporium on this city.", e);
		} catch (InvalidActionException e) {
			logger.log(Level.SEVERE, "The MessageTranslator should avoid this.", e);
		}
		return false;
	}

}
