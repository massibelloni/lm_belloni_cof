package it.polimi.ingsw.lm_belloni_cof.messages;

import java.io.Serializable;
/**
 * Message needed to submit a request to the server-side
 *
 */
public class RequestMessage implements Serializable {
	private static final long serialVersionUID = 1L;
	private PlayerToken sender;

	protected RequestMessage(PlayerToken sender) {
		this.sender = sender;
	}

	public PlayerToken getSender() {
		return this.sender;
	}

}
