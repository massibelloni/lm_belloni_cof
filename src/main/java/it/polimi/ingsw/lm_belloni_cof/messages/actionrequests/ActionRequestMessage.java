package it.polimi.ingsw.lm_belloni_cof.messages.actionrequests;

import it.polimi.ingsw.lm_belloni_cof.controller.MessageTranslator;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidMessageException;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
import it.polimi.ingsw.lm_belloni_cof.messages.RequestMessage;
/**
 * This abstract message represents the super-class of all ActRqstMsg
 *
 */
public abstract class ActionRequestMessage extends RequestMessage {
	private static final long serialVersionUID = 1L;

	protected ActionRequestMessage(PlayerToken sender) {
		super(sender);
	}
	/**
	 * This method checks the content of the message in order to translate it properly in an action
	 */
	public abstract Action getAction(MessageTranslator translator) throws InvalidMessageException;

}
