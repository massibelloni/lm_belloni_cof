package it.polimi.ingsw.lm_belloni_cof.messages.actionrequests;

import it.polimi.ingsw.lm_belloni_cof.controller.MessageTranslator;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidMessageException;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
/**
 * This message is sent to the server-side from a {@link Player} 
 * who wants to end his Market Offer
 *
 */
public class EndMarketOfferActRqstMsg extends ActionRequestMessage {
	private static final long serialVersionUID = 1L;
	/**
	 * Public constructor of {@link EndMarketOfferActRqstMsg}
	 * @param sender {@link Player} who has submitted the message
	 */
	public EndMarketOfferActRqstMsg(PlayerToken sender) {
		super(sender);
	}
	/**
	 * This method checks the content of the message in order to translate it properly in an action
	 */
	@Override
	public Action getAction(MessageTranslator translator) throws InvalidMessageException {
		return translator.translate(this);
	}

}
