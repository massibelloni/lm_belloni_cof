package it.polimi.ingsw.lm_belloni_cof.messages.actionrequests;

import it.polimi.ingsw.lm_belloni_cof.controller.MessageTranslator;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidMessageException;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
/**
 * This message is sent to the server-side from a {@link Player} 
 * who wants to perform the CoF main action "Build an emporium using a permit tile"
 *
 */
public class BuildAnEmporiumUsingAPermitTileActRqstMsg extends ActionRequestMessage {
	private static final long serialVersionUID = 1L;
	private String city;
	private String permit;
	/**
	 * Public constructor of {@link BuildAnEmporiumUsingAPermitTileActRqstMsg}
	 * @param sender {@link Player} who has submitted the message
	 * @param city	{@link City} on which built this emporium
	 * @param permit {@link Permit} to use for build this emporium on the specified [@link City} 
	 */
	public BuildAnEmporiumUsingAPermitTileActRqstMsg(PlayerToken sender, String city, String permit) {
		super(sender);
		this.city = city;
		this.permit = permit;

	}

	public String getCity() {
		return city;
	}

	public String getPermit() {
		return permit;
	}
	/**
	 * This method checks the content of the message in order to translate it properly in an action
	 */
	@Override
	public Action getAction(MessageTranslator translator) throws InvalidMessageException {
		return translator.translate(this);
	}

}
