package it.polimi.ingsw.lm_belloni_cof.messages;

import java.util.List;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;
/**
 * This {@link ResponseMessage} reply to a {@link PoliticRequestMessage} by sending the politic cards of the applicant
 *
 */
public class PoliticsResponseMessage extends ResponseMessage {

	private static final long serialVersionUID = 1L;
	private List<String> politicColors;

	public PoliticsResponseMessage(List<String> politicColors) {
		this.politicColors = politicColors;
	}

	public List<String> getPoliticColors() {
		return this.politicColors;
	}
	/**
	 * This method checks the content of the message in order to show it properly
	 */
	@Override
	public void visit(MessageVisitor visitor) {
		visitor.display(this);
	}

}
