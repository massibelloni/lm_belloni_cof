package it.polimi.ingsw.lm_belloni_cof.messages;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;
/**
 * This {@link ResponseMessage} is sent from the server-side to the client-side after a connection attempt.
 *
 */
public class ConnectionResponseMessage extends ResponseMessage {
	private static final long serialVersionUID = 1L;
	/**
	 * After a connection the client receive a token that identifies him
	 */
	private PlayerToken playerToken;
	/**
	 * This message contains the result of the connection attempt
	 */
	private String message;
	/**
	 * Public constructor of this {@link ResponseMessage}
	 * @param playerToken {@link PlayerToken} that identifies the player during this game session
	 * @param message The result of the connection attempt
	 */
	public ConnectionResponseMessage(PlayerToken playerToken, String message) {
		this.playerToken = playerToken;
		this.message = message;
	}

	public PlayerToken getPlayerToken() {
		return this.playerToken;
	}

	public String getMessage() {
		return this.message;
	}
	/**
	 * This method checks the content of the message in order to show it properly
	 */
	@Override
	public void visit(MessageVisitor visitor) {
		visitor.display(this);	
	}
}
