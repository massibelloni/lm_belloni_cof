package it.polimi.ingsw.lm_belloni_cof.messages;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;
/**
 * This {@link BroadcastMessage} is sent to all the players when the game starts
 *
 */
public class GameStartMessage extends GameUpdateMessage {
	private static final long serialVersionUID = 1L;
	private String data;
	/**
	 * Public constructor of this {@link BroadcastMessage}
	 * @param data contains the static informations of the game such as: regions, citites and so on
	 * @param update contains the dynamic informations about the game
	 */
	public GameStartMessage(String data, String update) {
		super(update, "The game has started.");
		this.data = data;
	}

	public String getData() {
		return this.data;
	}
	/**
	 * This method checks the content of the message in order to show it properly
	 */
	@Override
	public void visit(MessageVisitor visitor){
		visitor.display(this);
	}
}
