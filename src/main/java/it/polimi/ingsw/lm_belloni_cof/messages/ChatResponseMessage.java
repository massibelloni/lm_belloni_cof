package it.polimi.ingsw.lm_belloni_cof.messages;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;
/**
 * This message is sent from the server-side to the {@link Player} who has submitted a {@link ChatRequestMessage} 
 * after have extracted the message inside and sent it broadcast with a {@link ChatBroadcastMessage}
 */
public class ChatResponseMessage extends ResponseMessage {
	private static final long serialVersionUID = 1L;
	/**
	 * This method checks the content of the message in order to show it properly
	 */
	@Override
	public void visit(MessageVisitor visitor) {
		visitor.display(this);
	}
}
