package it.polimi.ingsw.lm_belloni_cof.messages.actionrequests;

import it.polimi.ingsw.lm_belloni_cof.controller.MessageTranslator;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidMessageException;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
/**
 * This message is sent to the server-side from a {@link Player} 
 * who wants to draw a {@link PoliticCard}. 
 * As provided for in regulation each [@link Player} has to draw a politic at the start of his own turn
 *
 */
public class DrawAPoliticActRqstMsg extends ActionRequestMessage {

	private static final long serialVersionUID = 1L;

	public DrawAPoliticActRqstMsg(PlayerToken sender) {
		super(sender);
	}
	/**
	 * This method checks the content of the message in order to translate it properly in an action
	 */
	@Override
	public Action getAction(MessageTranslator translator) throws InvalidMessageException {
		return translator.translate(this);
	}

}
