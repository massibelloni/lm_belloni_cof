package it.polimi.ingsw.lm_belloni_cof.messages.actionrequests;

import it.polimi.ingsw.lm_belloni_cof.controller.MessageTranslator;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidMessageException;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
/**
 * This message is sent to the server-side from a {@link Player} 
 * who wants to perform the CoF main action "Elect a councillor"
 *
 */
public class ElectACouncillorActRqstMsg extends ActionRequestMessage {
	private static final long serialVersionUID = 1L;
	private String region;
	private String councillorColor;
	/**
	 * Public constructor of {@link ElectACouncillorActRqstMsg}
	 * @param sender {@link Player} who has submitted the message
	 * @param region {@link Region} of the {@link Balcony} on which elect the specified {@link Councillor}
	 * @param councillorColor color of the {@link Councillor} that may be elected
	 */
	public ElectACouncillorActRqstMsg(PlayerToken sender, String region, String councillorColor) {
		super(sender);
		this.region = region;
		this.councillorColor = councillorColor;
	}

	public String getRegion() {
		return region;
	}

	public String getCouncillorColor() {
		return councillorColor;
	}
	/**
	 * This method checks the content of the message in order to translate it properly in an action
	 */
	@Override
	public Action getAction(MessageTranslator translator) throws InvalidMessageException {
		return translator.translate(this);
	}

}
