package it.polimi.ingsw.lm_belloni_cof.messages.actionrequests;

import it.polimi.ingsw.lm_belloni_cof.controller.MessageTranslator;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidMessageException;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
/**
 * This message is sent to the server-side from a {@link Player} 
 * who wants to obtain {@link Bonus}es from a token on a {@link City} where he has already built an emporium.
 * Possible request due to a {@link Bonus} inside the nobility track
 *
 */
public class CityTokenResponseActRqstMsg extends ActionRequestMessage {

	private static final long serialVersionUID = 1L;
	private String city;

	public CityTokenResponseActRqstMsg(PlayerToken sender, String city) {
		super(sender);
		this.city = city;
	}

	public String getCity() {
		return this.city;
	}
	/**
	 * This method checks the content of the message in order to translate it properly in an action
	 */
	@Override
	public Action getAction(MessageTranslator translator) throws InvalidMessageException {
		return translator.translate(this);
	}

}
