package it.polimi.ingsw.lm_belloni_cof.messages;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;

/**
 * This message is returned from the server to the client who has performed the
 * action, to notify him the success of the action
 *
 */
public class ActionExecutedMessage extends ResponseMessage {
	private static final long serialVersionUID = 1L;
	private String actionDescription;
	private int cityTokenResponsesAdded;
	private int faceUpPermitResponsesAdded;
	private int ownedPermitResponsesAdded;

	/**
	 * Public constructor
	 * 
	 * @param message
	 *            message showed when the action is correctly performed
	 */
	public ActionExecutedMessage(String message) {
		this.actionDescription = message;

	}

	public ActionExecutedMessage(String message, int cityTokenResponsesAdded, int faceUpPermitResponsesAdded,
			int ownedPermitResponsesAdded) {
		this(message);
		this.cityTokenResponsesAdded = cityTokenResponsesAdded;
		this.faceUpPermitResponsesAdded = faceUpPermitResponsesAdded;
		this.ownedPermitResponsesAdded = ownedPermitResponsesAdded;

	}

	public String getMessage() {
		return this.actionDescription;
	}

	
	public int getCityTokenResponsesAdded() {
		return cityTokenResponsesAdded;
	}

	public int getFaceUpPermitResponsesAdded() {
		return faceUpPermitResponsesAdded;
	}

	public int getOwnedPermitResponsesAdded() {
		return ownedPermitResponsesAdded;
	}

	/**
	 * This method checks the content of the message in order to show it
	 * properly
	 */
	
	@Override
	public void visit(MessageVisitor visitor) {
		visitor.display(this);
	}
}
