package it.polimi.ingsw.lm_belloni_cof.messages;

import java.util.Map;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;

public class PlayersTableResponseMessage extends ResponseMessage {
	private static final long serialVersionUID = 1L;
	private Map<String, Integer> gamesPlayed;
	private Map<String, Integer> gamesWon;
	private Map<String, Integer> gameTimeInMinutes;

	public PlayersTableResponseMessage(Map<String, Integer> gamesPlayed, Map<String, Integer> gamesWon,
			Map<String, Integer> gameTimeInMinutes) {
		this.gamesPlayed = gamesPlayed;
		this.gamesWon = gamesWon;
		this.gameTimeInMinutes = gameTimeInMinutes;
	}

	public Map<String, Integer> getGamesPlayed() {
		return gamesPlayed;
	}

	public Map<String, Integer> getGamesWon() {
		return gamesWon;
	}

	public Map<String, Integer> getGameTimeInMinutes() {
		return gameTimeInMinutes;
	}

	@Override
	public void visit(MessageVisitor visitor) {
		visitor.display(this);
	}

}
