package it.polimi.ingsw.lm_belloni_cof.messages.actionrequests;

import it.polimi.ingsw.lm_belloni_cof.controller.MessageTranslator;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidMessageException;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
/**
 * This message is sent to the server-side from a {@link Player} 
 * who wants buy an {@link Ownable} on the market
 * A {@link Player} can buy only one {@link Ownable} per market buy
 *
 */
public class MarketBuyActRqstMsg extends ActionRequestMessage {
	private static final long serialVersionUID = 1L;
	private String seller;
	private int indexInSellerOffers;
	/**
	 * Public constructor of {@link MarketBuyActRqstMsg}
	 * @param sender {@link Player} who has submitted the message
	 * @param seller {@link Player} who has offer the {@link Ownable} on the market
	 * @param indexInSellerOffers index of the chosen {@link Ownable} in sellable {@link Ownable} list
	 */
	public MarketBuyActRqstMsg(PlayerToken sender, String seller, int indexInSellerOffers) {
		super(sender);
		this.seller = seller;
		this.indexInSellerOffers = indexInSellerOffers;
	}

	public int getIndexInSellerOffers() {
		return this.indexInSellerOffers;
	}

	public String getSeller() {
		return this.seller;
	}
	/**
	 * This method checks the content of the message in order to translate it properly in an action
	 */
	@Override
	public Action getAction(MessageTranslator translator) throws InvalidMessageException {
		return translator.translate(this);
	}
}
