package it.polimi.ingsw.lm_belloni_cof.messages;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;
/**
 * The {@link BroadcastMessage} which represents a chat message from a player to the others
 *
 */
public class ChatBroadcastMessage extends BroadcastMessage {
	private static final long serialVersionUID = 1L;
	private String sender;
	private String message;
	/**
	 * Public constructor of this message
	 * @param sender represents the username of the sender
	 * @param message represents the message to deliver
	 */
	public ChatBroadcastMessage(String sender, String message) {
		this.sender = sender;
		this.message = message;
	}

	public String getSender() {
		return this.sender;
	}

	public String getMessage() {
		return this.message;
	}
	/**
	 * This method checks the content of the message in order to show it properly
	 */
	@Override
	public void visit(MessageVisitor visitor) {
		visitor.display(this);

	}
}
