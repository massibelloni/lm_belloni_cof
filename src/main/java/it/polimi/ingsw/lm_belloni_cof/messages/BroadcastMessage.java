package it.polimi.ingsw.lm_belloni_cof.messages;

import java.io.Serializable;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;
/**
 * Abstract class: represents a message that is sent to all players
 *
 */
public abstract class BroadcastMessage implements Serializable {
	private static final long serialVersionUID = 1L;
	public abstract void visit(MessageVisitor visitor);
}
