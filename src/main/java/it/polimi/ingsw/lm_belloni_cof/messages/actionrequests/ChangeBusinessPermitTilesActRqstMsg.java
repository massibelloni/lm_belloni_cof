package it.polimi.ingsw.lm_belloni_cof.messages.actionrequests;

import it.polimi.ingsw.lm_belloni_cof.controller.MessageTranslator;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidMessageException;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
/**
 * This message is sent to the server-side from a {@link Player} 
 * who wants to perform the CoF quick action "Change business permit tile"
 *
 */
public class ChangeBusinessPermitTilesActRqstMsg extends ActionRequestMessage {
	private static final long serialVersionUID = 1L;
	private String region;
	/**
	 * Public constructor of {@link ChangeBusnessPermitTilesActRqstMsg}
	 * @param sender {@link Player} who has submitted the message
	 * @param region {@link Region} on which change the face up permit tiles
	 */
	public ChangeBusinessPermitTilesActRqstMsg(PlayerToken sender, String region) {
		super(sender);
		this.region = region;
	}

	public String getRegion() {
		return this.region;
	}
	/**
	 * This method checks the content of the message in order to translate it properly in an action
	 */
	@Override
	public Action getAction(MessageTranslator translator) throws InvalidMessageException {
		return translator.translate(this);
	}

}
