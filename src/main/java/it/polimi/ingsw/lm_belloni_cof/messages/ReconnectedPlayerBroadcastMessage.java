package it.polimi.ingsw.lm_belloni_cof.messages;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;

/**
 * This {@link BroadcastMessage} is sent to all the players when a player
 * reconnects after disconnection.
 *
 */
public class ReconnectedPlayerBroadcastMessage extends BroadcastMessage {
	private static final long serialVersionUID = 1L;
	private String reconnected;
	public ReconnectedPlayerBroadcastMessage(String reconnected){
		this.reconnected = reconnected;
	}
	
	public String getReconnected(){
		return this.reconnected;
	}
	@Override
	public void visit(MessageVisitor visitor) {
		visitor.display(this);
	}

}
