package it.polimi.ingsw.lm_belloni_cof.messages.actionrequests;

import java.util.Collections;
import java.util.List;

import it.polimi.ingsw.lm_belloni_cof.controller.MessageTranslator;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidMessageException;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
/**
 * This message is sent to the server-side from a {@link Player} 
 * who wants to perform the CoF main action "Build an emporium with the help of the king"
 *
 */
public class BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg extends ActionRequestMessage {

	private static final long serialVersionUID = 1L;
	private String city;
	private List<String> politicColors;
	/**
	 * Public constructor of {@link BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg}
	 * @param sender {@link Player} who has submitted the message
	 * @param city {@link City} on which built this emporium
	 * @param politicColors {@link PoliticCard}s to use in order to satisfies king's council, and obtain the permit of build
	 */
	public BuildAnEmporiumWithTheHelpOfTheKingActRqstMsg(PlayerToken sender, String city, List<String> politicColors) {
		super(sender);
		this.city = city;
		this.politicColors = politicColors;
	}

	public String getCity() {
		return city;
	}

	public List<String> getPoliticColors() {
		return Collections.unmodifiableList(politicColors);
	}
	/**
	 * This method checks the content of the message in order to translate it properly in an action
	 */
	@Override
	public Action getAction(MessageTranslator translator) throws InvalidMessageException {
		return translator.translate(this);
	}

}
