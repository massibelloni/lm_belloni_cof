package it.polimi.ingsw.lm_belloni_cof.messages;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;
/**
 * Token returned to a connected player in order to identify him better 
 * @author fabioballabio
 *
 */
public class PlayerToken implements Serializable {
	private static final long serialVersionUID = 1L;
	private String token;
	private String username;
	/**
	 * Public constructor of {@link PlayerToken}, it assigns a randomly chosen token to each connected player
	 * @param username Username of the player to whom return this PlayerToken
	 */
	public PlayerToken(String username) {
		this.token = UUID.randomUUID().toString();
		this.username = username;
	}

	public String getUsername() {
		return this.username;
	}

	public String getToken() {
		return this.token;
	}
	/**
	 * Overrided equals method.
	 * Two PlayerTokens are equal if they have the same username and the same token
	 * 
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o != null && (o instanceof PlayerToken)) {
			PlayerToken playerToken = (PlayerToken) o;
			if (playerToken.getUsername().equals(this.username) && playerToken.getToken().equals(this.token)) {
				return true;
			}
			return false;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.token);
	}

}
