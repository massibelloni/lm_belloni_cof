package it.polimi.ingsw.lm_belloni_cof.messages;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;
/**
 * This message is returned from the server to the client who has performed the action,
 * to notify him an error during the action
 *
 */
public class ActionNotExecutedMessage extends ResponseMessage {
	private static final long serialVersionUID = 1L;
	private String reason;
	/**
	 * Public constructor
	 * @param reason error message showed when the action isn't well performed
	 */
	public ActionNotExecutedMessage(String reason) {
		this.reason = reason;
	}

	public String getReason() {
		return this.reason;
	}
	/**
	 * This method checks the content of the message in order to show it properly
	 */
	@Override
	public void visit(MessageVisitor visitor) {
		visitor.display(this);
	}

}
