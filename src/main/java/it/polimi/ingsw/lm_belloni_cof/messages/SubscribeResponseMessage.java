package it.polimi.ingsw.lm_belloni_cof.messages;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;

public class SubscribeResponseMessage extends ResponseMessage{
	private static final long serialVersionUID = 1L;
	@Override
	public void visit(MessageVisitor visitor) {
		visitor.display(this);
	}
}
