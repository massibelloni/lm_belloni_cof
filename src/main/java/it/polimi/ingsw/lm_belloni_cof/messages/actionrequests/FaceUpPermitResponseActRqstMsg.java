package it.polimi.ingsw.lm_belloni_cof.messages.actionrequests;

import it.polimi.ingsw.lm_belloni_cof.controller.MessageTranslator;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidMessageException;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
/**
 * This message is sent to the server-side from a {@link Player} 
 * who wants to specify a face up {@link Permit} in order to obtain its {@link Bonus}es
 *
 */
public class FaceUpPermitResponseActRqstMsg extends ActionRequestMessage {
	private static final long serialVersionUID = 1L;
	private String region;
	private int indexInRegion;
	/**
	 * Public constructor of {@link FaceUpPermitResponseActRqstMsg}
	 * @param sender {@link Player} who has submitted the message
	 * @param region {@link Region} in which search the face up permit tiles
	 * @param indexInRegion index of the chosen {@link Permit}, it must be 1 or 2
	 */
	public FaceUpPermitResponseActRqstMsg(PlayerToken sender, String region, int indexInRegion) {
		super(sender);
		this.region = region;
		this.indexInRegion = indexInRegion;
	}

	public String getRegion() {
		return region;
	}

	public int getIndexInRegion() {
		return indexInRegion;
	}
	/**
	 * This method checks the content of the message in order to translate it properly in an action
	 */
	@Override
	public Action getAction(MessageTranslator translator) throws InvalidMessageException {
		return translator.translate(this);
	}

}
