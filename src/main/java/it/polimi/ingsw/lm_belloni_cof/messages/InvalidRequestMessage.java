package it.polimi.ingsw.lm_belloni_cof.messages;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;
/**
 * This {@link ResponseMessage} is sent to the player who has submit a {@link RequestMessage} that generates an error
 */
public class InvalidRequestMessage extends ResponseMessage {
	private static final long serialVersionUID = 1L;
	private String error;

	public InvalidRequestMessage(String error) {
		this.error = error;
	}

	public String getError() {
		return this.error;
	}
	/**
	 * This method checks the content of the message in order to show it properly
	 */
	@Override
	public void visit(MessageVisitor visitor) {
		visitor.display(this);
	}
}
