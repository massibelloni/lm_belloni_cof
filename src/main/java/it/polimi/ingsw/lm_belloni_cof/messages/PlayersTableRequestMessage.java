package it.polimi.ingsw.lm_belloni_cof.messages;

/**
 * Message sent by an user requesting for the player's table (game played, won,
 * lost, ecc.)
 */
public class PlayersTableRequestMessage extends RequestMessage {
	private static final long serialVersionUID = 1L;

	public PlayersTableRequestMessage() {
		super(null); // no auth required
	}
}
