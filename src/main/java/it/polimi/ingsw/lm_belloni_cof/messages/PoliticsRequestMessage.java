package it.polimi.ingsw.lm_belloni_cof.messages;
/**
 * This {@link RequestMessage} is sent from a player who wants to know his politic cards
 *
 */
public class PoliticsRequestMessage extends RequestMessage{
	private static final long serialVersionUID = 1L;
	/**
	 * Public constructor of this {@link REquestMessage}
	 * @param sender player who wants to know his politic cards
	 */
	public PoliticsRequestMessage(PlayerToken sender) {
		super(sender);
	}
}
