package it.polimi.ingsw.lm_belloni_cof.messages;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;

/**
 * This {@link BroadcastMessage} is sent to all the players when a player log
 * off
 *
 */
public class DisconnectedPlayerBroadcastMessage extends BroadcastMessage {
	private static final long serialVersionUID = 1L;
	private String disconnected;

	/**
	 * Public contructor of this {@link BroadcastMessage}
	 * 
	 * @param disconnected
	 *            Username of the disconnected player
	 */
	public DisconnectedPlayerBroadcastMessage(String disconnected) {
		this.disconnected = disconnected;
	}

	public String getDisconnected() {
		return this.disconnected;
	}

	/**
	 * This method checks the content of the message in order to show it
	 * properly
	 */
	@Override
	public void visit(MessageVisitor visitor) {
		visitor.display(this);
	}

}
