package it.polimi.ingsw.lm_belloni_cof.messages.actionrequests;

import java.util.Collections;
import java.util.List;

import it.polimi.ingsw.lm_belloni_cof.controller.MessageTranslator;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidMessageException;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
/**
 * This message is sent to the server-side from a {@link Player} 
 * who wants to perform the CoF main action "Acquire a business permit tile"
 *
 */
public class AcquireABusinessPermitTileActRqstMsg extends ActionRequestMessage {
	private static final long serialVersionUID = 1L;
	private String region;
	private int indexInRegion;
	private List<String> politicColors;
	/**
	 * Public contructor of {@link AcquireABusinessPermitTileActRqstMsg}
	 * @param sender {@link Player} who has submitted the message
	 * @param region {@link Region} from which pick up the {@link Permit}
	 * @param indexInRegion index of the chosen {@link Permit} inside the specified {@link Region}
	 * @param politicColors {@link PoliticCard}s to use in order to satisfy the council and pick up the chosen {@link Permit}
	 */
	public AcquireABusinessPermitTileActRqstMsg(PlayerToken sender, String region, int indexInRegion,
			List<String> politicColors) {
		super(sender);
		this.region = region;
		this.indexInRegion = indexInRegion;
		this.politicColors = politicColors;
	}

	public String getRegion() {
		return this.region;
	}

	public int getIndexInRegion() {
		return this.indexInRegion;
	}

	public List<String> getPoliticColors() {
		return Collections.unmodifiableList(this.politicColors);
	}
	/**
	 * This method checks the content of the message in order to translate it properly in an action
	 */
	@Override
	public Action getAction(MessageTranslator translator) throws InvalidMessageException {
		return translator.translate(this);
	}
}
