package it.polimi.ingsw.lm_belloni_cof.messages;
/**
 * This message represents the request that a {@link Player} submits to the server in order to send a {@link ChatBroadcastMessage}
 * After this message the server side extract the message and send it broadcast with a {@link ChatBroadcastMessage}
 *
 */
public class ChatRequestMessage extends RequestMessage {
	private static final long serialVersionUID = 1L;
	private String message;

	public ChatRequestMessage(PlayerToken sender, String message) {
		super(sender);
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}

}
