package it.polimi.ingsw.lm_belloni_cof.messages;
/**
 * This {@link RequestMessage} is sent from the client-side to the server-side before the connection, to require it
 *
 */
public class ConnectionRequestMessage extends RequestMessage {
	private static final long serialVersionUID = 1L;
	private String username;
	private String password;
	/**
	 * Public constructor of this {@link RequestMessage} 
	 * @param username Username of the client who is trying to connect
	 * @param password Password of the client who is trying to connect
	 */
	public ConnectionRequestMessage(String username, String password) {
		super(null);
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return this.username;
	}

	public String getPassword() {
		return this.password;
	}

}
