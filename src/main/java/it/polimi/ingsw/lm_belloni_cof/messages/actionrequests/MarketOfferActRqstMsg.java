package it.polimi.ingsw.lm_belloni_cof.messages.actionrequests;

import it.polimi.ingsw.lm_belloni_cof.controller.MessageTranslator;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidMessageException;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
/**
 * This message is sent to the server-side from a {@link Player} 
 * who wants offer an {@link Ownable} on the market
 * A {@link Player} can offer only one {@link Ownable} per market offer
 *
 */
public class MarketOfferActRqstMsg extends ActionRequestMessage {
	private static final long serialVersionUID = 1L;
	private String forSale;
	private int price;
	/**
	 * Public constructor of {@link MarketOfferActRqstMsg}
	 * @param sender {@link Player} who has submitted the message, and who is offering the {@link Ownable}
	 * @param forSale {@link Ownable} to be put on the market
	 * @param price {@link Integer} that represents the price of the specified {@link Ownable}
	 */
	public MarketOfferActRqstMsg(PlayerToken sender, String forSale, int price) {
		super(sender);
		this.forSale = forSale;
		this.price = price;
	}

	public String getForSale() {
		return this.forSale;
	}

	public int getPrice() {
		return this.price;
	}
	/**
	 * This method checks the content of the message in order to translate it properly in an action
	 */
	@Override
	public Action getAction(MessageTranslator translator) throws InvalidMessageException {
		return translator.translate(this);
	}
}
