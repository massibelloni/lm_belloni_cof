package it.polimi.ingsw.lm_belloni_cof.messages;

import java.io.Serializable;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;
/**
 * Abstract class for a message that replies to a {@link RequestMessage}
 */
public abstract class ResponseMessage implements Serializable {
	private static final long serialVersionUID = 1L;

	public abstract void visit(MessageVisitor visitor);
}
