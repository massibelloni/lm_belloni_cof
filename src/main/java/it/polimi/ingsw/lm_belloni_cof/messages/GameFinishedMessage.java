package it.polimi.ingsw.lm_belloni_cof.messages;

import java.util.Map;

import it.polimi.ingsw.lm_belloni_cof.view.playerside.MessageVisitor;
/**
 * This {@link BroadcastMessage} is sent to all the players when one of them win the current game
 *
 */
public class GameFinishedMessage extends BroadcastMessage {
	private static final long serialVersionUID = 1L;
	private Map<String, Integer> scores;
	private String winner;
	/**
	 * Public constructor of this {@link BroadcastMessage}
	 * @param scores is a {@link Map} that contains usernames as a key and final score as a values
	 * @param winner Username of the winning player
	 */
	public GameFinishedMessage(Map<String, Integer> scores, String winner) {
		this.scores = scores;
		this.winner = winner;
	}

	public int getScore(String username) {
		return this.scores.get(username).intValue();
	}

	public Map<String, Integer> getScores() {
		return this.scores;
	}

	public String getWinner() {
		return this.winner;
	}
	/**
	 * This method checks the content of the message in order to show it properly
	 */
	@Override
	public void visit(MessageVisitor visitor) {
		visitor.display(this);
	}

}
