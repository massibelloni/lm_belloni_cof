package it.polimi.ingsw.lm_belloni_cof.messages.actionrequests;

import it.polimi.ingsw.lm_belloni_cof.controller.MessageTranslator;
import it.polimi.ingsw.lm_belloni_cof.controller.actions.Action;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidMessageException;
import it.polimi.ingsw.lm_belloni_cof.messages.PlayerToken;
/**
 * This message is sent to the server-side from a {@link Player} 
 * who wants to specify an already owned {@link Permit} in order to obtain its {@link Bonus}es
 *
 */
public class OwnedPermitResponseActRqstMsg extends ActionRequestMessage {
	private static final long serialVersionUID = 1L;
	private String permit;
	/**
	 * Public constructor of {@link OwnedPermitResponseActRqstMsg}
	 * @param sender {@link Player} who has submitted the message
	 * @param permit {@link String} that represents the chosen {@link Permit} between the owned {@link Permit}, 
	 * 					the right notation is "permit+indexInOwnedPermitHand"
	 */
	public OwnedPermitResponseActRqstMsg(PlayerToken sender, String permit) {
		super(sender);
		this.permit = permit;
	}

	public String getPermit() {
		return permit;
	}
	/**
	 * This method checks the content of the message in order to translate it properly in an action
	 */
	@Override
	public Action getAction(MessageTranslator translator) throws InvalidMessageException {
		return translator.translate(this);
	}

}
