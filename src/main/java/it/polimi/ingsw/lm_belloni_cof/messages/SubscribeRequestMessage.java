package it.polimi.ingsw.lm_belloni_cof.messages;

import java.io.Serializable;

public class SubscribeRequestMessage implements Serializable {
	private static final long serialVersionUID = 1L;
	private String username;

	public SubscribeRequestMessage(String username) {
		this.username = username;
	}

	public String getUsername() {
		return this.username;
	}

}
