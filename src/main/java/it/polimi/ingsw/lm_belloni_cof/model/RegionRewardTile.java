package it.polimi.ingsw.lm_belloni_cof.model;

/**
 * Represents the object physically placed on the {@link RegionBoard} and that
 * may be obtained by the {@link Player} that builds an emporium on each city of
 * the {@link Region} defined in the {@link RegionRewardTileContent}.
 * 
 * @see Game#buildAnEmporium(Player,City)
 */
public class RegionRewardTile extends RewardTile<RegionRewardTileContent> {
	/**
	 * Constructor of the RewardTile. A RegionRewardTileContent is needed and it
	 * will be "printed" on the card representing its content
	 * 
	 * @param rewardContent
	 *            a well formed RegionRewardTileContent
	 */
	public RegionRewardTile(RegionRewardTileContent rewardContent) {
		super(rewardContent);
	}

	/**
	 * Returns the {@link Region} of the RegionRewardTileContent printed on the
	 * RegionRewardTile.
	 * 
	 * @return {@link Region} of the RegionRewardTile.
	 */
	public Region getRegion() {
		return super.rewardContent.getRegion();
	}
}
