package it.polimi.ingsw.lm_belloni_cof.model;

import java.util.List;

/**
 * Implementation of {@link Board}, represents the actual region board where a
 * {@link Balcony}, a {@link PermitBoard} and a {@link RegionRewardTile} are
 * physically placed.
 *
 */
public class RegionBoard extends Board {
	/**
	 * The {@link Region} of the RegionBoard.
	 */
	private Region region;
	/**
	 * The {@link PermitBoard}, only one for each {@link Region}.
	 */
	private PermitBoard permitBoard;
	/**
	 * The {@link RegionRewardTile} obtained when a {@link Player} builds an
	 * emporium on each {@link City} of the {@link Region}.
	 * 
	 * @see Game#buildAnEmporium(Player,City)
	 */
	private RegionRewardTile regionRewardTile;

	/**
	 * Constructor of a RegionBoard
	 * 
	 * @param gameMap
	 *            a GameMap object where are stored informations about the
	 *            {@link RegionRewardTileContent} and the {@link PermitContent}
	 *            of this region.
	 * @param region
	 *            the {@link Region} of the RegionBoard
	 * @param balcony
	 *            an already well formed {@link Balcony}
	 * @param numberOfPlayers
	 *            number of players involved in this game, important to decide
	 *            rules about the {@link PermitContent] to {@link Permit}
	 *            assignment rules.
	 */
	public RegionBoard(Region region, List<PermitContent> permitContents,
			RegionRewardTileContent regionRewardTileContent, Balcony balcony, int numberOfPlayers) {
		super(balcony);
		this.region = region;
		this.regionRewardTile = new RegionRewardTile(regionRewardTileContent);
		this.permitBoard = new PermitBoard(PermitDeck.getPermitDeckInstance(permitContents, numberOfPlayers));
	}

	/**
	 * Returns the {@link Region} of this RegionBoard
	 * 
	 * @return the {@link Region} of this RegionBoard
	 */
	public Region getRegion() {
		return region;
	}

	/**
	 * Returns the {@link PermitBoard} for operations about the {@link @Permit}
	 * placed on this RegionBoard.
	 * 
	 * @return a well formed {@link PermitBoard} with {@link Permit} of
	 *         {@link Region}
	 */
	public PermitBoard getPermitBoard() {
		return permitBoard;
	}

	/**
	 * Return the {@link RegionRewardTile} placed on this RegionBoard.
	 * 
	 * @return the {@link RegionRewardTile} placed on this RegionBoard.
	 */
	public RegionRewardTile getRegionRewardTile() {
		return this.regionRewardTile;
	}
}
