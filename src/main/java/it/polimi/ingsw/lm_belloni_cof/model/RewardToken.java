package it.polimi.ingsw.lm_belloni_cof.model;

/**
 * A RewardToken defined in a {@link GameMap} to be placed on a {@link City}
 * during {@link Game} initialization.
 *
 */
public class RewardToken extends TileContent {
	/**
	 * Defines an empty RewardToken.
	 */
	public RewardToken() {
		super();
	}
}
