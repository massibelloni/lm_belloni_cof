package it.polimi.ingsw.lm_belloni_cof.model;

import java.util.List;

/**
 * The main abstraction for a RewardTile. This type will be actually
 * instantiated by objects that will be "phisically" used by a {@link Player}.
 * For example {@link RegionRewardTile}.
 * 
 * @param <T>
 *            The type of {@link RewardTileContent} printed on the RewardTile
 *            (ex. {@link ColorRewardTileContent})
 */
public abstract class RewardTile<T extends RewardTileContent> implements Usable, Ownable {
	/**
	 * The {@link RewardTileContent} where the bonuses ({@link Bonus}) are
	 * printed
	 */
	protected T rewardContent;
	/**
	 * Parameter due to the {@link Usable} interface implementation: true if the
	 * RewardTile has already been used, false otherwise
	 */
	private boolean used;
	/**
	 * Parameter due to the {@link Ownable} interface implementation: null if
	 * not owned.
	 */
	private Player owner;

	/**
	 * Constructor of the RewardTile used by subclasses.
	 * 
	 * @param rewardContent
	 *            is the implementation of a RewardContent with bonuses (
	 *            {@link Bonus}) or other informations depending on the dynamic
	 *            type.
	 */
	public RewardTile(T rewardContent) {
		this.used = false;
		this.owner = null;
		this.rewardContent = rewardContent;
	}

	/**
	 * Returns the list of {@link Bonus} of the reward tile.
	 * 
	 * @return the list of {@link Bonus} printed on the tile.
	 */
	public List<Bonus> getBonuses() {
		return this.rewardContent.getBonuses();
	}

	/**
	 * Method that returns if the usable object has been used
	 * 
	 * @return true or false whether the object has been used or not
	 * @see Usable
	 */
	@Override
	public boolean hasBeenUsed() {
		return this.used;
	}

	/**
	 * Set the usable object to "used" state. This operation cannot be undone.
	 * 
	 * @see Usable
	 */
	@Override
	public void setUsage() {
		this.used = true;

	}

	/**
	 * Get if the RewardTile has an owner.
	 */
	@Override
	public boolean hasAnOwner() {
		if (this.owner != null)
			return true;
		return false;
	}

	/**
	 * Get the owner of the RewardTile, null if {@link #hasAnOwner()} is false
	 */
	@Override
	public Player getOwner() {
		return this.owner;
	}

	/**
	 * Set the owner of the RewardTile. Set to null if it hasn't an owner
	 */
	@Override
	public void setOwner(Player owner) {
		this.owner = owner;

	}

}
