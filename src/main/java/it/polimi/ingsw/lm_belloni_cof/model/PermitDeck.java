package it.polimi.ingsw.lm_belloni_cof.model;

import java.util.List;

/**
 * Implementation of a {@link Deck} of {@link Permit}
 *
 */
public class PermitDeck extends Deck<Permit> {
	/**
	 * Number of permit tiles per {@link Region} in a standard four players
	 * game.
	 */
	public final static int FOUR_PLAYERS_PERMIT = 15;
	/**
	 * Number of permit tiles per {@link Region} per player (theoretically) in a
	 * non-standard game (more than 4 players).
	 */
	public final static int PER_PLAYER_PERMIT = 4;

	/**
	 * Private constructor that initialize an empty Deck
	 */
	private PermitDeck() {
		super();
	}

	/**
	 * The only way to get a PermitDeck instance based on the
	 * {@link PermitContent}s received as parameters. The number of players of
	 * the game is also needed for special rules when there are more than four
	 * players. This method "prints" on the empty card the contents defined in
	 * {@link PermitContent}s defined in a {@link GameMap}. If the number of
	 * {@link PermitContent} defined is not enough for a the number of permits
	 * needed in the game some contents are duplicated randomly to reach the
	 * quota needed.
	 * 
	 * @param permitContents
	 *            a list of {@link PermitContent}. No special constraint on the
	 *            content.
	 * @param numberOfPlayers
	 *            the number of players of the game.
	 * @return a well formed and ready-to-use PermitDeck
	 */
	public static PermitDeck getPermitDeckInstance(List<PermitContent> permitContents, int numberOfPlayers) {
		int permitsPerRegion;
		if (numberOfPlayers <= 4) {
			permitsPerRegion = PermitDeck.FOUR_PLAYERS_PERMIT;
		} else {
			permitsPerRegion = PermitDeck.PER_PLAYER_PERMIT*numberOfPlayers;
		}
		PermitDeck permitDeck = new PermitDeck();
		if (permitContents.size() >= permitsPerRegion) {
			for (PermitContent permitContent : permitContents) {
				permitDeck.discard(new Permit(permitContent));
			}
		} else {
			// i've to duplicate some PermitContent to make the Game
			// playable
			// first: adding all the PermitContent available
			int added = 0;
			for (PermitContent permitContent : permitContents) {
				permitDeck.discard(new Permit(permitContent));
				added++;
			}
			// randomly add PermitContents from the List to obtain
			// FOUR_PLAYERS_PERMIT permit cards
			while (added < permitsPerRegion) {
				permitDeck.discard(new Permit(permitContents.get(((int) (Math.random() * permitContents.size())))));
				added++;
			}
		}
		permitDeck.shuffle();
		return permitDeck;

	}
}
