package it.polimi.ingsw.lm_belloni_cof.model;
/**
 * All possible colors that a {@link City} may be.
 *
 */
public enum CityColor {
	RED, YELLOW, GRAY, CYAN, PURPLE
}
