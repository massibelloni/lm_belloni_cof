package it.polimi.ingsw.lm_belloni_cof.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import it.polimi.ingsw.lm_belloni_cof.exceptions.GameMapFormatException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidActionException;

/**
 * GameMapFactory is the class that generates GameMap instances. It has
 * hard-coded some maps usable during a {@link Game} and a XML-GameMap. If a
 * {@link GameMap} with a certain uniqueId has been already instantiated (and
 * it's stored in memory) it won't be loaded again. The {@link Game} object (and
 * the whole implementation of the project) uses the {@link GameMap} only as an
 * unmodifiable reference and doesn't change the state of its objects during the
 * gameflow.
 *
 */
public class GameMapFactory {
	private static final Logger logger = Logger.getLogger(GameMapFactory.class.getName());
	/**
	 * The data structure where all the already-loaded {@link GameMap}s are
	 * stored.
	 */
	private Map<String, GameMap> alreadyLoaded;

	/**
	 * Public constructor that instantiates an empty GameMapFactory with no
	 * {@link GameMap}s loaded.
	 **/
	public GameMapFactory() {
		this.alreadyLoaded = new HashMap<String, GameMap>();
	}

	/**
	 * The only way coded to have a ready-to-use {@link GameMap}.
	 * 
	 * @param mapId
	 *            the default identifier of a {@link GameMap}.
	 * @return the {@link GameMap} created by the factory.
	 */
	public GameMap getGameMap(String mapId) {
		if (alreadyLoaded.containsKey(mapId))
			return alreadyLoaded.get(mapId);
		switch (mapId) {
		case "default":
			GameMap defaultMap = createDefaultMap();
			this.alreadyLoaded.put(mapId, defaultMap);
			return defaultMap;

		default:
			GameMap xmlGeneratedGameMap;
			try {
				xmlGeneratedGameMap = this.fromXMLToGameMap(mapId);
			} catch (GameMapFormatException e) {
				logger.log(Level.INFO, mapId + " doesn't exist or isn't well formed.", e);
				xmlGeneratedGameMap = createDefaultMap();
			}
			this.alreadyLoaded.put(mapId, xmlGeneratedGameMap);
			return xmlGeneratedGameMap;
		}
	}

	private GameMap fromXMLToGameMap(String path) throws GameMapFormatException {
		File xmlToConvert = new File(path);
		DocumentBuilderFactory dbFactory = null;
		DocumentBuilder dBuilder = null;
		Document document = null;
		try {
			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
			document = dBuilder.parse(xmlToConvert);
			document.getDocumentElement().normalize();
		} catch (FileNotFoundException e) {
			throw new GameMapFormatException(path + " not found.", e);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			throw new GameMapFormatException("Error during xml parsing procedure.", e);
		}
		GameMap customGameMap = new GameMap();
		// ADD CITIES TO THE MAP and CONNECTIONS
		// Access to father node cities
		NodeList citiesNodeList = document.getElementsByTagName("cities");
		// Select the first instance of cities (the first and also the only)
		Node citiesPoolNode = citiesNodeList.item(0);
		// Go into the node, now i can see a node for each city
		Element cities = (Element) citiesPoolNode;
		// Take all the element with tag city
		NodeList cityFields = cities.getElementsByTagName("city");
		// How many cities?
		int numberOfCities = cityFields.getLength();
		// For each city i have to know: name, region and color
		for (int i = 0; i < numberOfCities; i++) {
			// Creating an element to explore the city sub-tags
			// On item(i) in order to navigate all instance of city specified in
			// the xml file, the item is the single
			// tag open-tag close with tag name city.
			Element city = (Element) cityFields.item(i);
			// Get name
			String cityName = city.getElementsByTagName("name").item(0).getTextContent();
			// Get region
			String cityRegionString = city.getElementsByTagName("region").item(0).getTextContent();
			Region cityRegion = null;
			try {
				cityRegion = Region.valueOf(cityRegionString.toUpperCase());
			} catch (IllegalArgumentException e) {
				throw new GameMapFormatException(cityRegionString + " is not a known region.", e);
			}
			// Get color
			String cityColorString = city.getElementsByTagName("color").item(0).getTextContent();
			CityColor cityColor = null;
			try {
				cityColor = CityColor.valueOf(cityColorString.toUpperCase());
			} catch (IllegalArgumentException e) {
				throw new GameMapFormatException(cityColorString + " is not a known color.", e);
			}
			// Get bonus number
			int numberOfBonus = Integer.parseInt(city.getElementsByTagName("bonusnumber").item(0).getTextContent());
			// Create city and add it to the map
			City newCity = new City(cityName, numberOfBonus, cityColor, cityRegion);
			customGameMap.addACity(newCity);
		}

		// Now i have to add connections between cities

		// For all cit in city list
		// Searching the current city by name (now i use the quicker method
		// below -line 109-,but i'm not sure it works
		// with another xml file so i write also this.If the other method
		// doesn't works, this surely does.)

		for (int i = 0; i < numberOfCities; i++) {
			Element cityToConnect = (Element) cityFields.item(i);

			// String
			// name=cityToConnect.getElementsByTagName("name").item(0).getTextContent();
			// Instruction related to
			// altrnative method

			// City currentCity=null; Instruction related to the alternative
			// method

			// Searching the current city by name (now i use the quicker method
			// below -line 118-,but i'm not sure it works
			// with another xml file so i write also this.If the other method
			// doesn't works, this surely does.)

			/*
			 * for(City city:customGameMap.getCityList()){
			 * if(name.equals(city.getName())) currentCity=city; }
			 */

			int numberOfConnections = cityToConnect.getElementsByTagName("connection").getLength();
			// I can assume that the current city, that i'm considering is the
			// i-esima city in the city list
			// of the gamemap because the xml file is the same, so i scan it in
			// the same order.
			City currentCity = customGameMap.getCityList().get(i);
			// for all connection that i have for this city
			for (int j = 0; j < numberOfConnections; j++) {
				// Name of cities connected to the city that i consider now
				String cityConnectedString = cityToConnect.getElementsByTagName("connection").item(j).getTextContent();
				for (City city : customGameMap.getCityList()) {
					// Search city by name in the gamemap's city-list
					if (cityConnectedString.equals(city.getName()) && city != currentCity) {
						// When i find it, add connection
						customGameMap.addAConnectionBetween(currentCity, city);
					}
				}
			}
		}
		// CITIES CORRECTLY INSTANCIATED, PLACED AND CONNECTED ON THE MAP

		// PERMIT

		// Access to nodes that have tag <permitcontents>
		NodeList permitContentsList = document.getElementsByTagName("permitcontents");
		// Access to the specific chose node, the first (and only)
		Node permitContentsNode = permitContentsList.item(0);
		// Look inside the element of permitContentsNode
		Element permitContents = (Element) permitContentsNode;
		NodeList permitContent = permitContents.getElementsByTagName("permitcontent");
		int numberOfPermitContent = permitContent.getLength();
		try {
			for (int i = 0; i < numberOfPermitContent; i++) {
				// Go into the single permit content
				Element permitElement = (Element) permitContent.item(i);
				// Chose an element by tag, select item 0 (there's only one
				// element
				// with tag <region>)
				// and take his value
				String permitRegionString = permitElement.getElementsByTagName("region").item(0).getTextContent();
				Region permitRegion = Region.valueOf(permitRegionString.toUpperCase());

				// How many city i have on this permit content?
				int numberOfCitiesOnPermit = permitElement.getElementsByTagName("city").getLength();
				List<String> citiesOnPermitString = new ArrayList<>();
				for (int j = 0; j < numberOfCitiesOnPermit; j++)
					citiesOnPermitString.add(permitElement.getElementsByTagName("city").item(j).getTextContent());
				// I have placed the name of the cities to print on this permit
				// in my collection
				// now I have to convert the name in the correspondent city
				// object
				List<City> citiesOnPermit = new ArrayList<>();
				for (String name : citiesOnPermitString) {
					// i've to check if the following cities are already in the
					// map.
					// if the cities aren't on the map i can't print it on a
					// permit content
					for (City city : customGameMap.getCityList()) {
						if (name.equals(city.getName()))
							citiesOnPermit.add(city);
					}
				}

				// here i have the List of the cities to be added to the
				// following
				// permit

				// Now i need the bonuses placed on

				// I have to access to the node bonus before see the effectively
				// bonus values
				Node permitBonus = permitElement.getElementsByTagName("bonus").item(0);
				Element permitBonusElement = (Element) permitBonus;
				int coinsBonus = Integer
						.parseInt(permitBonusElement.getElementsByTagName("coins").item(0).getTextContent());
				int assistantsBonus = Integer
						.parseInt(permitBonusElement.getElementsByTagName("assistants").item(0).getTextContent());
				int victoryBonus = Integer
						.parseInt(permitBonusElement.getElementsByTagName("victory").item(0).getTextContent());
				int nobilityBonus = Integer
						.parseInt(permitBonusElement.getElementsByTagName("nobility").item(0).getTextContent());
				int politicBonus = Integer
						.parseInt(permitBonusElement.getElementsByTagName("politic").item(0).getTextContent());
				// Now i have to create the bonuses
				List<Bonus> permitTileBonuses = new ArrayList<>();
				if (coinsBonus > 0)
					permitTileBonuses.add(new Bonus(BonusType.COIN, coinsBonus));
				if (assistantsBonus > 0)
					permitTileBonuses.add(new Bonus(BonusType.ASSISTANT, assistantsBonus));
				if (victoryBonus > 0)
					permitTileBonuses.add(new Bonus(BonusType.VICTORY_POINT, victoryBonus));
				if (nobilityBonus > 0)
					permitTileBonuses.add(new Bonus(BonusType.NOBILITY, nobilityBonus));
				if (politicBonus > 0)
					permitTileBonuses.add(new Bonus(BonusType.POLITIC, politicBonus));
				// now i can create the permit content and add it to the map
				// Creating
				PermitContent newPermitContent = new PermitContent(permitRegion);
				for (City city : citiesOnPermit)
					newPermitContent.addCity(city);
				for (Bonus bonus : permitTileBonuses)
					newPermitContent.addBonusToTileContent(bonus);
				// Adding to the map
				customGameMap.addAPermitContent(newPermitContent);
			}
		} catch (Exception e) {
			throw new GameMapFormatException("Wrong permit bonus format.", e);
		}
		// PERMIT CONTENT CORRECTLY INTANCIATED AND ADDED TO THE GAMEMAP
		try {
			// KINGREWARD

			// Access to the main node tagged by <kingrewardpile>
			Node kingRewardPileContentNode = document.getElementsByTagName("kingrewardpile").item(0);
			// Take his element in order to inspect it inside
			Element kingRewardPile = (Element) kingRewardPileContentNode;
			// how many kingRewardContentTile have I inside the pile?
			int numberOfKingRewardTileContent = kingRewardPile.getElementsByTagName("kingrewardtilecontent")
					.getLength();
			// Access to the list of kingRewadTileContent
			NodeList kingRewardTile = kingRewardPile.getElementsByTagName("kingrewardtilecontent");
			for (int i = 0; i < numberOfKingRewardTileContent; i++) {
				// Take the element of every kingRewardTileContent inside the
				// list
				// iteratively,
				// in order to look into his relative sub-tags
				Element kingRewardTileElement = (Element) kingRewardTile.item(i);
				int pickingOrder = Integer
						.parseInt(kingRewardTileElement.getElementsByTagName("picking").item(0).getTextContent());
				// Control the new kingRewardTileContent pickingOrder in order
				// to
				// prevent that the player subimt
				// two different tiles with the same pickingOrder
				List<Integer> alreadyUsedPosition = new ArrayList<>();
				if (!(alreadyUsedPosition.contains(pickingOrder))) {
					alreadyUsedPosition.add(pickingOrder);
					int kingRewardTileCoinsBonus = Integer
							.parseInt(kingRewardTileElement.getElementsByTagName("coins").item(0).getTextContent());
					int kingRewardTileAssistantsBonus = Integer.parseInt(
							kingRewardTileElement.getElementsByTagName("assistants").item(0).getTextContent());
					int kingRewardTileVictoryBonus = Integer
							.parseInt(kingRewardTileElement.getElementsByTagName("victory").item(0).getTextContent());
					int kingRewardTileNobilityBonus = Integer
							.parseInt(kingRewardTileElement.getElementsByTagName("nobility").item(0).getTextContent());
					int kingRewardTilePoliticBonus = Integer
							.parseInt(kingRewardTileElement.getElementsByTagName("politic").item(0).getTextContent());
					// create the set of bonuses for thi tile
					List<Bonus> kingRewardTileBonuses = new ArrayList<>();
					if (kingRewardTileCoinsBonus > 0)
						kingRewardTileBonuses.add(new Bonus(BonusType.COIN, kingRewardTileCoinsBonus));
					if (kingRewardTileAssistantsBonus > 0)
						kingRewardTileBonuses.add(new Bonus(BonusType.ASSISTANT, kingRewardTileAssistantsBonus));
					if (kingRewardTileVictoryBonus > 0)
						kingRewardTileBonuses.add(new Bonus(BonusType.VICTORY_POINT, kingRewardTileVictoryBonus));
					if (kingRewardTileNobilityBonus > 0)
						kingRewardTileBonuses.add(new Bonus(BonusType.NOBILITY, kingRewardTileNobilityBonus));
					if (kingRewardTilePoliticBonus > 0)
						kingRewardTileBonuses.add(new Bonus(BonusType.POLITIC, kingRewardTilePoliticBonus));

					// Create the king tile content
					KingRewardTileContent newKingRewardTileContent = new KingRewardTileContent(pickingOrder);
					// Adding bonus to it
					for (Bonus bonus : kingRewardTileBonuses) {
						newKingRewardTileContent.addBonusToTileContent(bonus);
					}
					// Loaded in the gamemap
					try {
						customGameMap.addAKingRewardTileContent(newKingRewardTileContent);
					} catch (InvalidActionException e) {
						logger.log(Level.SEVERE, "KingRewardTile in wrong order during custom map creation.", e);
					}
				} else {
					throw new GameMapFormatException("Wrong king reward tile contents format: same picking order.");
				}
			}
		} catch (Exception e) {
			throw new GameMapFormatException("Wrong king reward tile contents format.", e);
		}
		// KING REWARD TILE CONTENTS CORRECLTY CREATED AND LOADED IN THE MAP

		// REGION REWARD TILE
		// Access to the main node tagged by <regionrewardpile>
		Node regionRewardTilesContentNode = document.getElementsByTagName("regionrewardtiles").item(0);
		// Take his element in order to inspect it inside
		Element regionRewardPile = (Element) regionRewardTilesContentNode;
		// how many regionRewardContentTile have I inside the pile?
		int numberOfRegionRewardTileContent = regionRewardPile.getElementsByTagName("regionrewardtilecontent")
				.getLength();
		int numberOfRegions = Region.values().length - 1;// subtract king region
		List<String> alreadyUsedRegion = new ArrayList<>();
		// I go on only if i have the same number of content and region
		if (numberOfRegionRewardTileContent == numberOfRegions) {
			// Access to the list of regionRewadTileContent
			NodeList regionRewardTile = regionRewardPile.getElementsByTagName("regionrewardtilecontent");
			for (int i = 0; i < numberOfRegionRewardTileContent; i++) {
				// Take the element of every regionRewardTileContent inside the
				// list iteratively,
				// in order to look into his relative sub-tags
				Element regionRewardTileElement = (Element) regionRewardTile.item(i);
				// Checking attribute region
				String regionRewardTileRegionString = regionRewardTileElement.getElementsByTagName("region").item(0)
						.getTextContent();
				// No two tiles on a region
				Region regionRewardTileRegion = null;
				if (!(alreadyUsedRegion.contains(regionRewardTileRegionString))) {
					alreadyUsedRegion.add(regionRewardTileRegionString);
					// Mapping the string in the file in a Region object
					try {
						regionRewardTileRegion = Region.valueOf(regionRewardTileRegionString.toUpperCase());
					} catch (IllegalArgumentException e) {
						throw new GameMapFormatException(regionRewardTileRegionString + " is not a known region.", e);
					}
				} else {
					throw new GameMapFormatException(
							"Not possible to create two region reward tile contents for a single region");
				}
				try {
					// Now Bonuses
					Node regionRewardTileBonus = regionRewardTileElement.getElementsByTagName("bonus").item(0);
					Element regionRewardTileBonusElement = (Element) regionRewardTileBonus;
					int regionRewardTileCoinsBonus = Integer.parseInt(
							regionRewardTileBonusElement.getElementsByTagName("coins").item(0).getTextContent());
					int regionRewardTileAssistantsBonus = Integer.parseInt(
							regionRewardTileBonusElement.getElementsByTagName("assistants").item(0).getTextContent());
					int regionRewardTileVictoryBonus = Integer.parseInt(
							regionRewardTileBonusElement.getElementsByTagName("victory").item(0).getTextContent());
					int regionRewardTileNobilityBonus = Integer.parseInt(
							regionRewardTileBonusElement.getElementsByTagName("nobility").item(0).getTextContent());
					int regionRewardTilePoliticBonus = Integer.parseInt(
							regionRewardTileBonusElement.getElementsByTagName("politic").item(0).getTextContent());
					// create the set of bonuses for this tile
					List<Bonus> regionRewardTileBonuses = new ArrayList<>();
					if (regionRewardTileCoinsBonus > 0)
						regionRewardTileBonuses.add(new Bonus(BonusType.COIN, regionRewardTileCoinsBonus));
					if (regionRewardTileAssistantsBonus > 0)
						regionRewardTileBonuses.add(new Bonus(BonusType.ASSISTANT, regionRewardTileAssistantsBonus));
					if (regionRewardTileVictoryBonus > 0)
						regionRewardTileBonuses.add(new Bonus(BonusType.VICTORY_POINT, regionRewardTileVictoryBonus));
					if (regionRewardTileNobilityBonus > 0)
						regionRewardTileBonuses.add(new Bonus(BonusType.NOBILITY, regionRewardTileNobilityBonus));
					if (regionRewardTilePoliticBonus > 0)
						regionRewardTileBonuses.add(new Bonus(BonusType.POLITIC, regionRewardTilePoliticBonus));

					// now I can create the new content
					RegionRewardTileContent newRegionRewardTileContent = new RegionRewardTileContent(
							regionRewardTileRegion);
					for (Bonus bonus : regionRewardTileBonuses)
						newRegionRewardTileContent.addBonusToTileContent(bonus);
					// and add it to the map
					customGameMap.addARegionRewardTileContent(newRegionRewardTileContent);
				} catch (Exception e) {
					throw new GameMapFormatException("Wrong region reward tiles format.", e);
				}
			}
		} else {
			throw new GameMapFormatException("Error: more region reward tiles than regions");
		}

		// COLOR REWARD TILE
		Node colorRewardTilesContentNode = document.getElementsByTagName("colorrewardtiles").item(0);
		// Take his element in order to inspect it inside
		Element colorRewardPile = (Element) colorRewardTilesContentNode;
		// how many colorRewardContentTile have I inside the pile?
		int numberOfColorRewardTileContent = colorRewardPile.getElementsByTagName("colorrewardtilecontent").getLength();
		int numberOfColors = CityColor.values().length - 1; // subtract the
															// color purple
		List<String> alreadyUsedColor = new ArrayList<>();
		// I go on only if i have the same number of content and color
		if (numberOfColorRewardTileContent == numberOfColors) {
			// Access to the list of colorRewadTileContent
			NodeList colorRewardTile = colorRewardPile.getElementsByTagName("colorrewardtilecontent");
			for (int i = 0; i < numberOfColorRewardTileContent; i++) {
				// Take the element of every colorRewardTileContent inside the
				// list iteratively,
				// in order to look into his relative sub-tags
				Element colorRewardTileElement = (Element) colorRewardTile.item(i);
				// Checking attribute color
				String colorRewardTileColorString = colorRewardTileElement.getElementsByTagName("color").item(0)
						.getTextContent();
				// No two tiles on a color
				CityColor colorRewardTileColor = null;
				if (!(alreadyUsedColor.contains(colorRewardTileColorString))) {
					alreadyUsedColor.add(colorRewardTileColorString);
					// Mapping the string in the file in a Region object
					colorRewardTileColor = CityColor.valueOf(colorRewardTileColorString.toUpperCase());

				} else {
					throw new GameMapFormatException(
							"Not possible to create two color reward tile contents for a single color");
				}
				try {
					// Now Bonuses
					Node colorRewardTileBonus = colorRewardTileElement.getElementsByTagName("bonus").item(0);
					Element colorRewardTileBonusElement = (Element) colorRewardTileBonus;
					int colorRewardTileCoinsBonus = Integer.parseInt(
							colorRewardTileBonusElement.getElementsByTagName("coins").item(0).getTextContent());
					int colorRewardTileAssistantsBonus = Integer.parseInt(
							colorRewardTileBonusElement.getElementsByTagName("assistants").item(0).getTextContent());
					int colorRewardTileVictoryBonus = Integer.parseInt(
							colorRewardTileBonusElement.getElementsByTagName("victory").item(0).getTextContent());
					int colorRewardTileNobilityBonus = Integer.parseInt(
							colorRewardTileBonusElement.getElementsByTagName("nobility").item(0).getTextContent());
					int colorRewardTilePoliticBonus = Integer.parseInt(
							colorRewardTileBonusElement.getElementsByTagName("politic").item(0).getTextContent());
					// create the set of bonuses for this tile
					List<Bonus> colorRewardTileBonuses = new ArrayList<>();
					if (colorRewardTileCoinsBonus > 0)
						colorRewardTileBonuses.add(new Bonus(BonusType.COIN, colorRewardTileCoinsBonus));
					if (colorRewardTileAssistantsBonus > 0)
						colorRewardTileBonuses.add(new Bonus(BonusType.ASSISTANT, colorRewardTileAssistantsBonus));
					if (colorRewardTileVictoryBonus > 0)
						colorRewardTileBonuses.add(new Bonus(BonusType.VICTORY_POINT, colorRewardTileVictoryBonus));
					if (colorRewardTileNobilityBonus > 0)
						colorRewardTileBonuses.add(new Bonus(BonusType.NOBILITY, colorRewardTileNobilityBonus));
					if (colorRewardTilePoliticBonus > 0)
						colorRewardTileBonuses.add(new Bonus(BonusType.POLITIC, colorRewardTilePoliticBonus));

					// now I can create the new content
					ColorRewardTileContent newColorRewardTileContent = new ColorRewardTileContent(colorRewardTileColor);
					for (Bonus bonus : colorRewardTileBonuses)
						newColorRewardTileContent.addBonusToTileContent(bonus);
					// and add it to the map
					customGameMap.addAColorRewardTileContent(newColorRewardTileContent);
				} catch (Exception e) {
					throw new GameMapFormatException("Error in color reward tiles format.", e);
				}
			}
		} else {
			throw new GameMapFormatException("Error: more color reward tiles than colors");
		}
		try {
			// REWARD TOKEN
			Node rwdTokensNode = document.getElementsByTagName("rewardtokens").item(0);
			Element rwdTokensElement = (Element) rwdTokensNode;
			NodeList rwdTokens = rwdTokensElement.getElementsByTagName("rewardtoken");
			int numberOfRwdTokens = rwdTokens.getLength();
			for (int i = 0; i < numberOfRwdTokens; i++) {
				Element rwdToken = (Element) rwdTokens.item(i);
				int victoryRwdBonusNumber = Integer
						.parseInt(rwdToken.getElementsByTagName("victory").item(0).getTextContent());
				int drawPoliticRwdBonusNumber = Integer
						.parseInt(rwdToken.getElementsByTagName("politic").item(0).getTextContent());
				int coinsRwdBonusNumber = Integer
						.parseInt(rwdToken.getElementsByTagName("coins").item(0).getTextContent());
				int assistantsRwdBonusNumber = Integer
						.parseInt(rwdToken.getElementsByTagName("assistants").item(0).getTextContent());
				int nobilityTrackRwdBonusNumber = Integer
						.parseInt(rwdToken.getElementsByTagName("nobility").item(0).getTextContent());
				List<Bonus> bonusesOnThisRwd = new ArrayList<>();
				if (coinsRwdBonusNumber > 0)
					bonusesOnThisRwd.add(new Bonus(BonusType.COIN, coinsRwdBonusNumber));
				if (assistantsRwdBonusNumber > 0)
					bonusesOnThisRwd.add(new Bonus(BonusType.ASSISTANT, assistantsRwdBonusNumber));
				if (victoryRwdBonusNumber > 0)
					bonusesOnThisRwd.add(new Bonus(BonusType.VICTORY_POINT, victoryRwdBonusNumber));
				if (nobilityTrackRwdBonusNumber > 0)
					bonusesOnThisRwd.add(new Bonus(BonusType.NOBILITY, nobilityTrackRwdBonusNumber));
				if (drawPoliticRwdBonusNumber > 0)
					bonusesOnThisRwd.add(new Bonus(BonusType.POLITIC, drawPoliticRwdBonusNumber));

				RewardToken newRewardToken = new RewardToken();
				for (Bonus bonus : bonusesOnThisRwd)
					newRewardToken.addBonusToTileContent(bonus);
				customGameMap.addARewardToken(newRewardToken);
			}
		} catch (Exception e) {
			throw new GameMapFormatException("Error in reward tokens format", e);
		}
		try {
			// INITIAL SETTINGS
			Node initialAssignmentsNode = document.getElementsByTagName("initialassignments").item(0);
			Element initialAssignments = (Element) initialAssignmentsNode;
			int initialPoliticsPerPlayer = Integer
					.parseInt(initialAssignments.getElementsByTagName("initialpolitics").item(0).getTextContent());
			customGameMap.setInitialPoliticsPerPlayer(initialPoliticsPerPlayer);
			int initialAssistantsNumber = Integer
					.parseInt(initialAssignments.getElementsByTagName("initialassistants").item(0).getTextContent());
			customGameMap.setInitialAssistantsNumber(initialAssistantsNumber);
			int initialCoinAmount = Integer
					.parseInt(initialAssignments.getElementsByTagName("initialcoins").item(0).getTextContent());
			customGameMap.setInitialCoinsAmount(initialCoinAmount);
			int maxVictoryPoints = Integer
					.parseInt(initialAssignments.getElementsByTagName("maxvictory").item(0).getTextContent());
			customGameMap.setMaxVictoryPoints(maxVictoryPoints);
			int maxCoins = Integer
					.parseInt(initialAssignments.getElementsByTagName("maxcoins").item(0).getTextContent());
			customGameMap.setMaxCoins(maxCoins);
			int emporiumsPerPlayer = Integer
					.parseInt(initialAssignments.getElementsByTagName("emporiumsperplayer").item(0).getTextContent());
			customGameMap.setEmporiumsPerPlayer(emporiumsPerPlayer);
			String startingKingLocationString = initialAssignments.getElementsByTagName("kinglocation").item(0)
					.getTextContent();
			for (City city : customGameMap.getCityList()) {
				if (city.getName().equals(startingKingLocationString) && city.getCityColor() == CityColor.PURPLE) {
					customGameMap.setStartingKingLocation(city);
				} else {// in case of unknown, or not purple city, starting king
						// location is the purple city like in the rules
					for (City kingCity : customGameMap.getCityList()) {
						if (kingCity.getCityColor() == CityColor.PURPLE)
							customGameMap.setStartingKingLocation(kingCity);
					}
				}
			}
		} catch (Exception e) {
			throw new GameMapFormatException("Error in initial assigments format.", e);
		}

		// FINAL ASSIGNMENTS
		try {
			Node finalAssignmentsNode = document.getElementsByTagName("finalassignments").item(0);
			Element finalAssignments = (Element) finalAssignmentsNode;
			int lastEmporiumPoints = Integer
					.parseInt(finalAssignments.getElementsByTagName("lastemporium").item(0).getTextContent());
			int firstNobilityTrackPosition = Integer.parseInt(
					finalAssignments.getElementsByTagName("firstnobilitytrackposition").item(0).getTextContent());
			int secondNobilityTrackPosition = Integer.parseInt(
					finalAssignments.getElementsByTagName("secondnobilitytrackposition").item(0).getTextContent());
			int majorNumberOfPermits = Integer
					.parseInt(finalAssignments.getElementsByTagName("majornumberofpermits").item(0).getTextContent());
			FinalScoreRules finalScoreRules = new FinalScoreRules(lastEmporiumPoints, firstNobilityTrackPosition,
					secondNobilityTrackPosition, majorNumberOfPermits);
			customGameMap.setFinalRules(finalScoreRules);
		} catch (Exception e) {
			throw new GameMapFormatException("Error in final assignments format.", e);
		}

		// NOBILITY TRACK
		try {
			Node nobilityTrackNode = document.getElementsByTagName("nobilitytrack").item(0);
			Element nobilityTrackElement = (Element) nobilityTrackNode;
			int nobilityTrackSize = ((nobilityTrackNode.getChildNodes().getLength()) - 1) / 2;
			List<List<Bonus>> bonusesListsForTheTrack = new ArrayList<>();
			for (int i = 0; i < nobilityTrackSize; i++) {
				Element nobilityTrackPosition = (Element) nobilityTrackElement.getElementsByTagName("position").item(i);
				int permitBonusNumber = Integer
						.parseInt(nobilityTrackPosition.getElementsByTagName("ownedpermit").item(0).getTextContent());
				int victoryBonusNumber = Integer
						.parseInt(nobilityTrackPosition.getElementsByTagName("victory").item(0).getTextContent());
				int drawPermitBonusNumber = Integer
						.parseInt(nobilityTrackPosition.getElementsByTagName("drawpermit").item(0).getTextContent());
				int drawPoliticNumber = Integer
						.parseInt(nobilityTrackPosition.getElementsByTagName("drawpolitic").item(0).getTextContent());
				int addMainActionBonus = Integer
						.parseInt(nobilityTrackPosition.getElementsByTagName("addmainaction").item(0).getTextContent());
				int tokenFromCityWithAnEmporiumBonusNumber = Integer.parseInt(nobilityTrackPosition
						.getElementsByTagName("tokenfromcitywithanemporium").item(0).getTextContent());
				int coinsBonusNumber = Integer
						.parseInt(nobilityTrackPosition.getElementsByTagName("coins").item(0).getTextContent());
				int assistantsBonusNumber = Integer
						.parseInt(nobilityTrackPosition.getElementsByTagName("assistants").item(0).getTextContent());
				int nobilityTrackBonusNumber = Integer
						.parseInt(nobilityTrackPosition.getElementsByTagName("nobility").item(0).getTextContent());
				List<Bonus> bonusesInThisPosition = new ArrayList<>();
				if (permitBonusNumber > 0)
					bonusesInThisPosition.add(new Bonus(BonusType.PERMIT_BONUS, permitBonusNumber));
				if (victoryBonusNumber > 0)
					bonusesInThisPosition.add(new Bonus(BonusType.VICTORY_POINT, victoryBonusNumber));
				if (drawPermitBonusNumber > 0)
					bonusesInThisPosition.add(new Bonus(BonusType.TAKE_PERMIT, drawPermitBonusNumber));
				if (drawPoliticNumber > 0)
					bonusesInThisPosition.add(new Bonus(BonusType.POLITIC, drawPoliticNumber));
				if (addMainActionBonus > 0)
					bonusesInThisPosition.add(new Bonus(BonusType.MAIN_ACTION, addMainActionBonus));
				if (tokenFromCityWithAnEmporiumBonusNumber > 0)
					bonusesInThisPosition.add(new Bonus(BonusType.CITY_REWARD, tokenFromCityWithAnEmporiumBonusNumber));
				if (coinsBonusNumber > 0)
					bonusesInThisPosition.add(new Bonus(BonusType.COIN, coinsBonusNumber));
				if (assistantsBonusNumber > 0)
					bonusesInThisPosition.add(new Bonus(BonusType.ASSISTANT, assistantsBonusNumber));
				if (nobilityTrackBonusNumber > 0)
					bonusesInThisPosition.add(new Bonus(BonusType.NOBILITY, nobilityTrackBonusNumber));

				bonusesListsForTheTrack.add(bonusesInThisPosition);
			}
			NobilityTrack nobilityTrack = new NobilityTrack(bonusesListsForTheTrack);
			customGameMap.setNobilityTrack(nobilityTrack);
			// NOBILITY TRACK ADDED
		} catch (Exception e) {
			throw new GameMapFormatException("Error in nobility path format.", e);
		}
		return customGameMap;
	}

	/**
	 * Private method that creates the map shown on the instructions of Council
	 * of Four
	 * 
	 * @return the default GameMap for Council of Four.
	 */
	private GameMap createDefaultMap() {
		GameMap defaultMap = new GameMap();
		// setting general rules
		defaultMap.setEmporiumsPerPlayer(10);
		defaultMap.setInitialAssistantsNumber(1);
		defaultMap.setInitialCoinsAmount(10);
		defaultMap.setInitialPoliticsPerPlayer(6);
		defaultMap.setMaxCoins(20);
		defaultMap.setMaxVictoryPoints(99);
		FinalScoreRules defaultFinalScoreRules = new FinalScoreRules(3, 5, 2, 3);
		defaultMap.setFinalRules(defaultFinalScoreRules);
		// setting map and city connections
		City arkon = new City("Arkon", 1, CityColor.CYAN, Region.COAST);
		City burgen = new City("Burgen", 1, CityColor.YELLOW, Region.COAST);
		City castrum = new City("Castrum", 1, CityColor.RED, Region.COAST);
		City dorful = new City("Dorful", 1, CityColor.GRAY, Region.COAST);
		City esti = new City("Esti", 1, CityColor.GRAY, Region.COAST);
		City framek = new City("Framek", 1, CityColor.YELLOW, Region.HILLS);
		City graden = new City("Graden", 1, CityColor.GRAY, Region.HILLS);
		City hellar = new City("Hellar", 1, CityColor.YELLOW, Region.HILLS);
		City indur = new City("Indur", 1, CityColor.RED, Region.HILLS);
		City juvelar = new City("Juvelar", 1, CityColor.PURPLE, Region.HILLS);
		City kultos = new City("Kultos", 1, CityColor.YELLOW, Region.MOUNTAINS);
		City lyram = new City("Lyram", 1, CityColor.GRAY, Region.MOUNTAINS);
		City merkatim = new City("Merkatim", 1, CityColor.CYAN, Region.MOUNTAINS);
		City naris = new City("Naris", 1, CityColor.RED, Region.MOUNTAINS);
		City osium = new City("Osium", 1, CityColor.YELLOW, Region.MOUNTAINS);
		defaultMap.setStartingKingLocation(juvelar);
		defaultMap.addACity(arkon);
		defaultMap.addACity(burgen);
		defaultMap.addACity(castrum);
		defaultMap.addACity(dorful);
		defaultMap.addACity(esti);
		defaultMap.addACity(framek);
		defaultMap.addACity(graden);
		defaultMap.addACity(hellar);
		defaultMap.addACity(indur);
		defaultMap.addACity(juvelar);
		defaultMap.addACity(kultos);
		defaultMap.addACity(lyram);
		defaultMap.addACity(merkatim);
		defaultMap.addACity(naris);
		defaultMap.addACity(osium);
		defaultMap.addAConnectionBetween(arkon, burgen);
		defaultMap.addAConnectionBetween(arkon, castrum);
		defaultMap.addAConnectionBetween(burgen, dorful);
		defaultMap.addAConnectionBetween(burgen, esti);
		defaultMap.addAConnectionBetween(castrum, framek);
		defaultMap.addAConnectionBetween(dorful, graden);
		defaultMap.addAConnectionBetween(esti, hellar);
		defaultMap.addAConnectionBetween(framek, indur);
		defaultMap.addAConnectionBetween(graden, juvelar);
		defaultMap.addAConnectionBetween(hellar, juvelar);
		defaultMap.addAConnectionBetween(hellar, merkatim);
		defaultMap.addAConnectionBetween(indur, kultos);
		defaultMap.addAConnectionBetween(juvelar, indur);
		defaultMap.addAConnectionBetween(juvelar, lyram);
		defaultMap.addAConnectionBetween(kultos, naris);
		defaultMap.addAConnectionBetween(lyram, osium);
		defaultMap.addAConnectionBetween(naris, osium);
		defaultMap.addAConnectionBetween(osium, merkatim);
		// creating RewardTileContent
		// Region
		RegionRewardTileContent coastReward = new RegionRewardTileContent(Region.COAST);
		coastReward.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 5));
		RegionRewardTileContent hillsReward = new RegionRewardTileContent(Region.HILLS);
		hillsReward.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 5));
		RegionRewardTileContent mountainsReward = new RegionRewardTileContent(Region.MOUNTAINS);
		mountainsReward.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 5));
		defaultMap.addARegionRewardTileContent(coastReward);
		defaultMap.addARegionRewardTileContent(hillsReward);
		defaultMap.addARegionRewardTileContent(mountainsReward);
		// Color
		ColorRewardTileContent yellowReward = new ColorRewardTileContent(CityColor.YELLOW);
		yellowReward.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 20));
		ColorRewardTileContent grayReward = new ColorRewardTileContent(CityColor.GRAY);
		grayReward.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 12));
		ColorRewardTileContent redReward = new ColorRewardTileContent(CityColor.RED);
		redReward.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 8));
		ColorRewardTileContent cyanReward = new ColorRewardTileContent(CityColor.CYAN);
		cyanReward.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 5));
		defaultMap.addAColorRewardTileContent(yellowReward);
		defaultMap.addAColorRewardTileContent(grayReward);
		defaultMap.addAColorRewardTileContent(redReward);
		defaultMap.addAColorRewardTileContent(cyanReward);
		// King
		KingRewardTileContent k1 = new KingRewardTileContent(1);
		k1.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 25));
		KingRewardTileContent k2 = new KingRewardTileContent(2);
		k2.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 18));
		KingRewardTileContent k3 = new KingRewardTileContent(3);
		k3.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 12));
		KingRewardTileContent k4 = new KingRewardTileContent(4);
		k4.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 7));
		KingRewardTileContent k5 = new KingRewardTileContent(5);
		k5.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 3));
		try {
			defaultMap.addAKingRewardTileContent(k1);
			defaultMap.addAKingRewardTileContent(k2);
			defaultMap.addAKingRewardTileContent(k3);
			defaultMap.addAKingRewardTileContent(k4);
			defaultMap.addAKingRewardTileContent(k5);
		} catch (InvalidActionException e) {
			logger.log(Level.SEVERE, "KingRewardTile in wrong order during default map creation.", e);
		}
		// Permit
		PermitContent p1 = new PermitContent(Region.COAST);
		p1.addCity(dorful);
		p1.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 7));
		defaultMap.addAPermitContent(p1);

		PermitContent p2 = new PermitContent(Region.COAST);
		p2.addCity(arkon);
		p2.addBonusToTileContent(new Bonus(BonusType.COIN, 3));
		p2.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 4));
		defaultMap.addAPermitContent(p2);

		PermitContent p3 = new PermitContent(Region.COAST);
		p3.addCity(castrum);
		p3.addCity(dorful);
		p3.addCity(esti);
		p3.addBonusToTileContent(new Bonus(BonusType.NOBILITY, 1));
		p3.addBonusToTileContent(new Bonus(BonusType.POLITIC, 1));
		defaultMap.addAPermitContent(p3);

		PermitContent p4 = new PermitContent(Region.COAST);
		p4.addCity(burgen);
		p4.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 1));
		p4.addBonusToTileContent(new Bonus(BonusType.POLITIC, 3));
		defaultMap.addAPermitContent(p4);

		PermitContent p5 = new PermitContent(Region.COAST);
		p5.addCity(arkon);
		p5.addCity(dorful);
		p5.addCity(esti);
		p5.addBonusToTileContent(new Bonus(BonusType.COIN, 1));
		p5.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 2));
		defaultMap.addAPermitContent(p5);

		PermitContent p6 = new PermitContent(Region.COAST);
		p6.addCity(castrum);
		p6.addBonusToTileContent(new Bonus(BonusType.COIN, 3));
		p6.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 2));
		defaultMap.addAPermitContent(p6);

		PermitContent p7 = new PermitContent(Region.COAST);
		p7.addCity(arkon);
		p7.addCity(esti);
		p7.addBonusToTileContent(new Bonus(BonusType.NOBILITY, 2));
		defaultMap.addAPermitContent(p7);

		PermitContent p8 = new PermitContent(Region.COAST);
		p8.addCity(dorful);
		p8.addCity(esti);
		p8.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 1));
		p8.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 3));
		defaultMap.addAPermitContent(p8);

		PermitContent p9 = new PermitContent(Region.COAST);
		p9.addCity(burgen);
		p9.addCity(castrum);
		p9.addCity(dorful);
		p9.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 1));
		p9.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 1));
		defaultMap.addAPermitContent(p9);

		PermitContent p10 = new PermitContent(Region.COAST);
		p10.addCity(arkon);
		p10.addCity(burgen);
		p10.addCity(esti);
		p10.addBonusToTileContent(new Bonus(BonusType.COIN, 3));
		defaultMap.addAPermitContent(p10);

		PermitContent p11 = new PermitContent(Region.COAST);
		p11.addCity(burgen);
		p11.addCity(castrum);
		p11.addBonusToTileContent(new Bonus(BonusType.COIN, 3));
		p11.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 3));
		defaultMap.addAPermitContent(p11);

		PermitContent p12 = new PermitContent(Region.COAST);
		p12.addCity(castrum);
		p12.addCity(dorful);
		p12.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 1));
		p12.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 3));
		defaultMap.addAPermitContent(p12);

		PermitContent p13 = new PermitContent(Region.COAST);
		p13.addCity(arkon);
		p13.addCity(burgen);
		p13.addCity(castrum);
		p13.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 2));
		defaultMap.addAPermitContent(p13);

		PermitContent p14 = new PermitContent(Region.COAST);
		p14.addCity(arkon);
		p14.addCity(burgen);
		p14.addBonusToTileContent(new Bonus(BonusType.COIN, 3));
		p14.addBonusToTileContent(new Bonus(BonusType.POLITIC, 1));
		defaultMap.addAPermitContent(p14);

		PermitContent p15 = new PermitContent(Region.COAST);
		p15.addCity(esti);
		p15.addBonusToTileContent(new Bonus(BonusType.COIN, 2));
		defaultMap.addAPermitContent(p15);

		PermitContent p16 = new PermitContent(Region.HILLS);
		p16.addCity(framek);
		p16.addCity(juvelar);
		p16.addBonusToTileContent(new Bonus(BonusType.COIN, 1));
		p16.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 2));
		defaultMap.addAPermitContent(p16);

		PermitContent p17 = new PermitContent(Region.HILLS);
		p17.addCity(framek);
		p17.addCity(graden);
		p17.addBonusToTileContent(new Bonus(BonusType.COIN, 5));
		defaultMap.addAPermitContent(p17);

		PermitContent p18 = new PermitContent(Region.HILLS);
		p18.addCity(framek);
		p18.addCity(graden);
		p18.addCity(hellar);
		p18.addBonusToTileContent(new Bonus(BonusType.COIN, 1));
		p18.addBonusToTileContent(new Bonus(BonusType.NOBILITY, 1));
		defaultMap.addAPermitContent(p18);

		PermitContent p19 = new PermitContent(Region.HILLS);
		p19.addCity(framek);
		p19.addCity(indur);
		p19.addCity(juvelar);
		p19.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 1));
		p19.addBonusToTileContent(new Bonus(BonusType.NOBILITY, 1));
		defaultMap.addAPermitContent(p19);

		PermitContent p20 = new PermitContent(Region.HILLS);
		p20.addCity(framek);
		p20.addBonusToTileContent(new Bonus(BonusType.COIN, 4));
		p20.addBonusToTileContent(new Bonus(BonusType.POLITIC, 2));
		defaultMap.addAPermitContent(p20);

		PermitContent p21 = new PermitContent(Region.HILLS);
		p21.addCity(graden);
		p21.addCity(hellar);
		p21.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 3));
		defaultMap.addAPermitContent(p21);

		PermitContent p22 = new PermitContent(Region.HILLS);
		p22.addCity(hellar);
		p22.addCity(indur);
		p22.addCity(juvelar);
		p22.addBonusToTileContent(new Bonus(BonusType.POLITIC, 2));
		defaultMap.addAPermitContent(p22);

		PermitContent p23 = new PermitContent(Region.HILLS);
		p23.addCity(framek);
		p23.addCity(graden);
		p23.addCity(juvelar);
		p23.addBonusToTileContent(new Bonus(BonusType.COIN, 2));
		p23.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 1));
		defaultMap.addAPermitContent(p23);

		PermitContent p24 = new PermitContent(Region.HILLS);
		p24.addCity(indur);
		p24.addCity(juvelar);
		p24.addBonusToTileContent(new Bonus(BonusType.NOBILITY, 1));
		p24.addBonusToTileContent(new Bonus(BonusType.POLITIC, 2));
		defaultMap.addAPermitContent(p24);

		PermitContent p25 = new PermitContent(Region.HILLS);
		p25.addCity(graden);
		p25.addCity(hellar);
		p25.addCity(indur);
		p25.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 1));
		p25.addBonusToTileContent(new Bonus(BonusType.POLITIC, 1));
		defaultMap.addAPermitContent(p25);

		PermitContent p26 = new PermitContent(Region.HILLS);
		p26.addCity(hellar);
		p26.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 4));
		defaultMap.addAPermitContent(p26);

		PermitContent p27 = new PermitContent(Region.HILLS);
		p27.addCity(indur);
		p27.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 2));
		p27.addBonusToTileContent(new Bonus(BonusType.POLITIC, 2));
		defaultMap.addAPermitContent(p27);

		PermitContent p28 = new PermitContent(Region.HILLS);
		p28.addCity(hellar);
		p28.addCity(indur);
		p28.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 5));
		defaultMap.addAPermitContent(p28);

		PermitContent p29 = new PermitContent(Region.HILLS);
		p29.addCity(graden);
		p29.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 2));
		p29.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 2));
		p29.addBonusToTileContent(new Bonus(BonusType.NOBILITY, 1));
		defaultMap.addAPermitContent(p29);

		PermitContent p30 = new PermitContent(Region.HILLS);
		p30.addCity(juvelar);
		p30.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 2));
		defaultMap.addAPermitContent(p30);

		PermitContent p31 = new PermitContent(Region.MOUNTAINS);
		p31.addCity(merkatim);
		p31.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 5));
		p31.addBonusToTileContent(new Bonus(BonusType.NOBILITY, 1));
		defaultMap.addAPermitContent(p31);

		PermitContent p32 = new PermitContent(Region.MOUNTAINS);
		p32.addCity(kultos);
		p32.addCity(naris);
		p32.addCity(osium);
		p32.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 1));
		p32.addBonusToTileContent(new Bonus(BonusType.POLITIC, 1));
		defaultMap.addAPermitContent(p32);

		PermitContent p33 = new PermitContent(Region.MOUNTAINS);
		p33.addCity(lyram);
		p33.addBonusToTileContent(new Bonus(BonusType.COIN, 1));
		p33.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 3));
		defaultMap.addAPermitContent(p33);

		PermitContent p34 = new PermitContent(Region.MOUNTAINS);
		p34.addCity(lyram);
		p34.addCity(merkatim);
		p34.addCity(naris);
		p34.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 3));
		defaultMap.addAPermitContent(p34);

		PermitContent p35 = new PermitContent(Region.MOUNTAINS);
		p35.addCity(osium);
		p35.addBonusToTileContent(new Bonus(BonusType.NOBILITY, 1));
		p35.addBonusToTileContent(new Bonus(BonusType.POLITIC, 3));
		defaultMap.addAPermitContent(p35);

		PermitContent p36 = new PermitContent(Region.MOUNTAINS);
		p36.addCity(lyram);
		p36.addCity(merkatim);
		p36.addBonusToTileContent(new Bonus(BonusType.POLITIC, 3));
		defaultMap.addAPermitContent(p36);

		PermitContent p37 = new PermitContent(Region.MOUNTAINS);
		p37.addCity(kultos);
		p37.addCity(osium);
		p37.addBonusToTileContent(new Bonus(BonusType.NOBILITY, 1));
		p37.addBonusToTileContent(new Bonus(BonusType.POLITIC, 2));
		defaultMap.addAPermitContent(p37);

		PermitContent p38 = new PermitContent(Region.MOUNTAINS);
		p38.addCity(kultos);
		p38.addBonusToTileContent(new Bonus(BonusType.POLITIC, 4));
		defaultMap.addAPermitContent(p38);

		PermitContent p39 = new PermitContent(Region.MOUNTAINS);
		p39.addCity(naris);
		p39.addCity(osium);
		p39.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 2));
		p39.addBonusToTileContent(new Bonus(BonusType.POLITIC, 2));
		defaultMap.addAPermitContent(p39);

		PermitContent p40 = new PermitContent(Region.MOUNTAINS);
		p40.addCity(kultos);
		p40.addCity(lyram);
		p40.addCity(osium);
		p40.addBonusToTileContent(new Bonus(BonusType.COIN, 1));
		p40.addBonusToTileContent(new Bonus(BonusType.POLITIC, 1));
		defaultMap.addAPermitContent(p40);

		PermitContent p41 = new PermitContent(Region.MOUNTAINS);
		p41.addCity(merkatim);
		p41.addCity(naris);
		p41.addCity(osium);
		p41.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 1));
		p41.addBonusToTileContent(new Bonus(BonusType.NOBILITY, 1));
		defaultMap.addAPermitContent(p41);

		PermitContent p42 = new PermitContent(Region.MOUNTAINS);
		p42.addCity(kultos);
		p42.addCity(lyram);
		p42.addCity(merkatim);
		p42.addBonusToTileContent(new Bonus(BonusType.COIN, 1));
		p42.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 1));
		defaultMap.addAPermitContent(p42);

		PermitContent p43 = new PermitContent(Region.MOUNTAINS);
		p43.addCity(kultos);
		p43.addCity(lyram);
		p43.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 2));
		p43.addBonusToTileContent(new Bonus(BonusType.POLITIC, 1));
		defaultMap.addAPermitContent(p43);

		PermitContent p44 = new PermitContent(Region.MOUNTAINS);
		p44.addCity(naris);
		p44.addBonusToTileContent(new Bonus(BonusType.COIN, 7));
		defaultMap.addAPermitContent(p44);

		PermitContent p45 = new PermitContent(Region.MOUNTAINS);
		p45.addCity(merkatim);
		p45.addCity(naris);
		p45.addBonusToTileContent(new Bonus(BonusType.COIN, 3)); // ?
		defaultMap.addAPermitContent(p45);

		// RewardToken
		RewardToken r1 = new RewardToken();
		r1.addBonusToTileContent(new Bonus(BonusType.COIN, 3));
		defaultMap.addARewardToken(r1);

		RewardToken r2 = new RewardToken();
		r2.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 1));
		r2.addBonusToTileContent(new Bonus(BonusType.POLITIC, 1));
		defaultMap.addARewardToken(r2);

		RewardToken r3 = new RewardToken();
		r3.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 2));
		defaultMap.addARewardToken(r3);

		RewardToken r4 = new RewardToken();
		r4.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 1));
		defaultMap.addARewardToken(r4);

		RewardToken r5 = new RewardToken();
		r5.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 3));
		defaultMap.addARewardToken(r5);

		RewardToken r6 = new RewardToken();
		r6.addBonusToTileContent(new Bonus(BonusType.NOBILITY, 1));
		defaultMap.addARewardToken(r6);

		RewardToken r7 = new RewardToken();
		r7.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 1)); // ?
		defaultMap.addARewardToken(r7);

		RewardToken r8 = new RewardToken();
		r8.addBonusToTileContent(new Bonus(BonusType.COIN, 1));
		defaultMap.addARewardToken(r8);

		RewardToken r9 = new RewardToken();
		r9.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 2));
		defaultMap.addARewardToken(r9);

		RewardToken r10 = new RewardToken();
		r10.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 1));
		defaultMap.addARewardToken(r10);

		RewardToken r11 = new RewardToken();
		r11.addBonusToTileContent(new Bonus(BonusType.COIN, 2));
		defaultMap.addARewardToken(r11);

		RewardToken r12 = new RewardToken();
		r12.addBonusToTileContent(new Bonus(BonusType.POLITIC, 1));
		defaultMap.addARewardToken(r12);

		RewardToken r13 = new RewardToken();
		r13.addBonusToTileContent(new Bonus(BonusType.VICTORY_POINT, 1));
		r13.addBonusToTileContent(new Bonus(BonusType.POLITIC, 1));
		defaultMap.addARewardToken(r13);

		RewardToken r14 = new RewardToken();
		r14.addBonusToTileContent(new Bonus(BonusType.NOBILITY, 1));
		defaultMap.addARewardToken(r14);

		RewardToken r15 = new RewardToken();
		r15.addBonusToTileContent(new Bonus(BonusType.COIN, 1));
		r15.addBonusToTileContent(new Bonus(BonusType.ASSISTANT, 1));
		defaultMap.addARewardToken(r15);

		// NobilityTrack
		defaultMap.setNobilityTrack(NobilityTrack.getDefaultNobilityTrack());

		return defaultMap;
	}
}
