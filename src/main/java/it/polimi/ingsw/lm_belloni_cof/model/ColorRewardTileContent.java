package it.polimi.ingsw.lm_belloni_cof.model;
/**
 * This class represents the content that can be printed on a physical {@link ColroRewardTile}
 *
 */
public class ColorRewardTileContent extends RewardTileContent {
	/**
	 * Represents the {@link CityColor} associated to the RewartTile.
	 */
	private CityColor color;
	
	/**
	 * Constructor of a ColorRewardTileContent it associate a list of {@link Bonus} to a {@link CityColor}. 
	 * @param color Color of the ColorRewardTileContent chosen between {@link CityColor}. It can't be changed. 
	 */
	public ColorRewardTileContent(CityColor color) {
		super();
		this.color = color;
	}
	
	/**
	 * Returns the {@link CityColor} of the ColorRewardTileContent defined through the constructor. 
	 * @return {@CityColor} of the ColorRewardTileContent.
	 */
	public CityColor getColor() {
		return this.color;
	}
}
