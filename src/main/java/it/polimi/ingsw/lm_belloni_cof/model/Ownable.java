package it.polimi.ingsw.lm_belloni_cof.model;

/**
 * 
 * This interface represents every object that in the game may be owned by
 * someone (not matter who) such as {@link Permit} or {@link Politic}. In the
 * CoF gameflow an ownable object may not have an owner in a certain moment.
 *
 */
public interface Ownable {
	/**
	 * Get if the Ownable object has an owner.
	 */
	public boolean hasAnOwner();

	/**
	 * Get the owner of the Ownable object.
	 * 
	 * @return the owner of the object, might be null.
	 */
	public Player getOwner();

	/**
	 * Set the owner of the object. This operation overwrite the previous owner
	 * 
	 * @param owner
	 *            the new owner of an object
	 */
	public void setOwner(Player owner);
}
