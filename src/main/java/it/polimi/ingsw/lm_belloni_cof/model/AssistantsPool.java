package it.polimi.ingsw.lm_belloni_cof.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
/**
 * 
 * This class represents the set of already created available Assistants.
 *
 */
public class AssistantsPool {
	private Queue<Assistant> alreadyCreatedAssistants;
	
	/**
	 * This method creates a LinkedList where i can store the available Assistants.
	 */
	public AssistantsPool() {
		alreadyCreatedAssistants = new LinkedList<>();
	}
	
	/**
	 * This method checks if the set of available Assistants is empty or not, in order to return an {@link Assistant}, 
	 * in case of empty set this method generates a new {@link Assistant}.
	 * @return Assistant 
	 */
	public Assistant getAnAssistant() {
		if (this.alreadyCreatedAssistants.isEmpty())
			return new Assistant();
		return this.alreadyCreatedAssistants.remove();
	}
	
	/**
	 * This method checks if the set of available Assistants is empty or not, in order to, if it is possible, 
	 * return n Assistant from that set. If the set is empty or if it has a number of available Assistants lesser than n,
	 * it generates a necessary number of Assistants.
	 * @param n the number of required Assistants.
	 * @return a List of n Assistants.
	 */
	public List<Assistant> getNAssistants(int n) {
		int assistantsLeft = n;
		List<Assistant> assistants = new ArrayList<>();
		while (assistantsLeft > 0 && !this.alreadyCreatedAssistants.isEmpty()) {
			assistants.add(this.alreadyCreatedAssistants.remove());
			assistantsLeft--;
		}
		while (assistantsLeft > 0) {
			assistants.add(new Assistant());
			assistantsLeft--;
		}
		return assistants;
	}
	
	/**
	 * This method returns an {@link Assistant} to the {@link AssistantsPool}.
	 * @param assistant the {@link Assistant} to return to the {@link AssistantsPool}.
	 */
	public void returnAnAssistant(Assistant assistant){
		assistant.setOwner(null);
		this.alreadyCreatedAssistants.add(assistant);
	}
	
	/**
	 * This method returns n Assistants to the {@link AssistantsPool}.
	 * @param assistants the List of Assistants to return to the {@link AssistantsPool}.
	 */
	public void returnNAssistants(List<Assistant> assistants) {
		for(Assistant assistant: assistants){
			assistant.setOwner(null);
		}
		this.alreadyCreatedAssistants.addAll(assistants);
	}
}
