package it.polimi.ingsw.lm_belloni_cof.model;

/**
 * 
 * This interface represents every object that in the game may be used by
 * someone (not matter who) and for which is important to keep track of the
 * hasBeenUsed() property such as {@link Permit} or {@link RewardTile}. In the
 * CoF gameflow a object used cannot be unused
 *
 */
public interface Usable {
	/**
	 * Method that returns if the usable object has been used
	 * 
	 * @return true or false whether the object has been used or not
	 */
	public boolean hasBeenUsed();

	/**
	 * Set the usable object to "used" state. This operation cannot be undone.
	 */
	public void setUsage();
}
