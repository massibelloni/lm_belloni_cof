package it.polimi.ingsw.lm_belloni_cof.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * Abstract class representing everything can be "printed" on a Tile (ex.
 * {@link RewardTile} or {@link PermitTile}). In a very simple abstraction a Tile is something with
 * bonuses on it. The TileContent abstraction permits to divide the gameflow and
 * logic from {@link GameMap}'s rules. An example of this logic is well
 * described in {@link PermitDeck#getPermitDeckInstance(GameMap,Region,int)}.
 */

public abstract class TileContent {
	/**
	 * List of {@link Bonus} printed on a Tile.
	 */
	private List<Bonus> tileBonuses;

	/**
	 * Private constructor. Define an empty list.
	 */
	public TileContent() {
		this.tileBonuses = new ArrayList<>();
	}

	/**
	 * The only way to add a {@link Bonus} on the TileContent.
	 * 
	 * @param bonus
	 *            the bonus to be added to the TileContent
	 */
	public void addBonusToTileContent(Bonus bonus) {
		this.tileBonuses.add(bonus);
	}

	/**
	 * Get the list of bonuses for this TileContent.
	 * 
	 * @return a List of {@link Bonus}
	 */
	public List<Bonus> getBonuses() {
		return Collections.unmodifiableList(this.tileBonuses);
	}
}
