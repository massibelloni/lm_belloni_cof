package it.polimi.ingsw.lm_belloni_cof.model;

/**
 * This class represents the element Bonus, characterized by the
 * {@link BonusType} and the {@link Integer} numberOfTimes.
 */
public class Bonus {
	private BonusType bonusType;
	private int numberOfTimes;

	/**
	 * This method creates an instance of Bonus.
	 * 
	 * @param bonusType
	 *            represents the {@link BonusType} of the Bonus.
	 * @param numberOfTimes
	 *            represents the reps of the Bonus.
	 */
	public Bonus(BonusType bonusType, int numberOfTimes) {
		this.bonusType = bonusType;
		this.numberOfTimes = numberOfTimes;
	}

	/**
	 * This method returns the {@link BonusType} of the Bonus.
	 * 
	 * @return bonusType represents the {@link BonusType} of the Bonus.
	 */
	public BonusType getBonusType() {
		return bonusType;
	}

	/**
	 * This method return the number of repetitions associated to the Bonus.
	 * 
	 * @return numberOfTimes represents the {@link Integer} number of
	 *         repetitions associated to the Bonus.
	 */
	public int getNumberOfTimes() {
		return numberOfTimes;
	}
	/*
	@Override
	public boolean equals(Object o) {
		if ((o instanceof Bonus)) {
			Bonus bonus = (Bonus) o;
			if (bonus.bonusType == this.bonusType && bonus.numberOfTimes == this.numberOfTimes)
				return true;
		}
		return false;
	}*/

}
