package it.polimi.ingsw.lm_belloni_cof.model;

/**
 * This class represents a {@link Player} who don't really take part in a game.
 * It is useful when a {@link Game} starts with only 2 {@link Player}.
 *
 */
public class FakePlayer extends Player {

	/**
	 * Constructor of FakePlayer. It creates a {@link Player} with Username:
	 * "Council of Four".
	 */
	public FakePlayer() {
		super("Council of Four");
	}
}
