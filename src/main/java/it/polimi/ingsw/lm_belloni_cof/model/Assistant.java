package it.polimi.ingsw.lm_belloni_cof.model;

/**
 * 
 * This class represents the Assistant, it implements {@link Ownable} that force
 * each instance of this class to has an attribute {@link Player}
 * assistantsOwner.
 *
 */
public class Assistant implements Ownable {
	private Player assistantOwner;

	/**
	 * This method creates an Assistant, and set his attribute assistantOwner to
	 * null.
	 */
	public Assistant() {
		assistantOwner = null;
	}

	/**
	 * This method checks if an Assistant has or not an {@link Player} owner.
	 * 
	 * @return True if the Assistant has an owner, False otherwise.
	 */
	@Override
	public boolean hasAnOwner() {
		if (assistantOwner == null)
			return false;
		else
			return true;
	}

	/**
	 * This method returns the of an Assistant.
	 * 
	 * @return {@link Player} the owner the Assistant
	 * 
	 */
	@Override
	public Player getOwner() {
		return assistantOwner;
	}

	/**
	 * This method set the attribute assistantOwner of an Assistant.
	 * 
	 * @param newOwner
	 *            the new {@link Player} that owns this Assistant
	 * 
	 */
	@Override
	public void setOwner(Player newOwner) {
		assistantOwner = newOwner;
	}
	
	@Override
	public String toString(){
		return "assistant";
	}

}
