package it.polimi.ingsw.lm_belloni_cof.model;

import java.util.List;

/**
 * The actual Permit that can be acquired and used by a {@link Player}.
 *
 */
public class Permit implements Ownable, Usable {
	/**
	 * The {@link PermitContent} printed on the Permit.
	 * 
	 * @see PermitContent
	 * @see PermitDeck#getPermitDeckInstance(GameMap,Region,int)
	 */
	private PermitContent content;
	/**
	 * Parameter due to the {@link Ownable} interface implementation: null if
	 * not owned.
	 */
	private Player owner;
	/**
	 * Parameter due to the {@link Usable} interface implementation: true if the
	 * Permit has already been used, false otherwise
	 */
	private boolean used;

	/**
	 * Public constructor: it needs the {@link PermitContent} to be printed on
	 * the Permit. The permit is set to "not owned" and "not used"
	 * 
	 * @see #hasBeenUsed()
	 * @see #getOwner()
	 */
	public Permit(PermitContent content) {
		this.content = content;
		this.owner = null;
		this.used = false;
	}

	/**
	 * Get all the cities for which this tile will guarantee an emporium build
	 * permission.
	 */
	public List<City> getCities() {
		return this.content.getCities();
	}

	/**
	 * Get the list of bonuses for this Permit.
	 * 
	 * @return a List of {@link Bonus}
	 */
	public List<Bonus> getBonuses() {
		return this.content.getBonuses();
	}

	/**
	 * Get the {@link Region} of the {@link PermitContent}.
	 * 
	 * @return the {@link Region} of the PermitContent.
	 * @see PermitContent
	 */
	public Region getRegion() {
		return this.content.getRegion();
	}

	/**
	 * Method that returns if the usable object has been used
	 * 
	 * @return true or false whether the object has been used or not
	 * @see Usable
	 */
	@Override
	public boolean hasBeenUsed() {
		return this.used;
	}

	/**
	 * Set the usable object to "used" state. This operation cannot be undone.
	 * 
	 * @see Usable
	 */
	@Override
	public void setUsage() {
		this.used = true;
	}

	/**
	 * Get if the Permit has an owner.
	 */
	@Override
	public boolean hasAnOwner() {
		if (this.owner != null)
			return true;
		return false;
	}

	/**
	 * Get the owner of the Permit, null if {@link #hasAnOwner()} is false
	 */
	@Override
	public Player getOwner() {
		return this.owner;
	}

	/**
	 * Set the owner of the Permit. Set to null if it hasn't an owner
	 */
	@Override
	public void setOwner(Player owner) {
		this.owner = owner;

	}
	
	@Override
	public String toString(){
		return "permit tile";
	}
}
