package it.polimi.ingsw.lm_belloni_cof.model;

import java.security.InvalidParameterException;
import java.util.*;
/**
 * 
 * This class represents the Balcony inside a {@link Board}.
 * A Balcony is composed of four {@link Councillor}.
 *
 */
public class Balcony {
	public final static int BALCONY_SIZE = 4;
	private Queue<Councillor> balcony;
	
	/**
	 * This method creates an instance of Balcony filled with four {@link Councillor}.
	 * @throw {@link InvalidParameterException} if the input array is longer than the Balcony(4).
	 * @param councillors an array of {@link Councillors} to insert in the Balcony.
	 */
	public Balcony(Councillor[] councillors) {
		if (councillors.length != Balcony.BALCONY_SIZE) {
			throw new InvalidParameterException("councillors lenght must be " + Balcony.BALCONY_SIZE);
		}
		balcony = new LinkedList<>();
		for (int i = 0; i < Balcony.BALCONY_SIZE; i++) {
			balcony.add(councillors[i]);
		}
	}
	/**
	 * This method checks the {@link PoliticColor} of the Councillors inside the Balcony, and returns them.
	 * @return balconyColors an ArrayList that contains the Colors of Councillors on the Balcony.
	 */
	public List<PoliticColor> getBalconyColors() {
		ArrayList<PoliticColor> balconyColors = new ArrayList<>();
		Iterator<Councillor> balconyIterator = this.balcony.iterator();
		while (balconyIterator.hasNext()) {
			balconyColors.add(balconyIterator.next().getCouncillorColor());
		}
		return balconyColors;
	}
	
	/**
	 * This method inserts a new {@link Councillor} in the tile of the Balcony 
	 * and takes off a {@link Councillor} from the head.
	 * @param newCouncillor new {@link Councillor} to insert in the Balcony.
	 * @return free the {@link Councillor} removed frome the head of the Balcony.
	 */
	public Councillor shift(Councillor newCouncillor) {
		Councillor free = balcony.remove();
		balcony.add(newCouncillor);
		return free;
	}

}
