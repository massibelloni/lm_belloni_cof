package it.polimi.ingsw.lm_belloni_cof.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class represents the {@link Board} of the King. There is only one inside
 * a {@link Game}.
 */
public class KingRegionBoard extends Board {

	/**
	 * Represents the pile of {@link KingRewardTile}.
	 */

	private KingRewardsPile kingRewardsPile;

	/**
	 * A map that link the {@link CityColor} with the respective
	 * {@link ColorRewardTile}
	 */

	private Map<CityColor, ColorRewardTile> colorRewardTiles;

	/**
	 * Constructor creates a KingRegionBoard that contains a full
	 * {@link Balcony}, ColorRewardTiles for each {@link CityColor} in a
	 * {@link GameMap}, and a KingRewardsPile of KingRewardTiles taken from a
	 * {@link GameMap}.
	 * 
	 * @param colorContents
	 *            a Map<CityColor,ColorRewardTileContent> containing a
	 *            {@link ColorRewardTileContent} for each {@link CityColor} of
	 *            interest.
	 * @param kingRewardTileContents
	 *            a list of already ordered {@link KingRewardTileContents}.
	 * @param balcony
	 *            full {@link Balcony} to put in the KingRegionBoard.
	 */
	public KingRegionBoard(Map<CityColor, ColorRewardTileContent> colorContents,
			List<KingRewardTileContent> kingRewardTileContents, Balcony balcony) {
		super(balcony);
		this.colorRewardTiles = new HashMap<>();
		for (CityColor color : CityColor.values())
			if (color != CityColor.PURPLE)
				colorRewardTiles.put(color, new ColorRewardTile(colorContents.get(color)));
		this.kingRewardsPile = KingRewardsPile.getKingRewardPile(kingRewardTileContents);
	}

	/**
	 * Method to get the List of KingRewardTiles in the kingRegionBoard.
	 * 
	 * @return {@link KingRewardsPile} composed by {@link KingRewardTile} that
	 *         are in the KingRegionBoard.
	 */
	public KingRewardsPile getKingRewardsPile() {
		return this.kingRewardsPile;
	}

	/**
	 * This method returns the {@link ColorRewardTile} associated to a
	 * {@link CityColor}.
	 * 
	 * @param color
	 *            {@link CityColor} key to search the {@link ColorRewardTile} in
	 *            the Map.
	 * @return {@link ColorRewardTile} by {@CityColor}.
	 */
	public ColorRewardTile getColorRewardTile(CityColor color) {
		return this.colorRewardTiles.get(color);
	}

}
