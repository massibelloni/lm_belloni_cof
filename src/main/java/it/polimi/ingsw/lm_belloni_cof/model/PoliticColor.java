package it.polimi.ingsw.lm_belloni_cof.model;

/**
 * All the colors a {@link Politic} can have.
 *
 */
public enum PoliticColor {
	PURPLE, BLACK, LIGHT_BLUE, PINK, ORANGE, WHITE, MULTI_COLORED
}
