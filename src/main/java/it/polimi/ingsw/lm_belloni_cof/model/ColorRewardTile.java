package it.polimi.ingsw.lm_belloni_cof.model;

/**
 * This class represents physically object ColorRewardTile.
 * It contains his own {@link ColorRewardTileContent} and a {@link CityColor}.
 * A ColorRewardTile may be obtained by the {@link Player} that builds an emporium 
 * in each {@link City} of a given {@link CityColor} defined in the {@link ColorRewardTileContent}.
 * This class inherits from {@link RewardTile}.
 */
public class ColorRewardTile extends RewardTile<ColorRewardTileContent> {
	/**
	 * This constructor calls the constructor of {@link RewardTile}. 
	 * This constructor take a content, and print it on a physical Tile.
	 * 
	 * @param rewardContent the {@link ColorRewardTileContent} to print on the physical ColorRewardTile.
	 */
	public ColorRewardTile(ColorRewardTileContent rewardContent) {
		super(rewardContent);
	}
	
	/**
	 * This method returns the {@link CityColor} of the {@link ColorRewardTileContent} placed on the ColorRewardTile.
	 * 
	 * @return {@link CityColor} of the {@link ColorRewardTileContent}.
	 */
	public CityColor getColor() {
		return super.rewardContent.getColor();
	}
}
