package it.polimi.ingsw.lm_belloni_cof.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughAssistantsException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.NotEnoughCoinsException;

/**
 * The class representing the player in the {@link Game}. This contains its
 * status and all the {@link Ownable} it owns.
 *
 */
public class Player {
	/** The player username used to identify it during the gameflow */
	protected final String username;
	/**
	 * The amount of coins of the player (=position in coins path)
	 */
	private int coins;
	/**
	 * The number of emporiums left to the player
	 */
	private int numberOfEmporiums;
	/**
	 * The position on the nobility track of the {@link GameMap} of the
	 * {@link Game} played.
	 */
	private int nobilityTrackPosition;
	/** The amount of victory points earned by the player */
	private int victoryPoints;
	/**
	 * The {@link Politic} objects that the player has earned and that can be
	 * used to satisfy councils.
	 * 
	 * @see Game#satisfyACouncil(Player,Region,List<Politic>)
	 */
	private List<Politic> politicsHand;
	/**
	 * The {@link Permit} objects that the player has acquired and that can be
	 * used to build emporiums.
	 * 
	 * @see Game#buildAnEmporiumUsingAPermitTile(Player,Permit,City)
	 */
	private List<Permit> permitsAcquired;
	/**
	 * The {@link Assistant} objects that the owns.
	 */
	private Queue<Assistant> assistantsOwned;

	/**
	 * Public constructor of a new {@link Player}
	 * 
	 * @param username
	 *            the username to be used during the {@link Game}
	 * @param coins
	 *            number of initial coins
	 * @param numberOfEmporiums
	 *            number of emporiums at the beginning
	 * @param politicsHand
	 *            {@link Politic}s owned when the {@link Game} starts
	 * @param assistantsOwned
	 *            {@link Assistant}s owned when then {@link Game} starts
	 */
	public Player(String username, int coins, int numberOfEmporiums, List<Politic> politicsHand,
			List<Assistant> assistantsOwned) {
		this.username = username;
		this.numberOfEmporiums = numberOfEmporiums;
		this.coins = coins;
		this.nobilityTrackPosition = 0;
		this.victoryPoints = 0;
		this.permitsAcquired = new ArrayList<>();
		this.politicsHand = new ArrayList<>();
		this.giveNPolitics(politicsHand);
		this.assistantsOwned = new LinkedList<>();
		this.giveNAssistants(assistantsOwned);
	}

	/**
	 * Protected constructor used by the {@link FakePlayer}. This doesn't
	 * instantiate any object used during the {@link Game}
	 * 
	 * @param username
	 *            the username to be used during the {@link Game}
	 */
	protected Player(String username) {
		this.username = username;
	}

	/**
	 * Get the username of the player.
	 * 
	 * @return a {@link String} representing the username of the player.
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 * Get the player position in the victory path
	 * 
	 * @return the position of the player (amount of victory points earned so
	 *         far).
	 */
	public int getVictoryPoints() {
		return this.victoryPoints;
	}

	/**
	 * Increments the amount of victory points earned. If the quota exceeds the
	 * maximum it sets the victory points to the maximum.
	 * 
	 * @param n
	 *            number of points to be added
	 * @param maxVictoryPoints
	 *            maximum number of points for the {@link Game}
	 */
	public void incrementVictoryPoints(int n, int maxVictoryPoints) {
		this.victoryPoints += n;
		if (this.victoryPoints > maxVictoryPoints)
			this.victoryPoints = maxVictoryPoints;

	}

	/**
	 * Give an {@link Assistant} to the player.
	 * 
	 * @param assistant
	 *            the {@link Assistant} to give.
	 */
	public void giveAnAssistant(Assistant assistant) {
		assistant.setOwner(this);
		this.assistantsOwned.add(assistant);
	}

	/**
	 * Give n {@link Assistant}s to the player
	 * 
	 * @param assistants
	 *            a List of {@link Assistant}s
	 */
	public void giveNAssistants(List<Assistant> assistants) {
		for (Assistant assistant : assistants)
			assistant.setOwner(this);
		this.assistantsOwned.addAll(assistants);
	}

	/**
	 * Decrease the number of emporiums owned by the player.
	 * 
	 * @see Game#buildAnEmporium(Player,City)
	 * @return the updated number of emporiums owned.
	 */
	public int decreaseNumberOfEmporiums() {
		this.numberOfEmporiums--;
		return this.numberOfEmporiums;
	}

	/**
	 * Get the number of emporiums owned by the player.
	 * 
	 * @return the number of emporiums owned
	 */
	public int getNumberOfEmporiums() {
		return this.numberOfEmporiums;
	}

	/**
	 * Move the player on nobility track or stop him at the end of the path.
	 * 
	 * @param n
	 *            number of steps on the nobility track.
	 * @param nobilityTrackSize
	 *            size of the nobility track.
	 */
	public void moveOnNobilityTrackPosition(int n, int nobilityTrackSize) {
		this.nobilityTrackPosition += n;
		if (this.nobilityTrackPosition > nobilityTrackSize)
			this.nobilityTrackPosition = nobilityTrackSize;
	}

	/**
	 * Get the player position on the nobility track.
	 * 
	 * @return the position of the player on the nobility track.
	 */
	public int getNobilityTrackPosition() {
		return this.nobilityTrackPosition;
	}

	/**
	 * Get the list of the {@link Permit} acquired by the player. Both used and
	 * unused.
	 * 
	 * @return the unmodifiable List of {@link Permit}
	 */
	public List<Permit> getPermitsAcquired() {
		return this.permitsAcquired;
	}

	/**
	 * Get the player's coin amount
	 * 
	 * @return the amount of coins owned by the player
	 */
	public int getCoins() {
		return this.coins;
	}

	/**
	 * Give a {@link Permit} to the player. This method adds the {@link Permit}
	 * to the collection of the owned permits and set the ownership to the
	 * player.
	 * 
	 * @param permit
	 *            the {@link Permit} to be collected.
	 */
	public void giveAPermit(Permit permit) {
		permit.setOwner(this);
		this.permitsAcquired.add(permit);
	}

	/**
	 * Get the {@link Politic}s owned by the player. The collection returned may
	 * be updated
	 * 
	 * @return a modifiable List of {@link Politic}s.
	 */
	public List<Politic> getPoliticsHand() {
		return politicsHand;
	}

	/**
	 * Give a {@link Politic} to the {@link Player} setting the new ownership.
	 * 
	 * @param politic
	 *            the {@link Politic} to give.
	 */
	public void giveAPolitic(Politic politic) {
		politic.setOwner(this);
		this.politicsHand.add(politic);
	}

	/**
	 * Give N {@link Politic}s to the {@link Player} setting the new ownership.
	 * 
	 * @param politics
	 *            the {@link Politic}s to give.
	 */
	public void giveNPolitics(List<Politic> politics) {
		for (Politic politic : politics)
			politic.setOwner(this);
		this.politicsHand.addAll(politics);
	}

	/**
	 * Move the player on coins track upward (earning coins) and backward
	 * (spending coins).
	 * 
	 * @param coins
	 *            an integer (positive or negative) representing the number of
	 *            steps to take on the coins path
	 * @param maxCoins
	 *            the maximum number of coins that a player can have.
	 * @throws NotEnoughCoinsException
	 *             when the action requested can't be performed. It would cause
	 *             a negative amount of coins owned.
	 */
	public void moveOnCoinsTrack(int coins, int maxCoins) throws NotEnoughCoinsException {
		if (coins > 0) {
			int newValue = this.coins + coins;
			if (newValue > maxCoins)
				newValue = maxCoins;
			this.coins = newValue;
		} else {
			if (this.coins + coins < 0)
				throw new NotEnoughCoinsException();
			this.coins += coins;
		}
	}

	/**
	 * Get a random {@link Assistant} from the assistants owned by the player.
	 * 
	 * @return the {@link Assistant} no more owned by the player.
	 * @throws NotEnoughAssistantsException
	 *             when the player hasn't any assistant to return
	 */
	public Assistant getAnAssistant() throws NotEnoughAssistantsException {
		if (!this.assistantsOwned.isEmpty()) {
			return this.assistantsOwned.remove();
		} else
			throw new NotEnoughAssistantsException();
	}

	/**
	 * Get N assistants from the assistants owned by the player
	 * 
	 * @param n
	 *            the number of assistants needed.
	 * @return the List of the assistants that the player returns to the pool.
	 * @throws NotEnoughAssistantsException
	 *             when the player hasn't n assistants to return
	 */
	public List<Assistant> getNAssistants(int n) throws NotEnoughAssistantsException {
		if (this.assistantsOwned.size() >= n) {
			List<Assistant> assistants = new ArrayList<>();
			for (int i = 0; i < n; i++)
				assistants.add(this.assistantsOwned.remove());
			return assistants;
		} else
			throw new NotEnoughAssistantsException();
	}

	/**
	 * Get the number of assistants owned by the player.
	 * 
	 * @return the number of assistants owned.
	 */
	public int getNumberOfAssistants() {
		return this.assistantsOwned.size();
	}

}
