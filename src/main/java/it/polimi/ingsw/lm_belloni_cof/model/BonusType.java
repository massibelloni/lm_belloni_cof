package it.polimi.ingsw.lm_belloni_cof.model;

/**
 * This Enumeration identifies the kind of BonusType that can be created.
 *
 */
public enum BonusType {
	ASSISTANT, COIN, VICTORY_POINT, POLITIC, MAIN_ACTION, NOBILITY, TAKE_PERMIT, CITY_REWARD, PERMIT_BONUS
}
