package it.polimi.ingsw.lm_belloni_cof.model;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.*;

import it.polimi.ingsw.lm_belloni_cof.exceptions.CitiesNotConnectedException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.InvalidActionException;

/**
 * This class represents a {@link Game} configuration. Through this class'
 * attributes and get/set method all the {@link Game} dynamics may be configured
 * by an user.
 * 
 * @see GameMapFactory
 *
 */
public class GameMap {
	/**
	 * The data structure used to store the cities in Council of Four. It's a
	 * JGraphT UndirectedGraph in which there are no loops and each {@link City}
	 * may be connected to another with only one edge.
	 */
	private UndirectedGraph<City, DefaultEdge> cities;
	/**
	 * The {@link City} in which the king is placed when the {@link Game}
	 * starts.
	 */
	private City startingKingLocation;
	/**
	 * The {@link Map} of all the {@link PermitContent}s defined in this GameMap
	 * divided by {@link Region}
	 */
	private Map<Region, List<PermitContent>> permitContents;
	/**
	 * The {@link Map} of all the {@link ColorRewardTileContent}s defined in
	 * this GameMap divided by {@link CityColor}
	 */
	private Map<CityColor, ColorRewardTileContent> colorRewardTileContents;
	/**
	 * The {@link Map} of all the {@link RegionRewardTileContent}s defined in
	 * this GameMap divided by {@link Region}
	 */
	private Map<Region, RegionRewardTileContent> regionRewardTileContents;
	// not useful storing this like a Queue
	/**
	 * The {@link List} of all the {@link KingRewardTileContent} defined in this
	 * GameMap, already ordered by {@link KingRewardTileContent#pickingOrder}.
	 */
	private List<KingRewardTileContent> kingRewardTileContents;
	/** The {@link NobilityTrack} defined by this GameMap */
	private NobilityTrack nobilityTrack;
	/**
	 * The {@link List} of all the {@link RewardToken}s made available from this
	 * GameMap.
	 */
	private List<RewardToken> availableRewardTokens;
	/** The maximum amount of coins a {@link Player} may have. */
	private int maxCoins;
	/**
	 * The initial amount of coins that the first {@link Player} has. The second
	 * {@link Player} will have initialCoinsAmount+1 and so on
	 */
	private int initialCoinsAmount;
	/**
	 * The initial amount of {@link Assistant}s that the first {@link Player}
	 * has. The second {@link Player} will have initialAssistantsNumber+1 and so
	 * on
	 */
	private int initialAssistantsNumber;
	/**
	 * The initial amount of {@link Politic}s that a {@link Player} has.
	 */
	private int initialPoliticsPerPlayer;
	/**
	 * The maximum amount of Victory Points a {@link Player} may have. This
	 * represents the length of the Victory Path.
	 */
	private int maxVictoryPoints;
	/**
	 * The number of emporiums that a {@link Player} has when the {@link Game}
	 * starts. This variables represents the number of emporiums that a
	 * {@link Player} has to build to finish the Game.
	 */
	private int emporiumsPerPlayer;
	/**
	 * The {@link FinalScoreRules} defined by this GameMap.
	 * 
	 * @see FinalScoreRules
	 */
	private FinalScoreRules finalRules;

	/**
	 * The public constructor of the class: it initializes to an empty
	 * {@link Collection} all the collections of this class.}
	 */
	public GameMap() {
		this.cities = new SimpleGraph<>(DefaultEdge.class);
		this.regionRewardTileContents = new HashMap<>();
		this.colorRewardTileContents = new HashMap<>();
		this.availableRewardTokens = new ArrayList<>();
		this.permitContents = new HashMap<>();
		for (Region region : Region.values()) {
			if (region != Region.KING) {
				permitContents.put(region, new ArrayList<>());
			}
		}
		kingRewardTileContents = new ArrayList<>();
	}

	/**
	 * Add a {@link RewardToken} to the list of the reward tokens made available
	 * by this GameMap.
	 * 
	 * @param rewardToken
	 *            the {@link RewardToken} to be added.
	 */
	public void addARewardToken(RewardToken rewardToken) {
		this.availableRewardTokens.add(rewardToken);
	}

	/**
	 * Set the {@link NobilityTrack} for this GameMap.
	 * 
	 * @param nobilityTrack
	 *            the {@link NobilityTrack} to be used.
	 */
	public void setNobilityTrack(NobilityTrack nobilityTrack) {
		this.nobilityTrack = nobilityTrack;
	}

	/**
	 * Add (or replace) the {@link RegionRewardTileContent} associated to a
	 * {@link Region} for this GameMap.
	 * 
	 * @param regionRewardTileContent
	 *            the {@link RegionRewardTileContent} to be set.
	 */
	public void addARegionRewardTileContent(RegionRewardTileContent regionRewardTileContent) {
		this.regionRewardTileContents.put(regionRewardTileContent.getRegion(), regionRewardTileContent);
	}

	/**
	 * Get the {@link RegionRewardTileContent} defined by this GameMap for a
	 * particular {@link Region}.
	 * 
	 * @param region
	 *            a {@link Region}
	 * @return the {@link RegionRewardTileContent} defined by this GameMap for
	 *         the {@link Region} passed as an argument.
	 */
	public RegionRewardTileContent getRewardTileContentByRegion(Region region) {
		return this.regionRewardTileContents.get(region);
	}

	/**
	 * Get the {@link ColorRewardTileContent} defined by this GameMap for a
	 * particular {@link CityColor}.
	 * 
	 * @param color
	 *            a {@link CityColor}
	 * @return the {@link ColorRewardTileContent} defined by this GameMap for
	 *         the {@link CityColor} passed as an argument.
	 */
	public ColorRewardTileContent getRewardTileContentByCityColor(CityColor color) {
		return this.colorRewardTileContents.get(color);
	}

	/**
	 * Add (or replace) the {@link ColorRewardTileContent} associated to a
	 * {@link CityColor} for this GameMap.
	 * 
	 * @param colorRewardTileContent
	 *            the {@link ColorRewardTileContent} to be set.
	 */
	public void addAColorRewardTileContent(ColorRewardTileContent colorRewardTileContent) {
		this.colorRewardTileContents.put(colorRewardTileContent.getColor(), colorRewardTileContent);
	}

	/**
	 * Get all the {@link ColorRewardTileContent}s defined in this GameMap.
	 * 
	 * @return a {@link Map} containing all the {@link ColorRewardTileContent}
	 *         defined in this GameMap, one for each {@link CityColor}.
	 */
	public Map<CityColor, ColorRewardTileContent> getColorRewardTileContents() {
		return Collections.unmodifiableMap(this.colorRewardTileContents);
	}

	/**
	 * Add a {@link City} to the collection of cities of the GameMap.
	 * 
	 * @param city
	 *            the {@link City} to be added.
	 */
	public void addACity(City city) {
		this.cities.addVertex(city);
	}

	/**
	 * Add a connection between to cities previously added to the GameMap.
	 * 
	 * @see #addACity(City)
	 * @param city1
	 *            the first {@link City}.
	 * @param city2
	 *            the second {@link City}.
	 */
	public void addAConnectionBetween(City city1, City city2) {
		this.cities.addEdge(city1, city2);
	}

	/**
	 * Get the size of the {@link NobilityTrack} of this GameMap.
	 * 
	 * @see #setNobilityTrack(NobilityTrack)
	 * @return the size of the {@link NobilityTrack}, 0 if not set.
	 */
	public int getNobilityTrackSize() {
		if (this.nobilityTrack != null)
			return this.nobilityTrack.getSize();
		else
			return 0;
	}

	/**
	 * Get the {@link List} of {@link Bonus}es defined in a certain position of
	 * the {@link NobilityTrack} of this GameMa
	 * 
	 * @param position
	 *            the position on the {@link NobilityTrack} that I want to know
	 *            the {@link Bonus}es of.
	 * @return the {@link List} of {@link Bonus}es defined on the position
	 *         passed as an argument. 0 if the NobilityTrack defined hasn't that
	 *         position.
	 */
	public List<Bonus> getBonusesForANobilityTrackPosition(int position) {
		if (position < this.getNobilityTrackSize())
			return Collections.unmodifiableList(this.nobilityTrack.getBonuses(position));
		return Collections.unmodifiableList(new ArrayList<Bonus>());
	}

	/**
	 * Get the initial number of {@link Politic}s to give to each {@link Player}
	 * during {@link Game} initialization.
	 * 
	 * @see #initialPoliticsPerPlayer
	 * @return the initial number of {@link Politic}s to give to each
	 *         {@link Player} during {@link Game} initialization.
	 */
	public int getInitialPoliticsPerPlayer() {
		return initialPoliticsPerPlayer;
	}

	/**
	 * Set the initial number of {@link Politic}s to give to each {@link Player}
	 * during {@link Game} initialization.
	 * 
	 * @see #initialPoliticsPerPlayer
	 * @param initialPoliticPerPlayer
	 *            the initial number of {@link Politic}s to give to each
	 *            {@link Player} during {@link Game} initialization.
	 */
	public void setInitialPoliticsPerPlayer(int initialPoliticsPerPlayer) {
		this.initialPoliticsPerPlayer = initialPoliticsPerPlayer;
	}

	/**
	 * Get the {@link List} of all the {@link RewardToken}s made available from
	 * this GameMap.
	 * 
	 * @see #rewardTokens
	 * @return the {@link List} of all the {@link RewardToken}s made available
	 *         from this GameMap.
	 */
	public List<RewardToken> getRewardTokens() {
		return Collections.unmodifiableList(this.availableRewardTokens);
	}

	/**
	 * Get the {@link City} from which the king starts in a {@link Game}.
	 * 
	 * @return the {@link City} from which the king starts in a {@link Game}.
	 */
	public City getStartingKingLocation() {
		return this.startingKingLocation;
	}

	/**
	 * Set the {@link City} from which the king starts in a {@link Game}.
	 * 
	 * @param city
	 *            the {@link City} from which the king starts in a {@link Game}.
	 */
	public void setStartingKingLocation(City city) {
		this.startingKingLocation = city;
	}

	/**
	 * This method says if in the defined GameMap two cities are directly
	 * connected. In other words if exists a direct path from the source to the
	 * destination (or viceversa).
	 * 
	 * @param city1
	 *            the source {@link City}.
	 * @param city2
	 *            the destination {@link City}.
	 * @return true if the path exists, false otherwise
	 */
	public boolean areCitiesDirectlyConnected(City city1, City city2) {
		if (this.cities.getEdge(city1, city2) != null)
			return true;
		else
			return false;
	}

	/**
	 * Get a {@link List} of all the cities directly connected to the
	 * {@link City} passed as an argument. The method
	 * {@link #areCitiesDirectlyConnected} returns true for each {@link City}
	 * returned by this method.
	 * 
	 * @param city
	 *            the source {@link City}
	 * @return a {@link List} of cities directly connected to the {@link City}
	 *         passed as an argument.
	 */
	public List<City> getConnectedCities(City city) {
		List<City> connectedToCity = new ArrayList<>();
		for (DefaultEdge edge : this.cities.edgesOf(city))
			connectedToCity.add((this.cities.getEdgeSource(edge) == city) ? (this.cities.getEdgeTarget(edge))
					: (this.cities.getEdgeSource(edge)));
		return connectedToCity;
	}

	/**
	 * This methods calculates the number of roads that have to be crossed to
	 * reach city2 from city1.
	 * 
	 * @param city1
	 *            the source {@link City}.
	 * @param city2
	 *            the destination {@link City}.
	 * @return the number of roads that have to be crossed to reach city2 from
	 *         city1.
	 * @throws CitiesNotConnectedException
	 *             if the two cities passed as arguments are not connected in
	 *             any way.
	 */
	public int getRoadsBetweenCities(City city1, City city2) throws CitiesNotConnectedException {
		if (city1 == city2)
			return 0;
		List<DefaultEdge> path = DijkstraShortestPath.findPathBetween(this.cities, city1, city2);
		if (path == null)
			throw new CitiesNotConnectedException();
		return path.size();
	}

	/**
	 * Get a {@link List} of all the cities defined in this GameMap.
	 * 
	 * @return a {@link List} of {@link City}.
	 */
	public List<City> getCityList() {
		List<City> cityList = new ArrayList<>();
		Set<City> vertexSet = this.cities.vertexSet();
		Iterator<City> cityIterator = vertexSet.iterator();
		while (cityIterator.hasNext()) {
			cityList.add(cityIterator.next());
		}
		return cityList;
	}

	/**
	 * Get a {@link List} of all the cities in a particular {@link Region}.
	 * 
	 * @param region
	 *            a {@link Region}
	 * @return {@link List} of all the {@link City} in a particular
	 *         {@link Region}.
	 */
	public List<City> getCitiesByRegion(Region region) {
		List<City> cityList = new ArrayList<>();
		Set<City> vertexSet = this.cities.vertexSet();
		Iterator<City> cityIterator = vertexSet.iterator();
		while (cityIterator.hasNext()) {
			City city = cityIterator.next();
			if (city.getCityRegion() == region)
				cityList.add(city);
		}
		return cityList;
	}

	/**
	 * Get a {@link List} of all the cities of a particular {@link CityColor}.
	 * 
	 * @param color
	 *            a {@link CityColor}
	 * @return {@link List} of all the {@link City} of a particular
	 *         {@link CityColor}.
	 */
	public List<City> getCitiesByCityColor(CityColor color) {
		List<City> cityList = new ArrayList<>();
		Set<City> vertexSet = this.cities.vertexSet();
		Iterator<City> cityIterator = vertexSet.iterator();
		while (cityIterator.hasNext()) {
			City city = cityIterator.next();
			if (city.getCityColor() == color)
				cityList.add(city);
		}
		return cityList;
	}

	/**
	 * Add a {@link PermitContent} to this GameMap.
	 * 
	 * @param newPermitContent
	 *            the {@link PermitContent} that has to be added to the GameMap.
	 */
	public void addAPermitContent(PermitContent newPermitContent) {
		permitContents.get(newPermitContent.getRegion()).add(newPermitContent);
	}

	/**
	 * Get all the {@link PermitContent}s defined for a {@link Region}.
	 * 
	 * @param region
	 *            a {@link Region}
	 * @return a {@link List} of all the {@link PermitContent}s defined for the
	 *         {@link Region} passed as an argument.
	 */
	public List<PermitContent> getPermitContentsByRegion(Region region) {
		return this.permitContents.get(region);
	}

	/**
	 * Add a {@link KingRewardTileContent} to this GameMap. The
	 * {@link KingRewardTileContent} can be added only if its
	 * {@link KingRewardTileContent#pickingOrder} is greater then the
	 * {@link KingRewardTileContent#pickingOrder} of the previously added
	 * {@link KingRewardTileContent} (if exists). If the
	 * {@link KingRewardTileContent} added is the first it has to have 1 as
	 * {@link KingRewardTileContent#pickingOrder}.
	 * 
	 * @param kingRewardTileContent
	 *            a {@link KingRewardTileContent} that will be added to this
	 *            GameMap.
	 * @throws InvalidActionException
	 *             if the {@link KingRewardTileContent#pickingOrder} of
	 *             kingRewardTileContent is lower then the
	 *             {@link KingRewardTileContent#pickingOrder} of the previously
	 *             added {@link KingRewardTileContent}
	 */
	public void addAKingRewardTileContent(KingRewardTileContent kingRewardTileContent) throws InvalidActionException {
		if (kingRewardTileContent.getPickingOrder() != this.kingRewardTileContents.size() + 1)
			throw new InvalidActionException();
		this.kingRewardTileContents.add(kingRewardTileContent);
	}

	/**
	 * Get all the {@link KingRewardTileContent}s defined in this GameMap.
	 * 
	 * @return a {@link List} of {@link KingRewardTileContent}s ordered by
	 *         {@link KingRewardTileContent#pickingOrder}. 1 is first.
	 */
	public List<KingRewardTileContent> getKingRewardTileContents() {
		return Collections.unmodifiableList(this.kingRewardTileContents);
	}

	/**
	 * Get the maximum amount of Victory Points a {@link Player} may have.
	 * 
	 * @return the maximum amount of Victory Points
	 */
	public int getMaxVictoryPoints() {
		return maxVictoryPoints;
	}

	/**
	 * Set the maximum amount of Victory Points a {@link Player} may have.
	 * 
	 * @param maxVictoryPoints
	 *            the maximum amount of Victory Points
	 */
	public void setMaxVictoryPoints(int maxVictoryPoints) {
		this.maxVictoryPoints = maxVictoryPoints;
	}

	/**
	 * Get the number of emporiums that a {@link Player} has when the
	 * {@link Game} starts.
	 * 
	 * @return the number of emporiums that a {@link Player} has when the
	 *         {@link Game} starts.
	 */
	public int getEmporiumsPerPlayer() {
		return emporiumsPerPlayer;
	}

	/**
	 * Set the number of emporiums that a {@link Player} has when the
	 * {@link Game} starts.
	 * 
	 * @param emporiumsPerPlayer
	 *            the number of emporiums that a {@link Player} has when the
	 *            {@link Game} starts.
	 */
	public void setEmporiumsPerPlayer(int emporiumsPerPlayer) {
		this.emporiumsPerPlayer = emporiumsPerPlayer;
	}

	/**
	 * Get The maximum amount of coins a {@link Player} may have.
	 * 
	 * @return the maximum amount of coins a {@link Player} may have.
	 */
	public int getMaxCoins() {
		return maxCoins;
	}

	/**
	 * Set The maximum amount of coins a {@link Player} may have.
	 * 
	 * @param maxCoins
	 *            the maximum amount of coins a {@link Player} may have.
	 */
	public void setMaxCoins(int maxCoins) {
		this.maxCoins = maxCoins;
	}

	/**
	 * Get the initial amount of coins that the first {@link Player} has.
	 * 
	 * @return the initial amount of coins that the first {@link Player} has.
	 */
	public int getInitialCoinsAmount() {
		return initialCoinsAmount;
	}

	/**
	 * Set the initial amount of coins that the first {@link Player} has.
	 * 
	 * @param initialCoinAmount
	 *            the initial amount of coins that the first {@link Player} has.
	 */
	public void setInitialCoinsAmount(int initialCoinAmount) {
		this.initialCoinsAmount = initialCoinAmount;
	}

	/**
	 * Get the initial amount of {@link Assistant}s that the first
	 * {@link Player} has.
	 * 
	 * @return the initial amount of {@link Assistant}s that the first
	 *         {@link Player} has.
	 */
	public int getInitialAssistantsNumber() {
		return initialAssistantsNumber;
	}

	/**
	 * Set the initial amount of {@link Assistant}s that the first
	 * {@link Player} has.
	 * 
	 * @param initialAssistantsNumber
	 *            the initial amount of {@link Assistant}s that the first
	 *            {@link Player} has.
	 */
	public void setInitialAssistantsNumber(int initialAssistantsNumber) {
		this.initialAssistantsNumber = initialAssistantsNumber;
	}

	/**
	 * Get the {@link FinalScoreRules} defined by this GameMap.
	 * 
	 * @return an already validated {@link FinalScoreRules} object.
	 * @see FinalScoreRules
	 */
	public FinalScoreRules getFinalRules() {
		return finalRules;
	}

	/**
	 * Set the {@link FinalScoreRules} defined by this GameMap.
	 * 
	 * @param finalRules
	 *            an already validated {@link FinalScoreRules} object.
	 * @see FinalScoreRules
	 */
	public void setFinalRules(FinalScoreRules finalRules) {
		this.finalRules = finalRules;
	}
}
