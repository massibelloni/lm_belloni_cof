package it.polimi.ingsw.lm_belloni_cof.model;

import java.util.*;

import it.polimi.ingsw.lm_belloni_cof.exceptions.EmptyDeckException;

/**
 * This class represents a List of objects {@link Ownable}.
 *
 * @param <T> a Generic object that implements the interface {@link Ownable}.
 */
public class Deck<T extends Ownable> {
	/**
	 * Identify the List of {@link Ownable} objects.
	 */
	private LinkedList<T> deck;
	
	/**
	 * Constructor of the class.
	 * Returns an empty {@link LinkedList} where {@link Ownable} objects can be stored.
	 */
	public Deck() {
		deck = new LinkedList<>();
	}
	
	/**
	 * This method shuffles the List randomly.
	 */
	public void shuffle() {
		Collections.shuffle(deck);
	}
	
	/**
	 * This method returns to the {@link Player} an object from the head of the deck.
	 * @return an {@link Ownable} object from the head of the deck.
	 * @throws {@link EmptyDeckException} when the deck has no objects to draw.
	 */
	public T draw() throws EmptyDeckException {
		if(deck.isEmpty())
			throw new EmptyDeckException();
		return deck.remove();
	}
	/**
	 * This method add an {@link Ownable} object to the deck.
	 * @param card the {@link Ownable} object to return to the deck.
	 */
	public void discard(T card) {
		if(card != null){
			deck.add(card);
			card.setOwner(null);
		}
	}

}
