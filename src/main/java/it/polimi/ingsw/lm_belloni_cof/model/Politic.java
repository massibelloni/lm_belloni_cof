package it.polimi.ingsw.lm_belloni_cof.model;

/**
 * The actual politic card that can be owned and used by a {@link Player}.
 *
 */
public class Politic implements Ownable {
	/**
	 * Parameter due to the {@link Ownable} interface implementation: null if
	 * not owned.
	 */
	private Player owner;
	/** The color of the Politic */
	private PoliticColor politicColor;

	/** Public constructor of a Politic: set the owner to null and the color */
	public Politic(PoliticColor color) {
		this.owner = null;
		this.politicColor = color;
	}

	/**
	 * Get the color of the Politic.
	 * 
	 * @return the {@link PoliticColor} of the Politic
	 */
	public PoliticColor getPoliticColor() {
		return politicColor;
	}

	/**
	 * Get if the RewardTile has an owner.
	 */
	@Override
	public boolean hasAnOwner() {
		if (this.owner != null)
			return true;
		return false;
	}

	/**
	 * Get the owner of the Politic, null if {@link #hasAnOwner() is false
	 */
	@Override
	public Player getOwner() {
		return this.owner;
	}

	/**
	 * Set the owner of the Politic. Set to null if it hasn't an owner
	 */
	@Override
	public void setOwner(Player owner) {
		this.owner = owner;

	}

	@Override
	public String toString() {
		return this.politicColor.toString().toLowerCase() + " politic";
	}
}