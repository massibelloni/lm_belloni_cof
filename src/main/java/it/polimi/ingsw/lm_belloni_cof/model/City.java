package it.polimi.ingsw.lm_belloni_cof.model;

/**
 * This class represents the City on the {@link GameMap]}.
 * 
 */
public class City {
	private String name;
	private int bonusNumber;
	private CityColor cityColor;
	private Region cityRegion;
	
	/**
	 * This method create a new instance of City.
	 * @param name represents the name of the City.
	 * @param bonusNumber represents the number of Bonuses on the City.
	 * @param color represents the {@link CityColor} of the City.
	 * @param region represents the {@link Region} that contain the City.
	 */
	public City(String name, int bonusNumber, CityColor color, Region region) {
		this.name = name;
		this.bonusNumber = bonusNumber;
		this.cityColor = color;
		this.cityRegion = region;
	}
	
	/**
	 * This method returns the name of the City.
	 * @return name {@link String} that represents the name of the City.
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * This method returns the number of Bonuses on the City.
	 * @return bonusNumber {@link Integer} that represents the number of Bonuses on the City.
	 */
	public int getBonusNumber() {
		return this.bonusNumber;
	}
	
	/**
	 * This method returns the {@link CityColor} of the City.
	 * @return cityColor {@link CityColor} that represents the color of the City.
	 */
	public CityColor getCityColor() {
		return this.cityColor;
	}
	
	/**
	 * This method return the {@link Region} that contain the City.
	 * @return cityRegion {@link Region} that represents the region that contains the City.
	 */
	public Region getCityRegion() {
		return this.cityRegion;
	}

}
