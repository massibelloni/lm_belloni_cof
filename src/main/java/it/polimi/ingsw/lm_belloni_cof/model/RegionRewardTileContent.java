package it.polimi.ingsw.lm_belloni_cof.model;

/**
 * Class that actually represents the content that is printed on a
 * {@link RegionRewardTile}.
 * 
 * @see RegionRewardTile
 *
 */
public class RegionRewardTileContent extends RewardTileContent {
	/**
	 * Represents the {@link Region} of the RewardTile.
	 */
	private Region region;

	/**
	 * 
	 * @param region
	 *            region of the RewardTileContent. This will obviously be the
	 *            {@link Region} of the RegionRewardTile on which this will be
	 *            printed. This cannot be changed.
	 */
	public RegionRewardTileContent(Region region) {
		super();
		this.region = region;
	}

	/**
	 * Returns the {@link Region} of the RegionRewardTileContent defined while
	 * creating the object
	 * 
	 * @return {@link Region} of the RegionRewardTileContent.
	 */
	public Region getRegion() {
		return this.region;
	}
}
