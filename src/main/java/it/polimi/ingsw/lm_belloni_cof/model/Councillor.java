package it.polimi.ingsw.lm_belloni_cof.model;

import java.security.InvalidParameterException;

/**
 * 
 * This class represents a Councillor. It may be placed on a {@link Balcony} or
 * not, stored in the {@link Game} object ready to be used.
 */

public class Councillor {
	/**
	 * {@link PoliticColor} of the Councillor
	 */
	private PoliticColor councillorColor;

	/**
	 * The public constructor of a Councillor.
	 * 
	 * @param color
	 *            is the {@link PoliticColor} of the Councillor.
	 * @throws {@link
	 *             InvalidParameterException} if color is MULTI_COLORED
	 */
	public Councillor(PoliticColor color) {
		if (color == PoliticColor.MULTI_COLORED)
			throw new InvalidParameterException("Councillor can't be " + PoliticColor.MULTI_COLORED);
		this.councillorColor = color;
	}

	/**
	 * Returns the color of the Councillor.
	 * 
	 * @return {@link PoliticColor} of the Councillor object.
	 */
	public PoliticColor getCouncillorColor() {
		return this.councillorColor;
	}
}
