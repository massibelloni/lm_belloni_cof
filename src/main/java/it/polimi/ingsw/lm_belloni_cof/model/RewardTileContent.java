package it.polimi.ingsw.lm_belloni_cof.model;

/**
 * 
 * A more specific TileContent to be placed on a {@link RewardTile}. This class
 * is useful for a good hierarchy.
 *
 */
public abstract class RewardTileContent extends TileContent {
	public RewardTileContent() {
		super();
	}
}
