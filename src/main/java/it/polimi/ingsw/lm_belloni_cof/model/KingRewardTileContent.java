package it.polimi.ingsw.lm_belloni_cof.model;

/**
 * Class that actually represents the content that is printed on a
 * {@link KingRewardTile}.
 * 
 * @see KingRewardTile
 *
 */
public class KingRewardTileContent extends RewardTileContent {
	/**
	 * Represents the picking order of the {@link KingRewardTile} on which this
	 * content will be printed.
	 */
	private int pickingOrder;

	/**
	 * Public constructor of a new KingRewardTileContent.
	 * 
	 * @param pickingOrder
	 *            the picking order of the {@link KingRewardTile} on which this
	 *            content will be printed.
	 */
	public KingRewardTileContent(int pickingOrder) {
		super();
		this.pickingOrder = pickingOrder;
	}

	/**
	 * Get the picking order of the {@link KingRewardTile} on which this content
	 * will be printed.
	 * 
	 * @return the picking order of the {@link KingRewardTile} on which this
	 *         content will be printed.
	 */
	public int getPickingOrder() {
		return this.pickingOrder;
	}
}
