package it.polimi.ingsw.lm_belloni_cof.model;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.exceptions.*;
/**
 * Represents a single game instance with his whole attributes.
 * 
 *
 */
public class Game {
	private static final Logger logger = Logger.getLogger(Game.class.getName());
	private final GameMap gameMap;
	private City kingLocation;
	private final ArrayList<Player> players;
	private Player currentPlayer;
	private Player firstToFinish;
	private final Map<City, ArrayList<Player>> emporiumsBuilt;
	private final Map<City, ArrayList<RewardToken>> rewardTokens;
	private KingRegionBoard kingRegionBoard;
	private final Map<Region, RegionBoard> regionBoards;
	private final PoliticDeck politicDeck;
	private final AssistantsPool assistantsPool;
	private final ArrayList<Councillor> availableCouncillors;
	/* market */
	private Map<Player, List<MarketOffer>> marketSession;
	private boolean marketOfferSession;
	private boolean marketBuySession;
	
	/**
	 * Game public constructor
	 * @param gameMap {@link GameMap} chosen for the current game
	 * @param usernames	usernames of {@link Player}s that take part in the current game
	 */
	public Game(GameMap gameMap, List<String> usernames) {
		int numberOfPlayers = usernames.size();
		this.gameMap = gameMap;
		// this.marketSession = new HashMap<Player,List<MarketOffer>>();
		// collections initializations
		this.kingLocation = gameMap.getStartingKingLocation();
		this.rewardTokens = new HashMap<>();
		this.emporiumsBuilt = new HashMap<>();
		this.regionBoards = new HashMap<>();
		// game initialization dependent on GameMap
		LinkedList<RewardToken> rewardTokenList = new LinkedList<>(this.gameMap.getRewardTokens());
		Collections.shuffle(rewardTokenList);
		for (City city : this.gameMap.getCityList()) {
			this.emporiumsBuilt.put(city, new ArrayList<>());
			// placing reward tokens on cities
			this.rewardTokens.put(city, new ArrayList<>());
			for (int i = 0; i < city.getBonusNumber(); i++) {
				this.rewardTokens.get(city).add(rewardTokenList.remove());
			}
		}
		this.availableCouncillors = this.generateCouncillors();
		for (Region region : Region.values()) {
			Balcony balconyCreated = this.generateABalcony();// (availableCouncillors);
			if (region == Region.KING)
				this.kingRegionBoard = new KingRegionBoard(this.gameMap.getColorRewardTileContents(),
						this.gameMap.getKingRewardTileContents(), balconyCreated);
			else
				this.regionBoards.put(region, new RegionBoard(region, this.gameMap.getPermitContentsByRegion(region),
						this.gameMap.getRewardTileContentByRegion(region), balconyCreated, numberOfPlayers));
		}
		this.politicDeck = PoliticDeck.getPoliticDeckInstance(usernames.size());
		this.assistantsPool = new AssistantsPool();
		int coins = gameMap.getInitialCoinsAmount();
		int assistantsNumber = gameMap.getInitialAssistantsNumber();
		this.players = new ArrayList<>();
		for (String username : usernames) {
			players.add(new Player(username, coins, this.gameMap.getEmporiumsPerPlayer(),
					this.politicDeck.getNPolitics(this.gameMap.getInitialPoliticsPerPlayer()),
					this.assistantsPool.getNAssistants(assistantsNumber)));
			coins++;
			assistantsNumber++;
		}
		if (numberOfPlayers == 2) {
			// custom rules for 2 players game
			// - draw a permit tile for each region
			// - position an emporium in each of the cities which appear on the
			// tile
			// - shuffle the tiles back in their starting deck
			Player fake = new FakePlayer();
			int empCount = 0;
			for (Region region : Region.values()) {
				if (region != Region.KING) {
					Permit permit = this.regionBoards.get(region).getPermitBoard().pickUpAPermitTile(1);
					for (City city : permit.getCities()) {
						if (!this.emporiumsBuilt.get(city).contains(fake)) {
							this.emporiumsBuilt.get(city).add(fake);
							empCount++;
						}
						if (empCount == 9)
							break;
					}
					this.regionBoards.get(region).getPermitBoard().discardATileAndShuffle(permit);
					if (empCount == 9)
						break;
				}
			}
		}
	}
	/**
	 * Change the currentPlayer attribute
	 * @param player new current{@link Player}
	 */
	public void setCurrentPlayer(Player player) {
		this.currentPlayer = player;
	}
	/**
	 * Returns the username of the {@link Player} that is currently playing his turn
	 */
	public Player getCurrentPlayer() {
		return this.currentPlayer;
	}
	/**
	 * @return the first {@link Player} that finishes to built his own emporiums
	 */
	public Player getFirstToFinish() {
		return this.firstToFinish;
	}
	/**
	 * Set the attribute firstToFinish
	 * @param player the first {@link Player} that finishes to built his own emporiums
	 */
	public void setFirstToFinish(Player player) {
		this.firstToFinish = player;
	}
	/**
	 * This method starts a market offer session after the last {@link Player} perform his own actions. 
	 */
	public void startMarketOffer() {
		this.marketOfferSession = true;
		this.initializeMarketSession();
	}
	/**
	 * This method starts a market buy session and ends a market buy offer 
	 */
	public void startMarketBuy() {
		this.marketOfferSession = false;
		this.marketBuySession = true;
	}
	/**
	 * Returns a boolean value that can be true if the game is in a market offer session, false in other cases
	 */
	public boolean inMarketOffer() {
		return this.marketOfferSession;
	}
	/**
	 * Returns a boolean value that can be true if the game is in a market buy session, false in other cases
	 */
	public boolean inMarketBuy() {
		return this.marketBuySession;
	}
	/**
	 * Initializes the Map that joins {@link Player}s and {@link MarketOffer}s putting players in it.
	 */
	public void initializeMarketSession() {
		this.marketSession = new HashMap<>();
		for (Player player : this.players) {
			this.marketSession.put(player, new ArrayList<>());
		}
	}
	/**
	 * Close the entire market session(offer and buy), giving back unsold offered {@link Ownable} objects
	 */
	public void endMarketSession() {
		/* giving back to the initial owner all the unsold ownables */
		for (Player player : this.players) {
			for (MarketOffer offer : this.marketSession.get(player)) {
				Ownable o = offer.getObjectForSale();
				if (o instanceof Permit) {
					player.giveAPermit((Permit) o);
				} else if (o instanceof Assistant) {
					player.giveAnAssistant((Assistant) o);
				} else if (o instanceof Politic) {
					player.giveAPolitic((Politic) o);
				} else {
					logger.log(Level.SEVERE, "o isn't an acceptable instance.");
				}
			}
		}
		this.marketBuySession = false;
	}
	/**
	 * Add a market offer inside the market offer map, and set the owner of the offered {@link Ownable} to null
	 * @param player {@link Player} who offer the {@link Ownable} object
	 * @param offer {@link Ownable} object to offer, and related price
	 * @throws InvalidActionException thrown if the {@link Player} is not the owner of the offered {@link Ownable} 
	 */
	public void addPlayerOffer(Player player, MarketOffer offer) throws InvalidActionException {
		if (offer.getObjectForSale().getOwner() != player)
			throw new InvalidActionException();
		/*
		 * setting all owners to null before placing them in the marketSession
		 * obj, the ownables are no longer owned by the Player that put them for
		 * sale.
		 */

		offer.getObjectForSale().setOwner(null);
		// adding to list
		this.marketSession.get(player).add(offer);
	}
	/**
	 * This method try to buy an offered {@link Ownable} on behalf of a {@link Player}
	 * @param player {@link Player} who wants to buy the offered {@link Ownable}
	 * @param seller {@link Player} who offer the offered {@link Ownable}
	 * @param ownableToBuy offered {@link Ownable} that player wants to buy
	 * @throws InvalidActionException thrown if the {@link Ownable} is not for sale
	 * @throws NotEnoughCoinsException thown if the buyer {@link Player} hasn't enough money to complete the purchase
	 */
	public void tryToBuy(Player player, Player seller, Ownable ownableToBuy)
			throws InvalidActionException, NotEnoughCoinsException {
		// searching the ownable in the ownable for sale
		MarketOffer offerForTheOwnable = null;
		for (MarketOffer offer : this.marketSession.get(seller))
			if (offer.getObjectForSale() == ownableToBuy) {
				offerForTheOwnable = offer;
				break;
			}
		if (offerForTheOwnable == null)
			throw new InvalidActionException();
		// changing ownership
		player.moveOnCoinsTrack(-offerForTheOwnable.getPrice(), this.gameMap.getMaxCoins());
		seller.moveOnCoinsTrack(offerForTheOwnable.getPrice(), this.gameMap.getMaxCoins());
		if (ownableToBuy instanceof Permit) {
			player.giveAPermit((Permit) ownableToBuy);
		} else if (ownableToBuy instanceof Assistant) {
			player.giveAnAssistant((Assistant) ownableToBuy);
		} else if (ownableToBuy instanceof Politic) {
			player.giveAPolitic((Politic) ownableToBuy);
		} else {
			logger.log(Level.SEVERE, "ownableToBuy isn't an acceptable instance.");
		}
		// removing the offer from the list, that object is no longer for sale
		this.marketSession.get(seller).remove(offerForTheOwnable);
	}
	/**
	 * Returns the offered {@link Ownable} by {@link Player}
	 * @param player owner of the returned offered {@link Ownable}
	 * @return List of offered {@link Ownable} by {@link Player}
	 */
	public List<MarketOffer> getOffersByPlayer(Player player) {
		return Collections.unmodifiableList(this.marketSession.get(player));
	}
	/**
	 * Returns {@link Player} with the specified username
	 * @param username username of the required {@link Player}
	 * @return {@link Player} with the specified username
	 */
	public Player getPlayerByUsername(String username) {
		for (Player player : this.players)
			if (player.getUsername().equals(username))
				return player;
		return null;
	}
	/**
	 * Returns the whole player's usernames 
	 * @return usernames of all {@link Player}s in this {@link Game}
	 */
	public List<String> getPlayerUsernames() {
		List<String> usernames = new ArrayList<>();
		for (Player player : players)
			usernames.add(player.getUsername());
		return usernames;
	}
	/**
	 * This method gives back N assistants to the pool 
	 * @param assistants List of assistants that may be returned to the pool
	 */
	public void returnNAssistantsToThePool(List<Assistant> assistants) {
		this.assistantsPool.returnNAssistants(assistants);
	}
	/**
	 * Public method to perform the CoF main action "Build an emorium using a permit tile" on the model
	 * @param player {@link Player} who wants to perform this main action
	 * @param permit {@link Permit} to use in order to build the emporium
	 * @param city {@link City} where build the emporium
	 * @return List of gained {@link Bonus}es with this main action
	 * @throws InvalidActionException thrown from private method buildAnEmporium
	 * @throws NotEnoughAssistantsException thrown if the {@link Player} hasn't enough assistants 
	 * (one per player who has already build on the specified city) to perform this main action
	 */
	public List<Bonus> buildAnEmporiumUsingAPermitTile(Player player, Permit permit, City city)
			throws InvalidActionException, NotEnoughAssistantsException {
		if (player.getPermitsAcquired().contains(permit)) {
			if (!permit.hasBeenUsed()) {
				if (permit.getCities().contains(city)) {
					List<Bonus> bonusesObtained = buildAnEmporium(player, city);
					permit.setUsage();
					return bonusesObtained;
				} else
					throw new InvalidCityException();
			} else
				throw new UsableAlreadyUsedException();
		} else {
			throw new OwnableNotOwnedException();
		}
	}
	/**
	 * Public method to perform the CoF main action "Build an emorium with the help of the king" on the model
	 * @param player {@link Player} who wants to perform this main action
	 * @param city {@link City} where build the emporium
	 * @param politicsToUse {@link PoliticCard}s to use to satisfy the {@link KingRegionBoard} council
	 * @return List of gained {@link Bonus}es with this main action
	 * @throws NotEnoughCoinsException thrown if the {@link Player} hasn't the needed coins to satisfy the council 
	 * and to move the king
	 * @throws NotEnoughAssistantsException thrown if the {@link Player} hasn't enough assistants 
	 * (one per player who has already build on the specified city) to perform this main action
	 * @throws CitiesNotConnectedException thrown if the {@link City} where build the emporium is not connected to the king location
	 * @throws InvalidActionException
	 * @throws CouncilUnsatisfiedException thrown if the List of politics to use doesn't satisfy the council
	 */
	public List<Bonus> buildAnEmporiumWithTheHelpOfTheKing(Player player, City city, List<Politic> politicsToUse)
			throws NotEnoughCoinsException, NotEnoughAssistantsException, CitiesNotConnectedException,
			InvalidActionException, CouncilUnsatisfiedException {
		// if the player has enough money to pay for the council
		// but not enough to pay for the king path i've to be able
		// to rollback the whole action
		int beforeActionCoins = player.getCoins();
		try {
			int distance = gameMap.getRoadsBetweenCities(city, kingLocation);
			int coinsToPay = -2 * distance;
			satisfyACouncil(player, Region.KING, politicsToUse);
			player.moveOnCoinsTrack(coinsToPay, this.gameMap.getMaxCoins());
			List<Bonus> bonusesObtained = buildAnEmporium(player, city);
			kingLocation = city;
			return bonusesObtained;
		} catch (CitiesNotConnectedException e) {
			throw e;
		} catch (NotEnoughCoinsException e) {
			//rollback
			player.getPoliticsHand().addAll(politicsToUse);
			player.moveOnCoinsTrack(beforeActionCoins - player.getCoins(), this.gameMap.getMaxCoins());
			throw e;
		} catch (NotEnoughAssistantsException e) {
			//rollback
			player.getPoliticsHand().addAll(politicsToUse);
			player.moveOnCoinsTrack(beforeActionCoins - player.getCoins(), this.gameMap.getMaxCoins());
			throw e;
		} catch (InvalidActionException e) {
			//rollback
			player.getPoliticsHand().addAll(politicsToUse);
			player.moveOnCoinsTrack(beforeActionCoins - player.getCoins(), this.gameMap.getMaxCoins());
			throw e;
		}
	}
	/**
	 * @return the current king location
	 */
	public City getKingsLocation() {
		return this.kingLocation;
	}
	/**
	 * Public method to perform the CoF main action "Acquire a permit tile" on the model
	 * @param player {@link Player} who wants to perform this main action
	 * @param region {@link Region} from which pick up the {@link Permit} tile
	 * @param index chosen {@link Permit} between the two faced up. It must be 1 or 2
	 * @param politicsToUse {@link PoliticCard}s to use to satisfy the  council of the chosen {@link Region}
	 * @return List of gained {@link Bonus}es with this main action
	 * @throws InvalidActionException
	 * @throws NotEnoughCoinsException thrown if the {@link Player} hasn't the needed coins to satisfy the council 
	 * @throws CouncilUnsatisfiedException thrown if the List of politics to use doesn't satisfy the council
	 */
	public List<Bonus> acquireAPermitTile(Player player, Region region, int index, List<Politic> politicsToUse)
			throws InvalidActionException, NotEnoughCoinsException, CouncilUnsatisfiedException {
		satisfyACouncil(player, region, politicsToUse);
		return this.acquireAPermitTile(player, region, index);
	}
	/**
	 * Alternative public method to perform the CoF main action "Acquire a permit tile" on the model, 
	 * that doesn't specify the {@link PoliticCard}s to use
	 * @param player {@link Player} who wants to perform this main action
	 * @param region {@link Region} from which pick up the {@link Permit} tile
	 * @param index chosen {@link Permit} between the two faced up. It must be 1 or 2
	 * @return List of gained {@link Bonus}es with this main action
	 * @throws InvalidActionException
	 */
	public List<Bonus> acquireAPermitTile(Player player, Region region, int index) throws InvalidActionException {
		if (index == 1 || index == 2) {
			Permit chosenPermit = regionBoards.get(region).getPermitBoard().pickUpAPermitTile(index);
			player.giveAPermit(chosenPermit);
			return chosenPermit.getBonuses();
		} else
			throw new InvalidActionException();
	}
	/**
	 * Public method to perform the CoF quick action "Send an assistant to elect a councillor" on the model
	 * @param player {@link Player} who wants to perform this main action
	 * @param region {@link Region} of the council where the new {@link Councillor} may be elected
	 * @param councillor {@link Councillor} that may be elected
	 * @throws NotEnoughAssistantsException thrown if the {@link Player} hasn't enough assistants 
	 * @throws InvalidActionException
	 */
	public void sendAnAssistantToElectACouncillor(Player player, Region region, Councillor councillor)
			throws NotEnoughAssistantsException, InvalidActionException {
		if (this.availableCouncillors.contains(councillor)) {
			Assistant usedAssistant = player.getAnAssistant();
			assistantsPool.returnAnAssistant(usedAssistant);
			electACouncillor(region, councillor);
		} else
			throw new InvalidActionException();
	}
	/**
	 * Public method to perform the CoF main action "Elect a councillor" on the model
	 * @param player {@link Player} who wants to perform this main action
	 * @param region {@link Region} of the council where the new {@link Councillor} may be elected
	 * @param councillor {@link Councillor} that may be elected
	 * @throws InvalidActionException
	 */
	public void electACouncillor(Player player, Region region, Councillor councillor) throws InvalidActionException {
		if (this.availableCouncillors.contains(councillor)) {
			electACouncillor(region, councillor);
			try {
				player.moveOnCoinsTrack(+4, this.gameMap.getMaxCoins());
			} catch (NotEnoughCoinsException e) {
				logger.log(Level.SEVERE, "Adding coins to a Player can't cause exceptions.", e);
			}
		} else
			throw new InvalidActionException();
	}
	/**
	 * Public method to perform the CoF quick action "Change business permit tiles" on the model
	 * @param player {@link Player} who wants to perform this quick action
	 * @param region {@link Region} where the business permit tiles must be changed
	 * @throws NotEnoughAssistantsException thrown if the {@link Player} hasn't enough assistants 
	 */
	public void changeBusinessPermitTiles(Player player, Region region) throws NotEnoughAssistantsException {
		Assistant chosenAssistant = player.getAnAssistant();
		assistantsPool.returnAnAssistant(chosenAssistant);
		this.regionBoards.get(region).getPermitBoard().changeTiles();
	}
	/**
	 * Public method to perform the CoF quick action "Engage an assistant" on the model
	 * @param player {@link Player} who wants to perform this quick action
	 * @throws NotEnoughCoinsException thrown if the {@link Player} hasn't enough coins 
	 */
	public void engageAnAssistant(Player player) throws NotEnoughCoinsException {
		player.moveOnCoinsTrack(-3, this.gameMap.getMaxCoins());
		player.giveAnAssistant(assistantsPool.getAnAssistant());
	}
	/**
	 * This public method returns a list of the face up {@link Permit}s of a given {@link Region}
	 * @param region {@link Region} from which take the face up {@link Permit}s
	 * @return List of face up {@link Permit}s 
	 */
	public List<Permit> showFaceUpPermitTiles(Region region) {
		return (List<Permit>) this.regionBoards.get(region).getPermitBoard().showFaceUpPermitTiles();
	}
	/**
	 * This public method gives all the {@link Bonus}es that the {@link Player} has gained with an action
	 * @param player {@link Player} who has performed an action
	 * @param bonuses List of {@link Bonus}es gained from the action
	 * @return List of obtained {@link Bonus}es
	 */
	public List<Bonus> obtainBonuses(Player player, List<Bonus> bonuses) {
		// actually giving bonuses to the player returning bonuses got
		// while doing this to the Action that has called this method
		List<Bonus> bonusesObtained = new ArrayList<>();
		for (Bonus bonus : bonuses) {
			switch (bonus.getBonusType()) {
			case ASSISTANT:
				player.giveNAssistants(this.assistantsPool.getNAssistants(bonus.getNumberOfTimes()));
				break;
			case COIN:
				try {
					player.moveOnCoinsTrack(bonus.getNumberOfTimes(), this.gameMap.getMaxCoins());
				} catch (NotEnoughCoinsException e) {
					logger.log(Level.SEVERE, "Adding coins to a Player can't cause exceptions.", e);
				}
				break;
			case VICTORY_POINT:
				player.incrementVictoryPoints(bonus.getNumberOfTimes(), this.gameMap.getMaxVictoryPoints());
				break;
			case POLITIC:
				player.giveNPolitics((this.politicDeck.getNPolitics(bonus.getNumberOfTimes())));
				break;
			case NOBILITY:
				player.moveOnNobilityTrackPosition(bonus.getNumberOfTimes(), this.gameMap.getNobilityTrackSize());
				bonusesObtained
						.addAll(this.gameMap.getBonusesForANobilityTrackPosition(player.getNobilityTrackPosition()));
				break;
			default:
				logger.log(Level.SEVERE, "A non model-handleable bonus has been sent to the model.");
			}
		}
		return bonusesObtained;
	}
	/**
	 * Public method to perform the CoF "Draw a politic" action. This action must be executed at the start of every player's turn
	 * @param player {@list Player} who has to draw 
	 * @return {@link Politic} picked up
	 * @throws EmptyDeckException thrown if the {@link PoliticDeck} is empty
	 */
	public Politic drawAPolitic(Player player) throws EmptyDeckException {
		Politic drawn = this.politicDeck.draw();
		player.giveAPolitic(drawn);
		return drawn;

	}
	/**
	 * This method search if there's an available {@link Councillor} in the councillor pool of the specified {@link PoliticColor}
	 * @param color {@link PoliticColor} requested
	 * @return {@link Councillor} of the specified {@link PoliticColor}
	 * @throws InvalidActionException thrown if the councillor pool doesn't contain any {@link Councillor} of the specified {@link PoliticColor}
	 */
	public Councillor getACouncillorByPoliticColor(PoliticColor color) throws InvalidActionException {
		List<PoliticColor> availableCouncillorColors = getAvailableCouncillorColors();
		if (availableCouncillorColors.contains(color)) {
			for (Councillor councillor : this.availableCouncillors) {
				if (councillor.getCouncillorColor() == color)
					return councillor;
			}
		} else
			throw new InvalidActionException();
		return null; // never reached?
	}
	/**
	 * This method returns the {@link PoliticColor} available in the councillor pool
	 * @return List of {@link PoliticColor} that represents all the available {@link PoliticColor}s in the pool
	 */
	public List<PoliticColor> getAvailableCouncillorColors() {
		List<PoliticColor> availableCouncillorColors = new ArrayList<>();
		for (Councillor councillor : this.availableCouncillors)
			availableCouncillorColors.add(councillor.getCouncillorColor());
		return Collections.unmodifiableList(availableCouncillorColors);
	}
	/**
	 * This method return the {@link PoliticColor} of the {@link Councillor}s of a given {@link Balcony}
	 * @param region {@link Region} of the requested {@link Balcony}
	 * @return List of {@link PoliticColor} that represents the {@PoliticColor} of the {@link Councillor}s on this {@link balcony}
	 */
	public List<PoliticColor> getCouncilByRegion(Region region) {
		if (region != Region.KING)
			return this.regionBoards.get(region).getBalcony().getBalconyColors();
		else
			return this.kingRegionBoard.getBalcony().getBalconyColors();
	}
	/**
	 * This method returns all the {@link Player}s with an emporium on a given {@link City}
	 * @param city {@link City} where evaluates the {@link Player}s with an emporium
	 * @return List of {@link Player} with an emporium on the specified {@link City}
	 */
	public List<Player> getPlayersWithAnEmporiumOn(City city) {
		return this.emporiumsBuilt.get(city);
	}
	/**
	 * This method returns the number of available {@link Councillor}s currently in the pool
	 * @return {@link Integer} that represents the number of available {@link Councillor}s currently in the pool
	 */
	public int numberOfAvailableCouncillors() {
		return this.availableCouncillors.size();
	}
	/**
	 * This method returns the {@link RewardToken} on a given {@link City} 
	 * @param city chosen {@link City}
	 * @return {@link RewardToken} on a given {@link City}
	 */
	public List<RewardToken> getRewardTokensOn(City city) {
		return this.rewardTokens.get(city);
	}
	/**
	 * This method returns all cities on the current {@link GameMap}
	 * @return List of all {@link City} in the currently loaded {@link GameMap}
	 */
	public List<City> getCityList() {
		return this.gameMap.getCityList();
	}
	/**
	 * This method checks if a {@link RegionRewardTile} has been already used or not
	 * @param region {@link Region} of which evaluates the {@link RewardTile}
	 * @return true if the {@link RegionRewardTile} has been already used, false in other cases 
	 */
	public boolean hasRegionRewardTileAlreadyBeenUsed(Region region) {
		if (region != Region.KING) {
			RegionRewardTile tile = this.regionBoards.get(region).getRegionRewardTile();
			return tile.hasBeenUsed();
		}
		return false;
	}
	/**
	 * This method checks if a {@link ColorRewardTile} has been already used or not
	 * @param color {@link Color} of which evaluates the {@link RewardTile}
	 * @return true if the {@link ColorRewardTile} has been already used, false in other cases 
	 */
	public boolean hasColorRewardTileAlreadyBeenUsed(CityColor color) {
		if (color != CityColor.PURPLE) {
			ColorRewardTile tile = this.kingRegionBoard.getColorRewardTile(color);
			return tile.hasBeenUsed();
		}
		return false;
	}
	/**
	 * This method returns the {@link KingRewardTile} currently on the top of the {@link KingRewardsPile}
	 * @return {@link KingRewardTile} currently on the top of the {@link KingRewardsPile}
	 * @throws EmptyDeckException thrown if the {@link KingRewardsPile} is empty
	 */
	public KingRewardTile peekKingRewardTopTile() throws EmptyDeckException {
		return this.kingRegionBoard.getKingRewardsPile().peekTopTile();
	}

	// PRIVATE METHODS
	/**
	 * Private method that generates the correct number of {@link Councillor}s of the correct {@link PoliticColor} 
	 * at the start of a new {@link Game} and than shuffle them
	 * @return List of generated {@link Councillor}s shuffled
	 */
	private ArrayList<Councillor> generateCouncillors() {
		// 24 councillors with default rules
		ArrayList<Councillor> councillors = new ArrayList<>();
		for (PoliticColor color : PoliticColor.values()) {
			if (color != PoliticColor.MULTI_COLORED) {
				for (int i = 0; i < Region.values().length; i++) {
					councillors.add(new Councillor(color));
				}
			}
		}
		Collections.shuffle(councillors);
		return councillors;
	}
	/**
	 * Private method that generates a {@link Balcony} filling it randomly with four {@link Councillor}s 
	 * @return {@link Balcony} filled randomly with four {@link Councillor}s
	 */
	private Balcony generateABalcony() {
		Councillor[] councillorsForBalcony = new Councillor[Balcony.BALCONY_SIZE];
		for (int i = 0; i < Balcony.BALCONY_SIZE; i++) {
			// the list is already shuffled
			councillorsForBalcony[i] = this.availableCouncillors.remove(0);
		}
		return new Balcony(councillorsForBalcony);
	}
	/**
	 * This method checks if a {@link Player} with his {@link PoliticCard}s can satisfy a council 
	 * of a given {@link Region}, and if so establishes how much the {@link Player} has to pay.
	 * @param player {@link Player} who wants to satisfy the council
	 * @param region {@link Region} of the {@link Balcony} that may be satisfied
	 * @param politicsToUse {@link PoliticCard} chosen from the politics hand of the {@link Player} to satisfy the council
	 * @throws InvalidActionException thrown if the {@link Player} chose more than four {@link PoliticCard} 
	 * in order to satisfy the {@link Balcony} or zero
	 * @throws NotEnoughCoinsException thrown if the {@link Player} hasn't enough coins to satisfy the council
	 * @throws CouncilUnsatisfiedException thrown if the {@link PoliticCard}s chosen from the {@link Player} to 
	 * satisfy the council don't matches the {@link Councillor}s on the {@link Balcony}
	 */
	private void satisfyACouncil(Player player, Region region, List<Politic> politicsToUse)
			throws InvalidActionException, NotEnoughCoinsException, CouncilUnsatisfiedException {
		if (politicsToUse.size() <= 4 && !politicsToUse.isEmpty()) {
			List<Politic> playerPoliticsHand = player.getPoliticsHand();
			if (playerPoliticsHand.containsAll(politicsToUse)) {
				Board board = (region == Region.KING) ? kingRegionBoard : regionBoards.get(region);
				List<PoliticColor> balconyColors = board.getBalcony().getBalconyColors();
				int numberOfCouncillorsSatisfied = 0;
				int numberOfJokers = 0;
				for (Politic politic : politicsToUse) {
					if (balconyColors.remove(politic.getPoliticColor()))
						numberOfCouncillorsSatisfied++;
					else if (politic.getPoliticColor() == PoliticColor.MULTI_COLORED) {
						numberOfCouncillorsSatisfied++;
						numberOfJokers++;
					}
				}
				if (numberOfCouncillorsSatisfied == politicsToUse.size()) {
					int coinsNeeded = 0;
					switch (numberOfCouncillorsSatisfied) {
					case 0:
						throw new InvalidActionException();
					case 1:
						coinsNeeded = 10;
						break;
					case 2:
						coinsNeeded = 7;
						break;
					case 3:
						coinsNeeded = 4;
						break;
					case 4:
						coinsNeeded = 0;
						break;
					}
					coinsNeeded += numberOfJokers;
					player.moveOnCoinsTrack(-coinsNeeded, gameMap.getMaxCoins());
					for (Politic politic : politicsToUse) {
						player.getPoliticsHand().remove(politic);
						this.politicDeck.discard(politic);
					}
					// council satisfied
				} else {
					throw new CouncilUnsatisfiedException();
				}
			} else {
				throw new OwnableNotOwnedException();
			}
		} else {
			throw new InvalidActionException();
		}
	}
	/**
	 * Private method to build an emporium, with a {@link Permit} or the help of the king
	 * @param player {@link Player} who wants to build the emporium
	 * @param city	{@link City} on which build the emporium
	 * @return List of gained {@link Bonus}es gained with this action
	 * @throws EmporiumAlreadyBuiltException thrown if the {@link Player} has already built on this [@link City}
	 * @throws NotEnoughAssistantsException thrown if the {@link player} hasn't enough {@link Assistant}s to perform this action
	 */
	private List<Bonus> buildAnEmporium(Player player, City city)
			throws EmporiumAlreadyBuiltException, NotEnoughAssistantsException {
		if (!this.emporiumsBuilt.get(city).contains(player)) {
			// emporium building
			int alreadyBuilt = this.emporiumsBuilt.get(city).size();
			List<Assistant> assistantsUsed = player.getNAssistants(alreadyBuilt);
			this.assistantsPool.returnNAssistants(assistantsUsed);
			this.emporiumsBuilt.get(city).add(player);
			player.decreaseNumberOfEmporiums();
			// bonus calculation
			List<Bonus> bonusesObtained = new ArrayList<>();
			// adding all bonuses obtained by cities around with a player's
			// emporium on them
			// 1) searching for all cities matching the "emporium criteria"
			Set<City> linkedCitiesWithEmporium = new HashSet<>();
			Queue<City> citiesToAnalize = new LinkedList<>();
			linkedCitiesWithEmporium.add(city);
			citiesToAnalize.add(city);
			City cityToAnalize;
			while (!citiesToAnalize.isEmpty()) {
				cityToAnalize = citiesToAnalize.remove();
				List<City> citiesLinkedToCity = this.gameMap.getConnectedCities(cityToAnalize);
				for (City cityLinked : citiesLinkedToCity) {
					// there's a player's emporium on cityLinked
					if (this.emporiumsBuilt.get(cityLinked).contains(player)) {
						if (!citiesToAnalize.contains(cityLinked) && !linkedCitiesWithEmporium.contains(cityLinked)) {
							citiesToAnalize.add(cityLinked);
							linkedCitiesWithEmporium.add(cityLinked);
						}
					}
				}
			}
			// 2) getting all bonuses from those cities
			for (City linkedCityWithEmporium : linkedCitiesWithEmporium) {
				for (RewardToken rewardTokenOnCity : this.rewardTokens.get(linkedCityWithEmporium))
					bonusesObtained.addAll(rewardTokenOnCity.getBonuses());
			}
			// adding bonuses on reward tiles
			// region reward tiles
			// if has been used is pointless to check if the region has been
			// completed
			RegionRewardTile regionRewardTile = this.regionBoards.get(city.getCityRegion()).getRegionRewardTile();
			if (!regionRewardTile.hasBeenUsed()) {
				List<City> citiesByRegion = this.gameMap.getCitiesByRegion(city.getCityRegion());
				boolean regionCompleted = true;
				for (City cityOnRegion : citiesByRegion)
					if (!this.emporiumsBuilt.get(cityOnRegion).contains(player)) {
						regionCompleted = false;
						break;
					}
				if (regionCompleted) {
					regionRewardTile.setUsage();
					regionRewardTile.setOwner(player);
					bonusesObtained.addAll(regionRewardTile.getBonuses());
					// getting a king reward tile (if there)
					try {
						KingRewardTile kingRewardTile = this.kingRegionBoard.getKingRewardsPile().pickUpATile();
						bonusesObtained.addAll(kingRewardTile.getBonuses());
					} catch (EmptyDeckException e) {
						logger.log(Level.INFO, "The kingRewardsPile is empty.", e);
					}

				}
			}
			// color reward tiles (what if is PURPLE=>null)?
			ColorRewardTile colorRewardTile = this.kingRegionBoard.getColorRewardTile(city.getCityColor());
			if (colorRewardTile != null && !colorRewardTile.hasBeenUsed()) {
				List<City> citiesByColor = this.gameMap.getCitiesByCityColor(city.getCityColor());
				boolean colorCompleted = true;
				for (City cityOfColor : citiesByColor)
					if (!this.emporiumsBuilt.get(cityOfColor).contains(player)) {
						colorCompleted = false;
						break;
					}
				if (colorCompleted) {
					colorRewardTile.setUsage();
					colorRewardTile.setOwner(player);
					bonusesObtained.addAll(colorRewardTile.getBonuses());
					// getting a king reward tile (if there)
					try {
						KingRewardTile kingRewardTile = this.kingRegionBoard.getKingRewardsPile().pickUpATile();
						bonusesObtained.addAll(kingRewardTile.getBonuses());
					} catch (EmptyDeckException e) {
						logger.log(Level.INFO, "The kingRewardsPile is empty.", e);
					}
				}
			}
			return bonusesObtained;
		} else
			throw new EmporiumAlreadyBuiltException();
	}
	/**
	 * Private method to elect a {@link Councillor} in a given {@link Region} {@link Balcony}
	 * @param region {@link Region} of the {@link Balcony} on which elect the specified [@link Councillor}
	 * @param councillor {@link Councillor} that may be elected on the specified {@link Region}'s {@link Balcony}
	 */
	private void electACouncillor(Region region, Councillor councillor) {
		Board board = (region == Region.KING) ? kingRegionBoard : regionBoards.get(region);
		this.availableCouncillors.remove(councillor);
		this.availableCouncillors.add(board.getBalcony().shift(councillor));
	}
}
