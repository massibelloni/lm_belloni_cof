package it.polimi.ingsw.lm_belloni_cof.model;

/**
 * 
 * The regions available in Council of Four
 *
 */
public enum Region {
	COAST, HILLS, MOUNTAINS, KING
}
