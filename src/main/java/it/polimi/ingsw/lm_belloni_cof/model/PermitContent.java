package it.polimi.ingsw.lm_belloni_cof.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * A more specific {@link TileContent} to be placed on a {@link Permit}.
 * 
 * @see PermitDeck#getPermitDeckInstance(GameMap,Region,int)
 *
 */
public class PermitContent extends TileContent {
	/**
	 * The region of the PermitContent. {@link Region} of all the cities on the
	 * Permit.
	 */
	private Region region;
	/**
	 * The cities for which this tile will guarantee an emporium build
	 * permission
	 */
	private List<City> cities;

	/**
	 * Public constructor of a PermitContent.
	 * 
	 * @param region
	 *            unmodifiable {@link Region} of the PermitContent.
	 */
	public PermitContent(Region region) {
		super();
		this.region = region;
		this.cities = new ArrayList<>();
	}

	/**
	 * Get the {@link Region} of the PermitContent.
	 * 
	 * @return the {@link Region} of the PermitContent.
	 */
	public Region getRegion() {
		return this.region;
	}

	/**
	 * Add a {@link City} to the cities for which this tile will guarantee an
	 * emporium build permission.
	 * 
	 * @param city
	 *            a {@link City} of the {@link Region} previously defined.
	 */
	public void addCity(City city) {
		this.cities.add(city);
	}

	/**
	 * Get all the cities for which this tile will guarantee an emporium build
	 * permission.
	 */
	public List<City> getCities() {
		return this.cities;
	}

}
