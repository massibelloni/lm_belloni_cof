package it.polimi.ingsw.lm_belloni_cof.model;

import java.util.List;
import java.util.LinkedList;
import java.util.Queue;
import it.polimi.ingsw.lm_belloni_cof.exceptions.EmptyDeckException;

/**
 * The data structure that manages the access to {@link KingRewardTile}s with a
 * "stack" logic. The first {@link Player} gets the first tile, the second (that
 * can even be the one that already got a tile before) gets the second, and son
 * on. When the tiles finish an EmptyDeckException will be thrown.
 */
public class KingRewardsPile {
	/**
	 * The Queue storing the tiles: FIFO logic, a tile can't be even viewed if
	 * the one with a lower picking order hasn't already been picked up.
	 * 
	 * @see KingRewardTileContent#pickingOrder
	 */
	private Queue<KingRewardTile> kingRewardsPile;

	/**
	 * Private constructor of a KingRewardsPile: it only defines an empty
	 * collection.
	 */
	private KingRewardsPile() {
		kingRewardsPile = new LinkedList<>();
	}

	/**
	 * Pick up (if there) the first tile of the pile (lowest picking order).
	 * 
	 * @return the {@link KingRewardTile} on the top of the pile.
	 * @throws EmptyDeckException
	 *             if there isn't any {@link KingRewardTile} left.
	 */
	public KingRewardTile pickUpATile() throws EmptyDeckException {
		if (this.kingRewardsPile.isEmpty()) {
			throw new EmptyDeckException();
		}
		return this.kingRewardsPile.remove();
	}

	/**
	 * View, without picking it up, the {@link KingRewardTile} on the top of the
	 * pile.
	 * 
	 * @return the {@link KingRewardTile} on the top of the pile.
	 * @throws EmptyDeckException
	 *             if there isn't any {@link KingRewardTile} left.
	 */
	public KingRewardTile peekTopTile() throws EmptyDeckException {
		if (this.kingRewardsPile.isEmpty()) {
			throw new EmptyDeckException();
		}
		return this.kingRewardsPile.peek();
	}

	/**
	 * Creates and returns a {@link KingRewardsPile} instance based on a list of
	 * already ordered (1 is first) {@link KingRewardTileContent} list.
	 * 
	 * @param kingRewardTileContents
	 *            already ordered (1 is first) {@link KingRewardTileContent}
	 *            list.
	 * @return a well formed and ready-to-use {@link KingRewardsPile} instance.
	 */
	public static KingRewardsPile getKingRewardPile(List<KingRewardTileContent> kingRewardTileContents) {
		KingRewardsPile kingRewardsPile = new KingRewardsPile();
		for (KingRewardTileContent kingRewardTileContent : kingRewardTileContents)
			kingRewardsPile.kingRewardsPile.add(new KingRewardTile(kingRewardTileContent));
		return kingRewardsPile;
	}
}
