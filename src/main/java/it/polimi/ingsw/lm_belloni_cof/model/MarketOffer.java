package it.polimi.ingsw.lm_belloni_cof.model;

/**
 * The minimum market interaction: an Ownable object and a price for that
 * object.
 *
 */
public class MarketOffer {
	/**
	 * The object for sale. It has to implement the Ownable interface, it have
	 * to be ownable.
	 */
	private Ownable objectForSale;
	/** The price asked for the objectForSale */
	private int price;

	/**
	 * Public constructor for a MarketOffer. The object created won't be
	 * editable.
	 * 
	 * @param objectForSale
	 *            the Ownable object for sale.
	 * @param price
	 *            the price asked for the object for sale.
	 */
	public MarketOffer(Ownable objectForSale, int price) {
		this.objectForSale = objectForSale;
		this.price = price;
	}

	/**
	 * Get the price of this offer.
	 * 
	 * @return the price asked for the object for sale.
	 */
	public int getPrice() {
		return this.price;
	}

	/**
	 * Get the object for sale of this offer.
	 * 
	 * @return the Ownable object for sale.
	 */
	public Ownable getObjectForSale() {
		return this.objectForSale;
	}
}
