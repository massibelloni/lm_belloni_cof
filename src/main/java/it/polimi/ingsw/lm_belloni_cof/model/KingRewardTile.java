package it.polimi.ingsw.lm_belloni_cof.model;

/**
 * Represents the object physically placed on the {@link KingRegionBoard},
 * precisely in a {@link KingRewardsPile} and that may be obtained by the
 * {@link Player} that builds an emporium on each city of a {@link Region} or of
 * a certain {@link CityColor}.
 * 
 * @see Game#buildAnEmporium(Player,City)
 */

public class KingRewardTile extends RewardTile<KingRewardTileContent> {
	/**
	 * Constructor of the KingRewardTile. A KingRewardTileContent is needed and
	 * it will be "printed" on the card representing its content
	 * 
	 * @param rewardContent
	 *            a well formed KingRewardTileContent
	 */
	public KingRewardTile(KingRewardTileContent rewardContent) {
		super(rewardContent);
	}

	/**
	 * Get the picking order of the {@link KingRewardTile}.
	 * 
	 * @return the picking order of the {@link KingRewardTile}.
	 */
	public int getPickingOrder() {
		return super.rewardContent.getPickingOrder();
	}
}
