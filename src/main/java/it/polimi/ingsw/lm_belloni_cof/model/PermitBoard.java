package it.polimi.ingsw.lm_belloni_cof.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.exceptions.ActionException;
import it.polimi.ingsw.lm_belloni_cof.exceptions.EmptyDeckException;

/**
 * The "physical" place where the {@link PermitDeck} is placed. This handles all
 * the interactions with the deck allowing to get a face up tile or change the
 * tiles.
 *
 */
public class PermitBoard {
	private static final Logger logger = Logger.getLogger(PermitBoard.class.getName());
	/** The {@link PermitDeck} instance */
	private PermitDeck permitDeck;
	/** The first {@link Permit} placed face up */
	private Permit faceUpPermitTile1;
	/** The second {@link Permit} placed face up */
	private Permit faceUpPermitTile2;

	/**
	 * Public constructor of a board. This places the deck passed as an argument
	 * and draws the first two {@link Permit}s placed face up.
	 * 
	 * @param permitDeck
	 *            the deck placed on the board
	 */
	public PermitBoard(PermitDeck permitDeck) {
		this.permitDeck = permitDeck;
		try {
			faceUpPermitTile1 = permitDeck.draw();
			faceUpPermitTile2 = permitDeck.draw();
		} catch (ActionException e) {
			logger.log(Level.SEVERE, "PermitBoard launched an exception during initialization.", e);
		}
	}

	/**
	 * Shows the two {@link Permit} placed face up.
	 * 
	 * @return an unmodifiable list containing the two face up {@link Permit}
	 */
	public List<Permit> showFaceUpPermitTiles() {
		ArrayList<Permit> faceUpPermitTiles = new ArrayList<>();
		faceUpPermitTiles.add(faceUpPermitTile1);
		faceUpPermitTiles.add(faceUpPermitTile2);
		return (List<Permit>) Collections.unmodifiableList(faceUpPermitTiles);
	}

	/**
	 * Changes both the two tiles placed face up drawing and placing new ones.
	 */
	public void changeTiles() {
		permitDeck.discard(faceUpPermitTile1);
		permitDeck.discard(faceUpPermitTile2);
		try {
			faceUpPermitTile1 = permitDeck.draw();
			faceUpPermitTile2 = permitDeck.draw();
		} catch (EmptyDeckException e) {
			logger.log(Level.INFO, "PermitDeck finished.", e);
		}
	}

	/**
	 * Pick up the "index" (1 or 2) face up {@link Permit}. The method replaces
	 * the picked up with a new one drawn from the deck.
	 * 
	 * @param index
	 *            1 or 2, the first or the second face up tile.
	 * @return the {@link Permit} that was face up.
	 */
	public Permit pickUpAPermitTile(int index) {
		Permit picked = null;
		if (index == 1) {
			picked = faceUpPermitTile1;
			try {
				faceUpPermitTile1 = this.permitDeck.draw();
			} catch (EmptyDeckException e) {
				logger.log(Level.INFO, "PermitDeck finished.", e);
				faceUpPermitTile1 = null;
			}
		} else if (index == 2) {
			picked = faceUpPermitTile2;
			try {
				faceUpPermitTile2 = this.permitDeck.draw();
			} catch (EmptyDeckException e) {
				logger.log(Level.INFO, "PermitDeck finished.", e);
				faceUpPermitTile2 = null;
			}
		}
		return picked;
	}

	/**
	 * Method used during a 2-player {@link Game} initialization for discarding
	 * a {@link Permit} previously drawn.
	 * 
	 * @see Game#Game(GameMap,List<PlayerToken>)
	 * @param permit
	 *            the {@link Permit} to discard.
	 */
	public void discardATileAndShuffle(Permit permit) {
		this.permitDeck.discard(permit);
		this.permitDeck.shuffle();
	}

}
