package it.polimi.ingsw.lm_belloni_cof.model;


/**
 * This class represents the rules to calculate the points of the players,
 * when a {@link Player} complete to build his emporiums.
 *
 */
public class FinalScoreRules {
	
	/**
	 * Extra-points for the {@link Player} who builds his last emporium.
	 */
	private int lastEmporiumPoints;
	
	/**
	 * Extra-points for the {@link Player} who is in the first position in {@link Nobilitytrack}.
	 */
	private int firstNobilityTrackPoints;
	
	/**
	 * Extra-points for the {@link Player} who is in the second position in {@link Nobilitytrack}.
	 */
	private int secondNobilityTrackPoints;
	
	/**
	 * Extra-points for the {@link Player} who owns more {@link Permit} than any other.
	 */
	private int permitPoints;
	
	/**
	 * Constructor of this class, initialize the number of Extra-points for the various situation.
	 * @param lastEmporiumPoints Extra-points for the {@link Player} who builds his last emporium.
	 * @param firstNobilityTrackPointsExtra-points for the {@link Player} who is in the first position in {@link Nobilitytrack}.
	 * @param secondNobilityTrackPoints Extra-points for the {@link Player} who is in the second position in {@link Nobilitytrack}.
	 * @param permitPoints  Extra-points for the {@link Player} who owns more {@link Permit} than any other.
	 */
	public FinalScoreRules(int lastEmporiumPoints, int firstNobilityTrackPoints, int secondNobilityTrackPoints,
			int permitPoints) {
		this.lastEmporiumPoints = lastEmporiumPoints;
		this.firstNobilityTrackPoints = firstNobilityTrackPoints;
		this.secondNobilityTrackPoints = secondNobilityTrackPoints;
		this.permitPoints = permitPoints;
	}
	
	/**
	 * Returns the current setting for the lastEmporium extra-points.
	 * @return extra-points for the {@link Player} who builds his last emporium.
	 */
	public int getLastEmporiumPoints() {
		return lastEmporiumPoints;
	}
	
	/**
	 * This method allows to change settings of lastEmporium extra-points.
	 * @param lastEmporiumPoints new number of extra-points.
	 */
	public void setLastEmporiumPoints(int lastEmporiumPoints) {
		this.lastEmporiumPoints = lastEmporiumPoints;
	}
	
	/**
	 * Returns the current setting for the "first {@link Player} in {@link NobilityTrack}" extra-points.
	 * @return extra-points for the "first {@link Player} in {@link NobilityTrack}".
	 */
	public int getFirstNobilityTrackPoints() {
		return firstNobilityTrackPoints;
	}
	/**
	 * This method allows to change settings for the "first {@link Player} in {@link NobilityTrack}" extra-points.
	 * @param firstNobilityTrackPoints new number of extra-points.
	 */
	public void setFirstNobilityTrackPoints(int firstNobilityTrackPoints) {
		this.firstNobilityTrackPoints = firstNobilityTrackPoints;
	}
	
	/**
	 * Returns the current setting for the "second {@link Player} in {@link NobilityTrack}" extra-points.
	 * @return extra-points for the "second {@link Player} in {@link NobilityTrack}".
	 */
	public int getSecondNobilityTrackPoints() {
		return secondNobilityTrackPoints;
	}
	
	/**
	 * This method allows to change settings for the "second {@link Player} in {@link NobilityTrack}" extra-points.
	 * @param secondNobilityTrackPoints new number of extra-points.
	 */
	public void setSecondNobilityTrackPoints(int secondNobilityTrackPoints) {
		this.secondNobilityTrackPoints = secondNobilityTrackPoints;
	}
	
	/**
	 * Returns the current setting for the Extra-points for the {@link Player} who owns more {@link Permit} than any other.
	 * @return extra-points for the {@link Player} who owns more {@link Permit} than any other.
	 */
	public int getPermitPoints() {
		return permitPoints;
	}
	
	
	/**
	 * This method allows to change settings to the Extra-points for the {@link Player} who owns more {@link Permit} than any other.
	 * @param permitPoints new number of extra-points.
	 */
	public void setPermitPoints(int permitPoints) {
		this.permitPoints = permitPoints;
	}

}
