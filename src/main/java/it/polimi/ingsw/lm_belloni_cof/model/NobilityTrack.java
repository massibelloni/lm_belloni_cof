package it.polimi.ingsw.lm_belloni_cof.model;

import java.util.ArrayList;
import java.util.List;

/** The actual representation of the nobility track */
public class NobilityTrack {
	/**
	 * The nobility track is composed by positions, each positions contains a
	 * list of bonuses. This "double" list tries to describe the situation.
	 */
	private List<List<Bonus>> nobilityTrack;

	/** Public constructor that creates an instance from a double list */
	public NobilityTrack(List<List<Bonus>> nobilityTrack) {
		this.nobilityTrack = nobilityTrack;
	}

	/**
	 * Get size (number of positions) of the NobilityTrack instance
	 * 
	 * @return the size of the NobilityTrack instance.
	 */

	public int getSize() {
		return nobilityTrack.size();
	}

	/**
	 * Get bonuses stored in a certain position.
	 * 
	 * @param position
	 *            the position for which the bonuses are needed.
	 * @return the List of {@link Bonus} for the position asked.
	 */
	public List<Bonus> getBonuses(int position) {
		return this.nobilityTrack.get(position);
	}

	/**
	 * Get a instance of the {@link NobilityTrack} with the {@link Bonus}es
	 * defined in the official rules of Council of Four.}
	 */
	public static NobilityTrack getDefaultNobilityTrack() {
		List<List<Bonus>> defaultTrack = new ArrayList<List<Bonus>>();
		List<Bonus> pos0 = new ArrayList<>();
		defaultTrack.add(pos0);
		List<Bonus> pos1 = new ArrayList<>();
		defaultTrack.add(pos1);
		List<Bonus> pos2 = new ArrayList<>();
		pos2.add(new Bonus(BonusType.COIN, 1));
		pos2.add(new Bonus(BonusType.VICTORY_POINT, 2));
		defaultTrack.add(pos2);
		List<Bonus> pos3 = new ArrayList<>();
		defaultTrack.add(pos3);
		List<Bonus> pos4 = new ArrayList<>();
		pos4.add(new Bonus(BonusType.CITY_REWARD, 1));
		defaultTrack.add(pos4);
		List<Bonus> pos5 = new ArrayList<>();
		defaultTrack.add(pos5);
		List<Bonus> pos6 = new ArrayList<>();
		pos6.add(new Bonus(BonusType.MAIN_ACTION, 1));
		defaultTrack.add(pos6);
		List<Bonus> pos7 = new ArrayList<>();
		defaultTrack.add(pos7);
		List<Bonus> pos8 = new ArrayList<>();
		pos8.add(new Bonus(BonusType.VICTORY_POINT, 3));
		pos8.add(new Bonus(BonusType.POLITIC, 1));
		defaultTrack.add(pos8);
		List<Bonus> pos9 = new ArrayList<>();
		defaultTrack.add(pos9);
		List<Bonus> pos10 = new ArrayList<>();
		pos10.add(new Bonus(BonusType.TAKE_PERMIT, 1));
		defaultTrack.add(pos10);
		List<Bonus> pos11 = new ArrayList<>();
		defaultTrack.add(pos11);
		List<Bonus> pos12 = new ArrayList<>();
		pos12.add(new Bonus(BonusType.VICTORY_POINT, 5));
		pos12.add(new Bonus(BonusType.ASSISTANT, 1));
		defaultTrack.add(pos12);
		List<Bonus> pos13 = new ArrayList<>();
		defaultTrack.add(pos13);
		List<Bonus> pos14 = new ArrayList<>();
		pos14.add(new Bonus(BonusType.PERMIT_BONUS, 1));
		defaultTrack.add(pos14);
		List<Bonus> pos15 = new ArrayList<>();
		defaultTrack.add(pos15);
		List<Bonus> pos16 = new ArrayList<>();
		pos16.add(new Bonus(BonusType.CITY_REWARD, 2));
		defaultTrack.add(pos16);
		List<Bonus> pos17 = new ArrayList<>();
		defaultTrack.add(pos17);
		List<Bonus> pos18 = new ArrayList<>();
		pos18.add(new Bonus(BonusType.VICTORY_POINT, 8));
		defaultTrack.add(pos18);
		List<Bonus> pos19 = new ArrayList<>();
		pos19.add(new Bonus(BonusType.VICTORY_POINT, 2));
		defaultTrack.add(pos19);
		List<Bonus> pos20 = new ArrayList<>();
		pos20.add(new Bonus(BonusType.VICTORY_POINT, 3));
		defaultTrack.add(pos20);
		NobilityTrack defaultNobilityTrack = new NobilityTrack(defaultTrack);
		return defaultNobilityTrack;
	}

}
