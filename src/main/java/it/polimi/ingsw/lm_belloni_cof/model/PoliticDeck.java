package it.polimi.ingsw.lm_belloni_cof.model;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.lm_belloni_cof.exceptions.EmptyDeckException;

/**
 * Implementation of a {@link Deck} of {@link Politic}
 *
 */
public class PoliticDeck extends Deck<Politic> {
	private static final Logger logger = Logger.getLogger(PoliticDeck.class.getName());
	/**
	 * Number of {@link Politic} of a {@link PoliticColor} in a regular four
	 * players game.
	 */
	public final static int FOUR_PLAYERS_SINGLE_COLORED = 13;
	/**
	 * Number of MULTICOLORED {@link Politic} in a regular four players game.
	 */
	public final static int FOUR_PLAYERS_JOKERS = 12;
	/**
	 * Number of MULTICOLORED {@link Politic} per player in a non standard game
	 * (more than 4 players).
	 */
	public final static int JOKERS_PER_PLAYER = 3;
	/**
	 * Number of {@link Politic} of a {@link PoliticColor} per player in a non
	 * standard game (more than 4 players).
	 */
	public final static double SINGLE_COLORED_PER_PLAYER = 13.0 / 4.0;

	/** Private constructor that only instantiate an empty PoliticDeck */
	private PoliticDeck() {
		super();
	}

	/**
	 * The only way to get n {@link Politic} in a raw, used during {@link Game}
	 * initialization.
	 * 
	 * @see Game#Game(GameMap,List<PlayerToken>)
	 * @param n
	 *            numbers of {@link Politic} needed
	 * @return a List of {@link Politic} drawn from the {@link Deck}
	 */
	public List<Politic> getNPolitics(int n) {
		List<Politic> politics = new ArrayList<>();
		try {
			for (int i = 0; i < n; i++)
				politics.add(super.draw());
		} catch (EmptyDeckException e) {
			logger.log(Level.SEVERE, "No enough politics to give.", e);
			for (Politic politic : politics)
				super.discard(politic);
		}
		return politics;
	}

	/**
	 * The only way to get a PoliticDeck instance based on the number of players
	 * involved in the {@link Game}. This method calculates and instantiate the
	 * correct amount of {@link Politic} for each {@link PoliticColor}.
	 * 
	 * @param numberOfPlayers
	 *            number of {@link Player} involved in the {@link Game}
	 * @return a well formed and ready-to-use PoliticDeck
	 */
	public static PoliticDeck getPoliticDeckInstance(int numberOfPlayers) {
		if (numberOfPlayers <= 4) {
			PoliticDeck politicDeck = new PoliticDeck();
			for (PoliticColor color : PoliticColor.values()) {
				if (color != PoliticColor.MULTI_COLORED) {
					for (int i = 0; i < PoliticDeck.FOUR_PLAYERS_SINGLE_COLORED; i++)
						politicDeck.discard(new Politic(color));
				} else {
					for (int i = 0; i < PoliticDeck.FOUR_PLAYERS_JOKERS; i++)
						politicDeck.discard(new Politic(color));
				}
			}
			politicDeck.shuffle();
			return politicDeck;
		} else {
			PoliticDeck politicDeck = new PoliticDeck();
			for (PoliticColor color : PoliticColor.values()) {
				if (color != PoliticColor.MULTI_COLORED) {
					for (int i = 0; i < (int) (numberOfPlayers * PoliticDeck.SINGLE_COLORED_PER_PLAYER); i++)
						politicDeck.discard(new Politic(color));
				} else {
					for (int i = 0; i < numberOfPlayers * PoliticDeck.JOKERS_PER_PLAYER; i++)
						politicDeck.discard(new Politic(color));
				}
			}
			politicDeck.shuffle();
			return politicDeck;
		}
	}
}
