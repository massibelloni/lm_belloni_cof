package it.polimi.ingsw.lm_belloni_cof.model;

/**
 * This class represents an abstract Board of the {@link GameMap}.
 * This class has an attribute {@link Balcony} balcony that represents the {@link Balcony} inside the Board. 
 *
 */
public abstract class Board {
	private Balcony balcony;
	
	/**
	 * This method creates a {@link Balcony} for the Board.
	 * @param balcony The {@link Balcony} to insert in the Board.
	 */
	public Board(Balcony balcony){
		this.balcony = balcony;
	}
	
	/**
	 * This method returns the {@link Balcony} of the Board.
	 * @return balcony inside the Board.
	 */
	public Balcony getBalcony(){
		return this.balcony;
	}
}
