In order to launch a new game the server has to be started first (src/main/java/it/polimi/ingsw/lm_belloni_cof/view/gameside/GameSide.java). The class automatically launches both RMI and Socket on the ports defined in src/main/java/it/polimi/ingsw/lm_belloni_cof/view/shared/ConnectionPorts.java. The server is now ready to accept incoming requests.

The clients (both GUI and CLI) can be started launching src/main/java/it/polimi/ingsw/lm_belloni_cof/view/playerside/PlayerSide.java one time for each player. Keyboard input is requested to choose between GUI(1) and CLI(2). If GUI has been chosen a window appears, the login form has to be fulfilled and the connection method has to be selected. If otherwise CLI has been preferred the connection method has to be chosen and then the login has to be submitted.

If a new username-password couple is entered the user is automatically registered with this username-password; otherwise the login is rejected or accepted obviously depending on the password.

When at least two players connect to the game a timeout starts (WAITING_TIME field in src/main/java/it/polimi/ingsw/lm_belloni_cof/controller/GamesHandler.java), when the timeout expires a new game is actually started. There isn’t any upper limit to the number of players, the rules and objects scale with it.

This is a list of all the features implemented in the project:

- “Regole addizionali” of the requirements. In particular, 8 different maps are loaded in /maps and a random map among them is chosen when the game starts.
- The time that a player has to complete an action can be configured editing ACTION_TIMEOUT in src/main/java/it/polimi/ingsw/lm_belloni_cof/controller/GameController.java. If no valid actions are performed during this time the player is disconnected.
- “Gestione degli utenti” as in the requirements. The players’ table can be seen clicking on the players’ list during any game.
- “Ripristino sessione giocatore” as in the requirements, simply logging in after a reconnection.
- “Chat” as in the requirements using the input box on the bottom right (GUI) or with the command “chat message” in the CLI.

Fabio Ballabio 
Massimo Belloni